# Chapter 14

>    ¹janamējaya uvāca  
> ṡrutaṃ bhagavatas tasya māhātmyaṃ paramātmanaḥ  
> janma ²dharmagṛhē caiva naranārāyaṇātmakam  
> mahāvarāhasṛṣṭā ca piṇḍōtpattiḥ ³purātanī /14.1/  

¹ M B Da CE; Dn T G "ṡaunaka";
  speaker confusion continues in other than M  
² M5 T G1,3,6 CE PPS; M1,6,7 "dharmaṃ gṛhē"; G2 "dharmagṛhaiṡ"  
³ ‡T G B Dn CE PPS; M1,5,7 "purātanā"; M6 "sanātanā"

"utpatti" is feminine, so the adjective must also be feminine.
As per MW, "purātanī" (but not purātanā) is feminine of "purātana".
As per MW, "sanātanī" is feminine of "sanātana", but it can also be
"sanātanā" for the sake of metre/poetry.

> pravṛttau ca nivṛttau ca yō yathā parikalpitaḥ  
> sa tathā naḥ ṡrutō ¹brahman kathyamānas tvayānagha /14.2/  

¹ M B Dn CE; K Da "sautē"; T G1,3,6 "sūta"; G2 "tāta";
  speaker confusion continues in other than M  

> ⁰yac ca tat kathitaṃ pūrvaṃ tvayā hayaṡirō mahat  
> havyakavya¹bhujō viṣṇōr udakpūrvē mahōdadhau  
> tac ca dṛṣṭaṃ bhagavatā brahmaṇā paramēṣṭhinā /14.3/  

¹ †all; M5 "-bhujō viṣvaṃ"

> kiṃ tad utpāditaṃ pūrvaṃ hariṇā lōkadhāriṇā  
> ¹rūpaṃ prabhāva²mahatām apūrvaṃ dhīmatāṃ vara /14.4/  

¹ T G om. /14.4L2/ M1,6,7 om. /14.4d/ to /14.5a/  
² ‡B Dn CE; M5 "mahatā hy apū-"

> dṛṣṭvā ¹ca vibudhaṡrēṣṭham ²apūrvam ³amitaujasam  
> tad aṡvaṡirasaṃ ⁰puṇyaṃ brahmā kim akarōn munē /14.5/  

¹ M5 T G1,3,6 PPS; M1,6,7 om. /14.5a/; G2 CE "hi"  
² ?M5 CE; M1,6,7 "hy apū-"; T1 G1,3,6 "tv apū-"; T2 "taṃ pūrvam";
  G2 "� � �"; PPS "samprītam" (sic)  
³ all; M1,6,7 "amitaujasā"

> ētan naḥ saṃṡayaṃ ¹brahman purāṇajn̄ānasambhavam  
> kathayasvōttamamatē mahāpuruṣanirmitam  
> pāvitāḥ sma tvayā ²brahman ³puṇyāḥ kathaya tāḥ kathāḥ /14.6/  

¹ M B0,6-8 Dn CE; T G "sūta"; K D "sautē"; B9 "sarvaṃ";
  speaker confusion continues in other than M  
² M B Dn CE; T G "sūta"; K7 D4,5,9 "sautē";
  speaker confusion continues in other than M  
³ †M1,7 T G; M6 "puṇyā  kathayatā kathām";
             M5 "puṇyāṃ kathayatā kathāḥ";
            PPS "puṇyāḥ kathayatā kathāḥ"
             CE "puṇyāṃ kathayatā kathām"

>    ¹vaiṡampāyana uvāca  
> kathayiṣyāmi tē sarvaṃ purāṇaṃ vēdasammitam  
> jagau yad bhagavān vyāsō ²rājn̄ō dharmasutasya vai /14.7/  

¹ all; D T G "sūta"; speaker confusion continues in T G but not others  
² all; T G "yajn̄ē parīkṣitasya vai"; speaker confusion continues in T G but not others

After /14.7/ only M5 in. CE@894 (2 lines) but T G M1,6,7 B Dn CE om.

> ṡrutvāṡva¹ṡirasō mūrtiṃ dēvasya harimēdhasaḥ  
> utpannasaṃṡayō rājā ⁰tam ēva samacōdayat /14.8/  

¹ ‡all; M1,5,6 "-ṡirasīṃ"; M7 "-ṡirasī"

>    ¹yudhiṣṭhira uvāca  
> yat tad ²dadṛṡivān brahmā dēvaṃ hayaṡirōdharam  
> kimarthaṃ tat samabhavad vapur dēvōpa³kalpitam /14.9/  

¹ all; T G3,6 K "janamējaya"; G1,2 om.  
² M T2 G1,6 B; T1 G2,3 CE PPS "darṡitavān"  
³ M5 T G CE; M1,6,7 "-kalpitaḥ"

>    ¹vyāsa uvāca  
> yat kin̄cid iha lōkē vai ⁰dēhabaddhaṃ viṡāṃ patē  
> sarvaṃ pan̄cabhir āviṣṭaṃ bhūtair īṡvarabuddhijaiḥ /14.10/  

¹ M B CE; T G Dn "vaiṡampāyana"

> īṡvarō hi jagatsraṣṭā prabhur nārāyaṇō virāṭ  
> bhūtāntarātmā varadaḥ saguṇō nirguṇō’pi ca  
> bhūtapraḻayam avyaktaṃ ṡṛṇuṣva nṛpasattama /14.11/  

> dharaṇyām atha līnāyām apsu caikārṇavē purā  
> jyōtirbhūtē jalē ⁰cāpi līnē jyōtiṣi cānilē /14.12/  

¹ all; M1,6,7 "caiva"

> vāyau cākāṡasaṃlīnē ākāṡē ca ⁰manōnugē  
> ⁰vyaktē manasi saṃlīnē vyaktē cāvyaktatāṃ gatē /14.13/  

> avyaktē puruṣaṃ yātē puṃsi sarvagatē’pi ca  
> tama ēvābhavat sarvaṃ na ¹prajn̄āyata kin̄cana /14.14/  

¹ M T2 G1,2 PPS; T1 G3,6 CE "prājn̄āyata"

> tamasō brahma sambhūtaṃ tamōmūlam ṛtātmakam  
> tad viṡvabhāva¹san̄jn̄ātaṃ pauruṣīṃ tanum āsthitam /14.15/  

¹ M Cs; CE "saṃjn̄āntaṃ"; T1 G2,3,6 "xxx"

Interesting verse. "Brahman is born from Tamas (darkness)". Nilakantha, Arjuna Mishra, Vidyasagara and
Vadiraja give different interpretations. Does it refer to the Big Bang?

See also /9.7/ where M1,6,7 say "na vinā puruṣaṃ tamaḥ sambhavaṃ"

> sō’niruddha iti prōktas ¹taṃ pradhānaṃ pracakṣatē  
> tad avyaktam iti jn̄ēyaṃ triguṇaṃ ²dvijasattama /14.16/  

¹ M; et al. "tat"  
² ?M; et al. "nṛpa-"; Speaker confusion in M?

This verse is addressed by Sage Vyāsa to King Yudhiṣṭhira, so "nṛpasattama" makes sense. But
Kṣatriyas are also "dvija", so it is maybe ok. M also says "nṛpasattama" just above, /14.11/.

> vidyāsahāyavān dēvō viṣvaksēnō hariḥ prabhuḥ  
> apsv ēva ṡayanaṃ cakrē nidrāyōgam upāgataḥ  
> jagataṡ ¹cintayan sṛṣṭiṃ citrāṃ bahu²guṇōdayām /14.17/  

¹ all; M1,7 "cintayat"  
² M T1 G3,6; et al. "-guṇōdbhavām"

The Lord Hari is addressed as Viṣvaksēna here.

> tasya cintayataḥ sṛṣṭiṃ mahān ¹ātmāguṇaḥ smṛtaḥ  
> ahaṅkāras tatō jātō brahmā ṡubhacaturmukhaḥ  
> hiraṇyagarbhō bhagavān sarvalōkapitāmahaḥ /14.18/  

¹ M T1 G1,6; et al. "ātmaguṇaḥ"

> padmē’ni¹ruddhāt sambhūtas tadā padmanibhēkṣaṇaḥ  
> sahasrapatrē dyutimān upaviṣṭaḥ sanātanaḥ /14.19/  

¹ †M1,6,7 T G CE; M5 B Da "-ruddhaḥ"

> dadṛṡē’dbhutasaṅkāṡē lōkān āpōmayān prabhuḥ  
> sattvasthaḥ paramēṣṭhī ¹sa tatō bhūta²gaṇān ³sṛjan /14.20/  

¹ M5 T2 G1,2 CE PPS; M1,6,7 T1 G3,6 "ca"  
² all; M1,6 "-guṇān"  
³ M T G3,6; CE PPS "sṛjat"

> pūrvam ¹ēva ca padmasya patrē sūryāṃṡusaprabhē  
> nārāyaṇakṛtau bindū apām āstāṃ guṇōttarau /14.21/  

¹ all; M1,6,7 "ēṣa"

> tāv ¹apaṡyat sa bhagavān anādinidhanō’cyutaḥ  
> ēkas tatrābhavad bindur ²madhvābhō ruciraprabhaḥ /14.22/  

¹ all; M1,7 "apaṡyan"  
² all; M1,7 "madhvāpō"

> sa tāmasō madhur jātas ¹tadā nārāyaṇājn̄ayā  
> kaṭhinas tv aparō binduḥ kaiṭabhō ²rājasas tu saḥ /14.23/  

¹ all; M1,6,7 "tatō"  
² all; M7 "rājasaṃstutaḥ"

> tāv ⁰abhyadhāvatāṃ ṡrēṣṭhau tamōrajaguṇānvitau  
> balavantau gadāhastau padmanāḻānusāriṇau /14.24/  

> dadṛṡātē’ravindasthaṃ brahmāṇam amitaprabham  
> sṛjantaṃ prathamaṃ vēdāṃṡ caturaṡ cāru¹vikramān /14.25/  

¹ M T G; CE "-vigrahān"

> tatō vigraha¹vantas tān vēdān dṛṣṭvāsurōttamau  
> ²vēdān̄ jagṛhatū rājan brahmaṇaḥ paṡyatas tadā /14.26/  

¹ M T G1,3,6; CE "-vantau tau"  
² M T G; CE (hypermetric) "sahasā jagṛhatur vēdān"

M T G say that the Vedas were personified "vigrahavantas tān vēdan" (plural form *tān*)
but CE says "vigravantau tau" (dual form) meaning the embodied Madhu-Kaiṭabha.

> atha tau dānavaṡrēṣṭhau vēdān gṛhya sanātanān  
> rasāṃ viviṡatus tūrṇam udakpūrvē mahōdadhau /14.27/  

> tatō hṛtēṣu vēdēṣu brahmā kaṡmalam āviṡat  
> tatō vacanam ¹īṡānaṃ prāha vēdair vinākṛtaḥ /14.28/  

¹ M5 T1 G2,3,6 CE PPS; M1,6,7 T2 G1 "īṡānaḥ"

> vēdā mē paramaṃ cakṣur vēdā mē paramaṃ balam  
> vēdā mē paramaṃ dhāma vēdā mē brahma ¹cōttaram /14.29/  

¹ M Dn; et al. "cōttamam"

> ⁰mama vēdā hṛtāḥ sarvē dānavābhyāṃ balād itaḥ  
> andhakārā hi mē lōkā jātā vēdair vinākṛtāḥ  
> vēdān ṛtē hi kiṃ kuryāṃ ⁰lōkān vai sraṣṭum udyataḥ /14.30/  

> ahō bata mahad duḥkhaṃ vēdanāṡanajaṃ mama  
> prāptaṃ dunōti hṛdayaṃ tīvraṡōkāya randhayan /14.31/  

> kō hi ṡōkārṇavē magnaṃ mām itō’dya samuddharēt  
> vēdāṃs tān ānayēn naṣṭān kasya cāhaṃ priyō bhavē /14.32/  

> ity ēvaṃ bhāṣamāṇasya brahmaṇō nṛpasattama  
> harēḥ stōtrārtham udbhūtā buddhir buddhimatāṃ vara  
> tatō jagau paraṃ japyaṃ ¹sān̄jalipragrahaḥ ²vibhuḥ /14.33/  

¹ ?M5 B Cs CE; M1,6,7 T2 "sān̄jaliḥ pra-"; T1 G Dn "prān̄jalipra-"  
² M; et al. "prabhuḥ"

Speaker confusion continues. Before /14.33/ K7 D9 in. "vaiṡampāyana uvāca" whereas B8 in. "vyāsa
uvāca".

>    ¹brahmōvāca  
> namas tē brahmahṛdaya namas tē ²mama pūrvaja  
> lōkādya ³nidhanaṡrēṣṭha sāṅkhyayōganidhē vibhō /14.34/  

¹ M T2 G1,2,3 B Dn; CE om.  
² M5 T G CE; M1,6,7 "brahmapū-"  
³ M; et al. "bhuvana-"

> vyaktāvyakta¹tarācintya kṣēmaṃ panthānam āsthita  
> viṡvabhuk ⁰sarvabhūtānām antarātmann ayōnija /14.35/  

¹ M; et al. "-karā-"

> ahaṃ prasādajas tubhyaṃ lōka¹dhāmnē svayambhuvē  
> tvattō mē mānasaṃ janma prathamaṃ ²bījapūjitam /14.36/  

¹ all; M1,7 "-sāmnē"  
² all; M1,7 "bīja-"

> cākṣuṣaṃ ¹vai dvitīyaṃ mē ²janmēhāsīt purātanam  
> tvatprasādāc ca mē janma tṛtīyaṃ vācikaṃ mahat /14.37/  

¹ all; M1,6,7 swap "vai" and "mē"  
² M T1 G; T2 "janma āsīt"; CE "janma cāsīt"

> tvattaḥ ṡravaṇajaṃ cāpi ¹caturthaṃ janma mē vibhō  
> ²nāsityaṃ cāpi mē janma tvattaḥ ³pan̄camam ucyatē /14.38/  

¹ all; M1,7 "caturthē"  
² M; B Da Dn "nāsasthaṃ"; CE "nāsikyaṃ"; PPS "nāsatyaṃ";
  see /15.36/  
³ all; M6 Dn "paramam"

> aṇḍajaṃ cāpi mē janma tvattaḥ ṣaṣṭhaṃ vinirmitam  
> idaṃ ca saptamaṃ janma padmajaṃ mē’mitaprabha /14.39/  

> sargē sargē hy ahaṃ putras tava triguṇa¹varjitaḥ  
> ²prathamaḥ puṇḍarīkākṣa pradhānaguṇakalpitaḥ /14.40/  

¹ M1,5,7 T2 G3 CE; M6 T1 G1,2,6 "-varjita"  
² M B Dn; T G CE "prathitaḥ"

> tvam ¹īṡvarasvabhāvaṡ ca ²bhūtānām ca prabhāvanaḥ  
> tvayā vinirmitō’haṃ vai vēdacakṣur vayōtigaḥ /14.41/  

¹ M5 T G CE; M1,6,7 "īṡvaraḥ sva-"  
² M T G1,3,6; G2 CE PPS "svayambhūḥ puruṣōttamaḥ"

> tē mē vēdā hṛtāṡ cakṣur andhō jātō’smi ¹jāgṛmi  
> dadasva ²cakṣūṃṣi mama ³priyō’haṃ tē priyō’si mē /14.42/  

¹ ?M B0,8; et al. "jāgṛhi"  
² M B; T G CE "cakṣuṣī mahyaṃ"  
³ all; M1,6,7 "prasīda"

As per MW, "jāgṛmi √jāgṛ" is singular irregular (verb class 1).

>    ¹vyāsa uvāca  
> ēvaṃ stutaḥ sa bhagavān puruṣaḥ sarvatōmukhaḥ  
> jahau nidrām atha tadā vēdakāryārtham udyataḥ  
> aiṡvarēṇa ²prayatnēna dvitīyāṃ tanum āsthitaḥ /14.43/  

¹ M5 B8; et al. om.;
  needed to end conversation of Brahma starting /14.34/.  
² M; T2 "ca yōgēna"; G1 "prayōgēna"; T1 G2,3,6 CE PPS "prayōgēṇa"

> ¹tv anāmikēna kāyēna bhūtvā candraprabhas tadā  
> kṛtvā hayaṡiraḥ ṡubhraṃ vēdānām ālayaṃ prabhuḥ /14.44/  

¹ ?M1,5,7; M6 "sanāmikēna"; T G1,3,6 "svamānikēna"; CE "sunāsikēna"; PPS "svamānitēna";
  see /14.38/ where CE uses "nāsika" (nose)  

> tasya mūrdhā samabhavad dyauḥ sanakṣatra¹dēvatāḥ  
> kēṡāṡ cāsyābhavan dīrghā ravēr aṃṡusamaprabhāḥ /14.45/  

¹ M D4,9 K7; et al. "-tārakā"

> karṇāv ākāṡapātāḻē lalāṭaṃ bhūtadhāriṇī  
> gaṅgā sarasvatī ¹puṇyē bhruvāv āstāṃ ²mahādyutī /14.46/  

¹ M5,6 T G1,6; B Da Dn "ṡrōṇyau"; CE "puṇyā"  
² M; T G1,3,6 "mahādyutēḥ"; B Da Dn "mahōdadhī"; CE "mahānadī"

> cakṣuṣī sōmasūryau ¹tu nāsā sandhyā ²puraskṛtā  
> ōṅkāras tv atha saṃskārō vidyuj jihvā ca nirmitā /14.47/  

¹ M B Da; T G CE "tē"  
² M Cs; CE "punaḥ smṛtā"; T1 G3,6 "xxx"; T2 "xxx"; G1 "xxx"

> dantāṡ ca pitarō rājan sōmapā iti viṡrutāḥ  
> gōlōkō brahmalōkaṡca ōṣṭhāv āstāṃ mahātmanaḥ  
> grīvā cāsyābhavad rājan kāḻarātrir ¹guṇōttarā /14.48/  

¹ all; M1,6 "guṇākarā"

> ētad dhayaṡiraḥ kṛtvā nānāmūrtibhir āvṛtam  
> antardadhē sa viṡvēṡō vivēṡa ca rasāṃ prabhuḥ /14.49/  

> ¹rasāṃ punaḥ praviṣṭaṡ ca yōgaṃ paramam āsthitaḥ  
> ṡaikṣaṃ svaraṃ samāsthāya ōm iti prāsṛjat svaram /14.50/  

¹ all; M1,6,7 "rasān"

> sa svaraḥ sānunādī ca sarvagaḥ snigdha ēva ca  
> babhūvāntar¹jalagataḥ sarvabhūtaguṇōditaḥ /14.51/  

¹ M; CE "mahībhūtaḥ"; T2 "xxx"; T1 G3,6 "xxx"; G1 "xxx"

> tatas tāv asurau kṛtvā vēdān samayabandhanān  
> rasātalē vinikṣipya yataḥ ṡabdas tatō drutau /14.52/  

> ētasminn antarē rājan dēvō hayaṡirōdharaḥ  
> jagrāha vēdān akhilān rasātalagatān hariḥ  
> prādāc ca brahmaṇē bhūyas tataḥ svāṃ prakṛtiṃ gataḥ /14.53/  

> sthāpayitvā hayaṡira udakpūrvē mahōdadhau  
> vēdānām ālayaṡ cāpi babhūvāṡva¹ṡirās tataḥ /14.54/  

¹ M1,5 B Dn CE; M6,7 T G "-ṡiras"

> atha kin̄cid apaṡyantau dānavau madhukaiṭabhau  
> punar ājagmatus tatra vēgitau paṡyatāṃ ca ¹tam  
> yatra vēdā vinikṣiptās tat sthānaṃ ṡūnyam ēva ca /14.55/  

¹ M; et al. "tau"

> tata uttamam āsthāya vēgaṃ ¹balavatāṃ varau  
> punar uttasthatuḥ ṡīghraṃ rasānām ālayāt tadā  
> dadṛṡātē ca puruṣaṃ tam ēvādikaraṃ prabhum /14.56/  

¹ all; M1,6,7 "vēgavatāṃ"

> ¹ṡvētaṃ candraviṡuddhābham aniruddhatanau sthitam  
> bhūyō’py amitavikrāntaṃ nidrāyōgam upāgatam /14.57/  

¹ all; M5 "ṡvetacan-"

> ātmapramāṇaracitē apām upari kalpitē  
> ṡayanē nāgabhōgāḍhyē jvālāmālāsamā¹kulē /14.58/  

¹ M T2 G1,3,6; CE "-vṛtē"; T1 "xxx"

> niṣkalmaṣēṇa sattvēna ¹sampannaṃ ruciraprabham  
> taṃ dṛṣṭvā dānavēndrau tau mahāhāsam amun̄catām /14.59/  

¹ all; M1,6 T1 "sampannaru-"

> ūcatuṡ ca samāviṣṭau rajasā tamasā ca tau  
> ayaṃ sa puruṣaḥ ṡvētaḥ ṡētē nidrām upāgataḥ /14.60/  

> anēna nūnaṃ vēdānāṃ kṛtam āharaṇaṃ rasāt  
> kasyaiṣa kō nu khalv ēṣa kin̄ca svapiti bhōgavān /14.61/  

> ity uccāritavākyau tau bōdhayām āsatur harim  
> yuddhārthinau ¹hi vijn̄āya ²vibuddhaḥ puruṣōttamaḥ /14.62/  

¹ M5 T2 G1 B Da Dn Cs; CE "tu"  
² all; M1,7 G1 "vibudhaḥ"

> nirīkṣya cāsurēndrau tau tatō yuddhē manō dadhē  
> atha yuddhaṃ samabhavat tayōr nārāyaṇasya ca /14.63/  

> rajas¹tamōviṣṭatanū tāv ubhau madhukaiṭabhau  
> brahmaṇōpacitiṃ kurvan̄ jaghāna madhusūdanaḥ /14.64/  

¹ all; M1,6,7 "-tamāviṣṭatanū"

> tatas tayōr vadhēnāṡu ¹vēdānāṃ haraṇēna ca  
> ṡōkāpanayanaṃ cakrē brahmaṇaḥ puruṣōttamaḥ /14.65/  

¹ M1,5,6 T G PPS; M7 CE "vēdāpaharaṇēna"

> tataḥ ¹pravavṛtē brahmā hatārir vēdasatkṛtaḥ  
> nirmamē ²sa tadā lōkān kṛtsnān sthāvarajaṅgamān /14.66/  

¹ M T G3,6; CE PPS "parivṛtō"  
² all; M1,6,7 "sa"

> dattvā pitāmahāyāgryāṃ buddhiṃ ⁰lōkavisargikīm  
> tatraivāntardadhē dēvō yata ēvāgatō hariḥ /14.67/  

> tau dānavau harir hatvā kṛtvā hayaṡiras tanum  
> punaḥ pravṛttidharmārthaṃ tām ēva vidadhē tanum /14.68/  

M1,6,7 om. /14.68/ and /14.69L1/. Only M5 in. CE@897 (/14.69/) after /14.68/ and switches the speaker
of the following verses to Vaiṡampāyana. It makes sense because
the Vyāsa-Yudhiṣṭhira samvāda abruplty ends the chapter. The speaker of 
next chapter is Vaiṡampāyana-Janamējaya.

>    vaiṡampāyana uvāca  
> ēvaṃ sa bhagavān vyāsō gurur mama viṡām patē  
> kathayām āsa dharmajn̄ō dharmarājn̄ē dvijōttamaḥ /14.69/  

> ēvam ēṣa mahābhāgō babhūvāṡva¹ṡirō hariḥ  
> paurāṇam ētad ²prakhyātaṃ rūpaṃ ³varadam aiṡvaram /14.70/  

¹ M5 T1 G1,3,6; CE PPS "ṡirā"  
² ?M5 B Dn; T G1,3,6 "ākhyānaṃ"; M1,6,7 CE "ākhyātaṃ"  
³ M5,6,7 B Dn CE; M1 "vai padamīṡvaram"; T G3,6 "xxx"; G1 "xxx"

> yō hy ētad brāhmaṇō nityaṃ ṡṛṇuyād dhārayēta vā  
> na tasyādhyayanaṃ nāṡam upagacchēt kadācana /14.71/  

> ārādhya tapasōgrēṇa dēvaṃ hayaṡirōdharam  
> pān̄cālēna kramaḥ prāptō rāmēṇa pathi dēṡitē /14.72/  

> ētad dhayaṡirō rājann ākhyānaṃ tava kīrtitam  
> purāṇaṃ vēdasamitaṃ ¹yan māṃ tvaṃ paripṛcchasi /14.73/  

¹ all; M1 "yathā"; M7 "taṃ mā" (sic)

> yāṃ yām icchēt tanuṃ dēvaḥ kartuṃ kāryavidhau kvacit  
> tāṃ tāṃ kuryād vikurvāṇaḥ svayam ātmānam ātmanā /14.74/  

> ēṣa vēdanidhiḥ ṡrīmān ēṣa ¹vai tapasāṃ nidhiḥ  
> ēṣa yōgaṡ ca sāṅkhyaṃ ca brahma cāgryaṃ harir vibhuḥ /14.75/  

¹ M5 T G PPS; M1,6,7 "caiva tapōnidhiḥ"; CE "vai tapasō-"

> nārāyaṇaparā vēdā yajn̄ā nārāyaṇātmakāḥ  
> tapō nārāyaṇaparaṃ nārāyaṇaparā gatiḥ /14.76/  

> nārāyaṇaparaṃ satyam ṛtaṃ nārāyaṇ¹āṡrayam  
> nārāyaṇaparō dharmaḥ punarāvṛttidurlabhaḥ /14.77/  

¹ M T G3,6 PPS; CE "-ātmakam"

> pravṛttilakṣaṇaṡ caiva dharmō nārāyaṇātmakaḥ  
> nārāyaṇātmakō gandhō bhūmau ṡrēṣṭhatamaḥ smṛtaḥ /14.78/  

> apāṃ caiva guṇō rājan rasō nārāyaṇātmakaḥ  
> ¹jyōtiṣaṡ ca guṇō rūpaṃ smṛtaṃ nārāyaṇātmakam /14.79/  

¹ M T G; CE "jyōtiṣāṃ"

> nārāyaṇātmakaṡcāpi sparṡō vāyuguṇaḥ smṛtaḥ  
> nārāyaṇātmakaṡcāpi ṡabda ākāṡasambhavaḥ /14.80/  

> manaṡ cāpi tatō bhūtam avyaktaguṇalakṣaṇam  
> nārāyaṇaparaḥ kālō jyōtiṣām ayanaṃ ca yat /14.81/  

> nārāyaṇaparā kīrtiḥ ṡrīṡ ca lakṣmīṡ ca dēvatāḥ  
> nārāyaṇaparaṃ sāṅkhyaṃ yōgō nārāyaṇātmakaḥ /14.82/  

Three consorts of Nārāyaṇa are Kīrti, Ṡrī and Lakṣmī.

> kāraṇaṃ puruṣō yēṣāṃ pradhānaṃ cāpi kāraṇam  
> svabhāvaṡ caiva karmāṇi daivaṃ yēṣāṃ ca kāraṇam /14.83/  

> pan̄cakāraṇa¹saṅkhyātō niṣṭhā sarvatra vai hariḥ  
> tattvaṃ jijn̄āsamānānāṃ hētubhiḥ sarvatōmukhaiḥ /14.84/  

¹ ?M6 T G CE; M5 "-saṅkhyātā"; M1,7 "-saṅkhyātani-"

> tattvam ēkō mahāyōgī harir nārāyaṇaḥ prabhuḥ  
> ¹brahmādīnāṃ salōkānām ṛṣīṇāṃ ca mahātmanām /14.85/  

¹ M B Dn Cs; T2 G1 "brahmavādī"; CE "sabrahmakānāṃ lōkānām"; T1 G3,6 "xxx"

> sāṅkhyānāṃ yōgināṃ ¹caiva yatīnām ātmavēdinām  
> ²manīṣitāni jn̄ātāni kēṡavō na tu tasya tē /14.86/  

¹ M T G; CE "cāpi"  
² ?M5; M5 (also) "-jātāni-"; M1,7 "-pradadau-"; T G "-jānāti"; CE "manīṣitaṃ vijānāti"

> yē kē cit sarvalōkēṣu daivaṃ pitryaṃ ca kurvatē  
> dānāni ca prayacchanti tapyanti ca tapō mahat /14.87/  

> sarvēṣām āṡrayō viṣṇur aiṡvaraṃ vidhim āsthitaḥ  
> sarvabhūtakṛtāvāsō vāsudēvēti cōcyatē /14.88/  

> ¹ēṣō hi nityaḥ paramō maharṣir  
>  mahāvibhūtir guṇavān nirguṇākhyaḥ  
> guṇaiṡ ca saṃyōgam upaiti ṡīghraṃ  
>  kālō yathartāv ṛtusamprayuktaḥ /14.89/  

¹ M1,5,6 T G PPS; M7 "ēṣō’bhi ni-"; CE "ayaṃ"

> naivāsya vindanti gatiṃ mahātmanō  
>  na cāgatiṃ kaṡcid ihānupaṡyati  
> jn̄ānātmakāḥ saṃyaminō maharṣayaḥ  
>  paṡyanti nityaṃ puruṣaṃ ¹guṇātmakam /14.90/  

¹ M; T G "purāṇam"; CE "guṇādhikam"

Thus ends chapter M1,5,7 170; G1 173; T1 G6 174; G3 175; T2 187; G2 203.

It has 90 verses, 193 lines;  
    CE 89 verses, 191 lines;  
    GP 95 verses, 193 lines;  
    Ku 94 verses, ??? lines.

Chapter name:

M      "hayaṡirōpākhyānaṃ"  
T1, G6 "hariṡiraḥ prādurbhāvaḥ"  
