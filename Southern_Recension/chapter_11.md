# Chapter 11

>    ¹naranārāyaṇāv ūcatuḥ  
> dhanyō’sy anugṛhītō’si yat tē dṛṣṭaḥ svayaṃ ²prabhuḥ  
> na hi taṃ dṛṣṭavān kaṡcit padmayōnir api svayam /11.1/  

¹ all; M1 T2 "nārāyaṇāv ūcatuḥ"  
² all; M1,6,7 "bhuvaḥ"

> ¹ākāṡayōnīr bhagavān durdarṡaḥ puruṣōttamaḥ  
> nāradaitad dhi tē satyaṃ vacanaṃ samudāhṛtam /11.2/  

¹ M; et al. "avyaktayōnir"

CE's reading implies Bhagavān is "born of the Unmanifest" or "is of unmanifest origin".
M's reading commented on by Cs as "ākāṡādi-samasta-jagad-hētuḥ" is apt.

> nāsya bhaktaiḥ priyatarō lōkē kaṡcana vidyatē  
> tataḥ svayaṃ darṡitavān ¹tavātmānaṃ dvijōttama /11.3/  

¹ M; T G "tadā-"; CE "svam āt-"

> tapō hi tapyatas tasya yat sthānaṃ paramātmanaḥ  
> na tat samprāpnutē kaṡcid ṛtē hy āvāṃ dvijōttama /11.4/  

> yā hi sūryasahasrasya samastasya bhavēd dyutiḥ  
> sthānasya sā bhavēt tasya svayaṃ tēna ¹virājitā /11.5/  

¹ M5 T G1,3,6; M1,6,7 CE "virājatā"

> tasmād uttiṣṭhatē vipra dēvād ¹viṡvaṃ bhuvaḥ patēḥ  
> kṣamā kṣamāvatāṃ ṡrēṣṭha yayā bhūmis tu yujyatē /11.6/  

¹ M; et al. "viṡvabhuvaḥ"

> tasmāc cōttiṣṭhatē dēvāt sarvabhūtahitō rasaḥ  
> āpō yēna hi yujyantē dravatvaṃ prāpnuvanti ca /11.7/  

> tasmād ēva samudbhūtaṃ tējō ¹jyōtirguṇātmakam  
> yēna sma yujyatē sūryas tatō lōkān virājatē /11.8/  

¹ M; T G CE "rūpa"

> tasmād ¹ēva samudbhūtaḥ sparṡas ²puruṣasattamāt  
> yēna sma yujyatē vāyus ⁰tatō lōkān vivāty asau /11.9/  

¹ M T G; CE "dēvāt"  
² M T G; CE "tu puruṣōttamāt"

> tasmād ¹uttiṣṭhatē ṡabdaḥ sarvalōkēṡvarāt prabhōḥ  
> ākāṡaṃ yujyatē yēna tatas tiṣṭhaty asaṃvṛtam /11.10/  

¹ M T G; CE "cōttiṣṭ-"

> tasmāc cōttiṣṭhatē ¹viṡvaṃ sarva²lōkagataṃ manaḥ  
> candramā yēna saṃyuktaḥ prakāṡaguṇadhāra³kaḥ /11.11/  

¹ M B; T G3,6 CE "dēvāt"  
² M; T1 G1,3,6 "-bhūtagataṃ"  
³ M; et al. "-ṇaḥ"

> ṣaḍbhūtōtpādakaṃ nāma tat sthānaṃ vēdasan̄jn̄itam  
> vidyāsahāyō yatrāstē bhagavān havyakavyabhuk /11.12/  

> yē hi niṣkalmaṣā lōkē puṇyapāpavivarjitāḥ  
> tēṣāṃ vai kṣēmam adhvānaṃ gacchatāṃ dvijasattama /11.13/  

/11.14/ and /11.15/ are describing arcirādi mārga (via the Sun).

> sarvalōkatamōhantā ādityō dvāram ucyatē  
> jvālāmālī mahātējā yēnēdaṃ dhāryatē jagat /11.14/  

M T G in. CE@889 (/11.14L2/).

> āditya¹daṇḍasarvāṅgā adṛṡyāḥ kēna cit kvacit  
> ²paramātmabhūtā bhūtvā tu taṃ dēvaṃ praviṡanty uta /11.15/  

¹ M; et al. "-dagdhasar-"  
² M; CE "paramāṇu-"; T G "paramāṇv ātma bhūtās tē"

M's changes the meaning slightly. CE says "they (liberated ones) become atomic in size (paramāṇu) as
they enter Him" but M says "they become like Paramātma" (sāyujya?).

The path seems to match the hierarchy of Viṡiṣṭādvaita
(Āditya = Vibhava 11.14 → Paramātma = Antaryāmi 11.15 → Caturvyūha 11.16-17 → Para-Vāsudēva 11.18)

> tasmād api vinirmuktā aniruddhatanau sthitāḥ  
> manōbhūtās tatō ¹bhūtvā pradyumnaṃ praviṡanty uta /11.16/  

¹ M B Dn; T G CE "bhūyaḥ"

> pradyumnāc ¹ca vinirmuktā jīvaṃ saṅkarṣaṇaṃ ²tataḥ  
> viṡanti viprapravarāḥ sāṅkhyā ³yōgāṡ ca taiḥ saha /11.17/  

¹ M T1 G1,3,6 "cāpi nir-"  
² M5,6 B Dn; T G CE "tathā"  
³ M T G; CE "bhāgavataiḥ"

> tatas traiguṇyahīnās tē paramātmānam an̄jasā  
> praviṡanti dvijaṡrēṣṭha kṣētrajn̄aṃ nirguṇātmakam  
> ¹sarvāvāsaṃ vāsudēvaṃ kṣētrajn̄aṃ ⁰viddhi tattvataḥ /11.18/  

¹ all; M1,6,7 "sarvāvāsā"  

> samāhitamanaskāṡ ca niyatāḥ saṃyatēndriyāḥ  
> ēkāntabhāvōpagatā vāsudēvaṃ viṡanti tē /11.19/  

> āvām api ca dharmasya gṛhē jātau dvijōttama  
> ramyāṃ viṡālām āṡritya tapa ugraṃ samāsthitau /11.20/  

> yē tu tasyaiva dēvasya prādurbhāvāḥ surapriyāḥ  
> bhaviṣyanti trilōkasthās tēṣāṃ svastīty atō dvija  
> svārthēna vidhinā yuktaḥ sarvakṛcchravratē sthitaḥ /11.21/  

M substitutes CE@890 (/11.21L3/) for CE 12.332.21 ("vidhinā...uttamam").  
T G in. the same after CE 12.332.21. There is no loss of context, philosophy or meaning.

> āvābhyām api dṛṣṭas tvaṃ ṡvētadvīpē tapōdhana  
> samāgatō bhagavatā san̄jalpaṃ kṛtavān yathā /11.22/  

> ⁰sarvaṃ hi nau saṃviditaṃ trailōkyē sacarācarē  
> yad bhaviṣyati vṛttaṃ vā vartatē ¹vā ṡubhāṡubham /11.23/  

¹ all; M1,6,7 "cā ṡubhāṡubham"

>    ¹vaiṡampāyana uvāca  
> ētac chrutvā tayōr vākyaṃ tapasy ugrē ²vyavasthitaḥ  
> nāradaḥ prān̄jalir bhūtvā nārāyaṇaparāyaṇaḥ /11.24/  

¹ M5 B Dn CE; M1,6,7 T G om.  
² M G2 PPS; T G3,6 "vratē sthitaḥ"; G1 "bhūtē sthitaḥ"; CE "-’bhyavartata"

> jajāpa ¹vidhivan mantrān nārāyaṇa²parān bahūn  
> divyaṃ varṣasahasraṃ hi naranārāyaṇāṡramē /11.25/  

¹ all; M1,6,7 "vividhān"  
² M T G; CE "-gatān"

> avasat sa mahātējā nāradō bhagavān ṛṣiḥ  
> tam ēvābhyarcayan dēvaṃ naranārāyaṇau ca tau /11.26/  

Thus ends chapter M1,6,7 167; G1 170; T1 G6 171; G3 172; T2 184; G2 200; M5 202.

It has 26 verses, 54 lines;
    CE 26 verses, 54 lines;
    GP 27 verses, 55 lines;
    Ku 28 verses, 57 lines.

Chapter name:

M "nāradātapaṡ-caraṇaṃ"
