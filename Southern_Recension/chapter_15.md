# Chapter 15

Note: T2 G2 are missing for this adhyāya.

>    ¹janamējaya uvāca  
> ahō hy ēkāntinaḥ sarvān prīṇāti bhagavān hariḥ  
> vidhiprayuktāṃ pūjāṃ ca gṛhṇāti ¹ṡirasā svayam /15.1/  

¹ M T1 G1,3,6; CE "bhagavān"

> yē tu dagdhēndhanā lōkē puṇyapāpavivarjitāḥ  
> tēṣāṃ ¹tvayā hi nirdiṣṭā pāramparyāgatā gatiḥ /15.2/  

¹ M T G; CE "tvayā’bhi nir-"

> ¹caturthyā ²cāpi tē ³gatyāṃ gacchanti puruṣōttamam  
> ēkāntinas tu puruṣā gacchanti paramaṃ padam /15.3/  

¹ M; T G CE "cayurthyāṃ"  
² M T1 G3,6 PPS; CE "caiva"  
³ all; M1,6,7 "gatyā"

> nūnam ¹ēkāntidharmō’yaṃ ṡrēṣṭhō nārāyaṇapriyaḥ  
> agatvā gatayas tisrō yad gacchanty avyayaṃ harim /15.4/  

¹ M T1 G3,6 PPS; CE "ēkānta-"

> sahōpaniṣadān vēdān yē viprāḥ samyag āsthitāḥ  
> paṭhanti vidhim āsthāya yē cāpi yatidharmiṇaḥ /15.5/  

> tēbhyō ¹viṡiṣṭāṃ jānāmi gatim ēkāntināṃ nṛṇām  
> kēnaiṣa dharmaḥ kathitō dēvēna ṛṣiṇāpi vā /15.6/  

¹ all; M5 "viṡiṣṭaṃ"

> ēkāntināṃ ca kā caryā kadācōtpāditā vibhō  
> ¹ētan mē saṃṡayaṃ chindhi paraṃ kautūhalaṃ hi mē /15.7/  

¹ all; M7 "ētaṃ"

>    vaiṡampāyana uvāca  
> samupōḍhēṣv anīkēṣu kurupāṇḍavayōr mṛdhē  
> arjunē vimanaskē ¹vai gītā bhagavatā svayam /15.8/  

¹ M T1 G1,3,6 PPS; CE "ca"

> āgatiṡ ca gatiṡ caiva pūrvaṃ tē kathitā mayā  
> gahanō hy ēṣa dharmō vai durvijn̄ēyō’kṛtātmabhiḥ /15.9/  

> sammitaḥ sāmavēdēna puraivādiyugē kṛtaḥ  
> dhāryatē svayam īṡēna rājan nārāyaṇēna ha /15.10/  

> ētam arthaṃ mahārāja pṛṣṭaḥ pārthēna nāradaḥ  
> ṛṣimadhyē ¹mahābhāgaḥ ṡṛṇvatōḥ kṛṣṇabhīṣmayōḥ /15.11/  

¹ all; M1,6,7 "mahārāja"

> guruṇā ca mamāpy ēṣa kathitō nṛpasattama  
> yathā tu kathitas tatra nāradēna tathā ṡṛṇu /15.12/  

> yadāsīn mānasaṃ janma nārāyaṇamukhōdgatam  
> brahmaṇaḥ pṛthivīpāla tadā nārāyaṇaḥ svayam  
> tēna dharmēṇa kṛtavān daivaṃ pitryaṃ ca bhārata /15.13/  

> phēnapā ṛṣayaṡ caiva taṃ dharmaṃ pratipēdirē  
> vaikhānasāḥ phēnapēbhyō ¹taṃ dharmaṃ prapēdirē  
> vaikhānasēbhyaḥ sōmas tu tatas ²tv antardadhē punaḥ /15.14/  

¹ M T1 G3,6 PPS; CE "dharmam ētaṃ"  
² M1,6,7 T1 G3,6;  M5 B Dn CE PPS "sō’ntar-" (see /15.22/, /15.35/, /15.39/)

> yadāsīc cākṣuṣaṃ janma dvitīyaṃ brahmaṇō nṛpa  
> ¹tadā pitāmahēnaiṣa sōmād dharmaḥ pariṡrutaḥ  
> nārāyaṇātmakaṃ rājan rudrāya pradadau ca ²tam /15.15/  

¹ M T1 G3,6 PPS Dn; CE "tadā pitāmahāt sōmād ētaṃ dharmam ajānata"  
² M B Da Dn; T G CE "saḥ"

CE's reading of "pitāmahāt sōmāt" is ambiguous. Everyone else reads it as "pitāmahēna sōmāt"

> ¹tatō yōgasthitō rudraḥ purā kṛtayugē nṛpa  
> vālakhilyān ṛṣīn sarvān dharmam ²ēnam apāṭhayat  
> antardadhē tatō bhūyas tasya dēvasya māyayā /15.16/  

¹ all; M7 "tapōyōgē sthi-"  
² M5 T1 G3,6 PPS; M1,6,7 CE "ētam"; G1 "ē�m"

> tṛtīyaṃ brahmaṇō janma yadāsīd vācikaṃ mahat  
> tatraiṣa dharmaḥ sambhūtaḥ svayaṃ nārāyaṇān nṛpa /15.17/  

> suparṇō nāma tam ṛṣiḥ prāptavān puruṣōttamāt  
> tapasā vai sutaptēna damēna niyamēna ca /15.18/  

> triḥ parikrāntavān ¹ētaṃ suparṇō dharmam uttamam  
> yasmāt tasmād vrataṃ hy ētat ²trisuparṇam ihōcyatē /15.19/  

¹ M T1 G1,6; CE "ētat"; PPS "ēnaṃ"  
² ?M5 T1 G3,6; M1,6,7 G1 CE PPS "trisauparṇam"

> ṛgvēdapāṭhapaṭhitaṃ vratam ētad dhi duṡcaram  
> suparṇāc cāpy adhigatō dharma ēṣa sanātanaḥ /15.20/  

> vāyunā dvipadāṃ ṡrēṣṭha prathitō jagadāyuṣā  
> vāyōḥ sakāṡāt prāptaṡ ca ṛṣibhir vighasāṡibhiḥ /15.21/  

> tēbhyō mahōdadhiṡ cainaṃ prāptavān dharmam uttamam  
> ¹antardadhē tatō bhūyō nārāyaṇa²samāhṛtaḥ /15.22/  

¹ M T1 G3,6 B Da Dn PPS; CE "tataḥ sō’ntardadhē bhūyō"  
² M T1 G1,3,6 PPS; CE "-samāhitaḥ"

> yadā bhūyaḥ ṡravaṇajā ¹sṛṣṭir āsīn mahātmanaḥ  
> brahmaṇaḥ puruṣavyāghra tatra kīrtayataḥ ṡṛṇu /15.23/  

¹ †M1,6,7 B Dn CE; T1 G3,6 PPS "janma"; M5 "buddhir" (sic)

> jagat sraṣṭumanā dēvō harir nārāyaṇaḥ svayam  
> cintayām āsa puruṣaṃ jagatsargakaraṃ ¹pranhum /15.24/  

¹ M B Da Dn; T G CE "prabhuḥ"

> atha cintayatas tasya karṇābhyāṃ puruṣaḥ sṛtaḥ  
> prajāsargakarō brahmā tam uvāca jagatpatiḥ /15.25/  

> sṛja prajāḥ putra sarvā mukhataḥ pādatas tathā  
> ṡrēyas tava vidhāsyāmi balaṃ tējaṡ ca suvrata /15.26/  

PPS om. /15.26L2/-/15.27L1/ which mentions the establishment of "Sātvata" dharma.

> dharmaṃ ca mattō gṛhṇīṣva sātvataṃ nāma nāmataḥ  
> tēna sarvaṃ ¹kṛtayugē sthāpayasva yathāvidhi /15.27/  

¹ M; et al. "kṛtayugaṃ"

> tatō brahmā namaṡcakrē dēvāya harimēdhasē  
> dharmaṃ cāgryaṃ sa jagrāha sarahasyaṃ sasaṅgraham  
> āraṇyakēna sahitaṃ nārāyaṇamukhōd¹gatam /15.28/  

¹ †M1,6,7 T G CE; M5 B Dn "-bhavam"

Cs derives "harimēdha" as "harim indram ēdhayati iti", i.e, that Hari who causes Indra to propser.

> upadiṡya tatō dharmaṃ brahmaṇē’mitatējasē  
> taṃ kārtayugadharmāṇaṃ nirāṡīḥkarmasan̄jn̄itam  
> jagāma tamasaḥ pāraṃ yatrāvyaktaṃ vyavasthitam /15.29/  

> tatō’tha varadō dēvō brahmalōkapitāmahaḥ  
> asṛjat sa tadā lōkān kṛtsnān sthāvarajaṅgamān /15.30/  

> tataḥ prāvartata tadā ādau kṛtayugaṃ ṡubham  
> tatō hi sātvatō dharmō vyāpya lōkān avasthitaḥ /15.31/  

> tēnaivādyēna dharmēṇa brahmā lōkavisargakṛt  
> pūjayām āsa dēvēṡaṃ hariṃ nārāyaṇaṃ prabhum /15.32/  

> dharmapratiṣṭhāhētōṡ ca manuṃ svārōciṣaṃ tataḥ  
> adhyāpayām āsa tadā lōkānāṃ hitakāmyayā /15.33/  

> tataḥ svārōciṣaḥ putraṃ svayaṃ ṡaṅkhapadaṃ nṛpa  
> adhyāpayat purāvyagraḥ ¹sa ca lōkapatir ²babhau /15.34/  

¹ M T G1,3,6 PPS; CE "sarvalō-"  
² M T1 G3,6 PPS; CE "vibhuḥ"

M5,6 B Da in. /15.35L2/ as "diṡāpālaṃ sudharmāṇam dharmam ētam anuttamam"
after /15.34L1/ but M1,7 T G Dn CE PPS don't.

> tataḥ ṡaṅkhapadaṡ cāpi putram ¹ātmaprajōdbhavam  
> diṡāpālaṃ sudharmāṇam adhyāpayata bhārata  
> tataḥ ²cāntardadhē bhūyaḥ prāptē trētāyugē punaḥ /15.35/  

¹ M; T G CE PPS "ātmajam aurasam"  
² M1,5,6 T1 G3,6 PPS; M7 CE "sō’ntardadhē"

> ¹nāsikyajanmani purā brahmaṇaḥ pārthivōttama  
> dharmam ētaṃ svayaṃ dēvō harir nārāyaṇaḥ prabhuḥ  
> ²ujjagādāravindākṣō brahmaṇaḥ paṡyatas tadā /15.36/  

¹ ?CE; M5 B Dn PPS "nāsatyē"; M1,6,7 "nāsityē"; T1 G1,3,6 "nāsikyē";
  see /14.38/  
² M; T G CE "ujjagārāvin-"

> sanatkumārō bhagavāṃs tataḥ prādhītavān nṛpa  
> sanatkumārād api ca vīraṇō vai prajāpatiḥ  
> kṛtādau kuruṡārdūla dharmam ētam adhītavān /15.37/  

> vīraṇaṡ cāpy adhītyainaṃ ¹raucyāya manavē dadau  
> ²raucyaḥ putrāya ṡuddhāya suvratāya ³sudhanvanē /15.38/  

¹ †M1,6,7 G1,3,6 CE PPS; M5 "raibhyāya manavē"; T1 "nauvyāya"; B Da Dn "raibhyāya munayē"  
² †M6,7 T1 G1,3,6 CE PPS; M1,5 B Da Dn "raibhyaḥ"

As per MW, "Raucya" is 13th Manu. M5 mistakenly says "raibhyāya manave" but B Dn correct it mean to
a Raibhya Muni. Morever, M unanimously say "mighty-bowed" (sudhanvan) which is unlikely of a sage
(muni) but probable of the son of a Manu.

[Wikipedia](https://en.wikipedia.org/wiki/Manu_(Hinduism)) says Raucya and Raibhya are both Manus.

[Harivaṃṡa of Gita Press](http://mahabharata-resources.org/harivamsa/hv_1_7.html) uses Raucya Manu
in the verse, but commentary says Raibhya is the same person. Harivaṃṡa CE /7.5/ and /7.41/ say
Raucya, even in the Malayalam mss -- none say Raibhya.

> ¹kukṣināmnē’tha pradadau ²diṡāṃ pālāya dharmiṇē  
> tatas ³tv antardadhē bhūyō nārāyaṇamukhōdgataḥ /15.39/  

¹ ‡T G CE; M5 "kukṣipādāya"; M6 "kukṣipālāya"; M7 B Da "kukṣināmnē sa"; PPS "kukṣināmnē taṃ"  
² ?M5 CE; M1,6,7 G1 "diṡāpālāya"; T1 G3,6 PPS "vimalāya ca"  
³ M T1 G3,6 PPS; CE "sō’ntar-" (see /15.14/, /15.22/, /15.35/)

> aṇḍajē janmani punar ¹brahmaṇō harimēdhasaḥ  
> ēṣa dharmaḥ samudbhūtō nārāyaṇamukhāt punaḥ /15.40/  

¹ †M1,6,7 T1 G3,6 PPS; M5 "brahmaṇē-"; CE "brahmaṇē hariyōnayē"

> gṛhītō brahmaṇā rājan prayuktaṡ ca yathāvidhi  
> adhyāpitāṡ ca munayō nāmnā barhiṣadō nṛpa /15.41/  

> barhiṣadbhyaṡ ca saṅkrāntaḥ sāmavēdāntagaṃ dvijam  
> jyēṣṭhaṃ nāmnābhivikhyātaṃ ¹jyēṣṭhasāmavratāharam /15.42/  

¹ M; T1 G3,6 "dvijē sāmapradō hariḥ"; CE "jyēṣṭhasāmavratō hariḥ"

CE's reading is "He is Hari, who is the doer of Jyēṣṭha-sāmavrata". This Hari is different than Lord
Hari. M's reading is more suitable: "Jyēṣṭha, who accomplishes the Sāma vrata".

> jyēṣṭhāc cāpy anusaṅkrāntō rājānam avikampanam  
> antardadhē tatō rājann ēṣa dharmaḥ ¹prabhuṃ harim /15.43/  

¹ M; T G CE "prabhōr harēḥ"

> yad idaṃ saptamaṃ janma padmajaṃ brahmaṇō nṛpa  
> tatraiṣa dharmaḥ kathitaḥ svayaṃ nārāyaṇēna ¹ha /15.44/  

¹ M T1 G1; B Da "ca"; CE "hi"

> pitāmahāya ṡuddhāya yugādau lōkadhāriṇē  
> pitāmahaṡ ca dakṣāya dharmam ētaṃ purā dadau /15.45/  

> tatō jyēṣṭhē tu dauhitrē prādād dakṣō nṛpōttama  
> ādityē savitur jyēṣṭhē vivasvān̄ jagṛhē tataḥ /15.46/  

> trētāyugādau ca punar vivasvān manavē dadau  
> ¹manuṡ ca lōkabhūtyarthaṃ sutāyēkṣvākavē dadau /15.47/  

¹ all; M1,7 "manuṣyē"

> ikṣvākuṇā ca kathitō vyāpya lōkān avasthitaḥ  
> gamiṣyati kṣayāntē ca punar nārāyaṇaṃ nṛpa /15.48/  

> vratināṃ cāpi yō dharmaḥ sa tē ¹pūrvaṃ nṛpōttama  
> kathitō harigītāsu ²samāsavidhikalpitaḥ /15.49/  

¹ ?M1,5,6 B Dn CE PPS; M7 G3,6 "sarvanṛ-"; T1 G1 "sarvō"  
² all; M1,7 "samāsāvi-"

> nāradēna tu samprāptaḥ sarahasyaḥ sasaṅgrahaḥ  
> ēṣa dharmō jagannāthāt sākṣān nārāyaṇān nṛpa /15.50/  

> ēvam ēṣa mahān dharma ādyō rājan sanātanaḥ  
> durvijn̄ēyō duṣkaraṡ ca sātvatair dhāryatē sadā /15.51/  

> dharmajn̄ānēna caitēna suprayuktēna karmaṇā  
> ahiṃsādharmayuktēna prīyatē harir īṡvaraḥ /15.52/  

> ēkavyūha¹vibhānō vā kvacid ²dvirvyūhasan̄jn̄itaḥ  
> trivyūhaṡ cāpi ³saṅkhyātaṡ caturvyūhaṡ ca dṛṡyatē /15.53/  

¹ ?M; et al. "-vibhāgō"  
² M1,5,6 G1 Dn; T1 G3,6 CE PPS "dvivyū-"  
³ M5,6 T1 G3,6 CE PPS; M1,7 "saṅkhyātuṡ"; G1 "saṅkhyāṡ ca"

CE says "a division (vibhāga) of one vyūha".
M says "vibhānaḥ vā". One could take "vibhāna" to be homonym of "vibhānu", just like "bhāna" is
homonym of "bhānu". As per MW, "vibhānu", "bhānu" and "bhāna" all mean shining or lustre.
Bhāna also means appearance. So, you could read it as "lustre of one vyūha" or "appearance of one vyūha".

> harir ēva hi kṣētrajn̄ō ¹nirmalō niṣkaḻas tathā  
> jīvaṡ ca sarvabhūtēṣu pan̄cabhūtaguṇātigaḥ /15.54/  

¹ M B8; T1 G1,3,6 "nirguṇō"; B Dn CE PPS "nirmamō"

> ¹ataṡ ca prathitō rājan pan̄cēndriya²samīraṇaḥ  
> ēṣa lōka³vidhir dhīmān ēṣa lōkavisargakṛt /15.55/  

¹ †M1,6,7 T1 G1,3,6; M5 B Dn CE "manaṡ ca prathitaṃ"  
² M T1 G3,6; CE "-samīraṇam"  
³ M5,6 B Dn; M1,7 T G CE "-nidhir"

> akartā caiva kartā ca kāryaṃ kāraṇam ēva ca  
> yathēcchati tathā rājan krīḍatē puruṣō’vyayaḥ /15.56/  

> ēṣa ēkāntidharmas tē kīrtitō nṛpasattama  
> mayā guruprasādēna durvijn̄ēyō’kṛtātmabhiḥ  
> ēkāntinō hi puruṣā durlabhā bahavō nṛpa /15.57/  

> yady ēkāntibhir ākīrṇaṃ jagat syāt kurunandana  
> ahiṃsakair ātmavidbhiḥ sarvabhūtahitē rataiḥ  
> bhavēt kṛtayugaprāptir ¹īdṛṡaiḥ kāmavarjitaḥ /15.58/  

¹ M T1 G3,6; CE "āṡīḥkarmavivarjitaiḥ"

> ēvaṃ sa bhagavān vyāsō gurur mama viṡāṃ patē  
> kathayām āsa ¹dharmajn̄ō dharma²rājn̄ē dvijōttamaḥ /15.59/  

¹ all; M5 "dharmajn̄ē"  
² all; M5 G3 "-rājē"

> ṛṣīṇāṃ sannidhau rājan̄ ṡṛṇvatōḥ kṛṣṇabhīṣmayōḥ  
> tasyāpy akathayat pūrvaṃ nāradaḥ sumahātapāḥ /15.60/  

> dēvaṃ paramakaṃ brahma ṡvētaṃ candrābham acyutam  
> yatra caikāntinō yānti nārāyaṇa¹parāyaṇāḥ /15.61/  

¹ all; M6 "-parā janāḥ"

>    janamējaya uvāca  
> ēvaṃ bahuvidhaṃ dharmaṃ pratibuddhair niṣēvitam  
> na kurvanti kathaṃ viprā ¹ādyaṃ nānāvratē sthitāḥ /15.62/  

¹ M G3,6 PPS; T1 "adya"; G1 "hy anyē"; CE "anyē"

>    vaiṡampāyana uvāca  
> tisraḥ prakṛtayō rājan dēhabandhēṣu nirmitāḥ  
> sāttvikī rājasī caiva tāmasī ¹cēti bhārata /15.63/  

¹ M5 T G CE; M1,6,7 B Da Dn "caiva"

> dēhabandhēṣu puruṣaḥ ṡrēṣṭhaḥ kurukulōdvaha  
> sāttvikaḥ puruṣavyāghra bhavēn mōkṣārthaniṡcitaḥ /15.64/  

> atrāpi sa vijānāti puruṣaṃ brahmavartinam  
> ¹nārāyaṇaparō ²mōkṣē tatō vai sāttvikaḥ smṛtaḥ /15.65/  

¹ all; M7 "nārāyaṇaṃ paraṃ"  
² M T1 G6; CE "mōkṣas"

> manīṣitaṃ ca prāpnōti cintayan puruṣōttamam  
> ēkānta¹bhaktaḥ satataṃ nārāyaṇaparāyaṇaḥ /15.66/  

¹ M T1 G1,3,6; CE "-bhaktiḥ"

> manīṣiṇō hi yē kē cid yatayō mōkṣakāṅkṣiṇaḥ  
> tēṣāṃ vai chinna¹tṛṣṇānāṃ yōgakṣēmavahō hariḥ /15.67/  

¹ all; M1,7 "-tṛṣṇāyāṃ"

> jāyamānaṃ hi puruṣaṃ ¹yaṃ paṡyēn madhusūdanaḥ  
> sāttvikas tu sa vijn̄ēyō bhavēn mōkṣē ca niṡcitaḥ /15.68/  

¹ all; M1,7 "yat paṡyē"

> sāṅkhya¹yōgēna tulyō hi dharma ²ēkāntisēvitaḥ  
> nārāyaṇātmakē mōkṣē tatō yānti parāṃ gatim /15.69/  

¹ ?M5 B Dn CE; M6,7 "-yōgē ca"; G1 "-yōgair na"; T1 ,6 "-tulyē ca yōgoe ca";
  PPS "-tulyō hi bhūpāla"  
² M Da; et al. "ēkānta-"

> nārāyaṇēna dṛṣṭaṡ ca pratibuddhō bhavēt pumān  
> ēvam ātmēcchayā rājan pratibuddhō na jāyatē /15.70/  

> rājasī tāmasī caiva vyāmiṡrē prakṛtī smṛtē  
> tadātmakaṃ hi puruṣaṃ jāyamānaṃ viṡāṃ patē  
> pravṛttilakṣaṇair yuktaṃ nāvēkṣati hariḥ svayam /15.71/  

> paṡyaty ¹ēnaṃ jāyamānaṃ brahmā ²lōkapitāmahaḥ  
> rajasā tamasā caiva ³mānavaṃ samabhiplutam /15.72/  

¹ all; M5 "-ēvaṃ"  
² ?M5,6 B Dn CE; M1,7 T1 G1,3,6 "rudrō’tha vā punaḥ"  
³ ?M5 G6; M1,6,7 Dn "mānasaṃ"; CE "mānuṣaṃ"

> kāmaṃ dēvāṡ ¹ca rṣayaṡ ca sattvasthā nṛpasattama  
> hīnāḥ sattvēna sūkṣmēṇa tatō vaikārikāḥ smṛtāḥ /15.73/  

¹ ?M5; CE "ca ṛṣayaḥ"; M1,6,7 Dn "ṛṣayaṡ ca"; T1 G1,3,6 PPS "ca munayaḥ"

> kathaṃ vaikārikō gacchēt puruṣaḥ puruṣōttamam /15.74/  

Before /15.74/ CE PPS in. "janamējaya uvāca" but M T1 G1,3,6 om.

> susūkṣmasattva¹saṃyuktaḥ ²saṃyuktas tribhir akṣaraiḥ  
> puruṣaḥ puruṣaṃ gacchēn ³niṣkriyaṃ pan̄caviṃṡakam /15.75/  

¹ †M1,7 T1 G1,3,6; M5 B Dn CE "saṃyuktaṃ"; M6 "� � �"  
² M T1 G1,3,6; CE PPS "saṃyuktaṃ"  
³ M T1 G1,3,6; CE PPS "niṣkriyaḥ"

Before /15.75/ CE PPS in. "vaiṡampāyana uvāca" but M T1 G1,3,6 om.

> ēvam ēkaṃ sāṅkhyayōgaṃ vēdāraṇyakam ēva ca  
> parasparāṅgāny ētāni pan̄carātraṃ ca kathyatē  
> ēṣa ēkāntināṃ dharmō nārāyaṇaparātmakaḥ /15.76/  

> yathā samudrāt prasṛtā jalaughās  
>  tam ēva rājan punar āviṡanti  
> imē tathā jn̄ānamahājalaughā  
>  nārāyaṇaṃ vai punar āviṡanti /15.77/  

> ēṣa tē kathitō dharmaḥ sātvatō yadubāndhava  
> kuruṣvainaṃ yathānyāyaṃ yadi ¹ṡaknōṣi bhārata /15.78/  

¹ ?M7 T1 G3,6 CE; M1,5,6 "ṡaktōṣi"; G1 Dn "ṡaktō’si"; PPS "ṡaktō hi"

> ēvaṃ hi sumahābhāgō nāradō guravē mama  
> ṡvētānāṃ yatinām āha ¹ēkāntagatim avyayām /15.79/  

¹ M5 G1,3 CE PPS; M1,6,7 T1 G6 "ēkānti-"

> vyāsaṡ cākathayat prītyā dharmaputrāya dhīmatē  
> sa ēvāyaṃ mayā tubhyam ākhyātaḥ ¹praṡiṣā gurōḥ /15.80/  

¹ M T1 G3,6; CE PPS "prasṛtō"

> itthaṃ hi duṡcarō dharma ēṣa pārthivasattama  
> yathaiva tvaṃ tathaivānyē ¹na bhajantīha mōhitāḥ /15.81/  

¹ †M1,6,7 PPS; M5 "na bhavantīha mōhitāḥ"; T G CE "na bhajanti vimōhitāḥ";
  B0,8 Dn "bhavantīha mōhitāḥ"  

Seems M5 is corrupted by Dn's reading of "bhavanti" instead of "bhajanti".
M1,6,7 T G CE all give same meaning (mōhitāḥ = vimōhitāḥ anyway)

> kṛṣṇa ēva hi lōkānāṃ bhāvanō mōhanas tathā  
> saṃhārakārakaṡcaiva kāraṇaṃ ca viṡāṃ patē /15.82/  

Thus ends chapter M1,6,7 171; G1 174; T1 G6 175; G3 176; T2 � � �; G2 � � �.

It has 82 verses, 176 lines;
    CE 82 verses, 176 lines;
    GP 88 verses, 177 lines;
    Ku 88 verses, ??? lines.

Chapter name:

M      "ēkāntibhāvaḥ"
