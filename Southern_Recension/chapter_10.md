# Chapter 10

>    ¹janamējaya uvāca  
> brahman sumahad ākhyānaṃ bhavatā parikīrtitam  
> yac ²chrutvēmē munivarāḥ paraṃ vismayam āgatāḥ /10.1/  

¹ M CE; T G B Dn "ṡaunaka"  
² M; CE "chrutvā munayaḥ sarvē vismayaṃ paramaṃ gatāḥ"

After this, B D T G (27 mss.) in. CE@App-I,32 but M CE om.

> idaṃ ṡatasahasrād dhi bhāratākhyānavistarāt  
> ¹āmathya matimanthēna jn̄ānōdadhim anuttamam /10.2/  

¹ all; M1,7 "āmathyas iti manthēna"

> navanītaṃ yathā dadhnō ⁰malayāc candanaṃ yathā  
> āraṇyakaṃ ca vēdēbhya ōṣadhibhyō’mṛtaṃ yathā /10.3/  

> ¹tathā samuddhṛtam idaṃ kathāmṛtam anuttamam  
> tapōnidhē ²tvayēhōktaṃ nārāyaṇakathāṡrayam /10.4/  

¹ M; T G CE "samuddhṛtam idaṃ brahman"  
² M T G; CE "tvayōktaṃ hi"

> sa hīṡō bhagavān dēvaḥ sarva¹bhūtavarapradaḥ  
> ahō nārāyaṇaṃ tējō durdarṡaṃ dvijasattama /10.5/  

¹ M T G; CE "-bhūtātmabhāvanaḥ"

> ¹yamāviṡanti kalpāntē sarvē brahmādayaḥ surāḥ  
> ṛṣayaṡ ca sagandharvā yac ca kin̄cic carācaram  
> na tatō’sti paraṃ manyē pāvanaṃ divi cēha ca /10.6/  

¹ M T G; CE "yatrāvi-"

> sarvāṡramābhigamanaṃ sarvatīrthāvagāhanam  
> na tathā phaladaṃ ¹prōktaṃ nārāyaṇakathā yathā /10.7/  

¹ M T G; CE "cāpi"

> ¹sarvathā pāvitāḥ smēha ṡrutvēmām āditaḥ kathām  
> harēr viṡvēṡvarasyēha sarvapāpapraṇāṡanīm /10.8/  

¹ all; M1,6,7 "sarvadhā"

> na citraṃ kṛtavāṃs tatra yad āryō mē dhanan̄jayaḥ  
> ¹kēṡavēnābhisaṅguptaḥ ²prāptavānāhavē jayam /10.9/  

¹ M; T G B Dn CE "vāsudēvasahāyō yaḥ"  
² M T G; CE "prāptavān̄ jayam uttamam"

> na cāsya kin̄cid aprāpyaṃ manyē lōkēṣv api triṣu  
> trailōkyanāthō viṣṇuḥ sa yasyāsīt sāhyakṛt sakhā /10.10/  

> dhanyāṡ ca sarva ēvāsan brahmaṃs tē mama pūrvakāḥ  
> hitāya ṡrēyasē caiva yēṣām āsīj janārdanaḥ /10.11/  

> ¹tapasāpy asudarṡō hi bhagavām̐l lōkapūjitaḥ  
> yaṃ dṛṣṭavantas tē sākṣāc chrīvatsāṅkavibhūṣaṇam /10.12/  

¹ ?M5; CE "tapasāpi na dṛṡyō hi"; M1,6,7 "tapasā vāpy asadṛṡyō";
  T G "tapasā’pyuta durdarṡō"; Dn "tapasātha sudṛṡyō hi";
  (31 mss. different reading than CE)  

As per MW "sudarṡa" means "easily seen", so "asudarṡa" means "not easily seen".
The meaning matches that of M T G CE. But Dn says the opposite!

> tēbhyō dhanyataraṡ caiva nāradaḥ paramēṣṭhijaḥ  
> na cālpatējasam ṛṣiṃ ¹taṃ cāvaimyadya nāradam  
> ṡvētadvīpaṃ samāsādya yēna dṛṣṭaḥ svayaṃ hariḥ /10.13/  

¹ M T G1,3,6; G2 PPS "xxx"; CE "vēdmi nāradam avyayam"

> dēvaprasādānugataṃ vyaktaṃ tat tasya darṡanam  
> yad dṛṣṭavāṃs tadā dēvam aniruddhatanau sthitam /10.14/  

> badarīm āṡramaṃ yat tu nāradaḥ prādravat punaḥ  
> naranārāyaṇau draṣṭuṃ kiṃ nu tatkāraṇaṃ munē /10.15/  

> ṡvētadvīpān nivṛttaṡ ca nāradaḥ paramēṣṭhijaḥ  
> badarīm āṡramaṃ prāpya samāgamya ca tāv ṛṣī /10.16/  

> kiyantaṃ kālam avasat kāḥ kathāḥ pṛṣṭavāṃṡ ca saḥ  
> ṡvētadvīpād upāvṛttē tasmin vā sumahātmani /10.17/  

> kim abrūtāṃ mahātmānau naranārāyaṇāv ṛṣī  
> tad ētan mē yathātattvaṃ sarvam ākhyātum arhasi /10.18/  

>    vaiṡampāyana uvāca  
> namō bhagavatē tasmai vyāsāyāmitatējasē  
> yasya prasādād vakṣyāmi nārāyaṇakathām imām /10.19/  

> prāpya ṡvētaṃ mahādvīpaṃ dṛṣṭvā ca harim avyayam  
> nivṛttō nāradō rājaṃs tarasā mērum āgamat  
> hṛdayēnōdvahan bhāraṃ yad uktaṃ paramātmanā /10.20/  

> paṡcād asyābhavad rājann ātmanaḥ sādhvasaṃ mahat  
> yad gatvā dūram adhvānaṃ kṣēmī punar ihāgataḥ /10.21/  

> ¹mērōḥ pracakrāma tataḥ parvataṃ gandhamādanam  
> nipapāta ca khāt tūrṇaṃ viṡālāṃ badarīm anu /10.22/  

¹ M B Dn; CE "tatō mērōḥ pracakrāma" (swap)

> tataḥ sa dadṛṡē dēvau purāṇāv ṛṣisattamau  
> tapaṡ carantau sumahad ¹ātmaniṣṭaṃ mahāvratau /10.23/  

¹ M; et al. "ātmaniṣṭhau"

> tējasābhyadhikau sūryāt sarvalōkavirōcanāt  
> ṡrīvatsalakṣaṇau pūjyau jaṭāmaṇḍaladhāriṇau /10.24/  

> jālapādabhujau tau tu pādayōṡ cakralakṣaṇau  
> vyūḍhōraskau dīrghabhujau tathā muṣkacatuṣkiṇau /10.25/  

> ṣaṣṭidantāv aṣṭadaṃṣṭrau mēghaughasadṛṡasvanau  
> svāsyau pṛthulalāṭau ca ¹subāhū subhrunāsikau /10.26/  

¹ M; T G CE "suhanū" (hanu = jaw, chin)

> ātapatrēṇa sadṛṡē ṡirasī dēvayōs tayōḥ  
> ēvaṃ lakṣaṇasampannau mahāpuruṣa¹san̄jn̄itau /10.27/  

¹ all; M1,7 "-saṃṡritau"

> tau dṛṣṭvā nāradō hṛṣṭas tābhyāṃ ca pratipūjitaḥ  
> svāgatēn¹ābhibhāṣyaiva pṛṣṭaṡ cānāmayaṃ tadā /10.28/  

¹ M; CE "-ābhibhāṣyātha"; T G "-na tu bhāṣyaiva"

> babhūvāntargatamatir nirīkṣya puruṣōttamau  
> ¹samātagau tatra ²mayā sarvabhūtanamas³kṛtau /10.29/  

¹ M T G; CE "sadōgatās"  
² M; CE "yē vai"  
³ M T G; CE "-kṛtāḥ"

> ṡvētadvīpē ¹mayā ²dṛṣṭau tādṛṡāv ³iha sattamau  
> iti san̄cintya manasā kṛtvā ⁴cāpipradakṣiṇam  
> upōpaviviṡē tatra pīṭhē kuṡamayē ⁰ṡubhē /10.30/  

¹ all; M5 "yathā"  
² M1,6,7 T G; M5 B Dn CE "dṛṣṭās"  
³ M G1; CE PPS "ṛṣisattamau"  
⁴ M T G2; CE "cābhipra-"

> tatas tau tapasāṃ vāsau yaṡasāṃ tējasām api  
> ṛṣī ṡamadamōpētau kṛtvā ¹caivāhnikam vidhim /10.31/  

¹ M; CE "pūrvāhṇikaṃ"; T1 G3,6 "pūrvāhnikaṃ"; G1 PPS "paurvāhṇikaṃ";
  (31 mss. different reading than CE)  

> paṡcān nāradam avyagrau pādyārghyābhyām ¹athārcatām  
> ²viviṡātē pīṭhakayōḥ kṛtātithyāhnikau nṛpa /10.32/  

¹ M B; T G "samarcya tam"; CE "prapūjya ca";
  (31 mss. different reading than CE)  
² M T G; CE "pīṭhayōṡ cōpaviṣṭau tau"

> tēṣu tatrōpaviṣṭēṣu sa dēṡō’bhi¹vyarājata  
> ājyāhutimahājvālair yajn̄a²vāṭa ivāgnibhiḥ /10.33/  

¹ ‡all; M5 "-vyarājata"  
² M1,6,7 T G; M5 B Dn "-yathāgnibhiḥ"; CE "vāṭō’gnibhir yathā"

> atha nārāyaṇas tatra nāradaṃ vākyam abravīt  
> sukhōpaviṣṭaṃ viṡrāntaṃ kṛtātithyaṃ sukhasthitam /10.34/  

> apīdānīṃ sa ⁰bhagavān paramātmā sanātanaḥ  
> ṡvētadvīpē tvayā dṛṣṭa āvayōḥ prakṛtiḥ ⁰parā /10.35/  

>    nārada uvāca  
> dṛṣṭō mē puruṣaḥ ṡrīmān viṡvarūpadharō’vyayaḥ  
> ¹sarvē hi lōkās tatrasthās tathā dēvāḥ saharṣibhiḥ  
> adyāpi cainaṃ paṡyāmi yuvāṃ paṡyan sanātanau /10.36/  

¹ all; M1,6,7 "sarvē’pi"

> yair lakṣaṇair upētaḥ sa harir avyaktarūpadhṛk  
> tair lakṣaṇair upētau hi vyaktarūpadharau yuvām /10.37/  

> dṛṣṭau mayā yuvāṃ tatra tasya dēvasya pārṡvataḥ  
> iha caivāgatō’smy adya visṛṣṭaḥ paramātmanā /10.38/  

> kō hi nāma bhavēt tasya ¹tapasā yaṡasā ṡriyā  
> sadṛṡas triṣu lōkēṣu ṛtē dharmātmajau yuvām /10.39/  

¹ M T G; CE "tējasā"

> tēna mē ¹kathitaḥ kṛtsnō ²dharmaḥ kṣētrajn̄asan̄jn̄i³taḥ  
> prādurbhāvāṡ ca kathitā bhaviṣ⁴yantīha yē yathā /10.40/  

¹ M B Dn; T G CE "kathitaṃ pūrvaṃ"  
² M B Dn; T2 "yāma"; G1,3,6 "dhāma"; G2 "yan mē"; T1 CE "nāma"  
³ M B Dn; T G CE "-tam"  
⁴ M T1 G3,6; B Dn "-yā iha yē"; CE "-yanti hi yē";
  G2 "tēna pūrvē bhavē yathā"; PPS "yasmin yasmin bhavē yathā" (sic)  

> tatra yē puruṣāḥ ṡvētāḥ pan̄cēndriyavivarjitāḥ  
> pratibuddhāṡ ca tē sarvē bhaktāṡ ca puruṣōttamam /10.41/  

> tē’rcayanti sadā dēvaṃ taiḥ sārdhaṃ ramatē ca saḥ  
> priyabhaktō hi bhagavān paramātmā dvijapriyaḥ /10.42/  

> ramatē ¹yō’rcyamānō hi sadā bhāgavatapriyaḥ  
> viṡvabhuk sarvagō dēvō bāndhavō bhaktavatsalaḥ  
> sa kartā kāraṇaṃ caiva kāryaṃ cātibaladyutiḥ /10.43/  

¹ M; et al. "sō’-"

> tapasā yōjya ¹cātmānaṃ ṡvētadvīpāt paraṃ hi yat  
> tēja ity abhivikhyātaṃ svayambhāsāvabhāsitam /10.44/  

¹ M1,6,7 T G; M5 B Dn CE "sō’’tmānaṃ"

> ṡāntiḥ sā triṣu lōkēṣu ¹vihitā bhāvitātmanām  
> ⁰ētayā ṡubhayā buddhyā naiṣṭhikaṃ vratam āsthitaḥ /10.45/  

¹ M; T "viddhānāṃ"; G2 "ṡuddhānāṃ"; G1,3,6 B Dn CE "siddhānāṃ"

> na tatra sūryas tapati na sōmō’bhivirājatē  
> na ¹vāti vāyur dēvēṡē tapaṡ carati duṡcaram /10.46/  

¹ M T G; CE "vāyur vāti"

> vēdīm aṣṭatalōtsēdhāṃ ¹bhaumām āsthāya viṡvabhuk  
> ēkapādasthitō dēva ūrdhvabāhur udaṅmukhaḥ  
> sāṅgān āvartayan vēdāṃs tapas ²tēpē suduṡcaram /10.47/  

¹ M; CE "bhūmāv"  
² ?M5 B Dn CE; M1,6,7 "tēpē’tiduṡcaram"; T G1,3,6 "tēpē’tiduṣkaram";
  G2 "tēpē’tidāruṇam"  

> ¹brahmā yad ṛṣayaṡ caiva svayaṃ paṡupatiṡ ca yat  
> ṡēṣāṡ ca vibudhaṡrēṣṭhā daityadānavarākṣasāḥ /10.48/  

¹ M T2; CE "yad brahmā" (swap); T1 G3,6 "brahmātha"; G1,2 "brahmā ca"

> nāgāḥ suparṇā gandharvāḥ siddhā rājarṣayaṡ ca ¹yat  
> havyaṃ kavyaṃ ca satataṃ vidhi²yuktaṃ prayun̄jatē  
> kṛtsnaṃ tat tasya dēvasya caraṇāv upatiṣṭhati /10.49/  

¹ M B8; T1 G3,6 "vai"; T2 G1,2 CE PPS "yē"  
² M B Dn; T G CE "-pūrvaṃ"

> yāḥ kriyāḥ samprayuktās tu ēkāntagatabuddhibhiḥ  
> tāḥ sarvāḥ ṡirasā dēvaḥ pratigṛhṇāti vai svayam /10.50/  

> na tasyānyaḥ priyataraḥ pratibuddhair mahātmabhiḥ  
> vidyatē triṣu lōkēṣu tatō’smy ¹ēkāntatāṃ gataḥ  
> iha caivā²gatō’smy adya visṛṣṭaḥ paramātmanā /10.51/  

¹ M; CE "aikāntikaṃ"; T2 "xxx"; G2 "xxx"; G1,3,6 "xxx"  
² M T G; CE "-gatas tēna"

> ēvaṃ mē bhagavān dēvaḥ svayam ākhyātavān hariḥ  
> āsiṣyē tatparō bhūtvā yuvābhyāṃ saha nityaṡaḥ /10.52/  

Thus ends chapter M6 160; M1,7 166; G1 169; T1 G6 170; G3 167; T2 183; G2 199; M5 201.

It has 52 verses, 113 lines;
    CE 52 verses, 113 lines;
    GP 63 verses, 133 lines;
    Ku 67 verses, 139 lines.

Chapter name:

M1,6,7 "nāradāgamanaṃ"  
T1 G6  "nārada-nārāyaṇa-saṃvādaḥ"
