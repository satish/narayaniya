# Chapter 07

>    ¹janamējaya uvāca  
> kathaṃ sa bhagavān dēvō yajn̄ēṣv agraharaḥ prabhuḥ  
> yajn̄adhārī ca satataṃ vēdavēdāṅga²dhṛk ³tathā /7.1/  

¹ M CE; et al. "ṡaunaka"  
² ?M5,6; M1,7 "-sṛk"; CE "-vit"; T G "-kṛt"  
² †all; M5 "katham"

All 28 mss. except M in. "ṡaunaka" instead of "janamējaya".

T G reading "vēdavēdāṅgakṛt" is not suitable, because no one is the author of the Vedas, but here it
claims Him to be the creator of Veda and Vedanga.

> nivṛttaṃ cāsthitō dharmaṃ ¹kṣēmī ²bhāgavatapriyaḥ  
> pravṛttidharmān vidadhē sa ēva bhagavān prabhuḥ /7.2/  

¹ ?M5 B CE; M1,6 "kṣēmaṃ"; M7 "kṣamam"; Dn "kṣamī";
  T G1,3,6 "kṣētrī"; G2 PPS "mōkṣaṃ"  
² ?M5 CE; M1,6,7 B Dn PPS "bhāgavataḥ prabhuḥ";
  T G "bhāgavataḥ katham"  

> kathaṃ pravṛttidharmēṣu bhāgārhā dēvatāḥ kṛtāḥ  
> kathaṃ nivṛtti¹dharmāṡ ca kṛtā vyāvṛttabuddhayaḥ /7.3/  

¹ M B Dn CE; T G "-dharmēṣu"

> ētaṃ naḥ saṃṡayaṃ ¹vipra chindhi guhyaṃ sanātanam  
> tvayā nārāyaṇa²kathāḥ ṡrutā vai dharma²saṃhitāḥ /7.4/  

¹ M CE; B Dn "sautē chindhi"; T G "chindhi sautē"  
² M; et al. "-kathā"  
³ M T2 G1,2 B6,8 Da Dn; T1 G3,6 CE "-samhitā"

M's reading makes sense because both ² and ³ end in visarga.

After this, 28 mss. except M CE, in. CE@861 (4 lines) starting "sūta uvāca".  
The same 28 mss. switch the speaker back to "janamējaya uvāca" before /7.5/.

> imē sabrahmakā lōkāḥ sasurāsuramānavāḥ  
> kriyāsv abhyudayōktāsu saktā dṛṡyanti sarvaṡaḥ  
> mōkṣaṡ cōktas tvayā brahman nirvāṇaṃ paramaṃ sukham /7.5/  

> yē ca ¹muktā bhavantīha puṇyapāpavivarjitāḥ  
> tē sahasrārciṣaṃ dēvaṃ praviṡantīti ṡuṡrumaḥ /7.6/  

¹ all; M1,6,7 "bhaktā"

"sahasrārciṣaṃ" = Arcirādi-mārga (of 1000 suns)

> ahō hi duranuṣṭhēyō mōkṣadharmaḥ sanātanaḥ  
> yaṃ hitvā dēvatāḥ sarvā havyakavyabhujō’bhavan /7.7/  

> kiṃ nu brahmā ca rudraṡ ca ṡakraṡ ca ⁰balabhit prabhuḥ  
> sūryas tārādhipō vāyur agnir varuṇa ēva ca  
> ākāṡaṃ jagatī caiva yē ca ṡēṣā divaukasaḥ /7.8/  

> praḻayaṃ na vijānanti ātmanaḥ parinirmitam  
> ¹atas tam vāsthitā dharmaṃ dhruvam akṣayam avyayam /7.9/  

¹ M; T G "yatas-"; B Dn CE "tatas tē nāsthitā mārgaṃ"

CE reads "therfore, they have not followed the path which is certain,
indestructible and immutable."

T G read "from whom him or situated on dharma which is "

M reads "they do not understand the annihilation that is encompassed
by one's own efforts, henceforth, nor, the dharma that is situated in
Him which is certain, indestructible and immutable."

> ¹smṛtakālaparīmāṇaṃ ²pravṛttā ³yē ⁴samanvitāḥ  
> ⁵dōṣakālaparīmāṇō mahān ⁶ēṣa kriyāvatām /7.10/  

¹ †M1,6,7; M5 D4,9 "smṛtaṃ"; Dn "smṛtikā-"; B "smṛtiṃ kā-"; T G CE "smṛtvā kā-"  
² †M1,6,7; M5 T G CE "pravṛttiṃ"  
³ all; T G "tē"  
⁴ †M1,7; M5 Da D4,9 "samāṡritāḥ"; M6 T G CE "samāsthitāḥ"  
⁵ †M1,7; M5,6 B Da "dōṣaḥ kā-"; T G "ēṣa kā-" Dn CE "dōṣaḥ kālaparimāṇē"  
⁶ †M1,6,7 B Dn CE; M5 T G "dōṣaḥ"

In the above verse, the reading of M1,6,7 is followed because the reading of M5
doesn't align consitently with either B, Dn, TG or CE.

> ētan mē saṃṡayaṃ vipra hṛdi ṡalyam ivārpitam  
> chindhītihāsakathanāt paraṃ kautūhalaṃ hi mē /7.11/  

> kathaṃ bhāgaharāḥ prōktā dēvatāḥ kratuṣu dvija  
> kimarthaṃ cādhvarē brahmann ¹yajantē tridivaukasaḥ /7.12/  

¹ M5,6 T G; M1,7 "yajatē" (sic); B Dn CE "ijyantē"

> yē ca bhāgaṃ ⁰pragṛhṇanti yajn̄ēṣu dvijasattama  
> tē yajantō mahāyajn̄aiḥ kasya bhāgaṃ dadanti vai /7.13/  

>    vaiṡampāyana uvāca  
> ahō gūḍhatamaḥ praṡnas tvayā pṛṣṭō janēṡvara  
> nātaptatapasā hy ēṣa nāvēdaviduṣā tathā  
> nāpurāṇavidā cāpi ¹ṡakyaṃ vyāhartum an̄jasā /7.14/  

¹ M; et al. "ṡakyō"

> hanta tē kathayiṣyāmi yan mē pṛṣṭaḥ purā guruḥ  
> kṛṣṇadvaipāyanō vyāsō vēdavyāsō mahān ṛṣiḥ /7.15/  

> sumantur jaiminiṡ caiva pailaṡ ca sudṛḍhavrataḥ  
> ahaṃ caturthaḥ ṡiṣyō vai pan̄camaṡ ca ṡukaḥ ¹smṛtaḥ /7.16/  

¹ all; M1,6,7 "tathā"

> ētān samāgatān ¹sarvān pan̄ca ṡiṣyān damānvitān  
> ²ṡaucācārasamāyuktān̄ jitakrōdhān̄ jitēndriyān /7.17/  

¹ M5 swaps "ṡiṣyān" and "sarvān"; et al. don't  
² all; M1,6,7 "pan̄cācāra-"

> vēdān adhyāpayām āsa mahābhāratapan̄camān  
> mērau girivarē ramyē siddhacāraṇasēvitē /7.18/  

> tēṣām ¹abhyasyatāṃ vēdān kadācit saṃṡayō’bhavat  
> ēṣa ²vai yas tvayā pṛṣṭas tēna tēṣāṃ prakīrtitaḥ  
> tataḥ ṡrutō mayā cāpi ³yaugapadyēna bhārata /7.19/  

¹ ‡T1 G2,3,6 B Dn CE PPS; T2 G1 M5 "abhyasatāṃ"; M1,6,7 "abhyasatō";
  "abhyasyatāṃ" is the correct grammatical form.  
² all; M1,6,7 "varyas"  
³ ?M5,6; M1,7 "yōga-"; et al. "tavākhyēyō’dya"

As per MW, "yauga" is one who follows "yoga".
"yogapadyēna" or "yaugapadyēna" means "by the footsteps of Yoga".

> ṡiṣyāṇāṃ vacanaṃ ṡrutvā sarvājn̄ānatamōnudaḥ  
> parāṡarasutaḥ ṡrīmān vyāsō vākyam uvāca ha /7.20/  

> mayā hi sumahat taptaṃ tapaḥ paramadāruṇam  
> bhūtaṃ bhavyaṃ bhaviṣyac ca jānīyām iti sattamāḥ /7.21/  

Before /7.21/ B8,9 in. "vyāsa uvāca" but others om.

> tasya mē taptatapasō nigṛhītēndriyasya ca  
> nārāyaṇaprasādēna ¹kṣīrōdasyānukūlataḥ /7.22/  

¹ all; M1,6,7 "nāradasyānu-"

> traikālikam idaṃ jn̄ānaṃ prādurbhūtaṃ yathēpsitam  
> tac chṛṇudhvaṃ yathājn̄ānaṃ vakṣyē saṃṡayam uttamam  
> yathā vṛttaṃ hi kalpādau dṛṣṭaṃ mē jn̄ānacakṣuṣā /7.23/  

> paramātmēti yaṃ prāhuḥ sāṅkhyayōgavidō janāḥ  
> mahāpuruṣasan̄jn̄āṃ sa labhatē svēna karmaṇā /7.24/  

> tasmāt prasūtam avyaktaṃ pradhānaṃ ¹taṃ vidur budhāḥ  
> avyaktād vyaktam utpannaṃ lōkasṛṣṭyartham īṡvarāt /7.25/  

¹ M1,5 T2 B Dn K; CE "tad"

> aniruddhō hi lōkēṣu mahān ātmēti kathyatē  
> yō’sau vyaktatvam āpannō nirmamē ca pitāmaham  
> sō’haṅkāra iti prōktaḥ sarva¹tējōmayō hi saḥ /7.26/  

¹ M5 T G CE; M1,6,7 B "-tējōgatō"

> pṛthivī vāyur ākāṡam āpō jyōtiṡ ca pan̄camam  
> ¹ahaṅkāraprasūtāni ²pan̄cabhūtāni bhārata /7.27/  

¹ all; T G "ahaṅkārāt pra-"  
² M T2 G1,2 PPS; CE "mahā-"

> mahābhūtāni sṛṣṭvātha tadguṇān nirmamē punaḥ  
> bhūtēbhyaṡ caiva ¹niṣpannān ²aṣṭau mūrtimataḥ ṡṛṇu /7.28/  

¹ M T G; et al. "niṣpannān"  
² M T G; et al. "mūrtimantō’ṣṭa tān̄"

> marīcir aṅgirāṡ cātriḥ pulastyaḥ pulahaḥ kratuḥ  
> vasiṣṭhaṡ ca mahātmā vai manuḥ svāyambhuvas tathā  
> jn̄ēyāḥ prakṛtayō’ṣṭau tā yāsu lōkāḥ pratiṣṭhitāḥ /7.29/  

> vēdān vēdāṅgasaṃyuktān yajn̄ān yajn̄āṅgasaṃyutān  
> nirmamē lōka¹sṛṣṭyarthaṃ brahmā lōkapitāmahaḥ  
> aṣṭābhyaḥ prakṛtibhyaṡ ca jātaṃ viṡvam idaṃ jagat /7.30/  

¹ M5 T G; M1,6,7 CE "-siddhyarthaṃ"

> rudrō rōṣātmakō jātō daṡānyān ¹asṛjat svayam  
> ēkādaṡaitē rudrās tu vikārāḥ puruṣāḥ smṛtāḥ /7.31/  

¹ M T G; "sō’sṛjat"

> tē rudrāḥ prakṛtiṡ caiva sarvē caiva surarṣayaḥ  
> utpannā lōkasiddhyarthaṃ brahmāṇaṃ samupasthitāḥ /7.32/  

> vayaṃ ¹sṛṣṭās tu bhagavaṃs tvayā vai prabhaviṣṇunā  
> yēna yasminn adhīkārē vartitavyaṃ pitāmaha /7.33/  

¹ M T G; CE "hi sṛṣṭā"

Before /7.33/ K6 Da PPS in. "rudrā ūcuḥ".

> yō’sau tvayā vinirdiṣṭō ¹adhikārē’rthacintakaḥ  
> paripālyaḥ kathaṃ tēna ²sāhaṅkārēṇa kartṛṇā /7.34/  

¹ M G1; Dn "hy adhikārō"; T G CE "adhikārō"  
² M T G B Dn; CE "sō’dhikārō’dhikāriṇā"

> pradiṡasva balaṃ tasya yō’dhi¹kārārthacintakaḥ  
> ēvam uktō mahādēvō dēvāṃs tān idam abravīt /7.35/  

¹ all; M1,6,7 "-kārērtha-"

> sādhv ahaṃ jn̄āpitō dēvā yuṣmābhir bhadram astu vaḥ  
> mamāpy ēṣā samutpannā ¹cintā yā bhavatām ²iha /7.36/  

¹ all; M1,6 "cintayā"  
² M T2 G1,3,6; CE "matā"

Before /7.36/ B Dn D K PPS in. "brahmōvāca".

> lōkatantrasya kṛtsnasya kathaṃ kāryaḥ parigrahaḥ  
> kathaṃ balakṣayō na syād yuṣmākaṃ hy ātmanaṡ ¹tathā /7.37/  

¹ M D4,9 K7; T G "ca vai"; CE "ca mē"

> itaḥ sarvē’pi gacchāmaḥ ṡaraṇaṃ lōkasākṣiṇam  
> mahāpuruṣam avyaktaṃ sa nō vakṣyati yad dhitam /7.38/  

> tatas tē brahmaṇā sārdham ṛṣayō vibudhās tathā  
> kṣīrōdasyōttaraṃ kūlaṃ jagmur lōka¹hitārthinaḥ /7.39/  

¹ all; M1,6,7 "-hitaiṣiṇaḥ"

Before /7.39/ B8,9 PPS in. "vyāsa uvāca"

> tē tapaḥ samupātiṣṭhan ¹brahmōktaṃ vēdakalpitam  
> sa mahāniyamō nāma tapaṡcaryā sudāruṇā /7.40/  

¹ all; M1,7 "brahmāt taṃ"

> ¹ūrdhvā dṛṣṭir bāhavaṡ ca ²ēkāgra manasō’bhavan  
> ēkapādasthitāḥ samyak kāṣṭhabhūtāḥ samāhitāḥ /7.41/  

¹ M T1 G3,6; CE "ūrdhvaṃ"  
² M5 T G; M1,6,7 CE "ēkāgraṃ ca manō’bhavat"

> divyaṃ varṣasahasraṃ tē tapas taptvā tad uttamam  
> ṡuṡruvur madhurāṃ vāṇīṃ vēdavēdāṅgabhūṣitām /7.42/  

>    ¹vāg uvāca  
> bhō ²bhō sabrahmakā dēvā ṛṣayaṡ ca tapōdhanāḥ  
> svāgatēn³ārcya vaḥ sarvān̄ ṡrāvayē vākyam uttamam /7.43/  

¹ M T G K D; B Da Dn "bhagavān uvāca"; CE om.  
² M5 T G B8,9; M1,6,7 CE "bhōḥ"  
³ M CE; T G "-ādya"

> vijn̄ātaṃ vō mayā kāryaṃ tac ca lōkahitaṃ mahat  
> pravṛttiyuktaṃ kartavyaṃ yuṣmatprāṇōpabṛṃhaṇam /7.44/  

> sutaptaṃ vas tapō dēvā mamārādhanakāmyayā  
> bhōkṣyathāsya mahāsattvās tapasaḥ phalam uttamam /7.45/  

> ēṣa brahmā lōkaguruḥ sarvalōkapitāmahaḥ  
> yūyaṃ ca vibudhaṡrēṣṭhā māṃ yajadhvaṃ samāhitāḥ /7.46/  

> sarvē bhāgān kalpayadhvaṃ yajn̄ēṣu mama nityaṡaḥ  
> ¹tathā ṡrēyō vidhāsyāmi yathādhīkāram īṡvarāḥ /7.47/  

¹ M CE; T G "tatra"

>    ¹vaiṡampāyana uvāca  
> ṡrutvaitad dēvadēvasya vākyaṃ hṛṣṭatanūruhāḥ  
> tatas tē vibudhāḥ sarvē brahmā tē ca maharṣayaḥ /7.48/  

¹ M B K Dn; Da PPS "vyāsa"

> ¹vēdadṛṣṭēna vidhinā vaiṣṇavaṃ kratum ²āvahan  
> tasmin satrē tadā brahmā svayaṃ bhāgam akalpayat  
> dēvā dēvarṣayaṡ caiva sarvē bhāgān akalpayan /7.49/  

¹ all; M1,6,7 "dēva-"  
² M; T G CE "āharan"

As per MW, "āvah" and "āhṛ" both have similar meanings ("to bring near", "to fetch, procure", "to
use", "to manifest, utter, speak", "to bring home a bride", etc.)

> tē kārtayugadharmāṇō bhāgāḥ paramasatkṛtāḥ  
> prāpur ādityavarṇaṃ taṃ puruṣaṃ tamasaḥ param  
> bṛhantaṃ sarvagaṃ dēvam īṡānaṃ varadaṃ prabhum /7.50/  

> tatō’tha varadō dēvas tān sarvān ¹amarān sthitān  
> aṡarīrō babhāṣēdaṃ vākyaṃ khasthō mahēṡvaraḥ /7.51/  

¹ †all; M5 "vibudhān"

> ¹yōyēna kalpitō bhāgaḥ sa tathā ²tvam upāgataḥ  
> prītō’haṃ pradiṡāmy adya phalam āvṛttilakṣaṇam /7.52/  

¹ M; T G CE "yēna yaḥ" (swap)  
² M; B Da "mām"; T G CE "samupā-"

Before /7.52/ B8 in. "ṡrībhagavān uvāca" but others om.

> ¹ēvaṃ vō lakṣaṇaṃ dēvā matprasādasamudbhavam  
> yūyaṃ yajn̄air ²yajamānāḥ samāptavaradakṣiṇaiḥ  
> yugē yugē bhaviṣyadhvaṃ pravṛttiphalabhōginaḥ /7.53/  

¹ M; CE "ētad vō"; T G "ētad vai"  
² M Dn D; T1 G3,6 "ījamānāḥ"; G2 "ijyamānaḥ"; T2 G1 CE PPS "ijyamānāḥ"

> yajn̄air yē cāpi yakṣyanti sarvalōkēṣu vai surāḥ  
> kalpayiṣyanti vō bhāgāṃs tē narā ¹dēvakalpitān /7.54/  

¹ M G2 PPS; et al. "vēda-"

> yō mē yathā kalpitavān bhāgam asmin mahākratau  
> sa tathā yajn̄abhāgārhō vēdasūtrē mayā kṛtaḥ /7.55/  

> yūyaṃ lōkān dhārayadhvaṃ ⁰yajn̄abhāgaphalōditāḥ  
> sarvārthacintakā lōkē ¹yathādhīkāranirmitāḥ /7.56/  

¹ M CE; T G "mayādhi-"

> yāḥ kriyāḥ pracariṣyanti pravṛttiphala¹satkṛtāḥ  
> tābhir āpyāyitabalā lōkān vai dhārayiṣyatha /7.57/  

¹ all; M1,6,7 "-satkriyāḥ"

> yūyaṃ hi bhāvitā lōkē sarvayajn̄ēṣu mānavaiḥ  
> māṃ tatō bhāvayiṣyadhvam ēṣā vō bhāvanā mama /7.58/  

> ityarthaṃ ⁰nirmitā vēdā yajn̄āṡ cauṣadhibhiḥ saha  
> ēbhiḥ samyak prayuktair hi prīyantē dēvatāḥ kṣitau /7.59/  

> nirmāṇam ētad yuṣmākaṃ pravṛtti¹rūpakalpitā  
> mayā kṛtaṃ suraṡrēṣṭhā yāvat kalpakṣayād iti  
> cintayadhvaṃ lōkahitaṃ yathādhīkāram īṡvarāḥ /7.60/  

¹ M; T G CE "-guṇakalpitam"

> marīcir aṅgirāḥ ¹atriḥ pulastyaḥ pulahaḥ kratuḥ  
> vasiṣṭha iti saptaitē mānasā nirmitā hi vai /7.61/  

¹ M5; M1,6,7 "atriṡ ca"; T G CE "cātriḥ"

> ētē vēdavidō mukhyā vēdācāryāṡ ca kalpitāḥ  
> pravṛttidharmiṇaṡ caiva ¹prājāpatyē ca kalpitāḥ /7.62/  

¹ M5 T2 G1,2 Dn PPS; T1 G3,6 M1,6,7 B CE "prājāpatyēna"

> ¹ēṣa kriyāvatāṃ panthā vyaktībhūtaḥ sanātanaḥ  
> aniruddha iti prōktō lōkasargakaraḥ prabhuḥ /7.63/  

¹ M; T G "ahaṃ"; CE "ayaṃ"

> ¹sanaḥ sanatsujātaṡ ca sanakaṡ ²ca sanandanaḥ  
> sanatkumāraḥ kapilaḥ saptamaṡ ca sanātanaḥ /7.64/  

¹ †M1,6,7 CE; M5 "sanāt" (sic); T G PPS "sanātanaḥ ṡalākaṡ ca"  
² †M1,6,7 T G; M5 CE "sasanan-"

Here too M1,6,7 provide a consistent reading. The seven mind-born ṛṣis are given
correctly by the M family. Instead of "Sanatsujāta", T G substitute "Ṡalāka" and
B substitutes "Sanīka" as the second ṛṣi.

Chinnajeeyar swami says "Sanatsujāta", the 4 Kumāras as the only ones
(not even Caturmukha-Brahma!) capable of having vision of Vyūha-Vāsudeva on
the Milky Ocean. https://youtu.be/5CMP7i4wKik?t=950

> saptaitē mānasāḥ prōktā ṛṣayō brahmaṇaḥ sutāḥ  
> svayamāgatavijn̄ānā nivṛttaṃ dharmam āsthitāḥ /7.65/  

> ētē yōgavidō mukhyāḥ sāṅkhyadharmavidas tathā  
> ¹ācāryā mōkṣaṡāstrē ca mōkṣadharmapravartakāḥ /7.66/  

¹ all; M1,6,7 "ācārya vai mōkṣaṡāstrē"

> yatō’haṃ prasṛtaḥ pūrvam avyaktāt ¹triguṇātmakāt  
> tasmāt paratarō yō’sau kṣētrajn̄a iti kalpitaḥ  
> ²sa hy akriyāvatāṃ panthāḥ punarāvṛttidurlabhaḥ /7.67/  

¹ M T G; CE "triguṇō mahān"  
² M T G; CE "sō’haṃ kri-"

PPS omits the exact same lines which G2 omits (67d-68c) but not by the rest of TGM.

> yō yathā nirmitō jantur yasmin yasmiṃṡ ca karmaṇi  
> pravṛttau vā nivṛttau vā tatphalaṃ sō’ṡnutē’vaṡaḥ /7.68/  

> ēṣa lōkagurur brahmā jagadādikaraḥ prabhuḥ  
> ēṣa mātā pitā caiva yuṣmākaṃ ca pitāmahaḥ  
> mayānuṡiṣṭō bhavitā sarvabhūtavarapradaḥ /7.69/  

> asya caivānujō rudrō lalāṭād yaḥ samutthitaḥ  
> brahmānuṡiṣṭō bhavitā sarva¹sattvavarapradaḥ /7.70/  

¹ M T G; CE "-trasavara-"

> gacchadhvaṃ svān adhīkārāṃṡ cintayadhvaṃ yathāvidhi  
> pravartantāṃ kriyāḥ sarvāḥ sarvalōkēṣu māciram /7.71/  

> ¹praviṡyantāṃ ca karmāṇi prāṇināṃ gatayas tathā  
> parinirmitakālāni ²āyūṃṣīha surōttamāḥ /7.72/  

¹ M; T G CE PPS "pradṛṡyantāṃ"; Dn "pradiṡyantāṃ"  
² M B Dn; T G CE "āyūṃṣi ca"

M's reading is more apt. "praviṡ" means
"to enter upon, undertake, commence, begin, devote one's self to"

> idaṃ kṛtayugaṃ nāma ¹kālaḥ ṡrēṣṭhaḥ pravartatē  
> ahiṃsyā ²yajn̄apaṡavō yugē’smin ³naitad anyathā  
> catuṣpāt sakalō dharmō bhaviṣyaty atra vai surāḥ /7.73/  

¹ ?M5,6 Dn CE; M1,7 T G B "kālaṡrēṣṭaḥ"  
² M CE; T G "yatra-"  
³ M CE PPS; T2 G1,2 "ētad"; T1 G3,6 "na tad"

Here you can see the confused reading of T G. Half of them say "this is otherwise"
(ētad anyathā) and the other half say "it is not otherwise" (na tad anyathā).

> tatas trētāyugaṃ nāma trayī yatra bhaviṣyati  
> prōkṣitā ¹yatra paṡavō vadhaṃ prāpsyanti vai makhē  
> tatra pādacaturthō vai dharmasya na bhaviṣyati /7.74/  

¹ all; M1,6,7 "yajn̄apa-"  

> tatō vai dvāparaṃ nāma miṡraḥ kālō bhaviṣyati  
> dvipādahīnō dharmaṡ ca yugē tasmin bhaviṣyati /7.75/  

T G in. CE@865 (1 line) here but M CE om.

> tatas tiṣyē’tha samprāptē yugē kalipuraskṛtē  
> ēkapādasthitō dharmō yatra ⁰tatra bhaviṣyati /7.76/  

>    ¹dēvāḥ ūcuḥ  
> ēkapādasthitē dharmē yatrakvacanagāmini  
> ²kiṃ nu ³kartavyam asmābhir bhagavaṃs tad vadasva naḥ /7.77/  

¹ ‡T G CE "dēvā ūcuḥ"; M1,6,7 "dēvarṣayaṡ ca"; B Dn M5 om.  
² M; et al. "kathaṃ"  
³ all; T G "vastavyam"

>    ṡrībhagavān uvāca  
> yatra vēdāṡ ca yajn̄āṡ ca tapaḥ satyaṃ damas tathā  
> hiṃsāṡ cādharmasaṃyuktāḥ pracarēyuḥ surōttamāḥ  
> sa vai dēṡaḥ ²hi vaḥ sēvyō mā vō’dharmaḥ ³padā spṛṡēt /7.78/  

¹ M T1 G3,6; T2 G1,2 B Dn CE PPS "ahiṃsādharma"  
² ?M1,5,7; T G PPS "hi vastavyō"; M6 B Dn CE "sēvitavyō"  
³ all; M1,6 "parā"

Once again, the M family gives a superior reading. How so?

CE: "That place should be dwelt in, where the Vedas and yajn̄as and tapas and
also truth and self-restraint, where the best of gods wander endowed with the
law of non-injury (_ahmisādharma_). May adharma not even touch your foot!".

- It's hard to see why "tathā" (thus, so, in that manner) is used, instead of
  say, "caiva" which is what is translated.
- The ambiguity of _ahmisādharma_ which can be split as the "dharma of ahiṃsā"
  or "the adharma of ahiṃsā" (!!)
- Who are these "best of gods" who wander about? Are they same or different from
  those to whom the Lord is speaking?
- If they are asked to live in the land of dharma, why does Lord wish that
  adharma not touch their foot?

The M translation clears these doubts:

"O best of gods! (surōttamāḥ), wherever (yatra) Vedas and yajn̄as and tapas being
conjoined with (saṃyuktāḥ) truth and self-restraint, in that manner (tathā),
violence (hiṃsā) and (ca) adharma together (saṃyuktāḥ) would appear forth
(pracarēyuḥ > √pracur). Indeed (vai), that land (sa dēṡaḥ) certainly (hi) you
all (vas) shall dwell in (sēvyo); may adharma not touch your foot!"

Addressing the gods in the vocative, the Lord says that dharma and adharma would
co-exist in the same land, that's why he wishes them to stay away from adharma.

>    vyāsa uvāca  
> tē’nuṡiṣṭā bhagavatā dēvāḥ sarṣigaṇās tathā  
> namaskṛtvā bhagavatē jagmur ¹dēṡān yathēpsitān /7.79/  

¹ all; M1,6,7 "dēvān"

> gatēṣu tridivaukaḥsu brahmaikaḥ paryavasthitaḥ  
> didṛkṣur bhagavantaṃ tam aniruddhatanau sthitam /7.80/  

> taṃ dēvō darṡayām āsa kṛtvā hayaṡirō mahat  
> sāṅgān āvartayan vēdān kamaṇḍalu¹kadaṇḍadhṛk /7.81/  

¹ M; Dn "-tridaṇḍadhṛk"; G2 PPS "-guṇīkṛtam"; CE "-gaṇitradhṛk"

> tatō’ṡvaṡirasaṃ dṛṣṭvā taṃ dēvam amitaujasam  
> lōkakartā prabhur brahmā lōkānāṃ hitakāmyayā /7.82/  

> mūrdhnā praṇamya ¹varadaṃ tasthau prān̄jalir agrataḥ  
> ²sa pariṣvajya dēvēna vacanaṃ ṡrāvitas tadā /7.83/  

¹ all; M1,7 "pādantas"  
² all; M1,6,7 "sampa-"

> lōkakāryagatīḥ sarvās ¹carasva tvaṃ yathāvidhi  
> ²dhātā tvaṃ sarvabhūtānāṃ tvaṃ prabhur jagatō guruḥ  
> tvayy āvēṡitabhārō’haṃ dhṛtiṃ prāpsyāmy athān̄jasā /7.84/  

¹ ?M5; M1,6,7 "carēthās tvaṃ"; T G "carēha tvaṃ"; CE "tvaṃ cintaya"  
² M1,5,7 B Dn CE; T G PPS "pitā"; M6 "tāta"

Before /7.84/ Dn PPS in. "ṡrībhagavān uvāca".

> yadā ¹yat surakāryaṃ tē aviṣahyaṃ bhaviṣyati  
> prādurbhāvaṃ ²gamiṣyāmi tadātmajn̄āna³dēṡikam /7.85/  

¹ M; CE PPS "ca"  
² M CE; T G "kariṣyāmi"  
³ M; et al. "-dēṡikaḥ"

> ēvam uktvā hayaṡirās tatraivāntaradhīyata  
> tēnānuṡiṣṭō brahmāpi svaṃ lōkam acirād gataḥ /7.86/  

> ēvam ēṣa mahābhāgaḥ padmanābhaḥ sanātanaḥ  
> yajn̄ēṣv agraharaḥ prōktō yajn̄adhārī ca nityadā /7.87/  

> nivṛttiṃ cāsthitō dharmaṃ gatim ¹akṣaradharmiṇām  
> pravṛttidharmān vidadhē ²jn̄ātvā lōkasya citratām /7.88/  

¹ ?M1,5,6 K7 D4,9; et al. "akṣaya-"  
² ?M1,5,6 K7 D4,9; et al. "kṛtvā"

> sa ādiḥ sa madhyaḥ sa cāntaḥ prajānāṃ  
>  sa dhātā sa ¹dhēyaṃ sa kartā sa kāryam  
> yugāntē sa suptaḥ ²sa saṅkṣipya lōkān  
>  yugādau prabuddhō jagad ⁰dhy utsasarja /7.89/  

¹ M; et al. "dhēyaḥ"  
² M5,6 T G; CE PPS "susaṅ-"

He is the creator ("dhātṛ") and that what is created ("dhēya"). He creates His
own body (i.e., the universe) and enters it.

> tasmai namadhvaṃ dēvāya nirguṇāya guṇātmanē  
> ajāya viṡvarūpāya dhāmnē sarvadivaukasām /7.90/  

> mahābhūtādhipatayē rudrāṇāṃ patayē tathā  
> ādityapatayē caiva vasūnāṃ patayē tathā /7.91/  

> ¹aṡvinōḥ patayē caiva marutāṃ patayē tathā  
> vēdayajn̄ādhipatayē ²vēdāṅgapatayē ³tathā /7.92/  

¹ M T G; et al. "aṡvibhyāṃ"  
² all; M1,6,7 "dēvānām-"  
³ M5 T G; CE PPS "-’pi ca"

> samudravāsinē nityaṃ harayē mun̄jakēṡinē  
> ṡāntayē sarvabhūtānāṃ mōkṣadharmānubhāṣiṇē /7.93/  

> tapasāṃ tējasāṃ caiva patayē yaṡasō’pi ca  
> vācaṡ ca patayē nityaṃ saritāṃ patayē tathā /7.94/  

> kapardinē varāhāya ēkaṡṛṅgāya dhīmatē  
> vivasvatē’ṡvaṡirasē caturmūrtidhṛtē sadā /7.95/  

> ¹guhyāya jn̄ānadṛṡyāya ²akṣarāya kṣarāya ca  
> ēṣa dēvaḥ san̄carati sarvatragatir avyayaḥ /7.96/  

¹ all; T G "sūkṣmāya"  
² all; M5 "avyaktāyākṣayāya ca"

> ¹ēvam ētat purā dṛṣṭaṃ mayā vai jn̄ānacakṣuṣā  
> kathitaṃ tac ca vaḥ sarvaṃ mayā pṛṣṭēna tattvataḥ /7.97/  

¹ M1,5,6 repeat /7.96L1/ twice, the first time as  
  "ēṣa caitatparaṃ brahma jn̄ēyō vijn̄ānacakṣuṡā"  
  but the second time as given in the constitued text

> kriyatāṃ ¹tadvacō mē’dya sēvyatāṃ harir īṡvaraḥ  
> gīyatāṃ vēdaṡabdaiṡ ca pūjyatāṃ ca yathāvidhi /7.98/  

¹ M D4,9 K7; T G "tadvacaḥ ṡiṣyāḥ"; CE "madvacaḥ ṡiṣyāḥ"

>    vaiṡampāyana uvāca  
> vēdavyāsēna vyāsēna ēvam uktās tatō vayam  
> sarvē ṡiṣyāḥ sutaṡ cāsya ṡukaḥ paramadharmavit /7.99/  

¹ M T G B Da; CE "ity uktās tu vayaṃ tēna vēdavyāsēna dhīmatā"

> sa cāsmākam upādhyāyaḥ sahāsmābhir viṡāṃ patē  
> caturvēdōdgatābhis ¹tam ṛgbhis ²sam abhituṣṭuvē /7.100/  

¹ M T G B Da; CE "ca"  
² M T G B Da; CE "tam"

> ētat tē sarvam ākhyātaṃ yan ¹māṃ tvaṃ paripṛcchasi  
> ²ēvaṃ mē’kathayad rājan purā dvaipāyanō guruḥ /7.101/  

¹ ‡all; M "mā" (sic)  
² ‡all; M "ētad vai akatha-" (hypermetric?)

> yaṡ ¹cēdaṃ ṡṛṇuyān nityaṃ yaṡ ¹cēdaṃ parikīrtayēt    
> namō bhagavatē kṛtvā samāhitamanā naraḥ /7.102/  

¹ all; T G "cainaṃ"

> bhavaty arōgō dyutimān ¹rūpavarṇabalānvitaḥ  
> āturō mucyatē rōgād baddhō mucyēta bandhanāt /7.103/  

¹ M T G; CE "balarūpasamanvitaḥ"

> kāmakāmī labhēt ¹kāman dīrgham āyur avāpnuyāt  
> brāhmaṇaḥ sarvavēdī syāt kṣatriyō vijayī bhavēt  
> vaiṡyō vipulalābhaḥ syāc chūdraḥ sukham avāpnuyāt /7.104/  

¹ M5,6,7 T2 K1,2,4; M1 "kāmād"; CE PPS "kāmaṃ"

> aputrō labhatē putraṃ kanyā caivēpsitaṃ patim  
> lagnagarbhā vimucyēta garbhiṇī janayēt sutam  
> vandhyā prasavam āpnōti putrapautrasamṛddhimat /7.105/  

> kṣēmēṇa gacchēd adhvānam idaṃ yaḥ ¹paṭhatē pathi  
> yō yaṃ ²kāmayatē kāmaṃ sa tam āpnōti ca dhruvam /7.106/  

¹ M CE; T G "paṭhatē’dhvani"  
² M5 T G B; CE "kāmaṃ kāmayatē" (swap)

> idaṃ maharṣēr vacanaṃ ¹vipaṡcitō  
>   mahātmanaḥ puruṣavarasya kīrtanam  
> samāgamaṃ carṣidivaukasām imaṃ  
>   niṡamya ²bhaktō labhatē sukham mahat /7.107/  

¹ M T2 G1,2 PPS; T1 G3,6 "viniṡcitō"; CE "viniṡcitaṃ"  
² M5 T G; M1,6,7 CE "bhaktāḥ susukhaṃ labhantē"

Thus ends chapter M1,6,7 161; G1 165; T1 G6 166; G3 167; T2 181; G2 197; M5 197.

It has 107 verses, 234 lines;
    CE 107 verses, 234 lines;
   PPS 119 verses, 238 lines;
    Ku 117 verses, ??? lines.

Chapter name:

M1,6,7 "jn̄āna-grahaṇa-nimitta-kathanam"  
M5     "yajn̄āgraharaṇa-nimitta-kathanam"  
T1 G6  "ṛṣi-dēva-saṃvādaḥ"
