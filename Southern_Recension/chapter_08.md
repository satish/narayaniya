# Chapter 08

>    janamējaya uvāca  
> astauṣīd ¹vaidikō vyāsaḥ saṡiṣyō madhusūdanam  
> nāmabhir vividhair ēṣāṃ ²niruktaṃ bhagavan mama /8.1/  

¹ M; T G PPS "vaidikair"; CE "yair imaṃ"  
² M5 T G CE PPS; M1,6,7 "niruktō"

> vaktum arhasi ṡuṡrūṣōḥ prajāpatipatēr harēḥ  
> ṡrutvā bhavēyaṃ yat pūtaḥ ṡaraccandra ivāmalaḥ /8.2/  

>    vaiṡampāyana uvāca  
> ṡṛṇu rājan yathācaṣṭa phalgunasya harir ¹prabhuḥ  
> ²prahasannātmanō nāmnāṃ niruktaṃ guṇakarmajam /8.3/  

¹ M5 Dn PPS; M1,6,7 "svayam"; T G CE "vibhuḥ"  
² M; et al. "prasannātmā’’tmanō"

> nāmabhiḥ kīrtitair ¹asya kēṡavasya mahātmanaḥ  
> pṛṣṭavān kēṡavaṃ rājan ⁰phalgunaḥ paravīrahā /8.4/  

¹ M T2 G1,2 PPS; T1 G3,6 "tasya"; CE "tasya"

>    arjuna uvāca  
> bhagavan bhūtabhavyēṡa sarva¹bhūtasṛg avyaya  
> ²lōkanātha jagannātha lōkānām abhayaprada /8.5/  

¹ M5,6 T2 G1 Dn CE PPS; M1,7 "-avyayaḥ"; T1 G3,6 "-bhutabhavāvyayaḥ"  
² M G2 PPS; T G3,6 "lōkadhāman̄"; Dn CE "lōkadhāma"

> yāni nāmāni tē dēva kīrtitāni maharṣibhiḥ  
> vēdēṣu sapurāṇēṣu yāni guhyāni karmabhiḥ /8.6/  

> tēṣāṃ niruktaṃ tvattō’haṃ ṡrōtum icchāmi kēṡava  
> na hy anyō vartayēn nāmnāṃ niruktaṃ ¹tvām ṛtē prabhō /8.7/  

¹ M5 B Dn CE; G2,3,6 M1,6,7 PPS "tvad ṛtē"; T2 G1 "tatra tē"; T1 "tvatra tē"

>    ṡrībhagavān uvāca  
> ⁰ṛgvēdē sayajurvēdē tathaivātharvasāmasu  
> purāṇē ¹sōpaniṣadē tathaiva jyōtiṣē’rjuna /8.8/  

¹ M5 B Dn CE; T G M1,6,7 PPS "-ṣadi"

> sāṅkhyē ca yōgaṡāstrē ca āyurvēdē tathaiva ca  
> bahūni mama nāmāni kīrtitāni maharṣibhiḥ /8.9/  

> gauṇāni tatra nāmāni karmajāni ¹tvaṃ kānicit  
> niruktaṃ karmajānāṃ ca ṡṛṇuṣva prayatō’nagha /8.10/  

¹ M B Dn; T G CE PPS "ca"

> kathyamānaṃ mayā ¹tāta tvaṃ hi ⁰mē’rdhaṃ smṛtaḥ purā  
> namō’tiyaṡasē tasmai ²dēvānāṃ paramātmanē /8.11/  

¹ all; M1,6,7 "tattvaṃ"  
² M5,6,7 T2 G PPS; T1 M1 "vēdānāṃ"; B Dn CE "dēhināṃ"

> nārāyaṇāya ¹viṡvāya nirguṇāya guṇātmanē  
> yasya prasādajō brahmā rudraṡ ca krōdhasambhavaḥ /8.12/  

¹ all; M1,6,7 "dēvāya"

> yō’sau yōnir hi sarvasya sthāvarasya carasya ca  
> aṣṭādaṡaguṇaṃ yat tat sattvaṃ sattvavatāṃ vara /8.13/  

> prakṛtiḥ sā parā mahyaṃ rōdasī ⁰yōgadhāriṇī  
> ⁰ṛtā satyāmarājayyā lōkānām ātmasan̄jn̄itā /8.14/  

> ⁰tasmāt sarvāḥ pravartantē sargapraḻayavikriyāḥ  
> tatō ¹yajn̄aṡ ca yaṣṭā ca purāṇaḥ puruṣō virāṭ  
> aniruddha iti prōktō lōkānāṃ ⁰prabhavāpyayaḥ /8.15/  

¹ †all; M1,5,6 "yajn̄āṡ" (sic)

> brāhmē rātrikṣayē prāptē tasya hy amitatējasaḥ  
> prasādāt prādurabhavat padmaṃ padmanibhēkṣaṇa /8.16/  

> tatra brahmā samabhavat ¹tasyaivaṃ samprasādajaḥ  
> ²ahnaḥ kṣayē lalāṭāc ca sutō dēvasya vai tathā /8.17/  

¹ M G2; PPS "-ca pra-"; T1 G1,3,6 "sa tasyaivaṃ pra-"; CE "sa tasyaiva pra-"  
² M5 CE PPS; M1,6,7 "ahaḥkṣayē"

> krōdhāviṣṭasya san̄jajn̄ē rudraḥ saṃhārakārakaḥ  
> ētau dvau vibudhaṡrēṣṭhau prasādakrōdhajav smṛtau /8.18/  

> tadā¹darṡitapanthānau sṛṣṭisaṃhārakārakau  
> nimittamātraṃ tāv atra ²sarvaprāṇivarapradau /8.19/  

¹ ?M5 T G1,3,6 PPS; M1,7 "-dēhita-"; M6 Dn CE "-dēṡita-"  
² all; M5 "-prāṇyabhayapradau"

> kapardī jaṭilō muṇḍaḥ ṡmaṡānagṛhasēvakaḥ  
> ugravrata⁰dharō rudrō yōgī ⁰tripuradāruṇaḥ /8.20/  

> dakṣakratuharaṡ caiva bhaganētraharas tathā  
> nārāyaṇātmakō jn̄ēyaḥ pāṇḍavēya yugē yugē /8.21/  

> tasmin hi pūjyamānē vai ⁰dēvadēvē mahēṡvarē  
> sampūjitō bhavēt pārtha dēvō nārāyaṇaḥ prabhuḥ /8.22/  

> aham ¹ātmā hi lōkānāṃ ⁰viṡvānāṃ pāṇḍunandana  
> tasmād ātmānam ēvāgrē rudraṃ sampūjayāmy aham /8.23/  

¹ all; M7 "ātmā’bhi lō-"

> yady ahaṃ nārcayēyaṃ vai īṡānaṃ varadaṃ ṡivam  
> ātmānaṃ nārcayēt kaṡcid iti mē ⁰bhāvitaṃ manaḥ /8.24/  

> mayā pramāṇaṃ hi kṛtaṃ lōkaḥ samanu¹vatsyatē  
> pramāṇāni hi pūjyāni tatas taṃ pūjayāmy aham /8.25/  

¹ M; et al. "-vartatē"

> yas taṃ vētti sa māṃ vētti yō’nu taṃ sa hi mām anu  
> rudrō nārāyaṇaṡ ¹caiva ⁰sattvam ⁰ēkaṃ dvidhākṛtam /8.26/  

¹ ?M5 B Dn CE; T G3,6 M1,6,7 "cēti"; G2 PPS "bhūtvā"; G1 "�"

> lōkē carati kauntēya vyaktisthaṃ sarvakarmasu  
> na hi mē kēna cid dēyō varaḥ pāṇḍavanandana /8.27/  

> iti san̄cintya manasā ¹purāṇaṃ viṡvam īṡvaram  
> putrārtham ārādhitavān ²ātmānam aham ātmanā /8.28/  

¹ ?M7 CE; M1,5,6 T1 G1,3,6 "purāhaṃ"; G2 PPS "varāhaṃ"  
² M5 CE; B Dn T1 G1,6 "aham ātmānam" (swap); T2 G2,3 M1,6,7 PPS "ātmanā’’tmānam ātmanaḥ"

> na hi viṣṇuḥ praṇamati kasmai cid vibudhāya tu  
> ṛta ātmānam ēvēti tatō rudraṃ ¹bhajāmy aham /8.29/  

¹ M5 B Dn CE; T G M1,6,7 "namāmy"

> sabrahmakāḥ sarudrāṡ ca sēndrā dēvāḥ ¹saharṣibhiḥ  
> arcayanti suraṡrēṣṭhaṃ dēvaṃ nārāyaṇaṃ harim /8.30/  

¹ all; M1,6,7 "saharṣayaḥ"

> bhaviṣyatāṃ vartatāṃ ca bhūtānāṃ caiva bhārata  
> sarvēṣām agraṇīr viṣṇuḥ sēvyaḥ pūjyaṡ ca nityaṡaḥ /8.31/  

> namasva ¹havyadaṃ viṣṇuṃ tathā ṡaraṇadaṃ nama  
> varadaṃ ²namasva kauntēya havyakavyabhujaṃ nama /8.32/  

¹ M5,6 G1,2 B Dn CE; M1,7 "havyagaṃ"; T G3,6 PPS "bhavyadaṃ"  
² ?M1,5 T1 G3,6 Dn CE PPS; M7 T2 G2 B6 "nama"; B7,8,9 "namasya"; M6 G1 �  

/8.32L2/ is hypermetric in the prior half.

> caturvidhā mama janā bhaktā ⁰ēvaṃ hi ⁰tē ⁰ṡrutam  
> tēṣām ēkāntinaḥ ṡrēṣṭhās ⁰tē caivānanyadēvatāḥ /8.33/  

> aham ēva ⁰gatis tēṣāṃ nirāṡīḥkarmakāriṇām  
> yē ⁰ca ṡiṣṭās trayō bhaktāḥ phalakāmā hi ⁰tē matāḥ /8.34/  

> sarvē cyavana⁰dharmāṇaḥ pratibuddhas tu ⁰ṡrēṣṭhabhāk  
> brahmāṇaṃ ṡitikaṇṭhaṃ ca yāṡ cānyā dēvatāḥ smṛtāḥ /8.35/  

> ¹pratibuddhavarjyāḥ sēvantē mām ēvaiṣyanti ²mat parāḥ  
> bhaktaṃ prati viṡēṣas tē ³ēṣa pārthānukīrtitaḥ /8.36/  

¹ ?M (hypermetric); CE "prabuddhavaryāḥ sē-"; T G PPS "pratibuddhā na sē-"  
² ?M5; M1,6,7 "yat phalaṃ"; CE "yat param"; T G PPS "yasmāt parimitaṃ phalam" (latter half)  
³ M5 T G3,6 Dn CE; M7 "ēva"; M1,6 "ēvaṃ"; G1 "ēkaḥ"; G2 "ēṣā"

T G makes sense because it gives same meaning as M and is not hypermetric.
But the CE reading gives the exact opposite meaning of TGM!

CE meaning "The best of the awakened ones serve Brahmā, Ṡitikaṇṭha and all those other dēvatas".  
T G meaning "The awakened ones do not serve Brahmā, Ṡitikaṇṭha..."  
M meaning "Those devoid of awakening serve Brahmā, Ṡitikaṇṭha..."

MW says that "prabuddha" is synonymous with "pratibuddha" (= awakened).
So the M. reading can be made non-hypermetric to "prabuddhavarjyāḥ sēvantē"

> tvaṃ caivāhaṃ ca kauntēya naranārāyaṇau smṛtau  
> bhārāvataraṇārthaṃ hi praviṣṭau mānuṣīṃ tanum /8.37/  

> jānāmy adhyātma¹yōgaṃ ca ²yō’haṃ yasmāc ca bhārata  
> nivṛttilakṣaṇō dharmas ⁰tathābhyudayikō’pi ca /8.38/  

¹ M T2 G1; et al. "-yōgāṃṡ"  
² all; M1,7 "yōgaṃ"

> ¹narāṇām ayanaṃ ⁰khyātam ⁰aham ēkaḥ sanātanaḥ  
> āpō nārā iti prōktā āpō vai narasūnavaḥ  
> ayanaṃ mama ²tāḥ pūrvam atō nārāyaṇō ⁰hy aham /8.39/  

¹ all; M1 "nārāṇām"  
² T G M; et al. "tat"

> ⁰chādayāmi jagad viṡvaṃ bhūtvā sūrya ivāṃṡubhiḥ  
> sarvabhūtādhivāsaṡ ca vāsudēvas tatō ⁰hy aham /8.40/  

> gatiṡ ca sarvabhūtānāṃ prajānāṃ cāpi bhārata  
> ¹vyāptē mē rōdasī pārtha kāntiṡ cābhyadhikā mama /8.41/  

¹ M1,5,7 T2 G1 PPS Ku; M6 T1 G3,6 B Dn CE "vyāptā"; G2 "prāptō"

CE's Critical Notes says "vyāptā" is irregular form of "vyāptē".

> adhi¹bhūtaniviṣtaṡ ca tad ²icchaṡ cāsmi bhārata  
> kramaṇāc cāpy ahaṃ pārtha viṣṇur ity abhisan̄jn̄itaḥ /8.42/  

¹ T G M; CE "-bhūta cāntē’haṃ"; Dn "bhūta cāntēṣu"  
² ?M5,6 B6,9; M1 "icchā"; Dn CE "icchaṃṡ"; M7 "viṡvaṡ"; T G1,3,6 PPS "viṡvaṃ"  

> damāt siddhiṃ parīpsantō māṃ janāḥ kāmayanti ¹ha  
> ²divaṃ cōrvīṃ ³mē madhyaṃ ca tasmād dāmōdarō ⁰hy aham /8.43/  

¹ M B0,8,9 Dn; B6,7 T G CE PPS "hi"  
² †all; M5 "jagac"  
³ T G M; CE PPS "ca"

> pṛṡnir ity ucyatē cānnaṃ vēdā āpō’mṛtaṃ tathā  
> mamaitāni sadā ⁰garbhē pṛṡnigarbhas tatō hy aham /8.44/  

> ṛṣayaḥ prāhur ēvaṃ māṃ ⁰trita¹kūpābhipātinam  
> pṛṡnigarbha tritaṃ pāhīty ēkatadvita⁰pātitam /8.45/  

¹ ?M5 B; CE "-pātitam"; T2 M1,6 "-pātinaḥ"; PPS "-pātinaiḥ";
  G1 M7 "-rūpābhipātinaḥ"; G2 "-rūpābhiprārthinaḥ"; GP "-kūpānipātitam"  

> tataḥ sa brahmaṇaḥ putra ādyō ¹hy ṛṣivaras ⁰tritaḥ  
> uttatā²rōdapānād vai pṛṡnigarbhānukīrtanāt /8.46/  

¹ M B Dn; T G1,3,6 "-’py ṛṣi-"; PPS "tv ṛṣi-"; CE om.  
² M5 T G CE; M1,6,7 "-rōdupānād"

> sūryasya tapatō lōkān agnēḥ sōmasya cāpy uta  
> ⁰aṃṡavō yē prakāṡantē mama tē kēṡasan̄jn̄itāḥ  
> sarvajn̄āḥ kēṡavaṃ tasmān mām āhur dvijasattamāḥ /8.47/  

> svapatnyām āhitō garbha ¹ucathyēna mahātmanā  
> ²ucathyē’ntarhitē caiva kadācid ³dēvatājn̄ayā  
> bṛhaspatir athāvindat tāṃ patnīṃ tasya ⁴dhīmataḥ /8.48/  

¹ M T1 G2,3,6 PPS; CE "utathyēna"; G1 "ucakthyēna"; T2 "ucathyasya striyāṃ tathā"  
² M; CE "utathyē-"; G1 "ucakthyē"; T2 "tamasy antargatē"  
³ T G M PPS; CE "dēvamāyayā"  
⁴ T G M; CE "bhārata"

> tatō vai tam ṛṣiṡrēṣṭhaṃ maithunōpagataṃ tathā  
> uvāca garbhaḥ kauntēya pan̄cabhūtasamanvitaḥ /8.49/  

> pūrvāgatō’haṃ varada nārhasy ¹ambāṃ prabādhitum  
> ētad bṛhaspatiḥ ṡrutvā cukrōdha ca ṡaṡāpa ca /8.50/  

¹ M5 B Dn CE; M1,6,7 "imāṃ"; T G PPS "-sē mām"

> ¹maithunāyāgatō yasmāt tvayāhaṃ vinivāritaḥ  
> tasmād andhō ⁰jāsyasi tvaṃ macchāpān nātra saṃṡayaḥ /8.51/  

¹ M5,6 B Dn T1 G3,6; T2 G1,2 M1,7 CE PPS "maithunōpagatō"

> sa ṡāpād ṛṣimukhyasya dīrghaṃ tama upēyivān  
> sa hi dīrghatamā nāma nāmnā hy āsīd ṛṣiḥ purā /8.52/  

> vēdān avāpya caturaḥ sāṅgōpāṅgān sanātanān  
> prayōjayām āsa tadā nāma guhyam idaṃ mama  
> ānupūrvyēṇa vidhinā kēṡavēti punaḥ punaḥ /8.53/  

> sa cakṣuṣmān samabhavad gautamaṡ cābhavat punaḥ  
> ēvaṃ hi varadaṃ nāma kēṡavēti ¹mamārjuna  
> dēvānām atha sarvēṣām ṛṣīṇāṃ ca mahātmanām /8.54/  

¹ ‡T G B Dn CE PPS; M5 "mamāpy uta"; M1,6,7 "mamācyuta" (sic)

> agniḥ sōmēna saṃyukta ¹ēkayōni mukhaṃ kṛtam  
> ²agnīṣōmamayaṃ tasmāj jagat kṛtsnaṃ carācaram /8.55/  

¹ M T2 G1 CE; G2 PPS "ēkayōnir mukhākṛtiḥ"; Dn "ēkayōnitvamāgataḥ"  
² M T G Dn PPS; CE "agnīṣōmātmakaṃ"

> ¹api ca purāṇē bhavati (1) ²ēkayōny agnīṣōmau (2)  
> dēvāṡ cāgnimukhā iti (3) ēkayōnitvāc ca parasparam ³harṣayantō lōkān ⁴dhārayanta iti (4) /8.56/  

¹ M; Dn CE"api hi"; T G PPS "āha ca"  
² ?M5; T2 G1 PPS "ēkayōnyāv"; M1 "ēkayō hyāgni-"; M6,7 "ēkayōgy"; Dn CE "ēkayōnyātmakāv"  
³ M5 G1; M1,7 "harṣayatau"; T2 G2 PPS "harṣayantau"; T1 G3,6 "harṣatō"; Dn "arhantō"; CE "mahayantō"  
⁴ M5 T1 G Dn; T2 CE PPS "dhārayata"

Thus ends chapter M1,6,7 162; G1 166; T1 G6 167; G3 168; T2 180; M5 G2 198; GP 341; MND 342

It has 55 verses of 116 lines, 1 paragraph of 4 sentences;  
    CE 51 verses of 116 lines, 1 paragraph of 4 sentences;  
    GP 57 verses of 116 lines, 1 paragraph of 4 sentences;  
    Ku 57 verses of 116 lines, 1 paragraph of 4 sentences.

Chapter name:

M1,6,7 "bhagavannāma-nirvacanaṃ".
T1 G6  "kṛṣṇārjuna-samvādē bhagavannāma-nirvacanaṃ"  
