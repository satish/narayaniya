# Chapter 12

>    ¹vaiṡaṃpāyana uvāca  
> kasya cit tv atha kālasya nāradaḥ paramēṣṭhijaḥ  
> daivaṃ kṛtvā yathānyāyaṃ pitryaṃ cakrē tataḥ param /12.1/  

¹ all; M1,6,7 om.

> tatas taṃ vacanaṃ prāha jyēṣṭhō dharmātmajaḥ prabhuḥ  
> ka ijyatē dvija¹vara daivē pitryē ca kalpitē /12.2/  

¹ M T G; CE "-ṡrēṣṭha"

> tvayā matimatāṃ ṡrēṣṭha tan mē ṡaṃsa yathā¹tatham  
> kim ētat kriyatē karma phalaṃ ²vāsya kim iṣyatē /12.3/  

¹ M T G; CE "-gamam"  
² M Dn; T G CE "cāsya"

>    nārada uvāca  
> tvayaitat kathitaṃ pūrvaṃ daivaṃ kartavyam ity api  
> daivataṃ ca parō ¹jn̄ēyaḥ paramātmā sanātanaḥ /12.4/  

¹ M G1; T G2,3,6 CE "yajn̄aḥ"

> tatas tadbhāvitō nityaṃ ¹yajē vaikuṇṭham avyayam  
> tasmāc ca ²prasṛtaḥ pūrvaṃ brahmā lōkapitāmahaḥ /12.5/  

¹ all; M1,6,7 "yajēd"  
² all; M1,7 "prasṛtaḥ"

> mama vai pitaraṃ prītaḥ paramēṣṭhy apy ajījanat  
> ahaṃ saṃkalpajas tasya putraḥ prathamakalpitaḥ /12.6/  

> yajāmi ¹vai pitṝn sādhō nārāyaṇavidhau kṛtē  
> ēvaṃ sa ēva bhagavān pitā mātā pitāmahaḥ  
> ijyatē pitṛyajn̄ēṣu mayā nityaṃ jagatpatiḥ /12.7/  

¹ M B Dn; T G CE "ahaṃ"

> ṡrutiṡ cāpy ¹aparā dēva putrān hi pitarō’yajan  
> vēdaṡrutiḥ ²pranaṣṭā ca punar adhyāpitā sutaiḥ  
> tatas tē mantradāḥ putrāḥ ³pitṝṇām iti vaidikam /12.8/  

¹ all; M1,6,7 "akṣarā"  
² M B Dn; T G CE "praṇaṣṭā"  
³ M T G; CE "pitṛtvam upapēdirē"

> nūnaṃ puraitad viditaṃ yuvayōr bhāvitātmanōḥ  
> putrāṡ ca pitaraṡ caiva parasparam apūjayan /12.9/  

> trīn piṇḍān nyasya vai pṛthvyāṃ pūrvaṃ dattvā kuṡān iti  
> kathaṃ tu piṇḍasaṃjn̄āṃ tē pitarō lēbhirē purā /12.10/  

>    naranārāyaṇāv ūcatuḥ  
> imāṃ hi dharaṇīṃ pūrvaṃ naṣṭāṃ sāgaramēkhalām  
> gōvinda ujjahārāṡu vārāhaṃ rūpam āṡritaḥ /12.11/  

> sthāpayitvā tu dharaṇīṃ ¹svasthānē puruṣōttamaḥ  
> jalakardamaliptāṅgō lōkakāryārtham udyataḥ /12.12/  

¹ M T G; CE "svē sth-"

> prāptē cāhnikakālē sa ¹madhyadēṡagatē ravau  
> daṃṣṭrāvilagnān ¹mṛtpiṇḍān vidhūya sahasā prabhuḥ  
> sthāpayām āsa vai pṛthvyāṃ kuṡān āstīrya nārada /12.13/  

¹ M B Dn; "madhyandina-"  
² M5 T G; M1,6,7 B Dn "trīn-"

> sa tēṣv ātmānam uddiṡya pitryaṃ cakrē yathāvidhi  
> saṅkalpayitvā trīn piṇḍān svēnaiva vidhinā prabhuḥ /12.14/  

> ātmagātrōṣmasambhūtaiḥ snēhagarbhais tilair api  
> prōkṣyāpa¹savysaṃ dēvēṡaḥ prāṅmukhaḥ kṛtavān svayam /12.15/  

¹ M T1 G1,3,6; CE "-vargaṃ"

> maryādāsthāpanārthaṃ ca tatō vacanam uktavān  
> ahaṃ hi pitaraḥ sraṣṭum udyatō lōkakṛt svayam /12.16/  

Before /12.16L2/ M5,6 B Dn D in. "vṛṣākapir uvāca"

> tasya cintayataḥ sadyaḥ pitṛkāryavidhiṃ param  
> daṃṣṭrābhyāṃ pravinirdhūtā ¹mṛtpiṇḍā dakṣiṇāṃ diṡam  
> āṡritā dharaṇīṃ ²pīḍya tasmāt pitara ēva tē /12.17/  

¹ M T2; CE "mamaitē"  
² M T G; CE "piṇḍās"

> trayō mūrtivihīnā vai piṇḍamūrtidharās tv imē  
> bhavantu pitarō lōkē mayā sṛṣṭāḥ sanātanāḥ /12.18/  

> pitā pitāmahaṡ caiva tathaiva prapitāmahaḥ  
> aham ēvātra vijn̄ēyas triṣu piṇḍēṣu saṃsthitaḥ /12.19/  

> nāsti mattō’dhikaḥ kaṡcit kō vābhyarcyō mayā ¹bhavēt  
> ²aham ēva pitā lōkē aham ēva pitāmahaḥ /12.20/  

¹ M1,5,6 T G; M7 CE "svayam"  
² M T G; CE "kō vā mama"

> pitāmahapitā caiva aham ēvātra kāraṇam  
> ity ēvam uktvā vacanaṃ dēvadēvō vṛṣākapiḥ /12.21/  

> varāhaparvatē vipra dattvā piṇḍān savistarān  
> ātmānaṃ pūjayitvaiva tatraivādarṡanaṃ gataḥ /12.22/  

> ētadarthaṃ ṡubhamatē pitaraḥ piṇḍasan̄jn̄itāḥ  
> labhantē satataṃ pūjāṃ vṛṣākapi¹vacō yathā /12.23/  

¹ all; M1,7 "-vayō"

> yē yajanti pitṝn ¹bhaktyā gurūṃṡ ²devātithīṃs tathā  
> gāṡ caiva dvijamukhyāṃṡ ca pṛthivīṃ ³pitaraṃ tathā  
> karmaṇā manasā vācā viṣṇum ēva yajanti tē /12.24/  

¹ M T2 G1,2; CE "dēvān"  
² M T G; CE "caivā-"  
³ M T G; CE "mātaraṃ"

S. recension substitutes "father" for Northern's "mother".

> antargataḥ sa bhagavān sarvasattvaṡarīragaḥ  
> samaḥ sarvēṣu bhūtēṣu īṡvaraḥ sukhaduḥkhayōḥ  
> ¹sa vai mahātmā sarvātmā nārāyaṇa iti ²ṡrutiḥ /12.25/  

¹ M; et al. "mahān"  
² M G1,3,6; T G2 CE "ṡrutaḥ"

Thus ends chapter M1,7 168; G1 171; T1 G6 172; G3 173; T2 175; G2 201; M5,6 203.

It has 28 verses, 56 lines;
    CE 25 verses, 56 lines;
    GP 28 verses, 56 lines;
    Ku 27 verses, 56 lines.

Chapter name:

M1,7 "pitṛsarga-kathanaṃ"  
T1 G6   "nārada-nārāyaṇa-saṃvādaḥ"
