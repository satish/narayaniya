# Chapter 02

> sa ēvam uktō dvipadāṃ variṣṭhō  
>  nārāyaṇēnōttamapūruṣēṇa  
> jagāda vākyaṃ dvipadāṃ variṣṭhaṃ  
>  nārāyaṇaṃ lōkahitādhivāsam /2.1/  

> yadartham ātmaprabhavēha janma  
>  tavōttamaṃ dharmagṛhē caturdhā  
> tat sādhyatāṃ lōkahitārtham adya  
>  gacchāmi draṣṭuṃ prakṛtiṃ tavādyām /2.2/  

Before /2.2/ B Dn D in. "nārada uvāca"

> vēdāḥ svadhītā mama lōkanātha  
>  taptaṃ tapō nānṛtam uktapūrvam  
> ¹pūjāṃ gurūṇāṃ satataṃ karōmi  
>  parasya guhyaṃ na ca bhinnapūrvam /2.3/  

¹ all; M1,6,7 "dvijātipūjāṃ satataṃ karōmi ca"

> guptāni catvāri yathā¹gamaṃ mē  
>  ṡatrau ca mitrē ca samō’smi nityam  
> taṃ cādidēvaṃ satataṃ prapanna  
>  ēkāntabhāvēna vṛṇōmy ajasram  
> ēbhir viṡēṣaiḥ pariṡuddhasattvaḥ  
>  kasmān na paṡyēyam anantam īṡam /2.4/  

¹ all; M1,6,7 "-gataṃ"

> tat pāramēṣṭhyasya vacō niṡamya  
>  nārāyaṇaḥ ¹sātvatadharmagōptā  
> gacchēti taṃ nāradam uktavān ²vai  
>  ³sampūjayitvā vividha⁴kriyābhiḥ /2.5/  

¹ M5 T G CE; M1,6,7 B Dn "ṡāṡvata-"  
² M T G PPS; et al. "sa"  
³ ?M5 K6; M1,6,7 CE "sampūjayitvātmavidhikri-"; T G "sampūjayitvā vivivat kri-"  
⁴ all; M1,6,7 "-kriyāḥ saḥ"

Before /2.5/ B6,8,9 PPS in. "bhīṣma uvāca" but others om.

After /2.5L1/ only M5 in. CE@801 (1 line) but all others om.

> tatō visṛṣṭaḥ paramēṣṭhiputraḥ  
>  sō’bhyarcayitvā tam ṛṣiṃ purāṇam  
> kham utpapātōttamavēgayuktas  
>  tatō’dhimērau sahasā nililyē /2.6/  

> tatrāvatasthē ca munir muhūrtam  
>  ēkāntam āsādya girēḥ sa ṡṛṅgē  
> ālōkayann uttarapaṡcimēna  
>  dadarṡa cātyadbhuta¹citrarūpam /2.7/  

¹ M; T G6 B Da Dn "-muktarūpam"; G1-3 CE "-rūpayuktam"; G2 PPS "-yuktarūpam"

> kṣīrōdadhēr uttaratō hi dvīpaḥ  
>  ṡvētaḥ sa nāmnā prathitō viṡālaḥ  
> mērōḥ ¹sahasrē sa hi yōjanānāṃ  
>  ²dvātriṃṡadardhē kavibhir niruktaḥ /2.8/  

¹ M T2 G1; T1 G1,3,6 CE "sahasraiḥ"; G2 PPS "sahasraḥ"  
² ?M1,6,7; M5 "ṡatatrayārdhē"; PPS "ṡatatrayādvai"; T G PPS (ख घ) "dvātriṃṡadagrē"; B Dn CE "dvātriṃṡatōrdhvaṃ"  

All M. mss. end in "-ardhē". CE says "more than (ūrdhvaṃ) 32 thousand yojanas".
M1,6,7 say "in the 32½ thousand yojanas". M5 says "in the 300½ thousand yojanas".
PPS says "300 thousand yojanas".

> ¹atīndriyāṡ cānaṡanāṡ ca tatra  
>  ²niṣpandahīnāḥ susugandhinaṡ ca  
> ṡvētāḥ pumāṃsō gatasarvapāpāṡ  
>  cakṣurmuṣaḥ pāpakṛtāṃ ⁰narāṇām /2.9/  

¹ M T2 G2 CE; T1 G1,3,6 Dn PPS "anindriyāṡ"  
² ?M1,5,7 G1 B Dn CE PPS; M7 T G2,3,6 "niṣyanda-"  

> ¹vajrasthirakāyāḥ ²samamānōnmānā  
>  ⁰divyānvayarūpāḥ ⁰ṡubhasārōpētāḥ  
> chatrākṛtiṡīrṣā mēghaughaninādāḥ  
>  ³samamuṣkacatuṣkā ⁴rājīṡatasantatapādāḥ /2.10/  

¹ M T1 G1,3,6; B Dn CE PPS "vajrāsthikāyāḥ"; T2 "vajrasthirākārāḥ"; G2 "vajrastharākāyaḥ"  
² ?M5 B Dn CE PPS; M1,7 "svamamānōmānā"; M6 "svamamānōnmānā"; T1 G3,6 "samamānōnmānaḥ"; T2 "samānōnmānā"; G1 "xxx"  
³ M T2 G1 Dn PPS; T1 G3,6 "samuṣka-"; G2 "sumuṣka" (s/su/sa/); CE "satpuṣkara"  
⁴ ?M; T1 G3,6 "rājīvasamapadāḥ"; T2 "rājīnitapādāḥ"; G1 "rājīṡitapādāḥ" (s/ṡi/vaṡa/);
  G2 "rājīvantapādāḥ" (s/va/vaṡa/); CE "rājīvaṡatapādāḥ"; PPS Ku "rājīvacchadapādāḥ";
  Dn "rājīvacchatapādāḥ"

> ṣaṣṭyā dantair yuktāḥ ṡuklair  
>  aṣṭābhir ¹yē damṣṭrābhiṡ ca  
> jihvābhir yē ²vaktraṃ ṡubhraṃ  
>  ³lēlihyantē sūryaprakhyam /2.11/  

¹ ‡T G1,3,6; M5 om. "yē"; M1,6,7 om. "ca"; Dn CE "daṃṣṭrābhir yē"; PPS "yē ṣaṣṭyābhiṡ ca"; G2 "xxx";
  so that matches "Vidyunmāla" metre  
² M PPS; T1 G3,6 Dn Da "viṡvavaktraṃ"; T2 "viṡvaṃ vajraṃ"; G1 "viṡvadhīvaktraṃ";
  G2 PPS (ख घ) "viṡvagvyaktaṃ"; CE "viṣvagvaktraṃ"
³ all; PPS "lēlihantē"; M1,6,7 "lēlihatē" (breaks metre)

> ¹bhaktā devaṃ ²viṡvōtpattiṃ  
>  yasmāt sarvē lōkāḥ ³sūtāḥ  
> ⁰vēdā dharmā munayaḥ ṡāntā  
>  dēvāḥ sarvē tasya ⁴visargaḥ /2.12/  

¹ M T1 G PPS; T2 CE "bhaktyā dēvaṃ"  
² M; T1 G CE PPS (ख घ) Ku GP "viṡvōtpannaṃ"; T2 "viṡvōtparaṃ";
  PPS "vidhiviṡēṣōtpattiṃ"; Cv "viṡvāviṡvōtpattyavyayaṁ"  
³ M5,6 T2 G1,2,3 CE; T1 G6 "syūtāḥ"; M1,7 "prasūtāḥ"; PPS Ku GP "samprasūtāḥ"  
⁴ M PPS; T G CE "visargāḥ"; Dn "nisargaḥ"; Ku "nisarga iti"  

Thus ends chapter M7 155; G1 158; T1 G6 M1,6 159; G3 160; T2 172; M5 191.

Chapter name: M. "ṡvētadvīpa darṡanaṃ"

>    yudhiṣṭhira uvāca  
> ¹atīndriyā ²anāhārā ³aniṣyandāḥ sugandhinaḥ  
> kathaṃ tē puruṣā jātāḥ kā tēṣāṃ gatir uttamā /2.13/  

¹ M CE T2 G2; T1 G1,3,6 B Dn PPS MND "anindriyā"  
² M T2 G1,2 PPS; T1 G3,6 CE PPS "nirāhārā"  
³ ?M1,5,6; M7 T G CE PPS MND "aniṣpandāḥ"; B6,7 "xxx"; B9 "xxx"; Da "xxx"

Apte: niṣyandaḥ = nissyandaḥ = trickling or flowing down; consequence or result; uttering or declaring.

It must be "aniṣyandāḥ" because /2.13L1/ is same as /3.25L2/ where, in the latter case,
M T2 G1 all use "aniṣyandāḥ".

> ¹yē hi bhaktā bhavantīha narā bharatasattama  
> tēṣāṃ lakṣaṇam ētad dhi yac chvētadvīpavāsinām /2.14/  

¹ M PPS; T2 G1,2 CE PPS (घ) "yē vimuktā"; Ku GP "yē ca muktā"; T1 G3,6 "yē hi muktā"  

> tasmān mē saṃṡayaṃ chindhi paraṃ kautūhalaṃ hi mē  
> tvaṃ hi sarvakathārāmas tvāṃ ¹caivōpāṡritā vayam /2.15/  

¹ M5 T G1,3,6 CE PPS; M1,7 "caivāpaṡritā"; M6 om. /2.15L2/

>    bhīṣma uvāca  
> vistīrṇaiṣā kathā rājan̄ ṡrutā mē pitṛsannidhau  
> saiṣā tava hi vaktavyā kathā¹sārā hi ³sā smṛtā /2.16/  

¹ M; T G CE "-sārō"  
² M; T1 G2,3,6 CE PPS "sa smṛtaḥ"; Dn "sā matā"; G1 "saṃsmṛtaḥ"; T2 "na smṛtaḥ"  

M uses feminine throughout in second line in light of "kathā" which is feminine.
"saiṣā" = "sā ēṣā".

After /2.16/ T G M6 PPS Ku GPd in. CE@802 (2 lines) but M1,5,7 B Dn CE om.

> rājōparicarō nāma babhūvādhipatir bhuvaḥ  
> ākhaṇḍala¹samaḥ khyātō bhaktō nārāyaṇaṃ harim /2.17/  

¹ M5 T1 G3,6 PPS; M1,6 G1 "-samakhyā-"; M7, G2 "samā"; T2 B Dn CE "-sakhaḥ"  

All M. mss. contain "sama", meaning, "equal to Indra" (Ākhaṇḍala) instead of CE's "friend of Indra".

Before /2.17/ T G PPS in. "nārada uvāca" but others om.

> dhārmikō nityabhaktaṡ ca ¹pitur nityam atandritaḥ  
> sāmrājyaṃ tēna ²prāptam hi nārāyaṇavarāt purā /2.18/  

¹ ?M5 B Da Dn PPS; M1,6,7 "ṡucir"; T G CE "pitṝn"  
² M; et al. "saṃprāptaṃ"

> sātvataṃ vidhim āsthāya prāk sūryamukhaniḥsṛtam  
> pūjayām āsa dēvēṡaṃ tac¹chēṣēṇa pitāmahān /2.19/  

¹ all; M5,6 "-chēṣāc ca"

> ¹pitṝn ṛṣīṃṡ ca viprāṃṡ ca saṃvibhajyāṡritāṃṡ ca saḥ  
> ṡēṣānnabhuk satyaparaḥ sarvabhūtēṣv ahiṃsakaḥ /2.20/  

¹ M T G; CE PPS Ku GP "pitṛṡēṣēṇa"

After /2.21L1/ M T G PPS in. CE@803 /2.21L2/ (1 line).

> sarvabhāvēna bhaktaḥ sa dēvadēvaṃ janārdanam  
> anādimadhyanidhanaṃ lōkakartāram avyayam /2.21/  

> tasya nārāyaṇē bhaktiṃ vahatō’mitrakarṡana  
> ēkaṡayyāsanaṃ ṡakrō dattavān dēvarāṭ svayam /2.22/  

> ātmā rājyaṃ dhanaṃ caiva kaḻatraṃ ¹vāhanaṃ tathā  
> ētad ²bhagavataṃ sarvam iti tat prēkṣitaṃ sadā /2.23/  

¹ M B Da Dn; T G CE PPS "vāhanāni ca"  
² M G2 Dn PPS; B "bhāgavatē"; T G1,3,6 CE "bhagavatē"

> kāmyanaimitti¹kājasrā ²yājn̄īyāḥ ³paramakriyāḥ  
> ³sarvaṃ sātvatam āsthāya vidhiṃ cakrē samāhitaḥ /2.24/  

¹ M T G; CE "-kājasraṃ"; PPS "-kājasra"; Ku GP "-kā rājan"  
² ?M1,5,6 PPS; M7 "yajn̄īyāḥ"; T G1,3,6 "yājn̄iyāḥ"; G2 "yajikāḥ"; CE Ku GP "yajn̄iyāḥ"  
³ all; M1,6,7 K7 "paramāḥ kri-"  
⁴ M T1 G; T2 "sattvaṃ"; CE Ku GP "sarvāḥ"  

Since "kriyā" is feminine, M mss. uniformly use feminine plural for the qualifiers
("ajasrāḥ", "yājn̄īyāḥ", "paramakriyāḥ").

As per MW yājn̄iya = yājn̄īya

> pan̄carātravidō mukhyās tasya gēhē mahātmanaḥ  
> ¹prāyaṇaṃ bhagavatprōktaṃ bhun̄jatē cāgrabhōjanam /2.25/  

¹ M5 CE; M1,7 "prāvaṇaṃ"; M6 PPS "prāpaṇaṃ"; T1 G3,6 "pravaṇā";
  T2 G1 "pāvanaṃ"; G2 "praṇavā"; Ku "varānnaṃ"  

prāpaṇam = obtaining, acquisition, attainment
prāvaṇam = spade or shovel

Reading follows M5 CE as also confirmed by Apte:

    प्रायणम् prāyaṇam = A kind of food (prepared in milk);
    प्रायणं भगवत्प्रोक्तं भुञ्जते वाऽग्रभोजनम् Mb.12.335.25

> tasya praṡāsatō rājyaṃ dharmēṇāmitraghātinaḥ  
> nānṛtā vāk samabhavan manō duṣṭaṃ ¹ca nābhavat  
> na ca kāyēna kṛtavān sa pāpaṃ param aṇv api /2.26/  

¹ M T G; et al. "na cābhavat" (swap)

> yē hi tē ¹munayaḥ khyātāḥ sapta citraṡikhaṇḍinaḥ  
> tair ēkamatibhir bhūtvā yat prōktaṃ ṡāstram uttamam /2.27/  

¹ M5 T G CE PPS; M1,6,7 "yatayaḥ"; B Da Dn Ku "ṛṣayaḥ"

After /2.28/, M B Da Dn PPS Ku GP insert passage CE@804 /2.29/ (2 lines) but T G CE om.

> vēdaiṡ caturbhiḥ ¹samitaṃ kṛtaṃ mērau mahāgirau  
> āsyaiḥ saptabhir ²udgītaṃ ³lōkadharmaṃ hy anuttamam /2.28/  

¹ all; M1,6,7 "saṃhitaṃ"  
² M Da; et al. "udgīrṇaṃ"  
³ †all; M5 K7 "lōkadhāma hy-"; B Da Dn CE om. "hy"

> marīcir atryaṅgirasau pulastyaḥ pulahaḥ kratuḥ  
> vasiṣṭhaṡ ca mahātējā ētē citraṡikhaṇḍinaḥ /2.29/  

> sapta prakṛtayō hy ētās tathā svāyambhuvō’ṣṭamaḥ  
> ētābhir dhāryatē lōkas tābhyaḥ ṡāstraṃ viniḥsṛtam /2.30/  

> ēkāgramanasō dāntā munayaḥ saṃyamē ratāḥ  
> idaṃ ṡrēya idaṃ brahma idaṃ hitam anuttamam  
> ¹lōkānāṃ cintya manasā tataḥ ṡāstraṃ pracakrirē /2.31/  

¹ M T2 G1; T1 G2,3,6 CE Ku GP "lōkān san̄cintya manasā"

> tatra dharmārthakāmā hi mōkṣaḥ paṡcāc ca kīrtitaḥ  
> maryādā vividhāṡ caiva divi bhūmau ca saṃsthitāḥ /2.32/  

> ārādhya tapasā dēvaṃ hariṃ nārāyaṇaṃ prabhum  
> divyaṃ varṣasahasraṃ ¹tē ²tasthuḥ pavanapā dvijāḥ /2.33/  

¹ M; et al. "vai"  
² M PPS; T G PPS (घ) "samastair munibhiḥ samam";
  CE Ku GP "sarvē tē ṛṣibhiḥ saha"  

> nārāyaṇānu¹ṡāstā hi tadā dēvī sarasvatī  
> vivēṡa tān ṛṣīn sarvām̐l lōkānāṃ hitakāmyayā /2.34/  

¹ ?M7 CE; M1,5,6 "-ṡastā"; T G PPS Ku GP "-ṡiṣṭā"  

"anuṡāstā" (verb 2, feminine past passive participle of √anuṡās) "been ordered"
"ṡasta" or "anuṡasta" means praised, approved.

> tataḥ ¹pravartitaṃ samyak ²tapōvadbhir dvijātibhiḥ  
> ṡabdē cārthē ca hētau ca ēṣā prathamasargajā /2.35/  

¹ M T G; CE PPS Ku GP "pravartitā"  
² M T G; CE Ku GP "tapōvidbhir"; PPS "kriyāvidbhir"

> ādāv ēva hi tac chāstram ōṃkārasvarabhūṣitam  
> ṛṣibhir bhāvitaṃ tatra yatra kāruṇikō ¹hy asau /2.36/  

¹ ?M5 B Dn CE PPS Ku GP; M1,6,7 "hy ayam"; T1 G3,6 "guruḥ"; T2 G1 "guṇaḥ"; G2 "’ruṇaḥ"

> tataḥ prasannō bhagavān anirdiṣṭaṡarīragaḥ  
> ṛṣīn uvāca tān sarvān adṛṡyaḥ puruṣōttamaḥ /2.37/  

> kṛtaṃ ṡatasahasraṃ hi ṡlōkānām ¹idam uttamam  
> lōkatantrasya kṛtsnasya yasmād dharmaḥ pravartatē /2.38/  

¹ ?M1,6,7 CE PPS GP; M5 Ku "hitam"; T1 G3,6 "ṡāstram"; T2 G1,2 "ṡatam"

Before /2.38/ B6,8 in. "ṡrībhagavān uvāca"

> pravṛttau ca nivṛttau ca yōnir ētad bhaviṣyati  
> ṛgyajuḥsāmabhir juṣṭam atharvāṅgirasais tathā /2.39/  

> tathā pramāṇaṃ ¹ca mayā kṛtō brahmā prasādajaḥ  
> rudraṡ ca krōdhajō viprā yūyaṃ prakṛtayas tathā /2.40/  

¹ M T G PPS; CE GP Ku "hi"  

> sūryācandramasau ¹cātra bhūmir āpō’gnir ēva ca  
> sarvē ca nakṣatragaṇā yac ca bhūtābhiṡabditam /2.41/  

¹ M T G PPS; CE Ku GP "vāyur"

> adhikārēṣu vartantē yathāsvaṃ brahmavādinaḥ  
> ¹sarvapramāṇaṃ hi ²yathā tathaitac chāstram uttamam /2.42/  

¹ ?M1,6,7 T2 G1,2; M5 T1 G3,6 B Dn CE PPS "sarvē"  
² all; M1,6,7 "tathā yathaitac" (swap)

> bhaviṣyati pramāṇaṃ ¹vai ētan madanuṡāsanam  
> asmāt ²pravakṣyatē dharmān manuḥ svāyaṃbhuvaḥ svayam /2.43/  

¹ ‡T G B Dn CE PPS; M5 "vai ētāvad anuṡāsanam"; M1,6,7 "vas tathā ṡāstram anuttamam" (sic)  
² †all; M5 "pravartitān"

> uṡanā bṛhaspatiṡ caiva yadōtpannau bhaviṣyataḥ  
> tadā pravakṣyataḥ ṡāstraṃ yuṣmanmatibhir uddhṛtam /2.44/  

/2.44a/ is hypermetric.

> svāyaṃbhuvēṣu ¹ṡāstrēṣu ṡāstrē cōṡanasā kṛtē  
> bṛhaspatimatē caiva lōkēṣu pravicāritē /2.45/  

¹ M; et al. "dharmēṣu"

> yuṣmatkṛtam idaṃ ṡāstraṃ prajāpālō vasus tataḥ  
> bṛhaspatisakāṡād vai ¹prāpsyatē dvijasattamāḥ /2.46/  

¹ †all; M5 G2 "prāpyatē"

> sa hi madbhāvitō rājā madbhaktaṡ ca bhaviṣyati  
> tēna ṡāstrēṇa ¹lōkēṣu kriyāḥ sarvāḥ kariṣyati /2.47/  

¹ all; M1,7 B9 "ṡāstrēṣu"

> ētad dhi sarvaṡāstrāṇāṃ ṡāstram uttamasaṃjn̄itam  
> ētad arthyaṃ ca dharmyaṃ ca yaṡasyaṃ caitad uttamam /2.48/  

> asya pravartanāc caiva prajāvantō bhaviṣyatha  
> sa ca rājā ṡriyā yuktō bhaviṣyati mahān vasuḥ /2.49/  

> saṃsthitē tu nṛpē tasmin̄ ṡāstram ētat sanātanam  
> antardhāsyati ¹yat satyam ētad vaḥ kathitaṃ mayā /2.50/  

¹ ?M5 T1 G K7; M1,6,7 T2 B Dn CE "tat"  

> ētāvad uktvā vacanam adṛṡyaḥ puruṣōttamaḥ  
> visṛjya tān ṛṣīn sarvān kām api ¹prasthitō diṡam /2.51/  

¹ M1,5,6 B Dn CE; M7 T1 G PPS Ku GP "prasṛtō"

Before /2.49/ B8 in. "bhīṣma uvāca"

> tatas tē lōkapitaraḥ sarvalōkārthacintakāḥ  
> prāvartayanta tac chāstraṃ dharmayōniṃ sanātanam /2.52/  

> utpannē’’ṅgirasē caiva yugē prathamakalpitē  
> sāṅgōpaniṣadaṃ ṡāstraṃ sthāpayitvā bṛhaspatau /2.53/  

> jagmur yathēpsitaṃ dēṡaṃ tapasē kṛtaniṡcayāḥ  
> ¹dhārakāḥ sarvalōkānāṃ sarvadharmapravartakāḥ /2.54/  

¹ ?M1,5,6; T G1,3,6 GP Ku "dhāraṇāḥ"; M7 G2 CE PPS "dhāraṇāt"

Thus ends chapter M1,6,7 156; G1 159; T1 G6 160; G3 161; T2 173; G2 177; M5 192.

It has 54 verses, 111 lines;
    CE 52 verses, 108 lines;
    GP 55 verses, 112 lines;
    Ku 56 verses, ?? lines
