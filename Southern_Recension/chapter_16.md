# Chapter 16

Note: T2 G2 are missing for this adhyāya. Hence, "T G" means "T1 G1,3,6".

>    janamējaya uvāca  
> ¹sāṅkhyaṃ ²yōgaḥ pan̄carātraṃ ³vēdāraṇyakam ēva ca  
> jn̄ānāny ētāni brahmarṣē lōkēṣu pracaranti ⁴ha /16.1/  

¹ M5,6 T1 G1,6 Dn CE; M1,7 G3 "sāṅkhyayō-"  
² M5 T1 G1,3,6 PPS; CE "yōgaṃ"  
³ M CE; T G PPS "vēdāḥ pāṡupataṃ tathā"  
⁴ †M1,6,7 T1 CE; M5 "ca"; G3,6 PPS "hi"; G1 "xxx"

Speaker confusion continues. G1 repeats /16.1L1/ after /16.1/ as "vaiṡampāyana uvāca" and the second
time as "janamējaya uvāca".

> kim ētāny ēkaniṣṭhāni pṛthaṅniṣṭhāni vā munē  
> prabrūhi vai mayā pṛṣṭaḥ pravṛttiṃ ca yathākramam /16.2/  

>    vaiṡampāyana uvāca  
> jajn̄ē bahujn̄aṃ ¹paramābhyudāraṃ  
>  yaṃ dvīpamadhyē sutam ātmavantam  
> parāṡarād gandhavatī maharṣiṃ  
>  tasmai namō’jn̄ānatamōnudāya /16.3/  

¹ M T G; CE "param atyudāraṃ"

> pitāmahādyaṃ pravadanti ṣaṣṭhaṃ  
>  maharṣim ārṣēyavibhūtiyuktam  
> nārāyaṇasyāṃṡajam ēkaputraṃ  
>  dvaipāyanaṃ vēdamahānidhānam /16.4/  

> tam ¹ādikālē sa mahāvibhūtir  
>  nārāyaṇō brahmamahā²nidhānam  
> sasarja putrārtham udāratējā  
>  vyāsaṃ mahātmānam ³ajaḥ purāṇaḥ /16.5/  

¹ M; T G PPS "ādikālē su-"; CE "ādikālēṣu"  
² M5,6 T G CE PPS; M1,7 "-nidhānaḥ"  
³ M1,6,7 T G CE PPS; M5 Dn "ajaṃ purāṇaṃ"

>    janamējaya uvāca  
> tvayaiva ¹kathitaṃ pūrvaṃ sambhavō dvijasattama  
> vasiṣṭhasya sutaḥ ṡaktiḥ ṡaktēḥ putraḥ parāṡaraḥ /16.6/  

¹ M1,5,7 G1; et al. "kathitaḥ"

> parāṡarasya dāyādaḥ kṛṣṇadvaipāyanō muniḥ  
> bhūyō nārāyaṇasutaṃ tvam ēvainaṃ prabhāṣasē /16.7/  

> kim ataḥ ¹pūrvakaṃ janma vyāsasyāmitatējasaḥ  
> kathayasvōttamamatē janma nārāyaṇōdbhavam /16.8/  

¹ M T G; CE "pūrvajaṃ"

>    vaiṡampāyana uvāca  
> vēdārthān vēttukāmasya dharmiṣṭhasya tapōnidhēḥ  
> gurōr mē jn̄ānaniṣṭhasya himavatpāda āsataḥ /16.9/  

> kṛtvā bhāratam ākhyānaṃ ¹tapaṡrāntasya dhīmataḥ  
> ṡuṡrūṣāṃ tatparā rājan kṛtavantō vayaṃ tadā /16.10/  

¹ ?M1,5,7 G1; M6 T1 G3,6 CE PPS "tapaḥ"

> sumantur jaiminiṡ caiva ¹pailaṡ ca sudṛḍhavrataḥ  
> ahaṃ caturthaḥ ṡiṣyō vai ṡukō vyāsātmajas tathā /16.11/  

¹ ‡all; M5 "vainyaṡ"

> ēbhiḥ parivṛtō vyāsaḥ ṡiṣyaiḥ pan̄cabhir uttamaiḥ  
> ṡuṡubhē himavatpādē bhūtair bhūtapatir yathā /16.12/  

> vēdān āvartayan sāṅgān bhāratārthāṃṡ ca sarvaṡaḥ  
> tam ēkamanasaṃ dāntaṃ yuktā vayam upāsmahē /16.13/  

> kathānta¹rē’ca kasmiṃṡ cit pṛṣṭō’smābhir dvijōttamaḥ  
> vēdārthān bhāratārthāṃṡ ca janma nārāyaṇāt tathā /16.14/  

¹ M T1 G3,6; G1 CE "-rē’tha"

> sa pūrvam uktvā vēdārthān bhāratārthāṃṡ ca tattvavit  
> nārāyaṇād idaṃ janma vyāhartum upacakramē /16.15/  

> ṡṛṇudhvam ākhyānavaram ¹idam ārṣēyam uttamam  
> ādikālōdbhavaṃ viprās ²tapasādhigataṃ mayā /16.16/  

¹ ?M5 B Dn; T G "ētad"; M1,6,7 "ārṣēyam purātanam" (submetric)  
² all; M1,6,7 "tapasā vigataṃ"

> prāptē prajāvisargē vai ¹saptamē padmasambhavē  
> nārāyaṇō mahāyōgī ṡubhāṡubhavivarjitaḥ /16.17/  

¹ all; M1,7 "sattvam ēvam asambhavē"

> sasṛjē ¹nābhitaḥ putraṃ brahmāṇam amitaprabham  
> tataḥ sa prādurabhavad athainaṃ vākyam abravīt /16.18/  

¹ M5 G1 B Dn CE; M1,6,7 T1 G3,6 PPS "nābhijaṃ"

> mama tvaṃ nābhitō jātaḥ prajāsargakaraḥ prabhuḥ  
> sṛja prajās tvaṃ vividhā brahman sajaḻapaṇḍitāḥ /16.19/  

> sa ēvam uktō vimukhaṡ cintāvyākulamānasaḥ  
> praṇamya varadaṃ dēvam uvāca harim īṡvaram /16.20/  

> kā ṡaktir mama dēvēṡa prajāḥ sraṣṭuṃ namō’stu tē  
> ¹aprajn̄āvān ahaṃ dēva vidhatsva yad anantaram /16.21/  

¹ all; M7 "aprajāvāhanaṃ dēva vidhasva"

> sa ēvam uktō bhagavān ¹bhūtvāthāntarhitas tataḥ  
> cintayām āsa ²viṡvēṡō buddhiṃ buddhimatāṃ varaḥ /16.22/  

¹ ?M5 T1 G3,6 B Dn CE; M1,6,7 G1 "bhūtvā cāntar-"; PPS "xxx"  
² M G3; et al. "dēvēṡō"

> svarūpiṇī tatō buddhir upatasthē hariṃ prabhum  
> yōgēna caināṃ ¹samyōjya svayaṃ ²saṃyuyujē tadā /16.23/  

¹ M5,6,7 T1 G3,6 PPS; M1 "sampūjya"; G1 CE "niryōgaḥ"  
² M T1 G1; G3,6 "sa svayaṃ yu-"; CE "niyuyujē"

> sa tām aiṡvaryayōgasthāṃ buddhiṃ ṡaktimatīṃ satīm  
> uvāca vacanaṃ dēvō buddhiṃ vai prabhur avyayaḥ /16.24/  

> brahmāṇaṃ praviṡasvēti lōkasṛṣṭyarthasiddhayē  
> tatas tam īṡvarādiṣṭā buddhiḥ kṣipraṃ vivēṡa sā /16.25/  

> athainaṃ buddhi¹saṃyuktaṃ punaḥ ²sa dadṛṡē hariḥ  
> bhūyaṡ cainaṃ vacaḥ prāha sṛjēmā vividhāḥ prajāḥ /16.26/  

¹ all; M1,6,7 "-sampannaṃ"  
² ?M1,5,6 B Dn CE PPS; M7 G1 "saṃda-"; T1 G3,6 "taṃ"

> ¹sa ēvam uktvā bhagavāṃs tatraivāntaradhīyata  
> prāpa ²caiva muhūrtēna svasthānaṃ dēvasan̄jn̄itam /16.27/  

¹ M; et al. "ēvam uktvā sa" (swap)  
² †M1,6,7 T G CE; M5 Dn "cainaṃ"

> tāṃ caiva prakṛtiṃ prāpya ēkībhāvagatō’bhavat  
> athāsya buddhir abhavat punar anyā tadā kila /16.28/  

> sṛṣṭā imāḥ prajāḥ sarvā brahmaṇā paramēṣṭhinā  
> daityadānavagandharvarakṣōgaṇa¹samākulāḥ  
> jātā hīyaṃ vasumatī bhārākrāntā tapas²vinī /16.29/  

¹ M1,6,7 T G CE; M5 D "samākula"  
² all; M1,7 "-vinā"

> bahavō balinaḥ pṛthvyāṃ daityadānavarākṣasāḥ  
> bhaviṣyanti tapōyuktā varān prāpsyanti cōttamān /16.30/  

> avaṡyam ēva taiḥ sarvair varadānēna darpitaiḥ  
> bādhitavyāḥ suragaṇā ṛṣayaṡ ca tapōdhanāḥ  
> tatra ¹nyāyyam idaṃ kartuṃ bhārāvataraṇaṃ mayā /16.31/  

¹ M1,5 G1 B Dn CE PPS; M6,7 T1 G3,6 "nyāyam"

> atha nānāsamudbhūtair vasudhāyāṃ yathākramam  
> nigrahēṇa ca pāpānāṃ sādhūnāṃ pragrahēṇa ca /16.32/  

> ¹iyaṃ tapasvinī satyā ²dhārayiṣyati mēdinī  
> mayā hy ³ēṣāpi dhriyatē pātāḻasthēna bhōginā /16.33/  

¹ †M1,6,7 B Dn; M5 "iyaṃ sarasvatī satyā"; T1 G3,6 "iyaṃ tapasvinī sarvāṃ";
  G1 CE "imāṃ tapasvinīṃ satyāṃ"; PPS "iyaṃ tapasvinī dhārā"  
² ?M5 B Dn; M1,6,7 T1 "dhārayiṣyati mēdinīm"; G1,3,6 CE PPS "dhārayiṣyāmi mēdinīm"  
³ M; et al. "ēṣā hi"

It is clear that M T G B Dn PPS all start with "iyaṃ". The second word is "tapasvinī" in M T G B Dn
PPS except for M5 which says "sarasvatī" (sic). The last word is "satyā" in M B Dn.

> mayā dhṛtā dhārayati jagad dhi sacarācaram  
> tasmāt pṛthvyāḥ paritrāṇaṃ ¹kariṣyē sambhavaṃ gataḥ /16.34/  

¹ ‡all; M5 "kariṣyē’ham ihābhitaḥ"

> ēvaṃ sa cintayitvā tu bhagavān madhusūdanaḥ  
> rūpāṇy anēkāny asṛjat prādurbhāvabhavāya saḥ /16.35/  

> vārāhaṃ nārasiṃhaṃ ca vāmanaṃ mānuṣaṃ tathā  
> ēbhir mayā nihantavyā durvinītāḥ surārayaḥ /16.36/  

> atha bhūyō jagatsraṣṭā bhōḥṡabdēnānunādayan  
> sarasvatīm uccacāra tatra sārasvatō’bhavat /16.37/  

> apāntaratamā nāma sutō vāksambhavō vibhōḥ  
> bhūtabhavyabhaviṣyajn̄aḥ satyavādī dṛḍhavrataḥ /16.38/  

> tam uvāca nataṃ mūrdhnā dēvānām ādir avyayaḥ  
> vēdā¹khyānē ṡrutiḥ kāryā tvayā matimatāṃ vara  
> tasmāt kuru yathājn̄aptaṃ mayaitad vacanaṃ munē /16.39/  

¹ †M1,7 T G B Dn CE PPS; M5 "-khyānaṡru-"; M6 "-khyānaṃ ṡru-"

> tēna bhinnās ¹tadā vēdā manōḥ svāyambhuvē’ntarē  
> tatas tutōṣa bhagavān haris tēnāsya karmaṇā  
> tapasā ca sutaptēna yamēna niyamēna ca /16.40/  

¹ all; M1,6,7 "tatō"

>    ¹ṡrībhagavān uvāca  
> manvantarēṣu putra tvam ēvaṃ lōkapravartakaḥ  
> bhaviṣyasy acalō brahmann apradhṛṣyaṡ ca nityaṡaḥ /16.41/  

¹ CE PPS; most mss. om. (don't know about M.)

> punas tiṣyē ca samprāptē kuravō nāma ¹nāmataḥ  
> bhaviṣyanti ²mahātmānō rājānaḥ prathitā bhuvi /16.42/  

¹ M T1 G1,3,6 PPS; CE "bhāratāḥ"  
² all; M5,7 "mahābāhō"

> tēṣāṃ tvattaḥ prasūtānāṃ ¹kulabhēdō bhaviṣyati  
> ²parasparavināṡārthaṃ tvām ṛtē dvijasattama /16.43/  

¹ †M1,7 T G B CE; M5,6 K1,2,4 D4,9 "kulē"  
² all; M1,6,7 "parasparaṃ vi-"

> tatrāpy anēkadhā vēdān bhētsyasē tapasānvitaḥ  
> kṛṣṇē yugē ca samprāptē kṛṣṇavarṇō bhaviṣyasi /16.44/  

> dharmāṇāṃ vividhānāṃ ca kartā jn̄ānakaras tathā  
> bhaviṣyasi tapōyuktō na ca rāgād vimōkṣyasē /16.45/  

> vītarāgaṡ ca ¹putras tē paramātmā bhaviṣyati  
> mahēṡvaraprasādēna naitad vacanam anyathā /16.46/  

¹ all; M1,6,7 "tē putrāḥ" (swap)

> yaṃ mānasaṃ vai pravadanti ¹putraṃ  
>  pitāmahasyōttamabuddhiyuktam  
> vasiṣṭham agryaṃ tapasō nidhānaṃ  
>  yaṡ cāpi sūryaṃ vyatiricya bhāti /16.47/  

¹ M CE PPS; B Da Dn T1 G3,6 "viprāḥ"

> tasyānvayē cāpi tatō maharṣiḥ  
>  parāṡarō nāma mahāprabhāvaḥ  
> pitā sa tē vēdanidhir variṣṭhō  
>  mahā¹manā vai tapasō nivāsaḥ  
> kānīnagarbhaḥ pitṛkanya²kāyās  
>  tasmād ṛṣēs tvaṃ ³bhavitā ca putraḥ /16.48/  

¹ M T1 G3,6; CE "-tapā"  
² M1,5,7 T1 G3,6; CE "-kāyāṃ"  
³ ?M5 B Dn CE; M1,6,7 "bhāvitāsi"; T1 G3,6 D7 PPS "bhāvitā sa vipraḥ" (see ¹/16.47/)

> bhūtabhavyabhaviṣyāṇāṃ chinnasarvārthasaṃṡayaḥ  
> yē hy atikrāntakāḥ ¹pūrvaṃ sahasrayugaparyayāḥ /16.49/  

¹ ‡all; M5 "pūrvasa-"

> tāṃṡ ca sarvān ¹mayā diṣṭō drakṣyasē ²nātra saṃṡayaḥ  
> punar drakṣyasi cānēkasahasrayugaparyayān /16.50/  

¹ ?M5 D7 PPS; M1,6,7 T1 G6 "mayā diṣṭān"; G3 CE "mayōddiṣṭān"  
² M T1 G3,6 D7; CE "tapasānvitaḥ"

> anādi¹nidhanāl lōkē ²cakravaktraṃ mahāmunē  
> anudhyānān mama munē naitad vacanam anyathā /16.51/  

¹ M T1 G3,6; CE "-nidhanaṃ"; PPS "-nidhanān"  
² M T1 G3,6; PPS "cakravat tvaṃ-"; CE "cakrahastaṃ ca māṃ munē"  

> ṡanaiṡcaraḥ sūryaputrō bhaviṣyati ¹manur mahān  
> tasmin manvantarē caiva saptarṣigaṇa²pūrvakaḥ  
> tvam ēva bhavitā vatsa ³matprasādān na saṃṡayaḥ /16.52/  

¹ ‡all; M5 "munir-"  
² ?M7 CE PPS; M1,6 T1 G3,6 "-pūrvagaḥ"; M5 "-pūjitaḥ"  
³ all; M1,6,7 "samprasādān"

>    ¹vyāsa uvāca  
> ēvaṃ sārasvatam ṛṣim ²apāntaratamaṃ tadā  
> uktvā vacanam īṡānaḥ sādhayasvēty athābravīt /16.53/  

¹ CE PPS; et al. om.  
² ?M5 B Dn CE; M1,6,7 "apāntaratamas"; T1 G3,6 "avāntaratamam"; PPS "avāntaratapāṃ" (sic)

> sō’haṃ tasya prasādēna dēvasya harimēdhasaḥ  
> apāntaratamā nāma tatō jātō’’jn̄ayā harēḥ  
> punaṡ ca jātō vikhyātō vasiṣṭhakulanandanaḥ /16.54/  

> tad ētat kathitaṃ janma mayā pūrvakam ātmanaḥ  
> nārāyaṇaprasādēna ¹tadā ²nārāyaṇāṃṡajam /16.55/  

¹ †M1,6,7 T G; CE "tathā"; M5 "mayā"  
² all; M1,6,7 G1 "nārāyaṇāṅgajam"

> mayā hi sumahat taptaṃ tapaḥ paramadāruṇam  
> purā matimatāṃ ṡrēṣṭhāḥ paramēṇa samādhinā /16.56/  

> ētad vaḥ kathitaṃ ¹sarvaṃ yan ²māṃ pṛcchatha putrakāḥ  
> pūrvajanma bhaviṣyaṃ ca bhaktānāṃ snēhatō mayā /16.57/  

¹ all; M1,6,7 G3 "pūrvaṃ"  
² all; M "mā" (sic)

>    vaiṡampāyana uvāca  
> ēṣa tē kathitaḥ pūrvaṃ sambhavō’smadgurōr nṛpa  
> vyāsasyākliṣṭamanasō yathā pṛṣṭaḥ punaḥ ṡṛṇu /16.58/  

> sāṅkhyaṃ yōgaṃ pan̄carātraṃ ¹vēdāḥ pāṡupataṃ tathā  
> jn̄ānāny ētāni ²rājarṣē ³viddhi nānāmatāni vai /16.59/  

¹ all; M5 "vēdān"  
² all; M5 "rājēndra"  
³ all; M1,7 "vidhinā-"

> sāṅkhyasya vaktā kapilaḥ paramarṣiḥ sa ucyatē  
> hiraṇyagarbhō yōgasya vēttā ¹nānyaḥ purātanaḥ /16.60/  

¹ all; M1,7 "nānyapu-"; M6 "naitau purātanau"

> apāntaratamā ¹nāma vēdācāryaḥ sa ²ucyatē  
> prācīnagarbhaṃ tam ṛṣiṃ pravadantīha kē cana /16.61/  

¹ M T G; CE "caiva"  
² all; M1,6,7 "kīrtitaḥ"

> umāpatir bhūtapatiḥ ṡrīkaṇṭhō brahmaṇaḥ sutaḥ  
> uktavān idam avyagrō ¹jn̄ānaṃ pāṡupataṃ ṡivaḥ /16.62/  

¹ all; M1,6,7 "jn̄ānapāṡupatiṃ ṡivam"

> pan̄carātrasya kṛtsnasya ¹vaktā nārāyaṇaḥ svayam  
> sarvēṣu ca nṛpaṡrēṣṭha jn̄ānēṣv ētēṣu dṛṡyatē /16.63/  

¹ †M1,6,7 T1 G1,3,6 PPS; M5 B Dn CE "vēttā tu bhagavān svayam"

Northern recension says "Bhagavān is the knower of the Pan̄carātra" but Southern one says "Nārāyaṇa
is the speaker of the Pan̄carātra".

> yathā¹gamaṃ ²yathānyāyaṃ niṣṭhā nārāyaṇaḥ prabhuḥ  
> na cainam ³abhijānanti tamōbhūtā viṡāṃ patē /16.64/  

¹ †all; M5 "-yōgaṃ"; B Da "-gataṃ"  
² M T G Cs; CE "yathājn̄ānaṃ"  
³ M5 T1 G1,3,6 D7 PPS; M1 Da "ēva jā-"; M6,7 CE "ēvaṃ jā-"

> tam ēva ṡāstrakartāraṃ pravadanti manīṣiṇaḥ  
> ¹niṣṭhāṃ nārāyaṇam ṛṣiṃ nānyō’stīti ca vādinaḥ /16.65/  

¹ M5 G1,6 CE PPS; M1,6,7 T1 G3 "niṣṭhā nārāyaṇa"

> niḥsaṃṡayēṣu sarvēṣu nityaṃ ¹saṃvasatē hariḥ  
> sasaṃṡayān hētubalān nādhyāvasati mādhavaḥ /16.66/  

¹ M; et al. "vasati vai"

> pan̄carātravidō yē ¹tu yathākramaparā nṛpa  
> ²ēkāntabhāvōpagatās tē hariṃ praviṡanti vai /16.67/  

¹ all; M1,6,7 "ca"  
² all; M5 "ēkānti-"

> sāṅkhyaṃ ca yōgaṃ ca sanātanē dvē  
>  vēdāṡ ca sarvē nikhilēna rājan  
> sarvaiḥ samastair ṛṣibhir ¹niruktō  
>  ²nārāyaṇō viṡvam idaṃ purāṇam /16.68/  

¹ all; M5 "niyuktō"  
² ?M1,6,7 B Dn CE; M5 "nārāyaṇād"; T1 G3,6 "nārāyaṇaṃ"

"This ancient universe is Nārāyaṇa".

> ṡubhāṡubhaṃ karma samīritaṃ yat  
>  pravartatē sarvalōkēṣu kin̄cit  
> tasmād ṛṣēs tad bhavatīti vidyād  
>  divy antarikṣē bhuvi cāpsu cāpi /16.69/  

Thus ends chapter M1,6,7 172; G1 175; T1 G3 177; G6 176; T2 � � �; G2 � � �; M5 207.

It has 69 verses, 145 lines;
    CE 69 verses, 145 lines;
    GP 74 verses, 149 lines;
    Ku 74 verses, ??? lines.

Chapter name:

M      "kṛṣṇa-dvaipāyana-janma-kathanaṃ"
