# Chapter 05

>    bhīṣma uvāca  
> prāpya ṡvētaṃ mahādvīpaṃ nāradō bhagavān ṛṣiḥ  
> dadarṡa tān ēva narān̄ ṡvētāṃṡ candraprabhān̄ ṡubhān /5.1/  

After /5.1/ T1 G3,6 in. CE@825 but all others omit.

> pūjayām āsa ṡirasā manasā taiṡ ca pūjitaḥ  
> didṛkṣur japyaparamaḥ ¹sarvakṛcchravratē sthitaḥ /5.2/  

¹ M; T1 G3,6 CE "-radharaḥ"; G1 "-radhara"; T2 "-rard dayā";
  B0,7,9 Dn "-gataḥ"; G2 PPS "ṡvētāmbaradharaṃ hariṃ" (sic)  

> bhūtvaikāgramanā ¹hṛṣṭa ūrdhvabāhur mahāmuniḥ  
> stōtraṃ jagau sa viṡvāya nirguṇāya mahātmanē /5.3/  

¹ M T G; CE PPS Dn "vipra"

>    nārada uvāca  
> ¹ōṃ namō’stu tē ²dēvadēva (1) niṣkriya (2) ³nirguṇa (3) lōkasākṣin (4) kṣētrajn̄a (5)  
> ananta (6) puruṣa (7) ⁴mahāpuruṣa (8) triguṇa (9) pradhāna (10)  

¹ M1,5,7 (M5,7 om. "ōṃ"); T G "ōṃ namastē"; M6 B Dn CE "namastē"  
² M5 T G CE; M1,6,7 "dēva"; B Dn PPS "dēvadēvēṡa"  
³ M5 T G CE PPS; M1,6,7 om.  
⁴ M B Dn CE; T G PPS om.  

> ¹amṛta (11) vyōman (12) sanātana (13) sadasadvyaktāvyakta (14) ²ṛtadhāman (15)  
> pūrvādidēva (16) vasuprada (17) ³prajāpatē (18) vanaspatē (19)  

¹ M T2 CE Dn; T1 G PPS "amṛtākhya"  
² M5 G1,3,6 B Dn CE; M1,6,7 "ṛtupatē"; T2 "upadhāman"; G2 PPS "tridhāma"  
³ After this CE in. "suprajāpatē" but M G2 PPS om.

> ¹mahāprajāpatē (20) ²vācaspatē (21)  
> ³divaspatē (22) ⁴marutpatē (23) ⁵salilapatē (24) pṛthivīpatē (25) ⁰dikpatē (26)  

¹ M5 T2 G1 CE; M1,6,7 T1 G2,3,6 PPS om. After this T1 G1,3,6 CE in "ūrjaspatē" but T2 G2 M PPS om.  
² ?M5 B Dn CE; T1 G2,3,6 PPS "vākpatē"; T2 G1 M1,6,7 om. After this CE in. "manaspatē; jagatpatē" but T G M PPS om.  
³ M5 T2 G1 CE PPS; T1 G2,3,6 M6,7 om.  
⁴ M5 T G1,3,6 CE; M1,6,7 G2 PPS om.  
⁵ all; M1 G2 PPS om.  

> pūrvanivāsa (27) brahmapurohita (28) ⁰brahmakāyika (29) ¹mahākāyika (30) ²mahārājika (31)  
> ³caturmahārājika (32) ⁴ābhāsvara (33) ⁵mahābhāsvara (34) ⁶saptamahābhāsvara (35) yāmya (36)  

¹ all; B Dn T G2,3,6 om.  
² M5 T2 G1 CE PPS; T1 G3,6 M1,6,7 om.; G2 "mahārājājika";
  PPS combines (29)-(30) as "brahmarudrakāyika"  
³ ?M5 B Dn CE PPS; T G M1,6,7 om.  
⁴ M; CE "ābhāsura"; T G1,2,6 PPS "bhāsvara"; G3 om.  
⁵ M T G1,2,6 PPS; CE Ku "mahābhāsura"; G3 om.  
⁶ M5 T2 G1,6; M1,6,7 "saptāsvara"; T1 "tatpamahāsvara";
  CE "saptamahābhāsura"; G2,3 PPS om.; Dn Ku "saptamahābhāga"  

> mahāyāmya (37) san̄jn̄āsan̄jn̄a (38) tuṣita (39) mahātuṣita (40) pratardana (41)  
> ¹parinirmita (42) ²vaṡavartin (43) ³yajn̄a (44)  

¹ ?M5,6,7 T2 B Dn CE; G2 PPS "-nirvṛtta"; M1,5(marg.) G1 "-kīrtita"; T1 G3,6 om.  
² After this T2 G1,2 CE PPS in. "aparinirmita" but M T1 G3 om.  
³ After this M7 T1 G2,6 Dn CE PPS in. "mahāyajn̄a" but T2 G1,3 M1,5,6 om.

> ¹mahāyajn̄asambhava (45) yajn̄ayōnē (46) yajn̄agarbha (47) yajn̄ahṛdaya (48) ²yajn̄astuta (49)  
> ³yajn̄abhāgahara (50) pan̄cayajn̄adhara (51) ⁴pan̄cakālakartṛgatē (52) pan̄carātrika (53) vaikuṇṭha (54)  

¹ M1,5,6 G1; et al. om. "mahā-"  
² M5 T G1,3,6 B Dn CE; M1 "-samstuta"; G2 M6,7 PPS om.  
³ M5 T G1,3,6 B Dn CE; M1,6,7 G2 PPS om.  
⁴ M5 T G1,3,6 CE; M1,6,7 "pan̄cayajn̄akālakartṛgatē"; G2 PPS om.  

> ¹aparājita (55) ²mānasika (56) paramasvāmin (57) ³susnāna (58) haṃsa (59)  
> paramahaṃsa (60) ⁴paramayājn̄ika (61) ⁵sāṅkhyayōgagata (62) amṛtēṡaya (63) hiraṇyēṡaya (64)  

¹ all; M1,6,7 G2 PPS om.  
² †M1,6,7 B Dn CE; M5 T1 G3,6 "nāvamika"; G1 "anāvamika"; T2 G2 PPS om.   
³ M; G1 B Dn CE "susnāta"; T2 "svasvāta"; T1 G2,3,6 PPS om.  
⁴ ?M5 B Dn CE; T G M1,6,7 PPS om.  
⁵ M; et al. "sāṅkhyayōga"

> vēdēṡaya (65) ¹kuṡēṡaya (66) brahmēṡaya (67) ²padmēṡaya (68)  
> tvaṃ jagadanvayaḥ (69) tvaṃ jagatprakṛtiḥ (70) tavāgnir āsyam (71) ³baḍabāmukhāgniḥ (72) tvam āhutiḥ (73)  

¹ M5 T G1,3,6; M1,6,7 G2 PPS om.  
² After this T G Dn PPS CE in. "viṡvēṡvara" but M om.  
³ M; T2 G2,3,6 "tvaṃ baḍavāmukhāgniḥ"; G1 "vaḍavāmukhāgniḥ"; T1 PPS "tvaṃ baḍabāmukhāgniḥ"; CE "vaḍavāmukhō’gniḥ"

As per MW, "vadavā", "baḍavā", "baḍabā" are variant spellings of "vaḍabā".

> tvaṃ sārathiḥ (74) tvaṃ vaṣaṭkāraḥ (75) tvam ōṅkāraḥ (76) tvaṃ manaḥ (77) tvaṃ candramāḥ (78)  
> ¹tvaṃ cakṣur ādyam (79) tvaṃ sūryaḥ (80) tvaṃ diṡāṃ gajaḥ (81) ²digbhānō (82) ³hayaṡiraḥ (83)  

¹ M5,6,7 Dn CE; T G M1 PPS "-ādyaḥ"  
² After this T G PPS Ku in "vidigbhānō" but M CE om.  
³ M5,6 T G CE PPS; M1,7 "bhayaṡiraḥ" (sic)

> ¹prathamatrisauparṇadhara (84) pan̄cāgnē (85) ²triṇācikēta (86) ṣaḍaṅgavidhāna (87) prāgjyōtiṣa (88)  
> ³vāgjyēṣṭha (89) ⁴sāmagavratadhara (90) ⁵atharvāṅgirasa (91) pan̄camahākalpa (92) ⁶phēnapācārya (93)  

¹ M T2 G1; T1 G3 "-trisuparṇadhāra"; G6 "paramatrisuparṇadhara"; Dn CE Ku "-trisauparṇa"  
² ‡all; M T2 G1 "trinācikēta" (sic)  
³ ?M; et al. "jyēṣṭhasāmaga"  
⁴ ?M5,6; M7 "sāmagaprastara"; M1 Dn CE "sāmikavratadhara"; G1 "sāmikavratacara";
  G2 "sāmajaṅgamacara"; PPS "jyēṣṭhasāmaga maccara"; T G3,6 om.  
⁵ ?M; et al. "atharvaṡiraḥ"  
⁶ all; M1,6,7 "vēnapā-"

> ¹bālakhilya (94) vaikhānasa (95) abhagnayōga (96) abhagnaparisaṅkhyāna (97) yugādē (98)  
> yugamadhya (99) yuganidhana (100) ākhaṇḍala (101) prācīnagarbha (102) kauṡika (103)  

¹ M; et al. "vāla-"  

As per MW, "bālakhilya" is alternate spelling of "vālakhilya".

> puruṣṭuta (104) puruhūta (105) viṡvarūpa (106)   
> ¹anantagatē (107) anantabhōga (108) ²anantādē (109) amadhya (110) ³avyaktamadhya (111) avyaktanidhana (112)  

¹ After this M1,6,7 G2 CE in. "ananta" (=6) but M5 T G1,2,6 om.  
² ?M1,6,7; M5 "anantanābhē"; T G "anādimadhya"; CE splits as "ananta; anāde"  
³ †all; M5,6 om.

> ⁰vratāvāsa (113) samudrādhivāsa (114) ¹yaṡōdhivāsa (115) ²tapōdhivāsa (116) ³damādhivāsa (117) lakṣmyāvāsa (118) ⁴vidyāvāsa (119)  
> ⁵kīrtyāvāsa (120) ⁶ṡrīvāsa (121) sarvāvāsa (122) vāsudēva (123)  

¹ M; et al. "yaṡōvāsa"  
² M5,7; M1,6 "tapōnivāsa"; et al. "tapōvāsa"  
³ M5; B Dn T2 G1 M7 "damāvāsa"; et al. om.  
⁴ †all; M5 "vidyādhilakṣmyāvāsa"  
⁵ M5 T1 G2,3,6 Dn CE; T2 G1 "kīrtivāsa"; M1,6,7 om.  
⁶ M5 T2 G1,3,6 CE; M1,6,7 om.; T1 G3,6 "ṡrīyāvāsa";
  After this T2 G1,3,6 PPS "ṡrīnivāsa"  

> ¹sarvacchandagatē (124) harihaya (125) harimēdha (126) ²yajn̄abhāgahara (127)  
> ³varaprada (128=159) yama (129) niyama (130) mahāniyama (131) ⁴kṛcchra (132)  
> atikṛcchra (133) ⁵mahākṛcchra (134) sarvakṛcchra (135) ⁶niyamadhara (136)  
> ⁷nivṛttadharma (137) ⁸pravacanagatē (138) pravṛttavēdakriya (139) aja (140) sarvagatē (141)  

¹ M; T1 G PPS "-dōgata"; CE "-daka"; T2 "sarvavān̄chāchandagata" (sic.)  
² ?M5,7; M1,6 "yajn̄ayajn̄abhāgahara" (sic.); T G "yajn̄amahābhāgahara";
  CE Ku "mahāyajn̄abhāgahara"; PPS "mahāyajn̄abhāgabhava"  
³ B Dn T2 G1 M1,5,7 CE repeat this as (15?)  
⁴ all; G2 combines (130)-(131) as "-kṛcchrākṛcchra-"  
⁵ M5 T2 G1 CE; M1,6,7 combines (132)-(133) as "-mahāsarvakṛcchrakṛcchra-" (sic);
  PPS om.  
⁶ all; G2 PPS om. "-dhara";  
⁷ M5 T2 CE; M1,6,7 "-ttakarma"; Dn "-ttabhrama"; T1 G1,3,6 PPS "nivṛtti-"  
⁸ all; Dn "-gata"; G2 PPS "-pravacanagaura"

> sarvadarṡin (142) agrāhya (143) acala (144) mahāvibhūtē (145) māhātmyaṡarīra (146)  
> pavitra (147) mahāpavitra (148) hiraṇmaya (149) ¹bṛhat (150) apratarkya (151)  

¹ †M1,6,7 CE; M5 G1 "brahman"; T2 "bṛhan"; T1 G3,6 om.;
  G2 PPS combine (149)-(150) as "brahmavratakṛtya"  

> avijn̄ēya (152) ¹pṛṡnigarbha (153) brahmāgrya (154) prajāsargakara (155) prajānidhanakara (156) mahāmāyādhara (157)  
> citraṡikhaṇḍin (158) varaprada (159=128) purōḍāṡabhāgahara (160) ²gatādhvan (161) chinnatṛṣṇa (162)  

¹ M5,6 T G2,3,6 PPS; M1,7 "praṡnagarbha"; CE G1 om.  
² M5 T2 G1,2 CE PPS; M1,6,7 "gatasva"; Dn "-dhvara"; T1 G3,6 "atṛṣṇaḥ"  

> chinnasaṃṡaya (163) sarvatōnivṛtta (164) brāhmaṇarūpa (165) brāhmaṇapriya (166)  
> viṡvamūrtē (167) ¹mahāmūrtē (168) bāndhava (169) bhaktavatsala (170)  
> ²brahmaṇyadēva bhaktō’haṃ tvāṃ didṛkṣuḥ ³ēkāntadarṡanāya ⁴iti namō namaḥ (171) /5.4/  

¹ M5 G CE PPS; M1,6,7 "sarvamūrtē"; T om.  
² M B Dn CE; T G PPS "brahmaṇyadēvēṡa"  
³ all; M1,6,7 "ēkāgra-"  
⁴ M; B Dn CE om. "iti"; T G1,3,6 PPS "ity ōṃ namaḥ"; G2 "ēkōṃnaṃ" (sic.)

Thus ends chapter M1,6,7 159; G1 162; T1 G6 163; G3 164; T2 176; G2 194; M5 195.

It has 3 verses of 6 lines, 1 paragraph of 171 sentences;  
    CE 3 verses of 6 lines, 1 paragraph of 171 sentences;  
    GP 3 verses of 6 lines, 1 paragraph of 200 sentences;  
    Ku 3 verses of 6 lines, 1 paragraph of 1?? sentences.  

NOTES:

- From the substitution of the name "harihaya" by "harihara", it is clear that T1 G3,6 have Ṡaiva leanings.
- Possible to add "viṡvēṡvara", "paramayājn̄ika", "damādhivāsa"; delete "caturmahārājika"
- T G recension (see git history) and CE contain 169 names when "yama-niyama-..." is not split like
  above (and in Dn GP), even though the number of sentences is 171. Similarly, the Eastern recension
  contains 181 names (±2) after splitting "yama-niyama-..."
