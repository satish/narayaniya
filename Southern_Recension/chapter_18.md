# Chapter 18

Note: T2 G2 are missing for this adhyāya. G1 is damaged upto /17.4/.
Hence, "T G" means "T1 G1,3,6".

>    brahmōvāca  
> ṡṛṇu putra yathā hy ¹ēkaḥ puruṣaḥ ṡāṡvatō’vyayaḥ  
> akṣayaṡ cāpramēyaṡ ca sarvagaṡ ca nirucyatē /18.1/  

¹ M T G PPS; CE "ēṣaḥ"

> na sa ṡakyas tvayā draṣṭuṃ mayānyair vāpi sattama  
> ¹saguṇair nirguṇō ²viṡvō jn̄ānadṛṡyō hy asau smṛtaḥ /18.2/  

¹ M Da; et al. "saguṇō"  
² all; M1,6,7 "dṛṣyō"

> aṡarīraḥ ṡarīrēṣu sarvēṣu nivasaty asau  
> vasann api ṡarīrēṣu na sa lipyati karmabhiḥ /18.3/  

> mamāntarātmā tava ca yē cānyē ¹dēhisan̄jn̄itāḥ  
> sarvēṣāṃ sākṣibhūtō ²hi na grāhyaḥ kēna cit kvacit /18.4/  

¹ M G3; et al. "dēha-"  
² ?M5 T1 G6 PPS; M1,6,7 "-’yaṃ"; G1,3 CE "-’sau"

> viṡvamūrdhā viṡvabhujō viṡvapādākṣināsikaḥ  
> ēkaṡcarati kṣētrēṣu svairacārī yathāsukham /18.5/  

> kṣētrāṇi hi ṡarīrāṇi ¹bījaṃ cāpi ṡubhāṡubhē  
> tāni vētti sa yōgātmā tataḥ kṣētrajn̄a ucyatē /18.6/  

¹ M B Dn; et al. "bījāni ca"

> nāgatir na gatis tasya jn̄ēyā ¹bhūtēṣu kēna cit  
> sāṅkhyēna vidhinā caiva yōgēna ca yathākramam /18.7/  

¹ M B Dn; et al. "bhūtēna"

> cintayāmi gatiṃ ¹tasya ²nāgatiṃ vēdmi cōttamām  
> yathājn̄ānaṃ tu vakṣyāmi puruṣaṃ taṃ sanātanam /18.8/  

¹ ?M5 G1,3 PPS; M1,6,7 T1 G6 CE "cāsya"  
² M T1 G3,6; CE PPS "na gatiṃ"

> tasyai¹katvaṃ ²mahattvaṃ hi sa caikaḥ puruṣaḥ smṛtaḥ  
> mahāpuruṣaṡabdaṃ ³sa bibharty ēkaḥ sanātanaḥ /18.9/  

¹ M5 T1 G1,3 CE PPS; M1,6,7 G6 "-kasya"  
² †all; M5 "mamatvaṃ"  
³ all; M1,6,7 G1 "hi"

> ēkō hutāṡō bahudhā ¹samiddhaḥ  
>  ēkaḥ sūryas ²tapatāṃ yōnir ēkā  
> ³ēkō vāyur bahudhā vāti lōkē  
>  mahōdadhiṡ cāmbhasāṃ yōnir ēkaḥ  
> puruṣaṡ caikō nirguṇō viṡvarūpas  
>  taṃ nirguṇaṃ ⁴puruṣaṃ cāviṡanti /18.10/  

¹ M T G PPS; CE "samidhyatē"  
² ?M5,6 T1 G3,6 Cs PPS; M1,7 G1 CE "tapasāṃ"  
³ ?M1,6,7 G1 PPS CE; M5 T1 G3,6 "ēkā"  
⁴ ?M1,5 G3 CE PPS; T1 G6 M6,7 "puruṡāḥ"; G1 "xxx"

> hitvā guṇamayaṃ sarvaṃ karma hitvā ṡubhāṡubham  
> ubhē satyānṛtē tyaktvā ēvaṃ bhavati nirguṇaḥ /18.11/  

> acintyaṃ cāpi taṃ jn̄ātvā bhāvasūkṣmaṃ catuṣṭayam  
> vicarēd yō yatir ¹yuktaḥ sa gacchēt puruṣaṃ prabhum /18.12/  

¹ M T G; CE "yattaḥ"; PPS "xxx"

> ēvaṃ hi paramātmānaṃ kē cid icchanti paṇḍitāḥ  
> ēkātmānaṃ tathātmānam aparē’dhyātmacintakāḥ /18.13/  

> tatra yaḥ paramātmā hi sa ¹nityō nirguṇaḥ smṛtaḥ  
> sa hi nārāyaṇō jn̄ēyaḥ sarvātmā puruṣō hi saḥ  
> na lipyatē ²phalaiṡ cāpi padmapatram ivāmbhasā /18.14/  

¹ M T G; CE "nityaṃ"  
² ?M1,6,7 B Dn CE; M5 D K7 "karmaphalaiḥ"; T1 G3,6 "malaiṡ cāpi"; PPS "kutaṡ cāpi"; G1 "xxx"  

> karmātmā ¹tv aparō yō’sau mōkṣabandhaiḥ sa yujyatē  
> sasaptadaṡakēnāpi rāṡinā yujyatē ²ca saḥ  
> ēvaṃ bahuvidhaḥ prōktaḥ puruṣas tē yathākramam /18.15/  

¹ †all; M5 "ca parō"  
² M B PPS; G3,6 "-’ti"; T1 CE "hi"

> yat tat kṛtsnaṃ lōkatantrasya dhāma  
>  vēdyaṃ paraṃ bōdhanīyaṃ ¹sabōddhṛ  
> mantā ²mantavyaṃ prāṡitā prāṡitavyaṃ  
>  ghrātā ghrēyaṃ sparṡitā sparṡanīyam /18.16/  

¹ ?CE; M1 "ca vēdyam"; M1,6,7 "sa bōdhyam"; T1 G3,6 PPS "ca bōdhyam"; G1 "subudhyam";
  B Da Dn "sa bōddhā"  
² ?M5 B Dn CE; M1,6,7 T1 PPS "mānyaṃ"; G3,6 "manyaṃ"

> draṣṭā draṣṭavyaṃ ṡrāvitā ṡrāvaṇīyaṃ  
>  jn̄ātā jn̄ēyaṃ ¹saguṇaṃ ²nirguṇaṃ ca  
> yad vai prōktaṃ guṇasāmyaṃ pradhānaṃ  
>  nityaṃ caitac chāṡvataṃ cāvyayaṃ ca /18.17/  

¹ all; M6 "guṇayuṅ"  
² all; M1,6,7 "nirguṇaṡ"

> ¹yad vai sūtē dhātur ādyaṃ nidhānaṃ  
>  ²taṃ vai viprāḥ prava³dantē’niruddham  
> yad vai ⁴lōkē vaidikaṃ karma sādhu  
>  āṡīryuktaṃ tad dhi tasyōpabhōjyam /18.18/  

¹ ?CE; M5 "bibharti sūtē yac ca vai tat pradhānaṃ";
       G1 "bibharti sūtē yaṡ ca vai tat pradhānaṃ";
     M1,6 "yad vai sūtē dhartir ādyaṃ pradhānaṃ";
       M7 "yad vai sūtē dharti cānyaṃ pradhānaṃ";
  T1 G3,6 "dhattē sūtē yat tu cādyaṃ pradhānaṃ";
      PPS "dhattē sūtaṃ dhātur ādyaṃ pradhānaṃ"  
² M T1 G3,6 PPS; CE "tad"  
³ all; M7 "-dantē viruddham"; G1 "-dantīti ruddham"  
⁴ †all; M5 "sūtē" (sic, mimics /18.18a/)

Saṅkarṣaṇa = jīva-tattva; jn̄āna + bala; dissolution  
Pradyumna = manas-tattva; aiṡvarya + vīrya; creation  
Aniruddha = ahaṅkāra-tavva; ṡakti + tējas; maintenance; jāgrata  

https://www.thehindu.com/society/faith/accessible-to-all/article19829733.ece  
https://www.kinchit.org/dharma-sandeha/thread/vyuha-avataram/  
https://www.facebook.com/jeeyarswamy/photos/a.10151535473277205/10157406568427205/?type=3  
https://www.indianetzone.com/48/vyuha_avatara.htm  

Chinnajeeyar Swamiji says Saṅkarṣaṇa = laya, Pradyumna = maintenance, Anidruddha = creation
https://youtu.be/5CMP7i4wKik?t=830

Sirisinahal Venkatacharya Swami also says the same: https://youtu.be/-n6mBruIUzY?t=3240


> dēvāḥ sarvē munayaḥ sādhu dāntās  
>  taṃ ¹prāgvaṃṡē yajn̄a²bhōgair yajantē  
> ahaṃ brahmā ādya īṡaḥ prajānāṃ  
>  tasmāj jātas tvaṃ ca mattaḥ prasūtaḥ  
> mattō jagaj jaṅgamaṃ sthāvaraṃ ca  
>  sarvē vēdāḥ sarahasyā ³hi putra /18.19/  

¹ M T1 G3,6; CE "prāg yajn̄air"; PPS "prāhuḥ ṡrēyō"; G1 "xxx"  
² M T1 G3,6; CE "-bhāgaṃ"  
³ ?M5 B Dn CE; M6 T1 G3,6 D PPS "ca"; M1,7 "xxx"

> caturvibhaktaḥ puruṣaḥ sa krīḍati yathēcchati  
> ¹ēvaṃ sa ēva bhagavān̄ ²jn̄ānēna pratibōdhitaḥ /18.20/  

¹ ?all; M5 G1 B Dn "ēvaṃ sa bhagavān svēna"  
² ?CE B Dn; M1,6,7 "svēna jn̄ānēna bōdhayan";
       T1 G3,6 PPS "svēna jn̄ānēna bōdhayat";
                G1 "tēna jn̄ānēna ṡōdhayēt";
                M5 "jn̄ānēnaivānubōdhayan"  

> ētat tē kathitaṃ putra yathāvad anupṛcchataḥ  
> sāṅkhyajn̄ānē tathā yōgē yathāvad anuvarṇitam /18.21/  

Thus ends chapter M1,6,7 174; G1 177; T1 G6 178; G3 179; T2 � � �; G2 � � �; M5 209.

It has 21 verses, 46 lines;
    CE 21 verses, 46 lines;
    GP 23 verses, ?? lines;
    Ku 23 verses, ?? lines.

Chapter name:

M5      "mahāpuruṣa-kathanaṃ"

M G3,6 explicitly say "nārāyaṇīyaṃ samāptaṃ"
