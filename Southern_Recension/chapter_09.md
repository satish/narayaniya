# Chapter 09

>    arjuna uvāca  
> agnīṣōmau kathaṃ pūrvam ēkayōnī ¹pravartitau  
> ēṣa mē saṃṡayō jātas ²tvaṃ chindhi madhusūdana /9.1/  

¹ M CE; T G "prakīrtitau"  
² M CE; T G "tvaṃ"

>    ṡrībhagavān uvāca  
> hanta tē ¹kathayiṣyāmi purāṇaṃ pāṇḍunandana  
> ātma²tējōdbhavaṃ pārtha ṡṛṇuṣvaikamanā mama /9.2/  

¹ M T G; CE "varta-"  
² M CE; T G K D "tējōbhavaṃ"

> samprakṣāḻanakālē’tikrāntē ¹caturyugāntē  
> ²vyaktē sarvabhūtapraḻayē ³sarvasthāvarajaṅgamē  
> jyōtirdharaṇivāyurahitē’ndhē tamasi jalaikārṇavē lōkē  
> ⁴mamāyam ity aviditē bhūtasaṃjn̄itē’dvitīyē pratiṣṭhitē  
> naiva rātryāṃ na divasē na sati nāsati na vyaktē nāvyaktē vyavasthitē  

¹ M; T G "caturyugasahasrāntē"; CE "caturthē yugasahasrāntē"  
² M T G; CE "avyaktē"  
³ M; B Dn "sarvabhūta-"; T G CE om.  
⁴ M5 T G; M1,6,7 "mamāyayēm ity-"; CE "tama ity ēvābhibhūtē’san̄jn̄akē"  

> ētasyām avasthāyāṃ nārāyaṇaguṇāṡrayād ¹ajarād ²atīndriyād agrāhyād  
>  ³asambhavataḥ satyād ⁴ahiṃsyāl ⁵lalāmādvitīyapravṛttiviṡēṣāt  
> akṣayād ⁶amarād avairād ⁷amūrtitaḥ sarvavyāpinaḥ sarvakartuḥ  
>  ṡāṡvatāt tamasaḥ puruṣaḥ prādurbhūtō harir avyayaḥ /9.3/  

¹ M T G; CE "akṣayād ajarād"  
² M T2 G1,2 PPS; T1 G3,6 CE "anindriyād"  
³ M5 T G2,3,6 PPS; M1,6,7 "� � � � �"; G1 CE "asambhavāt"  
⁴ M5 T2 G1; M1,7 "ahīṃsyā"; PPS "ahiṃsāl"; M6 "ahīṃsrād"; T1 G3,6 CE "ahiṃsrāl"  
⁵ †M1,6,7; M5 "lavadbhir vividhaprav-"; T G PPS "lalāmād vividhavṛtti-";
  CE "lalāmād vivivdhapra-"  
⁶ †M1,6,7 PPS; M5 B Da "amarād ajarād"; CE "ajarāmarād"; T2 G1 "acalād"  
⁷ all; M1,7 "amūrārtitaḥ"

For ⁵, all except M5, say "lalāma" meaning beautiful, charming.  Instead of
"vividha-pravṛtti-viṡēṣāt", M1,6,7 say "advitīya-pravṛtti-viṡēṣāt" without
overall change in the meaning.

For ⁶, all except M5 CE mention "avairād". Since "ajarād" is already
mentioned on the first line, M1,7 don't repeat it.

> nidarṡanam api hy atra bhavati  
> nāsīd ahō na rātrir āsīt  
> na sad āsīn nāsad āsīt  
> tama ēva purastād abhavad viṡvarūpam  
> sā ¹viṡrūpasya ²rajanīty ēvam asyārthō’nu³bhāṣyaḥ /9.4/  

¹ M T G; CE "viṡvasya"  
² M T1 G2,3,6 PPS; T2 G1 CE "jananīty"  
³ M B Da Dn; CE "-bhāṣyatē"

> tasyēdānīṃ tamaḥsambhavasya puruṣasya ¹brahmayōnēr brahmaṇaḥ prādurbhāvē  
>  sa puruṣaḥ prajāḥ sisṛkṣamāṇō nētrābhyām agnīṣōmau sasarja  
> tatō bhūtasargē ²prasṛṣṭē ³kramavaṡāt prajā brahmakṣatram ⁴upātiṣṭhan  
> yaḥ sōmas tad brahma yad brahma tē brāhmaṇāḥ  
> yō’gnis tat kṣatraṃ kṣatrād brahma balavattaram  
> kasmād iti ¹lōkē pratyakṣaguṇam ētat tad yathā  
> brāhmaṇēbhyaḥ paraṃ bhūtaṃ nōtpannapūrvam  
> dīpyamānē’gnau juhōtīti kṛtvā bravīmi  
> bhūtasargaḥ kṛtō brahmaṇā bhūtāni ca pratiṣṭhāpya trailōkyaṃ dhāryata iti /9.5/  

¹ M B Da Dn T G2,6 PPS; CE "padmayōnēr"  
² M T G PPS; CE "pravṛttē"  
³ †M1,6,7; CE "prajākramavaṡād" (swap); M5 "prajāḥ kramaṡō"; T G "prajāḥ karmavaṡāt"  
⁴ M5,6 T G PPS; M1,7 CE "upātiṣṭhat"  
⁵ M T G; CE "lōkapra-"

> mantravādō’pi hi bhavati  
> tvam agnē yajn̄ānāṃ hōtā viṡvēṣām  
> hitō dēvēbhir mānuṣē janē iti  
> nidarṡanaṃ cātra bhavati  
> viṡvēṣām agnē yajn̄ānāṃ hōtēti  
> hitō ¹dēvair ²mānuṣē ³jana iti  
> agnir hi yajn̄ānāṃ hōtā kartā  
> sa cāgnir ⁴brahma /9.6/  

¹ all; M1,6,7 "dēvānāṃ"  
² M T2 G1; B Da Dn "manuṣyair"; G2 "manuṣē"; CE "mānuṣair"  
³ M T G; CE "jagata"  
⁴ all; M1,7 "brāhmaṇaḥ"

> na hy ṛtē mantrād dhavanam asti  
> na vinā puruṣaṃ ¹tapaḥ sambhavati  
> havir mantrāṇāṃ ²pūjā vidyatē dēva³mānuṣāṇāṃ ⁴agnēḥ tvaṃ hōtēti niyuktaḥ  
> yē ca mānuṣā hōtrādhikārās ⁵tē cakruḥ  
> brāhmaṇasya hi yājanaṃ vidhīyatē na kṣatravaiṡyayōr dvijātyōḥ  
> tasmād brāhmaṇā ⁶agnibhūtā yajn̄ān udvahanti  
> ⁷yajn̄ā dēvāṃs tarpayanti dēvāḥ pṛthivīṃ bhāvayanti /9.7/  

¹ †B Dn CE; M5 "mantra sambhavaḥ"; M1,6,7 D4,9 K7 "tamaḥ sambhavaṃ" (see /14.15/);
  T G "tat sambhava iti"  
² †M1,6,7 T G PPS; M5 "pūjayā"; CE B Dn "sampūjā"  
³ M T G; CE "-manuṣyāṇām"  
⁴ †M1,6,7; M5 "na" (sic); T G "ēkaḥ"; CE "anēna"  
⁵ M T G; CE "tē ca"  
⁶ M T; G "agnir bhūtvā"; CE "hy agni-"  
⁷ all; M1,6,7 G2 "yajn̄ād dēvās"

> ṡatapathē’pi hi ⁰brāhmaṇaṃ bhavati  
> agnau samiddhē ¹juhōti yō vidvān brāhmaṇa²mukhēnāhutiṃ juhōti  
> ēvam apy agnibhūtā brāhmaṇā vidvāṃsō’gniṃ bhāvayanti  
> agnir viṣṇuḥ sarvabhūtāny anupraviṡya prāṇān dhārayati  
> api cātra sanatkumāragītāḥ ṡlōkā bhavanti /9.8/  

¹ †all; M5 B0,6,7 Da "juhutē"  
² ?M5 B0,7 K6 D2,3,8; CE "-mukhē dānāhu-"; PPS "-mukhēnāhutīr";
  M1 "-mukhair nāhutir-"; M6 "-mukhēn nāhutir"; M7 "-mukhēr nnāhutīr"  

> viṡvaṃ brahmāsṛjat pūrvaṃ sarvādir niravaskaram  
> brahmaghōṣair divaṃ tiṣṭhanty amarā brahmayōnayaḥ /9.9/  

> brāhmaṇānāṃ matir vākyaṃ karma ṡraddhā ¹manāṃsi ca  
> dhārayanti mahīṃ dyāṃ ca ṡaityād vāry amṛtaṃ yathā /9.10/  

¹ M; et al. "tapāṃsi"

> nāsti satyāt parō dharmō nāsti mātṛ¹samō’ṡramaḥ  
> brāhmaṇēbhyaḥ paraṃ nāsti prētya cēha ca ²bhūtayē /9.11/  

¹ M; et al. "-samō guruḥ"  
² M CE; T G "bhūtalē"

> naiṣām ukṣā vardhatē nōta ⁰vāhā  
>  na gargarō ¹mathyati sampradānē  
> apadhvastā dasyubhūtā bhavanti  
>  yēṣāṃ rāṣṭrē brāhmaṇā vṛttihīnāḥ /9.12/  

¹ M B Da Dn; T G CE "mathyatē"

> vēdapurāṇētihāsa⁰prāmāṇyān nārāyaṇamukhōdgatāḥ  
>  sarvātmānaḥ sarvakartāraḥ sarvabhāvanāṡ ca brāhmaṇāḥ  
> ¹vāksamakālē hi tasya dēvasya ²varadasya ³brahmaṇō brāhmaṇāḥ prathamaṃ  
>  prādurbhūtā brāhmaṇēbhyaṡ ca ṡēṣā varṇāḥ prādurbhūtāḥ  
> itthaṃ ca surāsuraviṡiṣṭā brāhmaṇā ⁴yatra ⁵mayā brahmabhūtēna purā  
>  ⁶surāsuramaharṣayō svayam ēvōtpāditāḥ bhūtaviṡēṣāḥ sthāpitā nigṛhītāṡ ⁰ca /9.13/  

¹ M5 B Da Dn T G; et al. "vāksamakālaṃ"  
² M T G; CE "varapradasya"  
³ M; et al. om.  
⁴ M B Da Dn; T G CE "yadā"  
⁵ all; M1,6,7 "vai"  
⁶ M D K7; CE "svayam ēvōtpāditāḥ surāsuramaharṣayō" (swap)

> ahalyādharṣaṇanimittaṃ ⁰hi gautamād dhariṡmaṡrutām indraḥ prāptaḥ  
> ¹gautamanimittaṃ ²cēndrō muṣkaviyōgaṃ mēṣavṛṣaṇatvaṃ cāvāpa  
> aṡvinōr grahaprati¹ṣēdhāyōdyatavajrasya purandarasya ⁴cyavanēna ⁵stambhitau bāhū   
> kratuvadhaprāptamanyunā ca dakṣēṇa bhūyas tapasā cātmānaṃ  
>  ⁶yōjya nētrākṛtir anyā ⁷lalāṭē rudrasyōtpāditā /9.14/  

¹ †M1,6,7 T G; M5 B Dn Dv CE "kauṡika"  
² all; M1,6,7 "tēnēndrō"  
³ M; CE "-ṣēdhōdyatavaj-"; T1 G1,3,6 PPS "-ṣēdhāyōdyatasya"  
⁴ all; M1,6,7 "cyavanē"  
⁵ M T1 G3,6 B Da Dn; CE PPS "stambhitō bāhuḥ"  
⁶ M T G; CE "saṃyōjya"  
⁷ all; M "� � �"

It is interesting only one southern ms. says "kauṡika" (Viṡvāmitra)
whereas every T G says "gautama". Ramayana /1.48.28/ also accuses Gautama.

> tripuravadhārthaṃ dīkṣām abhyupagatasya  
>  rudrasyōṡanasā ¹◌ ²jaṭāḥ ṡirasa ³utkṛtya kṛtyā ⁴prayuktā  
> tataḥ prādurbhūtā bhujagāḥ  
> tair asya bhujagaiḥ pīḍyamānaḥ kaṇṭhō nīlatām upanītaḥ  
> ⁵pūrvaṃ ca manvantarē svāyambhuvē nārāyaṇahastabandhagrahaṇān nīlakaṇṭhatvam ⁶ēva vā /9.15/  

¹ M5 B Dn CE; M1,6,7 "ātma"; T G "sva"  
² M5 T G B Dn; M1,6,7 "jaṭām ṡirasa"; CE "ṡirasō jaṭā" (swap)  
³ M; T G "utkṛtyōtkṛtya"; CE om. "kṛtya"  
⁴ M; T G CE "prayuktāḥ"  
⁵ M5 T G PPS; M1,6,7 "pūrva man-"; CE "pūrvē"  
⁶ all; M "� � �"

> amṛtōt¹pādanapuraṡcaraṇatām upa²gataṡ cāṅgirasō  
>  bṛhaspatēr upaspṛṡatō na prasādaṃ gatavatyaḥ kilāpaḥ  
> atha bṛhaspatir apāṃ cukrōdha  
> yasmān mamōpaspṛṡataḥ kaluṣībhūtā na ³ca prasādam upagatās  
>  tasmād adyaprabhṛti jhaṣamakaramatsyakacchapa⁰jantusaṅkīrṇāḥ kaluṣībhavatēti  
> tadāprabhṛty āpō yādōbhiḥ saṅkīrṇāḥ saṃvṛttāḥ /9.16/  

¹ M Dn D; T G CE "-pādanē pura-"  
² M T G; CE "-gatasyāṅg-"  
³ M T G; CE om.

> viṡvarūpō vai tvāṣṭraḥ purōhitō dēvānām āsīt svasrīyō’surāṇām  
> sa pratyakṣaṃ dēvēbhyō bhāgam ¹adadat parōkṣam asurēbhyaḥ /9.17/  

¹ †M1,6,7 T CE; M5 "adadāt"; G PPS "avadat"; B Da Dn "adāt"

> atha hiraṇyakaṡipuṃ puraskṛtya viṡvarūpamātaraṃ svasāram asurā varam ayācanta  
> hē svasar ayaṃ tē putras tvāṣṭrō viṡvarūpas triṡirā dēvānāṃ purōhitaḥ  
>  pratyakṣaṃ dēvēbhyō bhāgam ¹adadat parōkṣam asmākam  
> tatō dēvā vardhantē vayaṃ ²kṣīyāmaḥ  
> tad ēnaṃ tvaṃ vārayitum arhasi tathā ³yathāsmān bhajēd iti /9.18/  

¹ †same as ¹/9.17/  
² M5 T2 G1,2 CE PPS; M1,6,7 T1 G3,6 "kṣayāmaḥ"  
³ all; M1,6,7 "yamasmān"

> atha viṡvarūpaṃ nandana¹vanagataṃ mātōvāca  
> putra kiṃ parapakṣavardhanas tvaṃ mātulapakṣaṃ nāṡayasi  
> nārhasy ēvaṃ kartum iti  
> sa viṡvarūpō mātur ⁰vākyam anatikramaṇīyam iti matvā sampūjya hiraṇyakaṡipum ²upāgāt /9.19/  

¹ M T G; CE "vanam upagataṃ"  
² †M1,6,7 (see ²/9.34/); M5 "upāgamat"; T G CE "agāt"

> hairaṇyagarbhāc ca vasiṣṭhād dhiraṇyakaṡipuḥ ṡāpaṃ prāptavān  
> yasmāt tvayānyō vṛtō hōtā tasmād asamāptayajn̄as tvam apūrvāt sattvajātād vadhaṃ prāpsyasīti  
> tacchāpadānād dhiraṇyakaṡipuḥ prāptavān vadham /9.20/  

M1,7 om. /9.20/ to /9.21L1/.

> viṡvarūpō mātṛpakṣavardhanō’tyarthaṃ tapasy abhavat  
> tasya vratabhaṅgārtham indrō bahvīḥ ṡrīmatyō’psarasō ¹niyuyōja  
> tāṡ ca dṛṣṭvā manaḥ kṣubhitaṃ tasyābhavat tāsu cāpsaraḥsu nacirād ²iva saktō’bhavat  
> saktaṃ cainaṃ jn̄ātvāpsarasa ūcur gacchāmahē vayaṃ yathāgatam iti /9.21/  

¹ M5 T1 G3,6 B Dn CE; M1,6,7 "niyōjata"; T2 G2 PPS "yuyōja"; G1 "yuyōji"  
² M T G1,3,6; G2 CE PPS "ēva"

> tās tvāṣṭra uvāca  
> ¹mā gamiṣyatha āsyatāṃ tāvan mayā ²saha ṡrēyō ³vō bhaviṣyatīti  
> tās tam abruvan  
> vayaṃ dēvastriyō’psarasa indraṃ varadaṃ purā prabhaviṣṇuṃ ⁴avṛṇīmahi iti /9.22/  

¹ M; T G CE "kva"  
² all; T G "saha vastum"  
³ M; et al. om.  
⁴ M; T G "vṛṇīmahi"; CE PPS "vṛṇīmaha"

"avṛṇīmahi > √vṛ" (class 9 verb, 1st pl.) "we chose, solicited".

> atha tā viṡvarūpō’bravīd adyaiva sēndrā ⁰dēvā na bhaviṣyantīti  
> tatō mantrān̄ jajāpa  
> tair mantraiḥ prāvardhata triṡirāḥ  
> ēkēnāsyēna sarvalōkēṣu dvijaiḥ kriyāvadbhir  
>  yajn̄ēṣu suhutaṃ sōmaṃ papāv ēkēnāpa ēkēna ⁰sēndrān dēvān  
> athēndras taṃ ⁰vivardhamānaṃ sōmapānāpyāyitasarvagātraṃ dṛṣṭvā cintām āpēdē ¹saha dēvaiḥ /9.23/  

¹ M T G B Dn; CE om.

> ¹tē dēvāḥ sēndrā brahmāṇam abhijagmur ūcuṡ ca  
> viṡvarūpēṇa sarvayajn̄ēṣu suhutaḥ sōmaḥ pīyatē  
> vayam abhāgāḥ saṃvṛttāḥ  
> asurapakṣō vardhatē vayaṃ kṣīyāmaḥ  
> tad arhasi nō vidhātuṃ ṡrēyō yad anantaram iti /9.24/  

¹ M T G B Dn; CE "dēvāṡ ca tē sahēndrēṇa"

> tān brahmōvāca ¹ēṣa ṛṣis ²tapas tapyatē dadhīcaḥ  
> sa yācyatāṃ varaṃ yathā kaḻēvaraṃ jahyāt  
> tasyāsthibhir ³vajraḥ kriyatām iti /9.25/  

¹ M T G; CE "ṛṣir bhārgavas"  
² M5 T1 G1,3,6 B Dn; M1,6,7 T2 G2 PPS om.  
³ M T1 G3,6; T2 G1,2 CE PPS "vajraṃ"

> dēvās ¹tē’gacchanta yatra dadhīcō bhagavān ṛṣis tapas tēpē  
> sēndrā dēvās tam abhigamyōcur bhagavaṃs ²tapaḥ kuṡalam avighnaṃ cēti  
> tān dadhīca uvāca svāgataṃ bhavad⁰bhyaḥ ³ucyatāṃ kiṃ kriyatām  
> yad vakṣyatha tat kariṣyāmīti  
> tē tam abruvan̄ ṡarīraparityāgaṃ lōkahitārthaṃ ⁰bhagavān kartum arhatīti  
> atha dadhīcas tathaivāvimanāḥ sukhaduḥkhasamō mahāyōgī  
>  ⁰ātmānaṃ samādhāya ṡarīraparityāgaṃ cakāra /9.26/  

¹ M T G; CE "tatrāgacchan"  
² M5 B Dn; M1,6,7 "tava"; G2 "tapasaṃ"; PPS "tava sarvaṃ"; CE "tapasaḥ"  
³ M T G B Dn; CE om.

> ¹tasmin paramātmany ²avasṛtē tasya tāny asthīni dhātā ³saṅgṛhya vajram akarōt  
> tēna vajrēṇābhēdyēnāpradhṛṣyēṇa brahmāsthisambhūtēna  
>  ⁴viṣṇupraviṣṭēnēndrō viṡvarūpaṃ jaghāna  
> ṡirasāṃ cāsya chēdanam ⁰akarōt  
> tasmād anantaraṃ viṡvarūpagātramathanasambhavaṃ  
>  ⁵tvaṣṭrōtpāditam ēvāriṃ vṛtram indrō jaghāna /9.27/  

¹ M T G; CE "tasya"  
² M5,6 G1,2 B Dn CE; M1,7 T G3,6 PPS "avasitē"  
³ all; M1,6,7 "parisaṅgṛhya"  
⁴ all; M1,6,7 "viṣṇuprabhāvāviṣṭēna"  
⁵ M1,5,6 G1,2 CE; T G3,6 B Dn D "tvāṣṭr-"

> ¹tasyāṃ ²dvidhābhūtāyāṃ ³brahmavadhyāyāṃ bhayād indrō dēvarājyaṃ  
>  parityajya ⁴apsambhavāṃ ca ⁵ṡītaḻāṃ mānasa⁶sarōgdatāṃ naḻinīṃ ⁷pratipēdē  
> tatra caiṡvaryayōgād aṇumātrō bhūtvā ⁸bisagranthiṃ pravivēṡa /9.28/  

¹ †M1,6,7 T G1 B Dn CE; G2,3,6 PPS "tasya"; M5 "tasmin" (see ¹/9.27/)  
² †M1,6,7 T G; M5 "-bhūtēdad"; B Dn CE "dvaidhībhūtāyāṃ"  
³ †all; M5 D4,9 "-vadhyābhayād"  
⁴ M T1 D7; T2 G2 PPS "hy asambhavāc"; G1 "hy apsu sam-"; B Dn CE "apsu sam-"  
⁵ M5 T1 G3,6 B Dn CE; M1,6,7 "mānaṡītaḻāṃ"; PPS "ṡitatōyāṃ"  
⁶ M; et al. "-sarōgatāṃ"  
⁷ M B Dn; T G CE "prapēdē"  
⁸ all; M1,7 "bisaṃ gran-"

CE "dvaidhībhūtāyāṃ" is unclear. Anyway M T G (incl. M5) use "dvidhā-".
So the reading of M1,6,7 (+ T G) is chosen for ¹, ² and ³.

> atha brahmavadhyābhayapranaṣṭē trailōkyanāthē ¹indrē jagad anīṡvaraṃ babhūva  
> dēvān rajas tamaṡ cāvivēṡa  
> mantrā na prāvartanta  
> ²maharṣīṇāṃ rakṣāṃsi prādurabhavan  
> brahma cōtsādanaṃ jagāma  
> anindrāṡ cābalā lōkāḥ ³supradhṛṣyā babhūvuḥ /9.29/  

¹ M; T G "jagadīṡvarē indrē"; B Dn CE "ṡacīpatau"  
² all; M1,6,7 "maharṣigaṇānāṃ"  
³ M5 B Dn CE; M1 "sudhṛṣṭyā"; M6,7 "sudhṛṣyā"; T1 G1,3,6 PPS "supradharṣā"

As per MW, both "supradhṛṣyā" (M5) and "sudhṛṣya" (M6,7) mean open to be
attacked, undefended, easy to be injured, etc.

> atha dēvā ṛṣayaṡ cāyuṣaḥ putraṃ nahuṣaṃ nāma dēvarājat¹vē’bhiṣiṣicuḥ  
> ²sa nahuṣaḥ pan̄cabhiḥ ṡatair jyōtiṣāṃ lalāṭē jvaladbhiḥ  
>  ⁰sarvatējōharais triviṣṭapaṃ pālayāṃ babhūva  
> atha lōkāḥ ³prakṛtim āpēdirē svasthāṡ ca babhūvuḥ /9.30/  

¹ M5,6 B Dn CE; M1,7 PPS "-vē’bhiṣicuḥ"; T2 "-vē’ty abhiṣēṣicuḥ"; G1 "xxx"; G2 "xxx"  
² M5,6 T1 G2,3,6; M1,7 CE PPS om.  
³ all; M1,7 "prakṛtam"

> athōvāca nahuṣaḥ  
> sarvaṃ māṃ ¹ṡakrōpabhōjyam upasthitam ṛtē ṡacīm iti  
> sa ēvam uktvā ṡacīsamīpam ²agacchad uvāca cainām  
> subhagē’ham indrō dēvānāṃ bhajasva mām iti  
> ³◌ taṃ ṡacī pratyuvāca  
> prakṛtyā tvaṃ dharmavatsalaḥ sōmavaṃṡōdbhavaṡ ca  
> nārhasi para⁴patnīdharṣaṇaṃ kartum iti /9.31/  

¹ M T G; CE "ṡakrōpabhuktam"  
² M5 T2 G1,2 PPS; M1,6,7 CE "agamad"; T1 G3,6 "āgacchad"  
³ all; M1,6,7 in. "sā"  
⁴ all; M1,6,7 "-dāradhar-"

> tām athōvāca nahuṣaḥ  
> aindraṃ padam adhyāsyatē mayā  
> indrasya rājyaratna¹harō’haṃ nātrādharmaḥ kaṡcit tvam indrabhuktēti  
> sā ²cainam uvāca  
> asti mama kin̄cid vratam aparyavasitam  
> tasyāvabhṛthē tvām ⁰upagamiṣyāmi kaiṡ cid ēvāhōbhir iti  
> ⁰sa ³ṡacyaivam abhihitō nahuṣō jagāma /9.32/  

¹ M T G; CE in. "aham" before "indrasya"  
² M T G; CE "tam"  
³ all; M1,6,7 "caivam"

> atha ṡacī duḥkhaṡōkārtā bhartṛdarṡanalālasā nahuṣa¹bhayaparigatā bṛhaspatim upāgacchat  
> sa ca tām ²āgatāṃ dṛṣṭvaiva dhyānaṃ ³praviṡya bhartṛkāryatatparāṃ jn̄ātvā bṛhaspatir uvāca ⁴◌  
> anēnaiva vratēna tapasā cānvitā dēvīṃ varadām upaṡrutim ⁵āhvaya  
> sā ⁶tē indraṃ darṡayiṣyatīti /9.33/  

¹ M T G3,6; G2 PPS "bhayaparītā"; CE "-bhayagṛhītā"  
² †T G; M "� � �"; CE "abhigatāṃ"  
³ all; M1,6,7 "praviṡyaiva"  
⁴ all; M1,6,7 in. "tām"  
⁵ M5 T2 G1,3,6 CE; M1,6,7 "upāhvaya"; T1 G2 "āvāhaya"  
⁶ M B Dn; T G CE "tavēndraṃ"

> sātha mahāniyamam āsthitā dēvīṃ varadām upaṡrutiṃ mantrair āhvayat  
> sōpaṡrutiḥ ¹◌ ²āgāt  
> uvāca cainām iyam asmi ³tvayā bhūtōpasthitā  
> kiṃ tē ⁰riyaṃ karavāṇīti  
> tāṃ mūrdhnā praṇamyōvāca ṡacī bhagavaty arhasi  
>  mē bhartāraṃ darṡayituṃ ⁴◌ tvaṃ satyā matā ⁵satāṃ cēti  
> saināṃ ⁶mānasasarō’nayat  
> tatrēndraṃ bisagranthigatam adarṡayat /9.34/  

¹ M T G; B Dn CE in. "ṡacīsamīpam"  
² M T2 G2 PPS; CE "agāt"  
³ M; T1 G PPS "tvayāhūtōpasthitā"; CE "tvayōpahūtōpasthitā"  
⁴ all; M1,6,7 in. "iti"  
⁵ M T G; CE om.  
⁶ M; et al. "mānasaṃ sa-"

> tām ¹atha patnīṃ kṛṡāṃ ²glānāṃ ³cēndrō dṛṣṭvā cintayāṃ babhūva  
> ahō mama mahad duḥkham idam adyōpagatam  
> naṣṭaṃ hi mām iyam ⁴adya anviṣya ⁵abhyāgamad duḥkhārtēti  
> tām indra uvāca kathaṃ ⁶vartayasīti  
> sā tam uvāca  
> nahuṣō mām ⁷tarkayati  
> kālaṡ cāsya mayā kṛta iti /9.35/  

¹ M T2 G1 B Da Dn; G2 PPS "anāmayaḥ"; T1 G3,6 om.; CE "indraḥ"  
² M5 B Dn CE; M1,6,7 "dīnāṃ"; G1,3,6 "aṅgāṃ"; T1 G2 PPS om.; T2 "gataṃ"  
³ M5 T G B Dn PPS; M1 "indrō"; M6,7 "caivam indrō"; CE "ca"  
⁴ M5 T G PPS; et al. om.  
⁵ M; B Dn "abhyagamad"; T G PPS "padbhyām āgatā"; CE "upāgamad"  
⁶ M5 B Dn CE; M1,6,7 "vartasīti"; T1 G "vartasē iti"; PPS "vā’’gatā iti"  
⁷ ?M5,6 G1,2; G3,6 PPS "tarjayati"; T1 "tarjati"; T2 "kartayati"; CE "āhvayati"

As per MW "tarkayati √tark" means to think of, have in one's mind, intend.
"tarjayati √tarj" means to threaten or frighten.

> tām indra uvāca  
> ¹gaccha  
> nahuṣas tvayā vācyō’pūrvēṇa ²mām ṛṣiyuktēna ³vāhanēna ⁴tvam ⁵adhirūḍha udvahasva  
> indrasya hi mahānti vāhanāni manasaḥ priyāṇy adhirūḍhāni mayā  
> tvam anyēnōpayātum arhasīti  
> saivam uktā ⁰hṛṣṭā jagāma  
> indrō’pi bisagranthim ēvāvivēṡa bhūyaḥ /9.36/  

¹ all; M1,6,7 om.  
² †M1,6,7 CE; M5 "maharṣi-"; T G in. "mām" after "adhirūḍha"  
³ M T G; CE "yānēna"  
⁴ all; M1,6,7 om.  
⁵ M5 T1 G3,6 B Dn CE; M1,6,7 "atirūḍhēna"; G2 PPS "adhigatō"; G1 "adhirūḍhau"

> ¹athēndrāṇīṃ ²dṛṣṭvā’bhyāgatām uvāca nahuṣaḥ  
>  ³yas tē mayā kālaḥ parikalpitaḥ sampūrṇa iti  
> taṃ ṡacy abravīc chakrēṇa yathōktam  
> ⁰sa maharṣiyuktaṃ vāhanam adhirūḍhaḥ ṡacīsamīpam ⁴agacchat /9.37/  

¹ M5 T2 G1,3,6 CE PPS; M1 "athainaṃ"; M6 "nāthaināṃ"; M7 "athainān"  
² M T2 G1,3,6; CE "abhyāgatō dṛṣṭvā" (swap)  
³ ?M1,6,7    "yas tē mayā kālaḥ           parikalpitaḥ sa(m)pūrṇa iti";
   M5        "yas tvayā samayaḥ parikālaḥ parikalpitaḥ sa pūrṇakālaḥ";
   T2 G1 PPS "yō mē tvayā kālaḥ           parikalpitaḥ sa sampūrṇa iti";
   T1 G6     "yō mē tvayā kālaḥ           parikalpitaḥ    sampūrṇa iti";
   B Dn CE   "pūrṇaḥ sa kāla iti"  
⁴  ?M5 G PPS; M6 CE "upāgacchat"; M1,7 "apagacchat"; T "gacchan"

> atha maitrāvaruṇiḥ kumbhayōnir agastyō ¹ṛṣivarō  
>  maharṣīn vikriyamāṇāṃs tān nahuṣēṇāpaṡyat  
> padbhyāṃ ¹cāspṛṡyata  
> tataḥ sa nahuṣam abravīd ³pāpa akāryapravṛtta patasva ⁴◌  
> ⁰sarpō bhava yāvad bhūmir girayaṡ ca tiṣṭhēyus tāvad iti  
> sa maharṣivākyasamakālam ēva tasmād yānād avāpatat /9.38/  

¹ M B Dn; T G CE om.  
² M G3,6; CE "ca tēnāspṛṡyata"  
³ M; CE "akāryapravṛtta pāpa" (swap); T G om. "pāpa"  
⁴ M T G; CE in. "mahīm"

Southern recension says "patasva", meaning "fall down" but Northern recension
explicitly says "patsva mahīm", fall onto earth.

> athānindraṃ punas ¹apy abhavat trailōkyam  
> tatō dēvā ṛṣayaṡ ca bhagavantaṃ viṣṇuṃ ṡaraṇam ²indrārtham abhijagmuḥ  
> ūcuṡ cainaṃ bhagavann indraṃ brahmavadhyābhibhūtaṃ trātum arhasīti  
> tataḥ sa ³varadas tān abravīd aṡvamēdhaṃ ⁴mahāyajn̄aṃ vaiṣṇavaṃ ṡakrō’bhiyajatu  
> tataḥ svaṃ sthānaṃ prāpsyatīti /9.39/  

¹ M; CE "trailōkyam abhavat" (swap) and om. "api"  
² M G1; CE "indrārthē"  
³ all; M1,7 "varuṇas"  
⁴ M T G; CE "yajn̄aṃ"

> ¹tē dēvā ṛṣayaṡ cēndraṃ nāpaṡyan yadā tadā ṡacīm    
>  ūcur gaccha subhagē indram ānayasvēti  
> sā ⁰punas ²tasmāt sarasaḥ ³indramānayat  
> indraṡ ca ⁴tasmāt sarasaḥ ⁵samutthāya bṛhaspatim abhijagāma  
> bṛhaspatiṡ cāṡvamēdhaṃ mahā⁶kratuṃ ṡakrāyāharat  
> ⁷tatra ca kṛṣṇasāraṅgaṃ mēdhyam aṡvam utsṛjya ⁸pāvanaṃ tam ēva  
>  kṛtvā indraṃ marutpatiṃ bṛhaspatiḥ svasthānaṃ prāpayām āsa /9.40/  

¹ M; T G om.; CE PPS "tatō"  
² M; T G CE "tat saraḥ"  
³ †M1,6,7; M5 "samabhigacchat"; T G1,3,6 "indram āhvayat";
  G2 PPS "tam indram arcayat"; CE "samabhyagacchat"  
⁴ all; M1,6,7 "asmāt"  
⁵ M1,6,7 T1 G1,3,6 B CE; M5 Dn "pratyutthāya"; T2 G2 PPS "satvaramutthāya"  
⁶ all; M1,7 "-yajn̄aṃ"  
⁷ M1,6,7 T G1,3,6; M5 B Dn om. "ca"; G2 CE PPS "tataḥ"  
⁸ M T G; CE "vāhanaṃ"

> tataḥ sa dēvarāḍ dēvair ṛṣibhiṡ ¹ca stūyamānas triviṣṭapasthō niṣkalmaṣō babhūva  
> ⁰brahmavadhyāṃ caturṣu sthānēṣu vanitāgnivanaspatigōṣu ²vyabhajat iti  
> ēvam indrō brahmatējaḥprabhāvōpabṛṃhitaḥ ṡatruvadhaṃ ³ca svasthānaṃ ⁴ca prāpitaḥ /9.41/  

¹ M T G1,3,6; G2 CE PPS om.  
² M5,6 T G1,3,6 CE; M1,7 "vyabhajaṃ";
  After this T G PPS Cv in. CE@874 but M om.  
³ †M1,6,7 T G; CE subst. "kṛtvā"; M5 om.; After "ca", T G in. "kṛtvā"  
⁴ M; et al. om.

> ākāṡagaṅgāgataṡ ca purā bharadvājō maharṣir ¹upaspṛṡaṃs trīn ²vikramān kramatā viṣṇu³nābhyāsāditaḥ  
> sa bharadvājēna ⁴satalēna pāṇinōrasi tāḍitaḥ ⁵brahmaṇyadēvaḥ salakṣaṇōraskaḥ saṃvṛttaḥ /9.42/  
  
¹ ?M5 T G3,6; M1,6,7 "upaspṛṡat"; B Dn PPS "upāspṛṡat"; G1 CE "upāspṛṡaṃs"  
² M T1 G1,3,6; T2 G2 PPS om.; CE "kramān"  
³ ?M5 B Dn CE; M1,6,7 "-nātyāsāditaḥ"; T G3,6 PPS "-nāsāditaḥ"; G1 "-nāsvāditaḥ"  
⁴ M G1; T1 G3,6 PPS "svatalēna"; T2 "sthalēna"; CE "sasalilēna"  
⁵ M; et al. om.

Before /9.42/ B6,8 in. "ṡrībhagavān uvāca".

> bhṛguṇā ¹ca maharṣiṇā ṡaptō’gniḥ sarvabhakṣatvam upanītaḥ /9.43/  

¹ M; et al. om.

> aditir vai dēvānām annam apacad ¹ētad bhuktvā ²dēvā āsurān haniṣyantīti  
> tatra budhō vratacaryāsamāp³tāv āgacchat  
> aditiṃ ⁴cāyacad bhikṣāṃ dēhīti  
> tatra dēvaiḥ pūrvam ētat prāṡyaṃ nānyēnēty aditir bhikṣāṃ nādāt  
> atha bhikṣāpratyākhyānaruṣitēna ⁵budhēna brahmabhūtēna vivasvatō  
>  dvitīyē janmany aṇḍasan̄jn̄itasyāṇḍaṃ māritam adityāḥ  
> ⁶tēna ⁷mārtāṇḍō vivasvān abhavac chrāddhadēvaḥ /9.44/  

¹ all; M1,6,7 "tē tad"  
² M5 T2 G PPS; T1 "dēvāsurādīn"; M1,6,7 CE om. "dēvā"  
³ †M1,6,7 G2 CE PPS; M5 "-tāv agacchat"; T G1 "-tō’vagacchat"; G6 "-tō’gacchat"  
⁴ M G3 PPS; T G1,6 "tāṃ cāyācad"; CE "cāvōcad"  
⁵ all; M1,6 om.  
⁶ M PPS; T1 G3,6 "tasmān"; T2 G1 CE "sa"  
⁷ M T2 G1 PPS; CE "mārtaṇḍō"

> dakṣasya vai duhitaraḥ ṣaṣṭir āsan  
> ¹tāsāṃ kāṡyapāya trayōdaṡa prādād daṡa dharmāya daṡa manavē saptaviṃṡatim indavē ²ca  
> tāsu tulyāsu nakṣatrākhyāṃ gatāsu sōmō rōhiṇyām ³abhyadhikaṃ prītimān abhūt  
> tatas tāḥ ṡēṣāḥ patnya īrṣyāvatyaḥ ⁴pitṛsamīpaṃ gatvēmam arthaṃ ṡaṡaṃsuḥ  
> bhagavann asmāsu tulyaprabhāvāsu sōmō rōhiṇīm adhikaṃ ⁵abhajat  
> sō’bravīd yakṣmainam āvēkṣyatīti /9.45/  

¹ M T G; CE "tābhyaḥ kaṡyapāya"  
² M; et al. om.  
³ †M1,6,7 B Dn PPS; M5 G1 "adhikaṃ-"; T1 G3,6 "prītimān adhikam abhūt";
  CE "³abhyadhikāṃ prītim akarōt"  
⁴ M1,6,7 T G PPS; M5 CE "pituḥ sa-"  
⁵ M T1 G1,3,6; T2 G2 PPS CE "bhajatīti"

> dakṣaṡāpāc ¹ca sōmaṃ rājānaṃ yakṣmāvivēṡa  
> sa yakṣmaṇāviṣṭō dakṣam agamat  
> dakṣaṡ cainam abravīn na samaṃ vartasa iti  
> ²tadā ṛṣayaḥ sōmam abruvan ³kṣīyasē yakṣmaṇā  
> paṡcimasyāṃ diṡi samudrē hiraṇyasarastīrtham  
> tatra gatvātmānam abhiṣēcayasvēti  
> athāgacchat sōmas tatra hiraṇyasarastīrtham  
> gatvā cātmanaḥ snapanam akarōt  
> snātvā cātmānaṃ pāpmanō mōkṣayām āsa  
> tatra ⁴cāvabhāsitas tīrthē yadā sōmas tadāprabhṛti tīrthaṃ tat prabhāsam iti nāmnā khyātaṃ babhūva  
> tacchāpād adyāpi kṣīyatē sōmō’māvāsyāntarasthaḥ  
> paurṇamāsīmātrē ⁵viṣṭhitō mēghalēkhā⁶praticchannaṃ vapur darṡayati  
> ⁷sa mēghasadṛṡaṃ varṇam agamat tad asya ṡaṡa⁸lakṣaṇaṃ vimalam abhavat /9.46/  

¹ M5 T2 G1 K PPS; et al. om.  
² M5 T G1,3,6; M1,6 "tadātrarṣyaḥ"; CE PPS "tatrarṣayaḥ"  
³ all; M1,7 "kṣīyatē sa"  
⁴ all; M1,6,7 "cābhāsitas"  
⁵ M T G; CE "-’dhiṣṭhitō"  
⁶ M5 T2 G1,2 CE PPS; M1,6,7 T1 G3,6 "-praticchannavapur-"  
⁷ M T1 G1,3,6; G2 CE PPS om.  
⁸ M1,6,7 T G PPS; M5 CE "-lakṣma"

> sthūlaṡirā maharṣir mērōḥ prāguttarē digbhāgē tapas tēpē  
> tasya tapas tapyamānasya sarvagandhavahaḥ ṡucir vāyur vivāyamānaḥ ṡarīram aspṛṡat  
> sa tapasā tāpitaṡarīraḥ kṛṡō vāyunōpavījyamānō hṛdayaparitōṣam ¹upāgamat  
> tatra ²kila tasyānilavyajanakṛtaparitōṣasya sadyō vanaspatayaḥ puṣpaṡōbhāṃ na  
>  darṡitavanta iti sa ētān̄ ³ṡaṡāpa na ⁴sarvakāla⁵puṣpaphalavantō bhaviṣyathēti /9.47/  

¹ M1,5,7 T G PPS; M6 "avāgamat"; CE "agamat" (see ²/9.19/)  
² M B Da Dn; T G CE om.  
³ M5 T1 G1,3,6 B Dn CE; M1,6,7 T2 G1 PPS "sa ṡasāpainān"  
⁴ M; et al. "sarvakālaṃ pu-"  
⁵ M T G PPS; CE "puṣpavantō"

> nārāyaṇō lōkahitārthaṃ ¹baḍabāmukhō nāma ²ṛṣiḥ purā babhūva  
> tasya mērau tapas tapyataḥ samudra āhūtō nāgataḥ  
> tēnāmarṣitēnātmagātrōṣmaṇā samudraḥ stimitajalaḥ kṛtaḥ  
> svēda³prasyandanasadṛṡaṡ cāsya lavaṇabhāvō janitaḥ  
> uktaṡ cāpēyō bhaviṣyasi ⁴iti  
> ētac ca ⁵◌ tōyaṃ ¹baḍabāmukhasan̄jn̄itēna ⁶◌ pīyamānaṃ ⁷◌ bhaviṣyati ⁴iti  
> tad ētad adyāpi ⁸baḍabāmukhasan̄jn̄itēn⁹ānivartinā tōyaṃ sāmudraṃ pīyatē /9.48/  

¹ ‡PPS (see ³/5[71]/); M1,5,7 G1,2 "baḍavā-"; M6 "� � �"; CE "vaḍavā-"  
² M T G; CE "maharṣiḥ purābhavat"  
³ all; M1,6,7 "-prajasyan-"  
⁴ M1,6,7 T G1,3,6 PPS; M5 G2 CE om.  
⁵ M5 T G1-3; M1,6,7 G6 CE in. "tē"  
⁶ all; T G in. "agninā" (see ⁸ below)  
⁷ M T G; CE in. "madhuraṃ"  
⁸ ‡all; M1,6,7 G2 "baḍavāmukhēna agninā"; M5 "baḍavāmukhasan̄jn̄itēna agninādyaṡvaṡ ca"  
⁹ M G2 K V Da; T G CE B Dn "-ānuvartinā"

> himavatō ¹girēr duhitaram umāṃ ²prāk kanyāṃ rudraṡ cakamē  
> bhṛgur api ca maharṣir himavantam ³āgamyābravīt kanyām umāṃ ⁴dēhīti  
> tam abravīd dhimavān abhilaṣitō ⁵mē varō rudra iti  
> tam abravīd bhṛgur yasmāt tvayāhaṃ ⁶kanyāvaraṇakṛtabhāvaḥ pratyākhyātas  
>  tasmān ⁷tvaṃ himānāṃ bhājanaṃ ⁸bhaviṣyasīti  
> adyaprabhṛty ētad avasthitam ṛṣivacanam /9.49/  

¹ †all; M5 om.  
² M T G; CE om.  
³ †all; M5 "upagamya"  
⁴ M1,6,7 T G1,3,6; M5 G2 PPS "dātum arhasi"; CE "mē dēhīti"  
⁵ M T G; CE om.  
⁶ M5 T2 G1,2 CE; M1,6,7 "kanyā(ṃ) varaṇakṛtaḥ pra-"; T1 G3,6 "kanyāvaraṇē vṛttabhā-";
  PPS "kanyā na dattā varaṇakṛtabhā-"  
⁷ M; G1,2 CE PPS "na ratnānāṃ bhavān"; G3,6 "na narasaṃjn̄ā nāma"; T1 "tvaṃ naranāmasaṃjn̄ānāṃ";
  T2 "tvam annarajn̄ā nāma" (sic)  

Bhṛgu's curse in CE PPS "you shall be devoid of gems (ratna)";
M "you shall be receptable of snow"

> ¹tad ēvaṃvidhaṃ māhātmyaṃ brāhmaṇānām  
> kṣatram api ²ca ṡāṡvatīm avyayāṃ ³ca pṛthivīṃ patnīm abhigamya bubhujē  
> tad ētad ⁴brahmakṣatram agnīṣōmīyaṃ  
> tēna jagad dhāryatē /9.50/  

¹ †all; M5,6 G1 "tadaivaṃvidhamāhāt-"; T2 "tad ēva vidhir mā-"  
² M B Dn K; T G CE om.  
³ M T2 G1,2 B Dn PPS; T1 G3,6 CE om.  
⁴ M T G; CE "brahmāgnīṣōmīyam"

M G1 B Dn D K (24 mss.)  **do not** end the chapter here.

CE ends chapter 329; PPS 330; T1 G6 168; T2 181; G2 198; G3 169.

> ¹ity ucyatē  
> sūryācandramasau ṡaṡvat kēṡair mē aṃṡusan̄jn̄itaiḥ  
> bōdhayaṃs ²sthāpayaṃṡ caiva jagad ³uttiṣṭhatē pṛthak /9.51/  

¹ M; B Dn "ucyatē"; CE PPS "ṡrībhagavān uvāca"  
² ?M6,7 (see ¹/9.52/); M1,5 B Dn CE "tāpayaṃs"; T G PPS "svāpayaṃṡ"  
³ M G2 B Dn PPS; T CE "uttiṣṭhatē"

> bōdhanāt ¹sthāpanāc caiva ²jagatō harṣaṇaṃ bhavēt  
> ³agnīṣōmakṛtair ēbhiḥ karmabhiḥ pāṇḍunandana  
> hṛṣīkēṡō’ham īṡānō varadō lōkabhāvanaḥ /9.52/  

¹ M; CE "tapanāc"; T G PPS "svāpanāc"  
² all; M1,7 "jagatē"  
³ all; M1 "āgnīṣōma"; M7 "agnīṣōmaiḥ"

> iḻōpa¹hūtaṃ ²gēyēṣu harē bhāgaṃ kratuṣv aham  
> varṇaṡ ca mē hariṡrēṣṭhas tasmād dharir ahaṃ smṛtaḥ /9.53/  

¹ M5 T G; M1,7 "-bhūtaṃ"; M6 "ity ēva bhūtaṃ" (sic); CE "-hūtayō-"  
² M; CE "-hutayōgēna"; T G "yōgēṣu"

> dhāma sārō hi lōkānām ṛtaṃ caiva ¹avadhāritam  
> ṛtadhāmā tatō vipraiḥ satyaṡ cāhaṃ prakīrtitaḥ /9.54/  

¹ M T G; CE "vicāritam"

> naṣṭāṃ ca dharaṇīṃ pūrvam avindaṃ vai ¹guhāgatām  
> gōvinda iti ²tēnāhaṃ dēvair vāgbhir abhiṣṭutaḥ /9.55/  

¹ M6 T G CE PPS; M1 "guhāgatam"; M5 "guhāṃ gatām"; M7 "guṇāgatam" (sic)  
² M T G B Dn PPS; CE "māṃ dēvā vāgbhiḥ samabhituṣṭuvuḥ"

> ṡipiviṣṭēti ¹yajn̄ākhyā ²hīnarōma ³ca yad bhavēt  
> tēnāviṣṭaṃ hi yat kin̄cic chipiviṣṭaṃ hi tat smṛtam /9.56/  

¹ M T2 G2; CE "cākhyāyāṃ"  
² M; T G CE PPS "hīnarōmā"  
³ M; CE "ca yad"; T G "ṡipir"

> yāskō mām ṛṣir avyagrō naikayajn̄ēṣu gītavān  
> ṡipiviṣṭa iti hy asmād guhyanāmadharō hy aham /9.57/  

> stutvā māṃ ṡipiviṣṭēti yāskō ¹hy ṛṣir udāradhīḥ  
> matprasādād ²athō naṣṭaṃ niruktam ³abhijagmivān /9.58/  

¹ M G2 PPS; T1 G3,6 "tv"; CE om.  
² M G2 PPS; et al. "adhō"  
³ M5 G2 CE PPS; M1,6,7 "api jag-"; T1 G3,6 "upa-"

> na hi jātō na jāyē’haṃ na janiṣyē kadācana  
> kṣētrajn̄aḥ sarvabhūtānāṃ tasmād aham ajaḥ smṛtaḥ /9.59/  

> nōktapūrvaṃ mayā kṣudram aṡlīlaṃ vā ¹kathan̄cana  
> ṛtā ²brahmasutā sā mē satyā dēvī sarasvatī /9.60/  

¹ M1,6,7 T G3,6 PPS; M5 G2 CE "kadācana"  
² ‡CE; M5 "-sutā yā"; M1,6,7 "brahmasarūpā"; T G PPS "brahmasvarūpā"

> sac cāsac caiva kauntēya mayāvēṡitam ātmani  
> pauṣkarē brahmasadanē satyaṃ mām ṛṣayō viduḥ /9.61/  

> sattvān na cyutapūrvō’haṃ sattvaṃ vai viddhi matkṛtam  
> ¹janmanīhābhavat sattvaṃ paurvikaṃ mē ²nṛpōttama /9.62/  

¹ †M1,6,7 B Dn CE; T2 G2 PPS "janma hitvā’bhavat sat-";
   M5 "janmahīnō’bhavat bhavaṃ"; T1 G3,6 "janma hitvā bhavēt sattvāt"  
² M T G; CE "dhanan̄jaya"  

> nirāṡīḥkarma¹saṃyuktaṃ sātvataṃ ²cāpy akalpayam  
> sātvatajn̄āna³dṛṣṭō’haṃ ⁴sātvataḥ sātvatīpatē /9.63/  

¹ ‡T1 G3,6 CE PPS; M5 "-samyuktān"; M1,6,7 "-samyuktāḥ";
  T2 G2 "-samyuktaṡ ca"  
² M5 T G PPS; M1,6,7 "-akalpayat"; CE "māṃ prakalpaya"  
³ all; M1,6,7 "-tuṣṭō"  
⁴ ?M5 T2 Cv; G2 PPS "sātvataḥ sātvatē pathē";
             M1,6,7 "sātvatē pathi sātvataḥ";
             T1 G3,6 CE "sātvataḥ sātvatāṃ patiḥ"  

> kṛṣāmi mēdinīṃ pārtha bhūtvā ¹kṛṣṇāyasō ²mahān  
> kṛṣṇō varṇaṡ ca mē yasmāt tasmāt kṛṣṇō’ham arjuna /9.64/  

¹ M T G; CE "kārṣṇāyasō"  
² M5 B Dn CE; M1,6,7 "mahat"; T G "halaḥ"

> mayā saṃṡlēṣitā bhūmir adbhir vyōma ca vāyunā  
> vāyuṡ ca tējasā sārdhaṃ vaikuṇṭhatvaṃ tatō mama /9.65/  

> nirvāṇaṃ paramaṃ ¹viddhi dharmō’sau para ucyatē  
> ²tasmān na cyutapūrvō’ham acyutas tēna karmaṇā /9.66/  

¹ M T G; B Dn "brahma"; CE "saukhyaṃ"  
² all; M1,7 "tasmān"; G2 "satyān"

> ¹nabhaṡ ca pṛṭhivī cōbhē viṡrutē viṡvalaukikē  
> tayōḥ sandhāraṇārthaṃ ²ca ³prāgadhaḥ khaṃ saman̄jasā /9.67/  

¹ M; CE PPS "pṛthivīnabhasī" (swap)  
² M; et al. "hi"  
³ ?M5; M1,6,7 "-samun̄janāḥ"; PPS "-svamun̄jasā";
  CE "mām adhōkṣajam an̄jasā"  

> ¹niruktavēdaviduṣō yē ca ṡabdārthacintakāḥ  
> tē māṃ gāyanti prāgvaṃṡē adhōkṣaja iti ²smṛtaḥ /9.68/  

¹ M T G6; CE "niruktaṃ vē-"  
² M T G3,6; CE "sthitiḥ"

> ṡabda ēkamatair ēṣa vyāhṛtaḥ paramarṣibhiḥ  
> nānyō hy adhōkṣajō lōkē ṛtē nārāyaṇaṃ prabhum /9.69/  

> ¹ghṛtaṃ mamārciṣō lōkē ⁰jantūnāṃ prāṇadhāraṇam  
> ghṛtārcir aham avyagrair vēdajn̄aiḥ parikīrtitaḥ /9.70/  

¹ all; M1,6,7 "kṛtaṃ madarciṣā"  
² all; M1,6,7 "ghṛtaṃ madarcir avyagrair ghṛtārcir vaidikaiḥ smṛtaḥ"

> trayō hi dhātavaḥ khyātāḥ karmajā iti ¹mē smṛtāḥ  
> pittaṃ ²vāyuṡ ca ṡlēṣmā ca ēṣa saṅghāta ucyatē /9.71/  

¹ M T G; CE "ca"  
² M1,6,7 T G PPS; M5 B Dn CE "ṡlēṣmā ca vāyuṡ" (swap)

> ētaiṡ ca dhāryatē jantur ētaiḥ kṣīṇaiṡ ca kṣīyatē  
> āyurvēdavidas tasmāt tridhātuṃ māṃ pracakṣatē /9.72/  

> vṛṣō hi bhagavān dharmaḥ khyātō lōkēṣu bhārata  
> ¹naikhaṇṭavapadākhyātaṃ viddhi māṃ vṛṣam uttamam /9.73/  

¹ M; CE "naighaṇṭuka-"; T2 G2,3,6 PPS "ēvaṃ vēdavidākhyātaṃ"

> kapir varāhaḥ ṡrēṣṭhaṡ ca dharmaṡ ca vṛṣa ucyatē  
> tasmād vṛṣākapiṃ prāha ¹kaṡyapō māṃ prajāpatiḥ /9.74/  

¹ ?M5,6 B Dn CE; M1,7 T G PPS "kāṡyapō"

> na cādiṃ na madhyaṃ ¹na cāntaṃ kadācid  
>  ²vidantē dvijā mē surāṡ ³cāsurāṡ ca  
> anādyō hy amadhyas tathā cāpy anantaḥ  
>  pragītō’ham īṡō vibhur lōkasākṣī /9.75/  

¹ M1,6,7 T G; M5 "tathā caiva nāntaṃ"; CE "tathā naiva cāntaṃ"  
² M; CE "kadācid vidantē"  
³ M5 T1 G3,6 CE; M1,6,7 T2 G2 PPS "sāsurāṡ ca"

> ṡucīni ṡravaṇīyāni ṡṛṇōmīha dhanan̄jaya  
> na ca pāpāni gṛhṇāmi tatō’haṃ vai ṡuciṡravāḥ /9.76/  

> ēkaṡṛṅgaḥ purā bhūtvā varāhō ¹nandivardhanaḥ  
> imām uddhṛtavān bhūmim ēkaṡṛṅgas tatō hy aham /9.77/  

¹ M T G B Dn; CE "divyadarṡanaḥ"

> ¹tathaivāhaṃ trikakudō vārāhaṃ rūpam āsthitaḥ  
> ²trikakutvēna vikhyātaḥ ṡarīrasya ³pramāpanāt /9.78/  

¹ M; CE "tathaivāsaṃ"  
² M5 T G; M1,7 "trikuttēna vivikhyātaḥ"; CE "trikakut tēna"  
³ M5 T G3,6; M1,7 "pratāpanāt"; G2 PPS "pramāpaṇāt"; CE "tu māpanāt"

> virin̄ca iti yaḥ prōktaḥ kapilajn̄ānacintakaiḥ  
> sa prajāpatir ēvāhaṃ cētanāt sarvalōkakṛt /9.79/  

> vidyāsahāyavantaṃ mām ādityasthaṃ sanātanam  
> kapilaṃ prāhur ācāryāḥ sāṅkhyā niṡcitaniṡcayāḥ /9.80/  

> hiraṇyagarbhō dyutimān ēṣa ¹yac chandasi ²ṣṭutaḥ  
> yōgaiḥ sampūjyatē nityaṃ sa ēvāhaṃ vibhuḥ smṛtaḥ /9.81/  

¹ M G2,3,6; CE "yaṡ"  
² M G2; CE "stutaḥ"

> ēkaviṃṡatiṡākhaṃ ca ṛgvēdaṃ māṃ pracakṣatē  
> sahasraṡākhaṃ ¹sāmaṃ ca yē vai vēdavidō janāḥ  
> gāyanty āraṇyakē ²viprā madbhaktās tē’pi durlabhāḥ /9.82/  

¹ M; CE "yat sāma"  
² M5 B Dn D K CE; M1,6,7 "dhīrā"; T G om. /9.82L3/

> ¹ṣaṭpan̄cāṡatam aṣṭau ca ²saptatriṃṡatam ity uta  
> yasmin̄ ṡākhā ³yajurvēdē sō’ham ⁴ādhvaryavē smṛtaḥ /9.83/  

¹ M CE; T G "ṣatpan̄cāṡat tathāṣṭau ca"  
² M5,6 CE; M1,7 "saptatridaṡam ity api";
  T G "saptatriṃṡat tathaiva ca"  
³ all; M1,6,7 "yajurvēdaḥ"  
⁴ all; M1,6,7 "adhvaryavaḥ"

Different number for Kṛṣṇa and Ṡukla Yajurvēda:

ṣaṭpan̄caṡatam 56 + aṣṭau 8 + saptatriṃṡa (or saptatridaṡa) 37 = 101

Complete list
[here](https://animeshnagarblog.wordpress.com/2015/06/23/a-list-of-hundred-and-one-yajurveda-shakhas/comment-page-1/)
and [here](https://studylib.net/doc/18017505/table-of-vedas-and-their-branches).

> pan̄cakalpam atharvāṇaṃ kṛtyābhiḥ paribṛṃhitam  
> kalpayanti hi māṃ viprā ¹atharvāṇavidas tathā /9.84/  

¹ M6 G2 CE PPS; T "ātharvaṇa-"; M5 G3,6 "atharvaṇa-"; M1,7 "atharvāda-" (sic)

> ṡākhābhēdāṡ ca yē kē cid yāṡ ca ṡākhāsu gītayaḥ  
> svaravarṇasamuccārāḥ sarvāṃs tān viddhi matkṛtān /9.85/  

> ¹yas tad dhayaṡiraḥ pārtha ²samudēti varapradam  
> sō’ham ēvōttarē bhāgē padakramavibhāgavit /9.86/  

¹ M; et al. "yat"  
² all; M1,6,7 "samudrēti"  
³ M T G; CE "kramākṣara-"

> rāmādēṡitamārgēṇa matprasādān mahātmanā  
> pān̄cālēna kramaḥ prāptas tasmād bhūtāt sanātanāt  
> ¹bābhravyagōtraḥ sa babhau ²prathamaḥ kramapāragaḥ /9.87/  

¹ ?M5 B Dn CE; M6 "bāhuvya-"; M1,7 "bahuvya-"; T G "bāhulya-"  
² M5 T2 G1,2 CE PPS; T1 G3,6 M1,6,7 "prathamakra-"

> ¹nārāyaṇād varaṃ labdhvā prāpya yōgam anuttamam  
> kramaṃ praṇīya ṡikṣāṃ ca praṇayitvā ²sugālavaḥ /9.88/  

¹ M5 T G CE PPS; M1,7 "-yaṇādhvaraṃ labdhaṃ"; M6 "-varaṃ labdhaṃ"  
² ?M5; M1,6,7 "sukhāvahaḥ"; T G "sa bāhulaḥ"; B Dn CE "sa gālavaḥ"

> puṇḍrīkō’tha rājā ca brahmadattaḥ pratāpavān  
> ²jātīmaraṇajaṃ duḥkhaṃ smṛtvā smṛtvā punaḥ punaḥ  
> saptajātiṣu mukhyatvād ⁰yōgānāṃ sampadaṃ ³gatāḥ /9.89/  

¹ M5 T G PPS; M1,6,7 B Dn CE "kaṇḍarīkō-"  
² ?M1,6,7 CE; M5 "jāti-"; T2 G1 "jani-"; T1 G2,3,6 "janī-"; PPS "janū-"  
³ M T1 G6; et al. "gataḥ"

> purāham ātmajaḥ pārtha prathitaḥ kāraṇāntarē  
> dharmasya kuruṡārdūla tatō’haṃ ¹dharmajaḥ smṛtaḥ /9.90/  

¹ all; M7 T2 G1,2 "dharmajasm-"

> naranārāyaṇau pūrvaṃ tapas tēpatur ¹uttamam  
> dharmayānaṃ samārūḍhau parvatē gandhamādanē /9.91/  

¹ M T G; CE "avyayam"

> tatkālasamayaṃ caiva dakṣayajn̄ō babhūva ha  
> na caivākalpayad bhāgaṃ dakṣō rudrasya bhārata /9.92/  

> tataḥ ¹sa dēvīvacanād dakṣayajn̄am ²upādravat  
> sasarja ṡūlaṃ krōdhēna prajvalantaṃ muhur muhuḥ /9.93/  

¹ M1,6,7 T2 G1,2,3 PPS; T1 G6 "svadēvī-"; M5 B Dn CE "dadhīcivac-"  
² M T G; CE "apāharat"

> tac chūlaṃ bhasmasāt kṛtvā dakṣayajn̄aṃ savistaram  
> āvayōḥ sahasāgacchad badaryāṡramam antikāt  
> ¹vēgēna mahatā pārtha patan nārāyaṇōrasi /9.94/  

¹ all; M1,6,7 "mahatā pārtha vēgēna" (swap)

> tataḥ ⁰svatējasāviṣṭāḥ kēṡā nārāyaṇasya ha  
> babhūvur mun̄javarṇās tu tatō’haṃ mun̄jakēṡavān /9.95/  

> tac ca ṡūlaṃ vinirdhūtaṃ huṅkārēṇa mahātmanā  
> jagāma ṡaṅkarakaraṃ nārāyaṇasamāhatam /9.96/  

> atha rudra upādhāvat tāv ṛṣī tapasānvitau  
> tata ēnaṃ samuddhūtaṃ kaṇṭhē jagrāha pāṇinā  
> nārāyaṇaḥ sa viṡvātmā ¹tatō’sya ṡitikaṇṭhatā /9.97/  

¹ M T1 G1,3,6; T2 G2 PPS "tatō mē"; CE "tēnāsya"

> atha rudra¹vighātārtham ²īṣīkāṃ nara uddharan  
> mantraiṡ ca saṃyuyōjāṡu sō’bhavat paraṡur mahān /9.98/  

¹ all; M1,7 "-vināṡārtham"  
² M B Dn; G1 CE "iṣīkāṃ jagṛhē naraḥ"; T G2,3,6 "iṣīkāṃ jagṛhē tataḥ"

> kṣiptaṡ ca sahasā rudrē khaṇḍanaṃ prāptavāṃs tadā  
> tatō’haṃ khaṇḍaparaṡuḥ smṛtaḥ paraṡukhaṇḍanāt /9.99/  

M5 ends chap 199 here (named "nāmagauṇātmakathanaṃ").

PPS om. /9.50/ to /9.67L1/ because he follows G2 ms., which does the same. None of the
other S. mss. om. them.

>    arjuna uvāca  
> ¹asmin yuddhē tu vārṣṇēya trailōkyamathanē tadā  
> jayaṃ kaḥ prāptavāṃs ²vīras ṡaṃsaitan mē janārdana /9.100/  

¹ M1,5,6 G1 CE; M7 T G3,6 "tasmin"  
² †M1,6,7 G3 "vīras"; T G1,6 "vīra"; M5 B Dn CE "tatra" (see ¹/9.101/)

>    ṡrībhagavān uvāca  
> tayōḥ ⁰saṃlagnayōr ¹yuddhē rudranārāyaṇātmanōḥ  
> udvignāḥ sahasā kṛtsnā lōkāḥ ²sarvē’bhavaṃs tadā /9.101/  

¹ all; M5 "vīra" (see ²/9.100/)  
² ?M6,7 T2 G1 CE; M1 "sarvē bhavāṃs"; M5 "krtsnāḥ sarvē";
  T1 G3,6 "samabhavaṃs tadā"  

> nāgṛhṇāt pāvakaḥ ṡubhraṃ makhēṣu suhutaṃ haviḥ  
> vēdā na pratibhānti sma ṛṣīṇāṃ bhāvitātmanām /9.102/  

> dēvān rajas tamaṡ caiva samāviviṡatus tadā  
> vasudhā san̄cakampē ¹ca nabhaṡ ca ²vipaphāla ha /9.103/  

¹ M T G; CE "-’tha"  
² ?M5 G1 CE; M1,6,7 T1 G3,6 "cāpi paphā-"; T2 "vihvalatāṃ gataḥ"

> niṣprabhāṇi ca tējāṃsi brahmā caivāsanāc cyutaḥ  
> agāc chōṣaṃ samudraṡ ca himavāṃṡ ca vyaṡīryata /9.104/  

> tasminn ēvaṃ samutpannē nimittē pāṇḍunandana  
> brahmā vṛtō dēvagaṇair ṛṣibhiṡ ca mahātmabhiḥ  
> ¹ājagāṃ ca taṃ dēṡaṃ yatra yuddham avartata /9.105/  

¹ M5 T G; M1,6,7 B Dn CE "ājagāmāṡu taṃ"

> ¹sō’n̄jalipragrahō bhūtvā caturvaktrō niruktagaḥ  
> uvāca vacanaṃ rudraṃ lōkānām astu vai ṡivam  
> ²tyajāyudhāni viṡvēṡa jagatō hitakāmyayā /9.106/  

¹ ?M5 Dn; M1,6,7 "prān̄jaliḥ pra-"; T1 G3,6 "sān̄jaliḥ pra-"; T2 G1 B CE "sānjali-"  
² M T G; CE "nyasyāyu-"

> yad akṣaram athāvyaktam īṡaṃ lōkasya bhāvanam  
> kūṭasthaṃ kartṛnirdvandvam akartēti ca yaṃ viduḥ /9.107/  

> ¹vyaktabhāvaṃ gatasyāsya ēkā mūrtir iyaṃ ṡivā  
> narō nārāyaṇaṡ caiva jātau dharmakulōdvahau /9.108/  

¹ ?M1,5,6; M7 "vyaktabhāvāga-"; T G3,6 "vyaktibhāvaṃ";
  G1 "vyaktībhāvaṃ"; CE "vyaktibhāvagata-"  

In ¹ all M T G (except M7) prefer anusvāra ("-ṃ"), hence the choice.

> tapasā mahatā yuktau dēvaṡrēṣṭhau mahāvratau  
> ahaṃ prasādajas tasya kasmiṃṡ cit kāraṇāntarē  
> tvaṃ caiva krōdhajas tāta pūrvasargē sanātanaḥ /9.109/  

> mayā ca sārdhaṃ ¹varadaṃ vibudhaiṡ ca maharṣibhiḥ  
> prasādayāṡu lōkānāṃ ṡāntir bhavatu ²ṡaṅkara /9.110/  

¹ all; M1,6,7 Dn "varadavi-"  
² M; et al. "māciram"

M addresses Rudra as "Ṡankara" in the vocative. "māciram" means "without delay".

> brahmaṇā tv ēvam uktas tu rudraḥ krōdhāgnim utsṛjan  
> prasādayām āsa tatō dēvaṃ nārāyaṇaṃ prabhum  
> ṡaraṇaṃ ca jagāmādyaṃ varēṇyaṃ varadaṃ ¹haraḥ /9.111/  

¹ M; B "vibhum"; Dn "prabhum"; T G CE "harim"

M addresses Rudra as "Hara" (haraḥ = nominative) in this verse whereas
CE says "Hari" (hariṃ = accusative). Both are correct (/9.111L3/):

CE: "[Rudra from L1] surrendered to the worshipful, powerful, boon-giving Lord Hari"
M: "Hara surrendered to the worshipful, powerful, boon-giving Nārāyaṇa [from L2]".

Nārāyaṇa is called Hari just two verses later anyway (/9.113/).

> tatō’tha varadō dēvō jitakrōdhō jitēndriyaḥ  
> prītimān abhavat tatra rudrēṇa saha saṃgataḥ /9.112/  

> ṛṣibhir brahmaṇā caiva ¹tridaṡaiṡ caiva pūjitaḥ  
> uvāca dēvam ²īṡānam īṡaḥ ³sarvagatō hariḥ /9.113/  

¹ M1,6,7 T G; M5 "vibudhaiṡ cābhipū-"; B Dn CE "vibudhaiṡ ca supūjitaḥ"  
² all; M1,6,7 "īṡānān dēvaḥ"  
³ M1,6,7 T G; M5 CE "sa jagatō"

> yas tvāṃ vētti sa māṃ vētti yas tvām anu sa mām anu  
> nāvayōr antaraṃ kiṃ cin mā tē bhūd ¹buddhir anyathā /9.114/  

¹ †all; M5 "anyathā matiḥ"

> adya prabhṛti ṡrīvatsaḥ ṡūlāṅkō’yaṃ bhavatv ayam  
> mama pāṇyaṅkitaṡ cāpi ṡrīkaṇṭhas tvaṃ bhaviṣyasi /9.115/  

> ēvaṃ lakṣaṇam utpādya parasparakṛtaṃ tadā  
> sakhyaṃ caivātulaṃ kṛtvā rudrēṇa sahitāv ṛṣī  
> tapas tēpatur avyagrau visṛjya ¹tridivaukasaḥ /9.116/  

¹ ?M5,7 B Dn CE; M1,6 T G "tu div-"

> ēṣa tē kathitaḥ pārtha nārāyaṇajayō ¹mṛdhē  
> nāmāni caiva guhyāni ²niruktāni ca bhārata  
> ṛṣibhiḥ kathitānīha yāni ³saṅkalpitāni tē /9.117/  

¹ all; M1,7 "dhṛtē"  
² †all; M1,5,6 "nimittāni"  
³ M; G1 "saṅkīrtanāni"; CE "saṅkīrtitāni"

> ēvaṃ bahuvidhai rūpaiṡ ¹carāmīha vasundharām  
> brahmalōkaṃ ca kauntēya ²gōlōkaṃ ca sanātanam  
> mayā tvaṃ rakṣitō yuddhē mahāntaṃ prāptavān̄ jayam /9.118/  

¹ all; M1,7 "cariṣyāmi"  
² all; M7 "nō lōkaṃ"; G2 PPS "bhūlōkaṃ"

> yas tu tē sō’gratō yāti yuddhē sampraty upasthitē  
> taṃ viddhi rudraṃ kauntēya dēvadēvaṃ kapardinam /9.119/  

> kālaḥ sa ēva ⁰kathitaḥ krōdhajēti mayā tava  
> nihatāṃs tēna ¹pūrva tvaṃ hatavān asi vai ripūn /9.120/  

¹ M T1 G1,3,6; G2 CE PPS "vai pūrvaṃ"

> apramēyaprabhāvaṃ taṃ dēvadēvam umāpatim  
> namasva ¹nityaṃ prayatō viṡvēṡaṃ ²haram avyayam /9.121/  

¹ M T G; CE "dēvaṃ"  
² all; T1 G3,6 "prabhum"

Thus ends chapter M1,6,7 165; G1 168; T1 G6 169; G3 167; T2 182; G2 198; M5 200.

It has 121 units comprising (2 + 71 = 73) verses in (4 + 155 = 159) lines and 48 passages in 228 lines;
    GP 141 units comprising (2 + 76 = 78) verses in (4 + 157 = 161) lines and 63 passages in 228 lines;
    CE (50 + 71 = 121) units comprising (2 + 71 = 73) verses in (4 + 155 = 159) lines and 48 passages in 228 lines;
    Ku ??? verses, ??? lines.

Chapter name:

M5     "rudra-nārāyaṇa-yuddha-kathanam"  
T1 G6  "ṡrīkṛṣṇārjuna-saṃvādē nāma-saṅkīrtanaṃ viṣṇu-rudra-yuddham"
T2     "nāma-nirvacanam"
