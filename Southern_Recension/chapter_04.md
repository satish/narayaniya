# Chapter 04

>    yudhiṣṭhira uvāca  
> yadā bhaktō bhagavata āsīd rājā mahāvasuḥ  
> kimarthaṃ sa paribhraṣṭō vivēṡa vivaraṃ bhuvaḥ /4.1/  

>    bhīṣma uvāca  
> atrāpy udāharantīmam itihāsaṃ purātanam  
> ṛṣīṇāṃ caiva saṃvādaṃ tridaṡānāṃ ca bhārata /4.2/  

> ajēna yaṣṭavyam iti dēvāḥ prāhur dvijōttamān  
> sa ca chāgō hy ajō jn̄ēyō nānyaḥ paṡur iti sthitiḥ /4.3/  

>    ṛṣaya ūcuḥ  
> bījair yajn̄ēṣu yaṣṭavyam iti vai vaidikī ṡrutiḥ  
> ajasan̄jn̄āni bījāni ¹na chāgaṃ ²hantum arhatha /4.4/  

¹ M; T G1,3,6 "bhāgaṃ taṃ"; G2 CE "chāgaṃ na" (swap)  
² M B Da Dn; T G1,3,6 "hartum"; CE "ghnantum"; G2 "xxx"

> naiṣa dharmaḥ satāṃ dēvā yatra vadhyēta vai paṡuḥ  
> idaṃ kṛtayugaṃ ṡrēṣṭhaṃ kathaṃ vadhyēta vai paṡuḥ /4.5/  

>    ¹bhīṣma uvāca  
> tēṣāṃ saṃvadatām ēvam ṛṣīṇāṃ vibudhaiḥ saha  
> mārgāgatō nṛpaṡrēṣṭhas taṃ dēṡaṃ prāptavān vasuḥ  
> antarikṣa²caraḥ ṡrīmān samagrabalavāhanaḥ /4.6/  

¹ †M1,6,7 B Dn CE; M5 T G om.  
² all; M1,7 T2 G1 "-cara-"

> taṃ dṛṣṭvā sahasāyāntaṃ vasuṃ tē tv antarikṣagam  
> ūcur dvijātayō dēvān ēṣa chētsyati saṃṡayam /4.7/  

> yajvā dānapatiḥ ṡrēṣṭhaḥ sarvabhūta¹hitapriyaḥ  
> kathaṃ svid anyathā brūyād vākyam ēṣa mahān vasuḥ /4.8/  

¹ M1,5,7 T G3,6 CE; M6 "-hitē mayaḥ"; G2 PPS "-hitē rataḥ"; G1 "-hitaḥ priyaḥ"  

> ēvaṃ tē saṃvidaṃ kṛtvā vibudhā ṛṣayas tathā  
> ¹apṛcchan ²sahitābhyētya vasuṃ rājānam antikāt /4.9/  

¹ all; M1,7 "apṛcchat"  
² ?M5,6,7 B Dn; M6 T G CE "sahasābhyētya"

> bhō rājan kēna yaṣṭavyam ¹ajairāhōsvid auṣadhaiḥ  
> ētan naḥ saṃṡayaṃ chindhi pramāṇaṃ nō bhavān mataḥ /4.10/  

¹ M; T2 G1 CE "ajēnāhōsvid-"; T1 G3,6 "ajēnasvid athauṣadhaiḥ";
  G2 "ajēnakim athauṣadhaiḥ"  

Before /4.10/ B8 in. "ṛṣaya ūcuḥ" but others om.

> sa tān kṛtān̄jalir bhūtvā ⁰paripapraccha vai vasuḥ  
> kasya ¹vā kō ²mataḥ ³pakṣō brūta satyaṃ samāgatāḥ /4.11/  

¹ †M1,6,7 T G; Da Dn "vai"; M5 CE "vaḥ"  
² †all; M5 "paraḥ"  
³ †M1,6,7 T G CE; M5 B Da Dn Cs "kāmō"

Before /4.11/ B6,8,9 K in. "bhīṣma uvāca" but others om.

>    ṛṣaya ūcuḥ  
> dhānyair yaṣṭavyam ity ēṣa pakṣō’smākaṃ narādhipa  
> dēvānāṃ ¹tu paṡuḥ pakṣō matō rājan vadasva naḥ /4.12/  

¹ all; M1,6,7 "paṡubhiḥ"

>    bhīṣma uvāca  
> dēvānāṃ tu mataṃ ¹ṡrutvā vasunā pakṣasaṃṡrayāt  
> ²chāgēnājēna yaṣṭavyam ēvam uktaṃ vacas tadā /4.13/  

¹ M T G; CE "jn̄ātvā"  
² M5 T1 G B Dn CE; T2 M1,6,7 "bhāgēnānēna"

> kupitās tē tataḥ sarvē munayaḥ sūryavarcasaḥ  
> ūcur vasuṃ vimānasthaṃ dēvapakṣārthavādinam /4.14/  

> surapakṣō gṛhītas tē yasmāt tasmād divaḥ pata  
> adya prabhṛti tē rājann ākāṡē vihatā gatiḥ  
> ¹asmacchāpābhighātēna mahīṃ bhittvā pravēkṣyasi /4.15/  

¹ all; G1 M1,7 "asmāc"

Before /4.15/ B9 in. "ṛṣaya ūcuḥ"

> tatas tasmin muhūrtē’tha rājōparicaras tadā  
> adhō vai sambabhūvāṡu bhūmēr vivaragō nṛpaḥ  
> smṛtis tv ēnaṃ na prajahau tadā nārāyaṇājn̄ayā /4.16/  

> dēvās tu sahitāḥ sarvē vasōḥ ṡāpavimōkṣaṇam  
> cintayām āsur avyagrāḥ ¹sukṛtaṃ hi nṛpasya ⁰tat /4.17/  

¹ †all; M5 "svakṛtaṃ"

> anēnāsmatkṛtē rājn̄ā ṡāpaḥ prāptō mahātmanā  
> asya pratipriyaṃ kāryaṃ sahitair nō divaukasaḥ /4.18/  

> iti buddhyā vyavasyāṡu gatvā niṡcayam īṡvarāḥ  
> ūcus taṃ hṛṣṭamanasō rājōparicaraṃ tadā /4.19/  

>    ¹dēvā ūcuḥ  
> brahmaṇyadēvaṃ tvaṃ bhaktaḥ surāsuraguruṃ harim  
> kāmaṃ sa tava tuṣṭātmā kuryāc chāpavimōkṣaṇam /4.20/  

Before /4.20/ B8,9 in. "dēvā ūcuḥ"

> ¹mānanaṃ tu dvijātīnāṃ ²kartavyaṃ vai mahātmanām  
> avaṡyaṃ tapasā tēṣāṃ ³patitavyaṃ nṛpōttama /4.21/  

¹ M; T G CE "mānanā"  
² M; T G CE "kartavyā"  
³ M; T G CE "phalitavyaṃ"

CE: "O best of kings, surely there will be the fruit
on account of their austerities".

M: "Surely by your austerities, your fallen-down state (shall be liberated), O best of kings"

patitavya = the going down to hell  
patitavyam = accusative case

> yatas tvaṃ sahasā bhraṣṭa ākāṡān mēdinītalam  
> ēkaṃ tv anugrahaṃ tubhyaṃ ¹dadma vai nṛpasattama /4.22/  

¹ M G1; T1 G2,3,6 "kurmō"; T2 CE "dadmō"

> yāvat tvaṃ ṡāpadōṣēṇa kālam āsiṣyasē’nagha  
> bhūmēr vivaragō bhūtvā ¹tāvat tvaṃ kālam āpsyasi  
> yajn̄ēṣu suhutāṃ viprair vasōr dhārāṃ mahātmabhiḥ /4.23/  

¹ M T1 G2,3,6 B Dn; CE "tāvantaṃ"

> prāpsyasē’smadanudhyānān mā ¹hi tvā glānir āspṛṡēt  
> na kṣutpipāsē rājēndra bhūmēṡ chidrē bhaviṣyataḥ /4.24/  

¹ M; T1 G "ca tvā"; CE "ca tvāṃ"

> vasōr dhārānupītatvāt tējasāpyāyitēna ca  
> sa dēvō’smadvarāt prītō brahmalōkaṃ hi nēṣyati /4.25/  

>    ¹bhīṣma uvāca  
> ēvaṃ dattvā varaṃ ²jagmuḥ sarvē tatra divaukasaḥ  
> gatāḥ svabhavanaṃ dēvā ṛṣayaṡ ca tapōdhanāḥ /4.26/  

¹ Before /4.26/ B6 in. "bhīṣma uvāca"  
² M5 T G; M1,6,7 "rājn̄aḥ"; CE "rājn̄ē"

> cakrē ca satataṃ pūjāṃ viṣvaksēnāya bhārata  
> japyaṃ jagau ca satataṃ nārāyaṇamukhōdgatam /4.27/  

> tatrāpi pan̄cabhir yajn̄aiḥ pan̄cakālān arindama  
> ¹yajad dhariṃ surapatiṃ bhūmēr vivaragō’pi san /4.28/  

¹ ?M5; M1,6,7 "yajēd"; T G CE "ayajad" (hypermetric)

M5's "yajat" (=worshipping Hari) is present active participle of _yaj(1)_,
M1,6,7's "yajēt" (=may he worship Hari) is optative third person singular,
whereas CE "ayajad" (=he worshipped Hari) is imperfect third person singular.

> ¹tatō’sya tuṣṭō bhagavān bhaktyā nārāyaṇō hariḥ  
> ananyabhaktasya satas tatparasya jitātmanaḥ /4.29/  

¹ all; M1,6,7 "tasya tuṣṭō’tha"

> varadō bhagavān viṣṇuḥ samīpasthaṃ dvijōttamam  
> garutmantaṃ ¹mahāvēgam ⁰ābabhāṣē smayann iva /4.30/  

¹ ?M7 T1 G1,3,6 CE; M1,5,6 T2 G2 PPS "mahābhāgam" (see ²/4.31/)

>    ¹ṡrībhagavān uvāca  
> dvijōttama ²mahābhāga gamyatāṃ vacanān mama  
> samrāḍ rājā vasur nāma dharmātmā ³saṃṡitavrataḥ /4.31/  

¹ Before /4.31/ B9 in. "ṡrībhagavān uvāca"  
² all; M1,6,7 "mahāvēga" (see ¹/4.30/)  
³ M T G Dn; B CE "māṃ samāṡritaḥ"

> brāhmaṇānāṃ prakōpēna praviṣṭō vasudhātalam  
> mānitās tē ⁰tu viprēndrās tvaṃ tu gaccha dvijōttama /4.32/  

> bhūmēr ⁰vivarasaṅguptaṃ garuḍēha mamājn̄ayā  
> adhaṡcaraṃ nṛpaṡrēṣṭhaṃ khēcaraṃ kuru māciram /4.33/  

>    ¹bhīṣma uvāca  
> garutmān atha vikṣipya pakṣau mārutavēgavān  
> vivēṡa vivaraṃ bhūmēr yatrāstē vāgyatō vasuḥ /4.34/  

¹ Before /4.34/ B8,9 in. "bhīṣma uvāca"

> tata ēnaṃ samutkṣipya sahasā vinatāsutaḥ  
> utpapāta nabhas tūrṇaṃ tatra cainam amun̄cata /4.35/  

> tasmin muhūrtē san̄jajn̄ē rājōparicaraḥ punaḥ  
> saṡarīrō gataṡ caiva brahmalōkaṃ nṛpōttamaḥ /4.36/  

> ēvaṃ tēnāpi kauntēya vāgdōṣād dēvatājn̄ayā  
> prāptā gatir ayajvārhā dvijaṡāpān mahātmanā /4.37/  

> kēvalaṃ puruṣas tēna sēvitō harir īṡvaraḥ  
> tataḥ ṡīghraṃ jahau ṡāpaṃ brahmalōkam avāpa ca /4.38/  

> ētat tē sarvam ākhyātaṃ tē bhūtā mānavā yathā  
> nāradō’pi yathā ṡvētaṃ dvīpaṃ sa gatavān ṛṣiḥ  
> tat tē sarvaṃ pravakṣyāmi ṡṛṇuṣvaikamanā nṛpa /4.39/  

¹ Before /4.39/ M T G B Dn in "bhīṣma uvāca" but CE PPS om.

Thus ends chapter M1,6,7 158; G1 161; T1 G6 162; G3 163; T2 175; G2 193; M5 194.

It has 39 verses, 83 lines;
    CE 39 verses, 83 lines;
    GP 41 verses, 83 lines;
    Ku 47 verses, 97 lines;
   PPS 49 verses, 98 lines.

Chapter name:

M5,6   "vasu-ṡāpa-kathanaṃ"
