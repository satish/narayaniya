# Chapter 17

Note: T2 G2 are missing for this adhyāya. Hence, "T G" means "T1 G1,3,6".

>    janamējaya uvāca  
> bahavaḥ puruṣā brahmann utāhō ēka ēva tu  
> kō hy atra ¹puruṣaḥ ṡrēṣṭhaḥ ¹kā vā yōnir ihōcyatē /17.1/  

¹ M1,5,6 B Dn CE PPS; M7 T1 G1,3,6 "puruṣaṡrē-"  
¹ M G1; et al. "kō"

>    vaiṡampāyana uvāca  
> bahavaḥ puruṣā ¹lōkē sāṅkhyayōga²vicāriṇaḥ  
> naitad icchanti puruṣam ēkaṃ kurukulōdvaha /17.2/  

¹ all; M1,6,7 "rājan"  
² ?M5 K1,2,4 PPS; M6,7 T G CE "-vicāriṇam"; M1 "-vicāriṇāḥ"; B Da Dn "-vicāraṇē"

> bahūnāṃ puruṣāṇāṃ ¹hi yathaikā yōnir ucyatē  
> tathā taṃ puruṣaṃ viṡvam ²ākhyāsyāmi ³guṇātigam /17.3/  

¹ M5 T1 G3,6 PPS; M1,6,7 "tu"; G1 CE "ca" (see /17.24L2/)  
² M T G; CE "vyākhyā-"  
³ M T1 G6 PPS; G3 "guṇāgatim"; G1 CE "guṇādhikam"

> namaskṛtvā tu guravē ¹vyāsāyāmitatējasē  
> ²tapōnityāya dāntāya vandyāya paramarṣayē /17.4/  

¹ M5 T G CE; M1,6,7 B Da Dn "vyāsāya viditātmanē"  
² M T1 G3,6 PPS; G1 CE "tapōyuktāya"

> idaṃ puruṣasūktaṃ hi sarvavēdēṣu ¹paṭhtyatē  
> ṛtaṃ satyaṃ ca vikhyātam ṛṣi²siṃhēna cintitam /17.5/  

¹ †M1,6,7 T1 G1,3,6; M5 CE "pārthiva"  
² †all; M5 B6,7,8 "-saṅghēna"

> utsargēṇāpavādēna ṛṣibhiḥ kapilādibhiḥ  
> adhyātmacintām āṡritya ṡāstrāṇy uktāni bhārata /17.6/  

> samāsatas tu ¹yad vyāsaḥ ²puruṣaikātmyam uktavān  
> tat tē’haṃ sampravakṣyāmi prasādād amitaujasaḥ /17.7/  

¹ all; M6 G1 "tad"  
² †M1,7 G3,6; M5 K1,2,4 "puruṣē kāryam"; T1 "puruṣaiḥ kāmyam"; G1 "puruṣaikārtham";
  M6 "puruṣē ṡāstram"; PPS "puruṣaikāgryam"; B Da Dn CE "puruṣaikatvam"  

The reading of M1,7 G3,6 gives similar meaning as of CE.

> atrāpy udāharantīmam itihāsaṃ purātanam  
> brahmaṇā saha saṃvādaṃ tryambakasya viṡāṃ patē /17.8/  

> kṣīrōdasya samudrasya madhyē hāṭakasaprabhaḥ  
> vaijayanta iti khyātaḥ parvatapravarō nṛpa /17.9/  

> ¹tatrādhyātmagatiṃ ²dēva ēkākī pravicintayan  
> vairāja³sadanān nityaṃ vaijayantaṃ niṣēvatē /17.10/  

¹ †all; M5 "yatra"  
² M1,5,7 G1 CE PPS; T1 G3,6 M6 "dēvamēk-"  
³ M T1 G3,6 PPS; CE "-sadanē"

> atha tatrāsatas tasya caturvaktrasya dhīmataḥ  
> lalāṭaprabhavaḥ putraḥ ṡiva āgād yadṛcchayā  
> ākāṡēnaiva yōgīṡaḥ purā trinayanaḥ prabhuḥ /17.11/  

> tataḥ khān nipapātāṡu dharaṇīdharamūrdhani  
> agrataṡ ¹cābhavat prītō vavandē cāpi pādayōḥ /17.12/  

¹ all; M1,6,7 "ca bhavaḥ"

> taṃ pādayōr nipatitaṃ ¹sṛṣṭvā savyēna pāṇinā  
> utthāpayām āsa tadā prabhur ēkaḥ prajāpatiḥ  
> uvāca cainaṃ bhagavāṃṡ cirasyāgatam ātmajam /17.13/  

¹ M T G; CE "dṛṣṭvā"

>    ¹brahmōvāca  
> svāgataṃ tē ²mahābāhō diṣṭyā prāptō’si ³mē’ntikam  
> kaccit tē kuṡalaṃ putra svādhyāyatapasōḥ sadā  
> nityam ugratapās ⁴hi tvaṃ tataḥ pṛcchāmi tē ⁵tapaḥ /17.14/  

¹ M G1 K; B Da Dn PPS "pitāmaha uvāca"; T1 G3,6 CE om.  
² M5,6 T1 G3,6 CE PPS; M1,7 G1 K "mahāyōgin"  
³ †all; M5 "cā’ntikam"  
⁴ M5 T1 G3,6 PPS; M1,6,7 CE "tvaṃ hi" (swap)  
⁵ M T G; CE "punaḥ"

>    rudra uvāca  
> tvatprasādēna bhagavan svādhyāya¹tapasōr mama  
> kuṡalaṃ cāvyayaṃ caiva sarvasya jagatas tathā /17.15/  

¹ M5 CE PPS "tapasōr mama"; M1,6,7 T1 G1,3,6 "-tapasō mama"

It has to be "svādhyāya tapasōr mama" (=dual locative),
because it is the same as in /17.14L2/.

> ciradṛṣṭō hi bhagavān vairājasadanē mayā  
> tatō’haṃ parvataṃ prāptas tv imaṃ tvatpādasēvitam /17.16/  

> kautūhalaṃ cāpi hi mē ēkāntagamanēna tē  
> naitat kāraṇam ¹alpaṃ hi bhaviṣyati pitāmaha /17.17/  

¹ all; M1,6,7 "apy alpaṃ"

> kiṃ nu tat sadanaṃ ṡrēṣṭhaṃ kṣutpipāsāvivarjitam  
> ¹surāsurair adhyuṣitam ṛṣibhiṡ cāmitaprabhaiḥ /17.18/  

¹ †all; M5 "purā surair"

> gandharvair apsarōbhiṡ ca satataṃ saṃniṣēvitam  
> utsṛ¹jyainaṃ girivaram ²ēkāntaṃ prāptavān asi /17.19/  

¹ M5 T1 G3,6 PPS; M1,6,7 "-jyētaṃ" G1 "-jyaivaṃ"; CE "-jyēmaṃ"  
² ?M5 T1 G3,6 PPS; M7 "ēkākiḥ"; M1,6 G1 CE "ēkākī"

>    brahmōvāca  
> vaijayantō girivaraḥ satataṃ sēvyatē mayā  
> atraikāgrēṇa manasā puruṣaṡ cintyatē virāṭ /17.20/  

>    rudra uvāca  
> bahavaḥ puruṣā brahmaṃs tvayā sṛṣṭāḥ svayambhuvā  
> sṛjyantē cāparē brahman sa ¹ca kaḥ puruṣō virāṭ /17.21/  

¹ M T1 G3,6 PPS; G1 "tv ēṣa"; CE "caikaḥ"

> kō hy asau cintyatē brahmaṃs ¹tvyaikaḥ puruṣōttamaḥ  
> ētan mē saṃṡayaṃ ²chindhi paraṃ kautūhalaṃ hi mē /17.22/  

¹ M T1 G3,6 PPS; G1 CE "tvayā vai"  
² M T1 G3,6 PPS; CE "brūhi mahat"

>    brahmōvāca  
> bahavaḥ puruṣāḥ putra yē tvayā samudāhṛtāḥ  
> ēvam ētad atikrāntaṃ draṣṭavyaṃ ¹naivam ity api /17.23/  

¹ ?M5,6 B Dn CE; M1,7 "naitad"; T1 G3,6 PPS "caivam" (sic)

All of them except T G speak in the negative ("na + ēnaṃ/ētad")

> ādhāraṃ tu pravakṣyāmi ēkasya puruṣasya ¹vai  
> bahūnāṃ puruṣāṇāṃ ²sa yathaikā yōnir ucyatē /17.24/  

¹ ?M5 T1 G3,6 PPS; M1,6,7 B Dn CE "tē"  
² all; M1,7 "tu" (see /17.3L1/)

> tathā taṃ ¹paramaṃ viṡvaṃ puruṣaṃ surasattamam  
> nirguṇaṃ nirguṇā bhūtvā praviṡanti sanātanam /17.25/  

¹ ?M5; M1,7   "puruṣaṃ sarvē puruṣāḥ surasattama";
  M6          "puruṣaṃ viṡvaṃ ṡṛṇuṣva surasattama";
  CE B Dn     "puruṣaṃ viṡvaṃ paramaṃ sumahattamam";
  Da          "puruṣaṃ viṡvaṃ paramaṃ sukham uttamam";
  T1 G3,6 PPS "puruṣaṃ viṡvaṃ ākhyāsyāmi guṇātigam"; T2 G1,2 "� � �";  

M5 retains the closest meaning to CE -- "that Puruṣa, who is universal, supreme and the best of the gods".  
"paramaṃ" means supreme; "mahattamam" means "very great", thus "sumahattamam" means "extremely great".  
This redundancy is explained by Nīlakaṇṭha as "paramaṃ - sūtrātmānaṃ; sumahattamaṃ - kāraṇam".

M. unanimously agree that the last word should mean "the best of gods" (surattama[ṃ]).

Thus ends chapter M1,6,7 173; G1 � � �; T1 G6 177; G3 178; T2 � � �; G2 � � �; M5 208.

It has 25 verses, 53 lines;
    CE 25 verses, 53 lines;
    GP 27 verses, 53 lines;
    Ku 25 verses, 53 lines.

Chapter name:

M      "brahma-rudra-saṃvādaḥ"
