# Chapter 1  

>    yudhiṣṭhira uvāca  
> gṛhasthō brahmacārī ¹ca vānaprasthōʼtha bhikṣukaḥ  
> ya icchēt siddhimāsthātuṃ dēvatāṃ kāṃ yajēta saḥ /1.1/  

¹ M T2 G1; CE "vā"

> kutō hyasya dhruvaḥ svargaḥ kutō ¹naiṡrēyasaṃ param  
> vidhinā kēna juhuyād ²daivaṃ pitryam ³athāpi vā /1.2/  

¹ M T2 G1; T1 G3,6 PPS Ku GP "naiḥ-"; CE "niḥ-"  
² M5,6 T1 G1,3,6 CE; M1,7 T2 G2 "dēvaṃ"  
³ M T G PPS; CE Ku GP "tathaiva ca"  

> muktaṡca kāṃ gatiṃ gacchēn mōkṣaṡcaiva kimātmakaḥ  
> svargataṡcaiva kiṃ kuryād ¹yēna na cyavatē divaḥ /1.3/  

¹ †all; M1,5,6 "kathaṃ"

> dēvatānāṃ ca kō dēvaḥ pitṝṇāṃ ca ¹pitā tathā  
> ²tasmāt parataraṃ yacca tanmē brūhi pitāmaha /1.4/  

¹ M T G PPS Ku GP; CE "tathā pitā" (swap)  
² M CE Ku GP; T G PPS (ख घ) "sṛṣṭēḥ"

>    bhīṣma uvāca  
> gūḍhaṃ ¹māṃ praṡnavit praṡnaṃ pṛcchasē tvam ihānagha  
> na hy ēṣa tarkayā ṡakyō vaktuṃ varṣaṡatair api /1.5/  

¹ †all; M5,6 "mā"; M1,7 "matpra-"  

*tarkayā* should probably read *tarkēṇa* because *tarka* is masculine and the
former is a feminine form. Sastri, therefore, renders it as *anyathā ṡakyaṃ*

> ṛtē dēvaprasādād vā rājan̄ ¹jn̄ānāgamēna vā  
> gahanaṃ hy ētad ²ākhyānaṃ vyākhyātavyaṃ tavārihan /1.6/  

¹ all; M1,7 "jn̄ānagamēna"  
² †all; M1,5,6 "ākhyātaṃ"

> atrāpyudāharantīmam itihāsaṃ purātanam  
> nāradasya ca saṃvādam ṛṣērnārāyaṇasya ca /1.7/  

/1.7/ is one of those verses for which there are no variant readings at all!
All 34 mss. agree completely.

>    nārada uvāca  
> nārāyaṇō hi viṡvātmā caturmūrtiḥ sanātanaḥ  
> dharmātmajaḥ sambabhūva pitaivaṃ mēʼbhyabhāṣata /1.8/  

> kṛtē yugē mahārāja purā ¹svāyambhuvēʼntarē  
> ²narō nārāyaṇaṡcaiva hariḥ kṛṣṇastathaiva ca /1.9/  

¹ all; M1,7 "svāyambhuvāntarē"  
² all; M1,7 "namō"

> ¹tēṣāṃ nārāyaṇanarau tapastēpatur²avyayau  
> badaryāṡramamāsādya ṡakaṭē kanakāmayē /1.10/  

¹ †M1,6,7 B Da PPS; M5 CE "tebhyō"; T G "tatra"  
² all; M1,6,7 "avyayam"  

> aṣṭacakraṃ hi tadyānaṃ bhūtayuktaṃ manōramam  
> tatrādau lōkanāthau tau kṛṡau dhamanisantatau /1.11/  

> tapasā tējasā caiva ¹durnirīkṣau surairapi  
> yasya prasādaṃ kurvātē sa dēvau draṣṭumarhati /1.12/  

¹ M5,6 G1 CE; M1,7 T G2,3,6 B Ku PPS GP "durnirīkṣyau"

> nūnaṃ tayōr anumatē hṛdi hṛcchayacōditaḥ  
> mahāmērōḥ girēḥ ṡṛṅgāt pracyutō gandhamādanam /1.13/  

> nāradaḥ sumahadbhūtaṃ ¹sarvalōkān ⁰acīcarat  
> ²sa dēṡamagamad rājan badaryāṡramamāṡugaḥ /1.14/  

¹ M5,7 T G B Da Dn; M1,6 PPS "sarvāl̐lōkān-"; CE "lōkān sarvān-"  
² M; et al. "taṃ"

> tayōr āhnikavēḻāyāṃ tasya kautūhalaṃ tvabhūt  
> idaṃ tad āspadaṃ kṛtsnaṃ yasmil̐ lōkāḥ pratiṣṭhitāḥ /1.15/  

> sadēvāsuragandharvāḥ saṛṣikinnaralēlihāḥ  
> ēkā mūrtiriyaṃ pūrvaṃ jātā bhūyaṡcaturvidhā /1.16/  

> dharmasya kula¹santānē ²dharma ēbhir ²vivardhitaḥ  
> ahō hy anugṛhītōʼdya dharma ēbhiḥ surair iha  
> naranārāyaṇābhyāṃ ca kṛṣṇēna hariṇā tathā /1.17/  

¹ M; et al. "-santānō"  
² M G1; T2 G2 "dharma eva"; T1 G3 G5 "dharma ēvābhi-";
  PPS "dharma ēbhiṡca"; CE "mahānebhir"  
² †M1,6,7 T2 CE; M5 "vivardhitē"; G1 "vivardhitāḥ"; G2 "vivardhitam";
  T1 G3 G5 PPS "-vardhitaḥ"  

> ¹atra kṛṣṇō hariṡ caiva kasmiṃṡ cit kāraṇāntarē  
> sthitau ²dharmasya tau gēhē ³imau tapasi ⁴viṣṭhitau /1.18/  

¹ M5 T G B Da Dn; M1,6,7 CE "tatra"  
² M; T G "dharmasutāv ēva"; CE "dharmōttarau hy ētau"  
³ M; et al. "tathā"  
⁴ M B Da; CE "tathā"; T G "xxx"

> ētau hi paramaṃ dhāma ⁰kānayōr āhnikakriyā  
> pitarau sarvabhūtānāṃ daivataṃ ca ¹yaṡasvinau  
> kāṃ dēvatāṃ nu ²yajataḥ ³pitṝn vā kān mahāmatī /1.19/  

¹ all; M1,7 "yaṡasvinē"  
² M5,6 B Dn CE; M1,7 "yajataṃ"; T G "yajatāṃ"  
³ all; M1,6,7 "pitṛpākān"

> iti san̄cintya manasā bhaktyā nārāyaṇasya ha  
> sahasā prādurabhavat samīpē dēvayōs tadā /1.20/  

> kṛtē daivē ca pitryē ca tatas tābhyāṃ nirīkṣitaḥ  
> pūjitaṡ caiva vidhinā yathāprōktēna ṡāstrataḥ /1.21/  

> taṃ dṛṣṭvā mahad āṡcaryam ¹apūrvavidhivistaram  
> upōpaviṣṭaḥ suprītō nāradō bhagavān ṛṣiḥ /1.22/  

¹ M Cs; T G CE "apūrvaṃ vi-"

> nārāyaṇaṃ sannirīkṣya prasannēnāntarātmanā  
> ¹namaskṛtya mahādēvam idaṃ vacanam abravīt /1.23/  

¹ M T2 G1; et al. "namaskṛtvā"

>    ¹nārada uvāca  
> vēdēṣu sapurāṇēṣu sāṅgōpāṅgēṣu ²gīyasē  
> tvam ajaḥ ṡāṡvatō dhātā ³vidhātāmṛtam uttamam /1.24/  

¹ M B Da Dn; T G CE om.  
² all; M1,7 "nīyasē"  
³ M; CE "matō’mṛtam anuttamam"; T G "vidhātā mṛtyur uttamaḥ"

> pratiṣṭhitaṃ bhūtabhavyaṃ tvayi sarvam idaṃ jagat  
> catvārō hy āṡramā dēva sarvē gārhasthyamūlakāḥ /1.25/  

> yajantē tvām aharahar nānāmūrtisamāsthitam  
> pitā mātā ca sarvasya ¹daivatatvaṃ ca ṡāṡvatam /1.26/  

¹ M T G; CE "jagataḥ ṡāṡvatō guruḥ"  

After /1.26L1/ M T G in. CE@800 (1 line) but others om.

> kaṃ tv adya ¹yajasē dēvaṃ pitaraṃ kaṃ ²nu vidma tē  
> kam arcasi mahābhāga tan mē ³prabrūhi pṛcchataḥ /1.27/  

¹ M T G CE; M1,6,7 "yajatē"  
² M G1; T1 G2,3,6 "tu manyasē"; CE "na vidmahē"  
³ M T2 G1; T1 G2,3,6 "brūhīha"

>    ṡrībhagavān uvāca  
> avācyam ētad vaktavyam ātmaguhyaṃ sanātanam  
> tava bhaktimatō brahman vakṣyāmi tu yathātatham /1.28/  

> yat tat sūkṣmam avijn̄ēyam avyaktam acalaṃ dhruvam  
> indriyair indriyārthaiṡ ca sarvabhūtaiṡ ¹vivarjitam /1.29/  

¹ M T2 G1,3; G2 "visar-"; T1 G6 CE "ca var-"

> ¹sō’bhyantarātmā bhūtānāṃ kṣētrajn̄aṡ cēti kathyatē  
> triguṇavyatiriktō’sau puruṣaṡ cēti kalpitaḥ  
> tasmād avyaktam utpannaṃ triguṇaṃ ²dvijasattama /1.30/  

¹ M; et al. "sa hy an-"  
² †all; M5 "muni-"

"The Lord is the interior Self (abhyantara ātma) of all beings" -- supports the Tēṅgalai view that
the Lord pervades even the jīvas.

> ¹avyaktād vyaktabhāvasthā yā sā prakṛtir avyayā  
> tāṃ yōnim āvayōr viddhi yō’sau sadasadātmakaḥ  
> āvābhyāṃ pūjyatē ²sō’tha daivē pitryē ca kalpitē /1.31/  

¹ †M1,6,7 T2 G1,2; et al. "avyaktā"  
² M; T G "-’yaṃ hi"; CE "-’sau hi"

> nāsti tasmāt parō’nyō hi pitā dēvō’tha vā ¹dvija  
> ātmā hi nau sa vijn̄ēyas tatas taṃ ²pūjayāvahē /1.32/  

¹ M T B Dn G1,3,6; G2 CE "dvijaḥ"  
² all; M1 T1 G6 "pūjavāmahē"

> ¹tēnaiva prathitā brahman maryādā lōka²bhāvanī  
> daivaṃ pitryaṃ ca kartavyam iti tasyānuṡāsanam /1.33/  

¹ M; T G "tēnaiva sthāpitā"; CE "tēnaiṣā pra-"  
² M T2 G1; T1 G2,3,6 CE "-bhāvinī"

> brahmā sthāṇur manur dakṣō bhṛgur dharmas ¹damas tapaḥ  
> marīcir aṅgirātriṡ ca pulastyaḥ pulahaḥ kratuḥ /1.34/  

¹ M; T G CE "tapō damaḥ" (swap)

> vasiṣṭhaḥ paramēṣṭhī ca vivasvān sōma ēva ca  
> kardamaṡcāpi yaḥ prōktaḥ ¹krītō vikrīta ēva ca /1.35/  

¹ M; T G PPS (ख घ) "kṛtō vikṛtir"; B Da Dn CE "krōdhō vikrīta"; PPS "ēkatō dvita"

"Krīta" (literally meaning purchased or bought) is the name of a man mentioned in Maitrāyaṇī Saṃhitā
and Siddhānta Kaumudī. It also means a son adopted from his biological parents.

As per MW "vikṛti" and "vikrīta" are the same.
*krōdhō vikrīta* is wrong because krōdha is not mentioned as Prajāpati anywhere.

*ēkatō dvita eva ca* is wrong because what about Trita? He's also one of the
three Āptyas.

However, Kṛta is one of the Viṡvēdēvas, also name of father of Uparicara, etc.

> ēkaviṃṡatir utpannās tē prajāpatayaḥ smṛtāḥ  
> tasya dēvasya maryādāṃ pūjayanti sanātanīm /1.36/  

> daivaṃ pitryaṃ ca satataṃ tasya vijn̄āya tattvataḥ  
> ātmaprāptāni ca tatō jānanti dvijasattamāḥ /1.37/  

> svarga¹sthāpi hi yē kē cit taṃ namasyanti dēhinaḥ  
> tē tatprasādād gacchanti ²tadādiṣṭaphalāṃ gatim /1.38/  

¹ M; et al. "-sthā api yē"  
² M T G; CE "tēnā-"

> yē hīnāḥ saptadaṡabhir guṇaiḥ karmabhir ēva ca  
> kalāḥ pan̄cadaṡa tyaktvā tē muktā iti niṡcayaḥ /1.39/  

> muktānāṃ tu gatir brahman kṣētrajn̄a iti kalpitaḥ  
> sa hi sarvagataṡ caiva nirguṇaṡ ¹caiva kathyatē /1.40/  

¹ M5 T G CE; M1,6,7 "cāpi"

> dṛṡyatē jn̄ānayōgēna āvāṃ ca prasṛtau tataḥ  
> ēvaṃ jn̄ātvā ¹tam ātmānaṃ pūjayāvaḥ sanātanam /1.41/  

¹ all; M1,6,7 "mahātmānaṃ pūjayāva sa-"

> taṃ ¹dēvāṡ cāṡramāṡ caiva nānātanu²samāṡritam  
> bhaktyā sampūjayanty ādyaṃ gatiṃ caiṣāṃ ³dadhāti saḥ /1.42/  

¹ M; T G CE "vēdāṡ"  
² M1,6,7 T G; M5 "-samāṡritāḥ"; CE "-samāsthitāḥ"  
³ M G2 PPS; T G1,3,6 CE "dadāti"

> yē tu tadbhāvitā lōkē ēkāntitvaṃ ¹samāṡritāḥ  
> ētad abhyadhikaṃ tēṣāṃ ²tat tējaḥ praviṡanty uta /1.43/  

¹ M5 T G; M1,6,7 CE "samāsthitāḥ"  
² M T G; CE "yat tē taṃ"

> iti ¹guhyaḥ samuddēṡaḥ tava nārada kīrtitaḥ  
> bhaktyā prēmṇā ca ¹tasyarṣē asmad bhaktyā ca tē ³ṡrutam /1.44/  

¹ †M1,6,7 T2 G1 "guhyaḥ sa-"; T1 G2,3,6 "guhyaḥ sudurdarṡaḥ"; M5 B Dn CE PPS "guhyasa-"  
² M; T2 G1 PPS "tasyarṡēr"; T1 G2,3,6 "tarṣēṇa"; Ku GP CE "viprarṣē"  
³ M1,5,7 T2; et al. "ṡrutaḥ"

It is quite clear that all southern manuscripts begin with "ta" instead of
"viprarṣē".

Thus ends chapter M1,6,7 154; G1 157; T1 G6 158; G3 159; T2 171; G2 175; M5 190.

It has 44 verses, 92 lines;
    CE 43 verses, 91 lines;
    GP 45 verses, 91 lines;
    Ku 46 verses, ?? lines

Chapter name:

M1,6,7   "guhyōpākhyānaṃ"  
T1 G6    "nārāyaṇa-nārada-saṃvādaṃ"
