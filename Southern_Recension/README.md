# Narayaniya

Nārāyaṇīya (also spelled Narayaniya or Narayaneeyam) is a Pān̄carātra text
occuring in the Mokṣadharma sub-parva in the Book 12 of the Mahābhārata. It
forms the last 18 (or 19) chapters of the Ṡānti Parva. It discusses the
supremacy of Lord Viṣṇu, His various *vyūhas* and the importance of
*jn̄āna-bhakti* towards Him.

This repository contains the Sanskrit text derived from the Southern recension
(primarily Malayalam but also Telugu-Grantha manuscripts) as recorded in the
footnotes and appendices of the Critical Edition (CE) of the Mahabharata
published by Bhandarkar Oriental Research Institute (BORI).

## Why?

Because, why not? As Sukthankar himself says in his Prolegomena:

> For, who and what is to prevent him from constituting his own text from this
> critical edition?

In the opinion of the renowned Vedic scholar and author Dr.K.S.Narayanacharya,
the TG mss. (Kumbakonam edition) are infiltrated with unauthentic verses and do
not represent a "pure" recension. The closest to a pure recension we have is the
Bengali tradition in the north and the Malayalam tradition in the south (which
is what is followed here).

* BORI's edition is the lowest common denomitor of all Mahabharata recensions.
  It is neither Northern nor Southern but an intersection of them to appease the
  academics and not the devotee. It's basically a (Kashmir & Kerala) recension,
  given their utter disparagement and condescension towards everything that lies
  *in-between* Kashmir and Kerala: the Devanagari and Telugu-Grantha recensions,
  Nilakantha's and PPS Sastri's.
* Kumbhakonam edition suffers from [FOMO](https://en.wikipedia.org/wiki/FOMO) --
  it includes all verses from all possible recensions lest you forget an obscure
  verse that no one will ask you.
* PPS Sastri's Southern edition follows *one* peculiar manuscript (G2) which
  is majorly divergent with the rest of TG manuscripts. Being a _smārta_, he
  prefers _Ṡaiva_ readings (e.g. /16.1/ "pāṡupata" for "āraṇyaka").

### What do I actually want?

Telugu-Grantha (TG) recension offers the _longest_ known version of the
Mahabharata. Malayalam (M) offers a "purer", shorter Southern recension which has
less additions and diversions compared to TG manuscripts.

The idea here is to reproduce a distinctly Southern recension following along
the M group with some help from TG. When M and TG are a disjoint set, union with
the Eastern (E) recension (esp. Bengali) is preferred because, like the BORI CE
notes[^1], it is well-known that the Southern recension lends/borrows with the E
more than the Western or Northern group.

**Every word in the constituted text must be present in at least one Malayalam
manuscript**.

Exceptions to this guideline are indicated by a double dagger (‡) in the footnotes.

Plus, it is fun to reconstruct a recension :)

[^1]: Sabha Parva, pg xlv, "Common secondary insertions found in W and S, but
    not in E, are extremely rare...Common secondary insertions found in E and S,
    but not in W are more numerous."

## Concordance

Since there are different recensions of Mahābhārata, here I enumerate the
relavant sections:

* BORI critical edition: Chapters 12.321 to 12.339 (19 chapters)
* P.P.S Sastri's South Indian recension: Chapters 12.321 to 12.339 (19 chapters)
* Nilakantha's Bhavadipa (North Indian recension): Chapters 12.334 to 12.351 (18 chapters)
* Gita Press, Gorakhpur edition: follows same readings as Nilakantha's.
* [Asiatic Society of Bengal][AB], Calcutta edition (1830s): 12.336 to 12.353
  (18 chapters). Only [Vol.3][AB] and [Vol.4][AB4] are available. Note that this
  is **not** a Bengali recension but that of Nilakantha's.
* Kumbakonam edition (South Indian composite recension): 12.342 to 12.361 (20 chapters)
* [Dvaita critical edition][dv] (Palimaru Matha):
  Chapters 166 to 184 of Mokṣadharma, i.e., 12.334 to 12.352 (19 chapters) are
  identical to BORI except for 16 additional verses in 12.336 (starred passages
  837, 846, 848, App. II No. 31 are present but verse 82, 108, 109 missing in BORI)
* Kisari Mohan Ganguli's translation: [12.335][km1] to [12.352][km2] (18 chapters)
* Manmatha Nath Dutt's translation: 12.335 to 12.352 (18 chapters)

[AB]: http://dspace.wbpublibnet.gov.in:8080/jspui/handle/10689/5939
[AB4]: http://dspace.wbpublibnet.gov.in:8080/jspui/handle/10689/5715
[km1]: http://www.sacred-texts.com/hin/m12/m12c034.htm
[km2]: http://www.sacred-texts.com/hin/m12/m12c051.htm
[dv]: https://play.google.com/store/books/details/?id=QSGADwAAQBAJ

The conversation starts with the great King Yudhiṣṭhira questioning grandsire
Bhīṣma lying on a bed of arrows. It ends with Caturmukha-Brahmā answering Rudra
about the Supreme Puruṣa. The first verse of the first chapter of Nārāyaṇīya is
thus (Yudhiṣṭhira addressing Bhīṣma):

>    yudhiṣṭhira uvāca  
> gṛhasthō brahmacārī ca vānaprasthōʼtha bhikṣukaḥ  
> ya icchēt siddhimāsthātuṃ dēvatāṃ kāṃ yajēta saḥ  

The first verse of the last chapter is thus (Brahmā addressing Rudra):

>    brahmōvāca  
> ṡṛnu putra yathā hy ēkaḥ puruṣaḥ ṡāṡvatōʼvyayaḥ  
> akṣayaṡcāpramēyaṡca sarvagaṡca nirucyatē  

The entire Ṡānti Parva ends with the last chapter of Nārāyaṇīya (12.339) in PPS
Sastri's edition. However, all the other editions continue with many more
chapters after the Nārāyaṇīya (Ucchravṛtti-upākhyāna, etc). Not only that, even
the starting chapters are different. BORI and Sastri explicitly mention the
start of the Nārāyaṇīya with the Yudhiṣṭhira's verse, as above. Kumbakonam
edition, however, suggests its 12.344 chapter as the first chapter of Nārāyaṇīya
(it ends with ... *nārāyaṇīyē* ... unlike 12.343). Similarly, Gita Press
suggests its 12.335 as the first chapter of Nārāyaṇīya (it also ends the chapter
with ... *nārāyaṇīyē* ... unlike 12.334). BORI, Gita Press and Nilakantha
explicitly mention that the last chapter of Nārāyaṇīya is the Brahma-Rudra
conversation, as above.


| Gita Press | Verses    | BORI    | Verses | Kumbakonam | Verses | PPS Sastri | Verses        | Dvaita  | Verses | M.N.Dutt | Verses |
|:-----------|:----------|:--------|:-------|:-----------|:-------|:-----------|:--------------|:--------|:-------|:---------|-------:|
| Ch. 334    | 45  (½)   | Ch. 321 | 43     | Ch. 342    | 46     | Ch. 321    | 46            | Mo. 166 | 43     | Ch. 335  |     44 |
| Ch. 335    | 56  (1)   | Ch. 322 | 52     | Ch. 343    | 56     | Ch. 322    | 56            | Mo. 167 | 52     | Ch. 336  |     53 |
| Ch. 336    | 65        | Ch. 323 | 57     | Ch. 344    | 69     | Ch. 323    | 67            | Mo. 168 | 57     | Ch. 337  |     62 |
| Ch. 337    | 41  (1)   | Ch. 324 | 39     | Ch. 345    | 47     | Ch. 324    | 49            | Mo. 169 | 39     | Ch. 338  |     39 |
| Ch. 338    | 4         | Ch. 325 | 4      | Ch. 346    | 4      | Ch. 325    | 4  (3 + 1)    | Mo. 170 | 4      | Ch. 339  |      4 |
| Ch. 339    | 141 (15½) | Ch. 326 | 124    | Ch. 347    | 76     | Ch. 326    | 77            | Mo. 171 | 140    | Ch. 340  |    136 |
| -＂-       | -＂-      | -＂-    | -＂-   | Ch. 348    | 91     | Ch. 327    | 91            | -＂-    | -＂-   | -＂-     |   -＂- |
| Ch. 340    | 119       | Ch. 327 | 107    | Ch. 349    | 117    | Ch. 328    | 119           | Mo. 172 | 107    | Ch. 341  |    112 |
| Ch. 341    | 51        | Ch. 328 | 53     | Ch. 350    | 59     | Ch. 329    | 60 (58 + 2)   | Mo. 173 | 53     | Ch. 342  |     56 |
| Ch. 342    | 142 (2)   | Ch. 329 | 50     | Ch. 351    | 66     | Ch. 330    | 194 (7 + 187) | Mo. 174 | 50     | Ch. 343  |    140 |
| -＂-       | -＂-      | Ch. 330 | 71     | Ch. 352    | 76     | Ch. 331    | 58            | Mo. 175 | 71     | -＂-     |   -＂- |
| Ch. 343    | 66        | Ch. 331 | 52     | Ch. 353    | 67     | Ch. 332    | 69            | Mo. 176 | 52     | Ch. 344  |     64 |
| Ch. 344    | 27        | Ch. 332 | 26     | Ch. 354    | 28     | Ch. 333    | 28            | Mo. 177 | 26     | Ch. 345  |     27 |
| Ch. 345    | 28        | Ch. 333 | 25     | Ch. 355    | 27     | Ch. 334    | 28            | Mo. 178 | 25     | Ch. 346  |     28 |
| Ch. 346    | 22        | Ch. 334 | 17     | Ch. 356    | 20     | Ch. 335    | 19            | Mo. 179 | 17     | Ch. 347  |     20 |
| Ch. 347    | 96        | Ch. 335 | 89     | Ch. 357    | 94     | Ch. 336    | 94            | Mo. 180 | 89     | Ch. 348  |     93 |
| Ch. 348    | 88        | Ch. 336 | 82     | Ch. 358    | 88     | Ch. 337    | 87            | Mo. 181 | 82     | Ch. 349  |     88 |
| Ch. 349    | 74        | Ch. 337 | 69     | Ch. 359    | 74     | Ch. 338    | 97            | Mo. 182 | 69     | Ch. 350  |     73 |
| Ch. 350    | 27        | Ch. 338 | 25     | Ch. 360    | 25     | -＂-       | -＂-          | Mo. 183 | 25     | Ch. 351  |     25 |
| Ch. 351    | 23        | Ch. 339 | 21     | Ch. 361    | 23     | Ch. 339    | 24            | Mo. 184 | 21     | Ch. 352  |     23 |
| TOTAL      | 1115 (20) |         | 1006   |            | 1153   |            | 1267          |         | 1022   |          |   1087 |


## Choice of Manuscripts

Bhandarkar's Critical Edition mentions the following manuscripts for Ṡānti
parva, especially the Mokṣadharma:

* Malayalam manuscripts M1 (Malabar), M5, M6 (both Cochin), M7 (Calicut).
* Telugu manuscripts T1 (Tanjore), T2 (Punjab).
* Grantha manuscripts G1 (Melkote), G2 (Punjab), G3, G6 (both Tanjore).
* Devanagari composite manuscripts (eight): D2-9. All except D4, D6, D9 begin
  with "ṡrī gaṇēṡāya namaḥ". In the "nārāyaṇaṃ namaskṛtya" mantra D8 reads
  "vyāsaṃ" but others D2,3,5,7 read "caiva".
  - D2,3,8 esp. D2,3,5,8, form a set, often aligning with Bengali.
  - D4,9 esp. with D4,5,7,9 go well with T G (esp. D7 T G).
    D8 is explictly Ṡaiva (see CE12*0870_01)

I have great respect for the work of BORI CE. {T2 G1 M5} are especially quite
agreeable with CE compared to rest of TGM manuscripts, hence the preference.
Similarly, {T1 G3,6} often occur as a group completely divergent from the CE and
are rarely considered in constituting the final text.

The constituted text is determined in this order:

    M5     ∩ M1,6,7 or
    M5     ∩ T2 G1  or
    M1,6,7 ∩ TG≥3   or
    M5     ∩ B Dn   or
    CE

Superscript 0 (that is ⁰) means `M ∩ CE` as the constituted text but other
mss. have uninteresting variants.

Anusvāra, visarga and halanta variants, esp. at end of a line, are corrected as
per CE's text.  Being a Southern edition, `ḻ` is preferred in certain words
(such as _praḻaya_) instead of a plain `l`.

The scheme of division of the adhyāyas still follows the Northern pattern. I see
no reason to deviate from that, because TGM mss. are totally inconsistent
amongst themselves.

The verses are consistently checked for [Anuṣṭubh metre][mtr] or any other metre
as applicable. This usually helps to weed out spurious readings.

[mtr]: https://sanskritmetres.appspot.com/

### Exceptions

Four mss. (M1,5-7) form a small sample space to make conclusive readings.
Focusing on the Malayalam group, especially M5, brings its own challenges.
Hence, exceptions have to be made.

The downside is that we're exposed to all the peculiarities and errors of a
single manuscript (M5). But this is easy to overcome. Out of the ten Southern
mss., if (TG + M≥1) mss. (i.e., at least seven mss.) provide a _uniform_
reading, then that majority reading is chosen (instead of M5's). All such
instances are noted in the footnotes with a dagger (†).

When the constituted text is completely different from the M. group, it is shown
with a double dagger (‡).

By _uniform_ reading is meant an identical reading among the mss. (if it exists)
or the same passages being added/deleted in all of them.

### Example

Consider this:

> ¹maithunāyāgatō yasmāt tvayāhaṃ vinivāritaḥ  
> tasmād andhō ⁰jāsyasi tvaṃ macchāpān nātra saṃṡayaḥ /8.51/  

¹ M5,6 B Dn T1 G3,6; T2 G1,2 M1,7 CE PPS "maithunōpagatō"

The constituted text is determined as follows:

1. Does M5 agree with the rest of Malayalam mss. (M1,6,7)? No.
2. Does M5 agree with T2 G1? No.
3. Does M5 agree with Bengali? Yes. So choose this.

The fact that M5 agrees with other TG mss (T1 G3,6) or disagrees with the CE is
inconsequential.

Similarly, `⁰jāsyasi` means that Malayalam and CE agree on the reading but other
mss. offer variant readings which don't affect the final constituted text. The
exact variations can be found from BORI CE itself. In this case:

> B Dn "yāsyasi"; G1 "jāyasi"; T1 G3,6 "tasmād andhōsi sa tvaṃ hi";
> G2 "tasmād andhō bhava tvaṃ hi"

## Formatting

* All the verse lines begin with ">" and end with two spaces.
* All the "uvāca" and "ūcuḥ" lines begin with ">    " (that's not SPACE \x20 but
  Unicode EM QUAD \x2001).
* Long pādas are separated to next line starting with ">  " (that's not SPACE
  \x20 but Unicode EN SPACE \x2002).
* To count the number of lines in Emacs, M-s o, enter "> ".

## Abbreviations
 
* � -- denotes missing reading in ms
* ◌  -- denote omissions in constituted text but not others
* Cs -- Ānandapūrṇa Vidyāsāgara's commentary (Vyākhyāna-ratnāvalī)
* Cv -- Vadiraja's commentary (Lakṣālaṅkāra)
* CE -- BORI critical edition
* Dv -- Dvaita (Palimaru Matha) edition
* GP -- Gita Press edition
* in. -- inserts, inserted
* Ku -- Kumbhakonam edition
* L -- Line, as in L1 for the first line, L2 for the second line.
* marg -- margin text
* MW -- Monier-Williams dictionary
* om. -- omits, omitted
* orig. -- original text
* PPS -- PPS Sastri's edition
* (sic) -- incorrect reading, but as given in manuscript anyway

"K" is shorthand for "K1,2,4,7" (four Kashmiri manuscripts).  
"D" is shorthand for "D4,5,7,9" (four Devanagari manuscripts).  
"T" is shorthand for "T1,2" (two manuscripts).  
"G" is shorthand for "G1,2,3,6" (four manuscripts).  
"M" is shorthand for "M1,5,6,7" (four manuscripts).

Starred passages in CE are numbered like CE@809.  
Uncertain and inconclusive readings are denoted by ?.

TODO

* Replace "M5 B" or "M5 Dn" (but not "M B" or "M D"!) with respective T G variants.
* Prefix or suffix hyphen when superscript numbers occur in between words.
* Check for "uvāca"/"ūcuḥ" sequence repetitions
* Check for missing verse references, like //
* Check end-of-chapter verse counts of GP, Ku
