# Chapter 06

>    bhīṣma uvāca  
> ēvaṃ stutaḥ sa bhagavān guhyais ¹stavyaiṡ ca nāmabhiḥ  
> taṃ muniṃ darṡayām āsa nāradaṃ viṡvarūpadhṛk /6.1/  

¹ M T G PPS; CE Ku GP "tathyaiṡ"

> kin̄cic ¹candrād viṡuddhātmā kin̄cic ²candrād viṡēṣavān  
> ³kṛṡānuvarṇaḥ kin̄cic ca kin̄cid ⁴dhiṣṇyavad īkṣyatē /6.2/  

¹ M B Dn; et al. "candravi-"  
² all; T G "candravi-"  
³ all; M1,6,7 "kaṡānu-"  
⁴ ?M1,6,7; M5 "dhiṣṇyēva lakṣyatē"; B Dn CE "dhiṣṇyākṛtiḥ prabhuḥ";
  T G1,3,6 PPS "-kṛtiprabhaḥ"; G2 "-d vyajyākṛtiprabhaḥ"  

> ṡukapatra¹varṇaḥ kin̄cit ca kin̄cit sphaṭika²saprabhaḥ  
> nīlān̄janacayaprakhyō jātarūpa³prabhaḥ kva cit /6.3/  

¹ M T2 G1 CE; T1 G3,6 B Dn "-nibhaḥ kin̄cit"; G2 PPS "ṡukavarṇas trivarṇaṡ ca"  
² M CE; T G B Dn PPS  "-sannibhaḥ"  
³ ?M5 B Dn CE; M1,6,7 T1 G2,3,6 PPS "-nibhaḥ"; T2 G1 "� � "

/6.3L1/ prior-half is hypermetric in M CE (which reads "kin̄cic ca kin̄cit").

> pravāḻāṅkuravarṇaṡ ca ṡvētavarṇaḥ kvacid babhau  
> kvacit ¹suvarṇavarṇābhō vaiḍūryasadṛṡaḥ kvacit /6.4/  

¹ all; M1 "svarṇa-"; M7 "svavarṇa-"

> nīlavaiḍūryasadṛṡa indranīlanibhaḥ kvacit  
> mayūragrīvavarṇābhō muktāhāranibhaḥ kvacit /6.5/  

> ētān varṇān bahuvidhān rūpē bibhrat sanātanaḥ  
> sahasranayanaḥ ṡrīmān̄ ṡataṡīrṣaḥ sahasrapāt /6.6/  

> sahasrōdarabāhuṡ ca avyakta ¹iva ca kvacit  
> ōṅkāram udgiran vaktrāt sāvitrīṃ ca tadanvayām /6.7/  

¹ M T G2,3,6; et al. "iti"

> ṡēṣēbhyaṡ caiva vaktrēbhyaṡ caturvēdōdgataṃ vasu  
> āraṇyakaṃ jagau dēvō harir nārāyaṇō vaṡī /6.8/  

> ¹vēdīṃ kamaṇḍaluṃ darbhān maṇirūpān ²athō navān  
> ajinaṃ daṇḍakāṣṭhaṃ ca jvalitaṃ ca hutāṡanam  
> dhārayām āsa dēvēṡō hastair yajn̄apatis tadā /6.9/  

¹ M1,6 B CE; M6,7 T G1,3,6 Dn PPS "vēdim"; G2 "vēdir"  
² M; CE "athōpalān"; GP "-ahau kuṡān"; PPS "athō dhruvān";
  T2 G1 "athō sruvaṃ"; Ku "tathā kuṡān"; G3 "athō srucaṃ"  

> taṃ prasannaṃ prasannātmā nāradō dvijasattamaḥ  
> vāgyataḥ ¹prayatō bhūtvā vavandē paramēṡvaram  
> tam uvāca nataṃ mūrdhnā dēvānām ādir avyayaḥ /6.10/  

¹ M5 T G CE; M1,6,7 B Dn "praṇatō"

> ēkataṡ ca dvitaṡ caiva tritaṡ caiva maharṣayaḥ  
> imaṃ dēṡam anuprāptā mama darṡanalālasāḥ /6.11/  

Before /6.11/ B Dn in. "ṡrībhagavān uvāca".

> na ca māṃ tē dadṛṡirē na ca drakṣyati kaṡcana  
> ṛtē hy ¹ēkāntinaṡrēṣṭhāt tvaṃ caivaikāntikō mataḥ /6.12/  

¹ M T2 G1,2 PPS; T1 G3,6 D7 "ēkāntinaḥ ṡrē-"; CE "ēkantika-"

> mamaitās tanavaḥ ṡrēṣṭhā jātā dharmagṛhē dvija  
> tās tvaṃ bhajasva satataṃ sādhayasva yathāgatam /6.13/  

> ¹vṛṇīṣva ca varaṃ vipra mattas tvaṃ ²yad ihēcchasi  
> ³prasannō hi tavādyāhaṃ viṡvamūrtir ihāvyayaḥ /6.14/  

¹ M B Dn CE; T G "varaṃ vṛṇīṡva viprēndra"  
² M B Dn G1,2 PPS; T G3,6 CE "yam"  
³ M5 T G1,3,6; G2 PPS "-tavātmānaṃ"; CE Dn "prasannō’haṃ tavādyēha"

>    nārada uvāca  
> adya mē tapasō dēva yamasya niyamasya ca  
> sadyaḥ phalam avāptaṃ vai dṛṣṭō yad bhagavān mayā /6.15/  

> vara ēṣa mamātyantaṃ dṛṣṭas tvaṃ yat sanātanaḥ  
> bhagavān ¹viṡvakṛt siṃhaḥ sarvamūrtir mahā²prabhuḥ /6.16/  

¹ ?M D4,9; et al. "viṡvadṛk"  
² all; M1,6 "-prabhaḥ"

>    ¹bhīṣma uvāca  
> ēvaṃ sandarṡayitvā tu nāradaṃ paramēṣṭhijam  
> uvāca vacanaṃ bhūyō gaccha nārada māciram /6.17/  

¹ M T G om. but CE PPS don't.

>    ¹ṡrībhagavān uvāca  
> imē hy ²atīndriyāhārā  madbhaktāṡ candravarcasaḥ  
> ēkāgrāṡ cintayēyur māṃ naiṣāṃ vighnō bhavēd iti /6.18/  

¹ ‡No manuscript in. this, but needed so that the speaker changes to "bhīṣma  
  uvāca" again /6.100/ (which is in. by all manuscripts).
² ?M D5,7 Da3; et al. "anindriyāhārā"

> siddhāṡ caitē mahābhāgāḥ purā hy ēkāntinō’bhavan  
> ¹tamōrajōvinirmuktā māṃ pravēkṣyanty asaṃṡayam /6.19/  

¹ M CE; T G PPS "-rajōbhyāṃ nirmuktā"

> na dṛṡyaṡ cakṣuṣā yō’sau na spṛṡyaḥ sparṡanēna ca  
> na ghrēyaṡ caiva gandhēna rasēna ca vivarjitaḥ /6.20/  

T G D7 PPS repeat /6.20/ and /6.21/ after /6.40L1/ and  
M K1,2,4,7 D4,5,9 repeat the same after /6.45L1/ but Dn CE Ku om.

> sattvaṃ rajas tamaṡ caiva na guṇās taṃ bhajanti vai  
> yaṡ ca sarvagataḥ sākṣī lōkasyātmēti kathyatē /6.21/  

> bhūtagrāmaṡarīrēṣu naṡyatsu na vinaṡyati  
> ajō nityaḥ ṡāṡvataṡ ca nirguṇō niṣkaḻas tathā /6.22/  

> ¹dvirdvādaṡēbhyas tattvēbhyaḥ khyātō yaḥ pan̄caviṃṡakaḥ  
> puruṣō niṣkriyaṡ caiva jn̄ānadṛṡyaṡ ca kathyatē /6.23/  

¹ †all; M1,5,6 "dvidvā-"

> yaṃ praviṡya bhavantīha ¹muktā vai dvijasattama  
> sa vāsudēvō vijn̄ēyaḥ paramātmā sanātanaḥ /6.24/  

¹ all; M1,6 "mattā"

> paṡya dēvasya māhātmyaṃ mahimānaṃ ca nārada  
> ṡubhāṡubhaiḥ karmabhir yō na lipyati kadācana /6.25/  

> sattvaṃ rajas tamaṡ ¹cēti guṇān ētān pracakṣatē  
> ētē sarva²ṡarīrēṣu tiṣṭhanti vi³caranti ca /6.26/  

¹ M B Dn; T G CE PPS "caiva"  
² all; M1,6,7 "-ṡarīrē ca"  
³ ‡all; M "-ramanti"

> ētān guṇāṃs tu kṣētrajn̄ō bhuṅktē ¹naibhiḥ sa bhujyatē  
> nirguṇō guṇabhuk caiva guṇasraṣṭā guṇādhikaḥ /6.27/  

¹ all; T G "tair na"

> jagatpratiṣṭhā dēvarṣē pṛthivy apsu pralīyatē  
> jyōtiṣy āpaḥ pralīyantē jyōtir ¹vāyau pralīyatē /6.28/  

¹ all; M1,6,7 "vāyōḥ"

> khē vāyuḥ praḻayaṃ yāti manasy ākāṡam ēva ca  
> manō hi paramaṃ bhūtaṃ tad avyaktē pralīyatē /6.29/  

> avyaktaṃ puruṣē brahman niṣkriyē sampralīyatē  
> nāsti tasmāt ¹parataraṃ puruṣād vai sanātanāt /6.30/  

¹ all; T G "paraṃ bhūtaṃ"

> nityaṃ hi nāsti jagati ¹bhūtaṃ sthāvarajaṅgamam  
> ṛtē tam ēkaṃ puruṣaṃ vāsudēvaṃ sanātanam  
> sarvabhūtātmabhūtō hi vāsudēvō mahābalaḥ /6.31/  

¹ all; M1,6,7 "jagat"

> pṛthivī vāyur ākāṡam āpō jyōtiṡ ca pan̄camam  
> tē samētā mahātmānaḥ ṡarīram iti san̄jn̄itam /6.32/  

> tadāviṡati ¹antarātmā adṛṡyō laghuvikramaḥ  
> utpanna ēva bhavati ṡarīraṃ ²cēṣṭayan ³prabhuḥ /6.33/  

¹ M T G; CE "yō brahmann"  
² †all; M5 "cēṣṭayēt"; M6 "cēṣṭayat"  
³ all; M1,6,7 "punaḥ"

> na vinā dhātusaṅghātaṃ ṡarīraṃ ¹bhavati kvacit  
> na ca jīvaṃ vinā brahman dhātavaṡ cēṣṭayanty uta /6.34/  

¹ all; M6 "cēṣṭayat prabhuḥ"; M7 "cēṣṭayan prabhuḥ" (cf. /6.33/)

> sa jīvaḥ parisaṅkhyātaḥ ṡēṣaḥ saṅkarṣaṇaḥ prabhuḥ  
> tasmāt sanatkumāratvaṃ ¹yō’labhat svēna karmaṇā /6.35/  

¹ M B Dn; CE "yō labhēta svakarmaṇā"; T G "labhatē yaḥ svakarmaṇā"

> yasmiṃṡ ca ¹sarvabhūtāni praḻayaṃ yānti saṅkṣayē  
> ²mānasaḥ sarvabhūtānāṃ pradyumnaḥ paripaṭhyatē /6.36/  

¹ all; M1,7 "pan̄ca-"  
² M; T G CE "sa manaḥ"

> tasmāt prasūtō yaḥ kartā ¹kāraṇaṃ kāryam ēva ca  
> yasmāt sarvaṃ ²sambhavati jagat sthāvarajaṅgamam  
> sō’niruddhaḥ sa īṡānō ³vyaktiḥ sā sarvakarmasu /6.37/  

¹ M B Dn; et al. "kāryaṃ kāraṇaṃ" (swap)  
² M T G; CE "prabha-"  
³ M5 G1 CE; M1,6,7 "vyaktisthaḥ"; B Dn "vyaktiḥ sa sar-"; T G2,3,6 "vyaktaḥ sā sātvatakarmasu"

> yō vāsudēvō bhagavān kṣētrajn̄ō nirguṇātmakaḥ  
> jn̄ēyaḥ sa ēva bhagavān̄ jīvaḥ saṅkarṣaṇaḥ prabhuḥ /6.38/  

> saṅkarṣaṇāc ca pradyumnō manōbhūtaḥ sa ucyatē  
> pradyumnād ¹yō’niruddhas tu sō’haṅ²kārō mahēṡvaraḥ /6.39/  

¹ ‡T G B Dn CE PPS; M1,6,7 "sō’"; M5 "� �"  
² ‡T G CE PPS; M1,6,7 "-kārō’ham īṡvaraḥ"; B Dn "-kārō sa īṡvaraḥ"

> mattaḥ sarvaṃ sambhavati jagat sthāvarajaṅgamam  
> akṣaraṃ ca kṣaraṃ caiva sac cāsac caiva nārada /6.40/  

> māṃ praviṡya bhavantīha muktā bhaktās tu yē mama  
> ahaṃ hi puruṣō jn̄ēyō niṣkriyaḥ pan̄caviṃṡakaḥ /6.41/  

> nirguṇō niṣkaḻaṡ caiva nirdvandvō niṣparigrahaḥ  
> ētat tvayā na vijn̄ēyaṃ rūpavān ¹iva dṛṡyatē  
> icchan muhūrtān naṡyēyam īṡō’haṃ jagatō guruḥ /6.42/  

¹ M T G3,6; et al. "iti"

> māyā hy ēṣā mayā ¹sṛṣṭā yan māṃ ²paṡyasi nārada  
> sarvabhūtaguṇair yuktaṃ naivaṃ ³tvaṃ jn̄ātum arhasi  
> mayaitat ⁴kathitaṃ samyak tava mūrticatuṣṭayam /6.43/  

¹ all; M1,7 "dṛṣṭā"  
² all; M1,7 D7 "paṡyati"  
³ M CE; T G "māṃ"  
⁴ all; M1,6,7 "kathitā"

> siddhā ¹hy ētē mahābhāgā narā hy ēkāntinō’bhavan  
> tamōrajōbhyāṃ nirmuktāḥ pravēkṣyanti ca māṃ munē /6.44/  

¹ M5 T G CE; B Dn M1,6,7 "hi tē"

> ahaṃ kartā ca kāryaṃ ca kāraṇaṃ cāpi nārada  
> na dṛṡyaṡ cakṣuṣā ¹jīvō na spṛṡyaḥ sparṡanēna ca  
> na ghrēyaṡ caiva gandhēna rasēna ca vivarjitaḥ /6.45/  

¹ M T2 G2; CE "yo’sau"

M T G PPS repeat /6.20/ as /6.45L2/ and /6.45L3/ (with variations).

> sattvaṃ rajas tamaṡ caiva na guṇās taṃ bhajanti ¹mām  
> ²sa hi sarvagataḥ sākṣī lōkasyātmēti kathyatē /6.46/  

¹ M; CE "vai"  
² M T G; CE "yaṡ ca"

M T G PPS repeat /6.21/ as /6.46/ (with variations).

> ahaṃ hi jīvasan̄jn̄ō vai mayi jīvaḥ ¹samāhitaḥ  
> maivaṃ tē buddhir ²abhavad dṛṣṭō jīvō mayēti ³ha /6.47/  

¹ all; M1,6,7 "sanātanaḥ"  
² M; T G "bhavatu"; CE "atrābhūd"  
³ M T2 G1; et al. "ca"

> ahaṃ sarvatragō brahman bhūtagrāmāntarātmakaḥ  
> bhūtagrāmaṡarīrēṣu naṡyatsu na naṡāmy aham /6.48/  

M T G in. "Additional Passages -- Part 1" here.

> hiraṇyagarbhō lōkādiṡ ¹caturvaktrō niruktagaḥ  
> ²brahmā sanātanō dēvō mama bahvarthacintakaḥ /6.49/  

¹ all; M1,6,7 "-vaktrē"  
² M CE; T G "brahmāsana gatō dēvō"

> paṡyaikādaṡa mē rudrān dakṣiṇaṃ ¹pārṡvam ²āsthitān  
> dvādaṡaiva ³tathādityān vāmaṃ ¹pārṡvaṃ ⁴samāsthitān /6.50/  

¹ M CE; T G "pakṣam"  
² ?M1,5 B Dn CE; T G M6,7 "āṡritān"  
³ all; M1,6,7 "yathā-"  
⁴ ?M5 B Dn CE; M1,6,7 T G1,3,6 "samāṡritān"; G2 PPS "athāṡritān"

> agrataṡ caiva mē paṡya vasūn aṣṭau ⁰surōttamān  
> nāsatyaṃ caiva dasraṃ ca bhiṣajau paṡya pṛṣṭhataḥ /6.51/  

> sarvān prajāpatīn paṡya paṡya sapta ṛṣīn api  
> vēdān yajn̄āṃṡ ca ⁰ṡataṡaḥ ¹paṡyāmṛtam ²athauṣadhīḥ /6.52/  

¹ M5 G1 B Dn CE; M1,6,7 "paṡyāmūṡ ca"; T G2,3,6 "paṡya sruk ca"  
² ‡CE; T G M1,6,7 "mahauṣadhīḥ"; M5 "ahauṣadhīḥ"

> tapāṃsi niyamāṃṡ caiva yamān api pṛthagvidhān  
> tathāṣṭaguṇam aiṡvaryam ēkasthaṃ paṡya ¹mūrtimat /6.53/  

¹ all; M1,6,7 "mūrtimān"

> ṡriyaṃ lakṣmīṃ ca kīrtiṃ ca pṛthivīṃ ca kakudminīm  
> vēdānāṃ mātaraṃ paṡya matsthāṃ dēvīṃ sarasvatīm /6.54/  

> dhruvaṃ ca jyōtiṣāṃ ṡrēṣṭhaṃ paṡya nārada khēcaram  
> ambhōdharān samudrāṃṡ ca sarāṃsi saritas tathā /6.55/  

> ¹mūrtiyuktān pitṛgaṇāṃṡ ²caturaḥ paṡya sattama  
> trīṃṡ caivēmān ³guṇān paṡya matsthān mūrtivivarjitān /6.56/  

¹ M D K; T G CE "mūrtimantaḥ"  
² all; M1,6,7 "paṡya mat sthāṃ caturvidhān"  
³ all; M1,6,7 "gaṇān"

> dēvakāryād api munē pitṛkāryaṃ viṡiṣyatē  
> dēvānāṃ ca pitṝṇāṃ ca pitā hy ēkō’ham āditaḥ /6.57/  

> ahaṃ hayaṡirō bhūtvā samudrē paṡcimōttarē  
> ¹pibāmi suhutaṃ havyaṃ kavyaṃ ca ṡraddhayānvitam /6.58/  

¹ †all; M5 "aṡnāmi"

> mayā sṛṣṭaḥ purā brahmā madyajn̄am ayajat svayam  
> tatas tasmai varān prītō dadāv aham anuttamān /6.59/  

> matputratvaṃ ca kalpādau lōkādhyakṣatvam ēva ca  
> ahaṅkārakṛtaṃ caiva nāma paryāyavācakam /6.60/  

> tvayā kṛtāṃ ca maryādāṃ nāti¹krāmati kaṡcana  
> tvaṃ caiva varadō brahman varēpsūnāṃ bhaviṣyasi /6.61/  

¹ M T G; CE "-krāmyati"

> surāsuragaṇānāṃ ca ¹maharṣīṇām tathaiva ca  
> pitṝṇāṃ ca mahābhāga satataṃ saṃṡitavrata  
> vividhānāṃ ca bhūtānāṃ tvam upāsyō bhaviṣyasi /6.62/  

¹ M T G; CE "ṛṣīṇāṃ ca tapōdhana"

> prādurbhāvagataṡ cāhaṃ surakāryēṣu nityadā  
> anuṡāsyas tvayā brahman niyōjyaṡ ca sutō yathā  /6.63/  

> ētāṃṡ cānyāṃṡ ca rucirān brahmaṇē’mitatējasē  
> ahaṃ dattvā varān prītō nivṛttiparamō’bhavam /6.64/  

> nirvāṇaṃ sarvadharmāṇāṃ nivṛttiḥ paramā smṛtā  
> tasmān ¹nivṛttim āpannaṡ carēt ²sarvatranirvṛtaḥ /6.65/  

¹ ‡all; M "nivṛttiparamaṡ"  
² M T G; CE "sarvāṅga-"

> vidyāsahāyavantaṃ mām ādityasthaṃ sanātanam  
> kapilaṃ prāhur ācāryāḥ ⁰sāṅkhyaniṡcitaniṡcayāḥ /6.66/  

> hiraṇyagarbhō bhagavān ēṣa ¹yac chandasi ṣṭutaḥ  
> sō’haṃ yōga²ratir brahman yōgaṡāstrēṣu ³ṡabditaḥ /6.67/  

¹ M; T G "-si stutaḥ"; CE "chandasi suṣṭutaḥ"  
² M1,5,6 B Dn; T G CE "-ratir"  
³ †all; M5 "ṡabditaḥ"

> ¹ēṣō’haṃ vyaktim āgamya tiṣṭhāmi ²divi ṡāṡvataḥ  
> tatō yugasahasrāntē saṃhariṣyē jagat punaḥ  
> kṛtvātmasthāni bhūtāni sthāvarāṇi carāṇi ca /6.68/  

¹ all; M1,7 "ēkō’haṃ vyatkam"  
² M CE; T G "bhuvi"

> ēkākī vidyayā sārdhaṃ ¹vicariṣyē dvijōttama  
> tatō bhūyō jagat sarvaṃ kariṣyām²īha vidyayā /6.69/  

¹ M D K; et al. "vihariṣyē"  
² M CE; T G "-īti"

> ¹asmanmūrtiṡ caturthī yā sāsṛjac chēṣam avyayam  
> sa hi saṅkarṣaṇaḥ prōktaḥ pradyumnaṃ ²sō’py ajījanat /6.70/  

¹ M CE; T G "ātma-"  
² all; M1,6,7 "sō’vyajījagam"

> pradyumnād aniruddhō’haṃ sargō mama punaḥ punaḥ  
> aniruddhāt tathā brahmā tatrādikamalōdbhavaḥ /6.71/  

> brahmaṇaḥ sarvabhūtāni ¹carāṇi sthāvarāṇi ca  
> ētāṃ sṛṣṭiṃ vijānīhi ²kalpāntēṣu punaḥ punaḥ /6.72/  

¹ all; M5,6 "trasāni"  
² M B6,9 Da K6; et al. "kalpādiṣu"

As per MW, "trasāni" and "carāṇi" both mean the group of moving beings.

> yathā ¹sūryasya gamanād ²udayāstamanē iha  
> ³naṣṭē punar balāt kāla ānayaty amitadyutiḥ  
> tathā balād ahaṃ pṛthvīṃ sarvabhūtahitāya vai /6.73/  

¹ M T2 G2; T1 G1,3,6 "sūryaḥ svagamanād"; CE "-gaganād"  
² M B Da; et al. "udayāstamayāv"  
³ M B Dn; T G CE "naṣṭau"

T G PPS Ku end chapter here but M B Dn CE don't.  
In the new chapter, T G PPS Ku continue to in. CE@835 (10 lines) here but M CE om.

> sattvair ākrāntasarvāṅgāṃ naṣṭāṃ sāgaramēkhalām  
> ānayiṣyāmi ¹svaṃ sthānaṃ ²vārāhaṃ ³rūpam āsthitaḥ /6.74/  

¹ M5 T G CE; Dn M1,6,7 "svasthā-"  
² all; M1,7 "varāhaṃ"  
³ †all; M5 "vapur"

> hiraṇyākṣaṃ haniṣyāmi daitēyaṃ balagarvitam  
> nārasiṃhaṃ vapuḥ kṛtvā hiraṇyakaṡipuṃ punaḥ  
> surakāryē haniṣyāmi yajn̄aghnaṃ ditinandanam /6.75/  

> virōcanasya balavān baliḥ putrō mahāsuraḥ  
> bhaviṣyati sa ṡakraṃ ca svarājyāc ¹cyāvayiṣyati /6.76/  

¹ M B Dn CE; T G "cālayiṣyati"

> trailōkyē’pahṛtē tēna vimukhē ca ¹ṡacīpatau  
> adityāṃ dvādaṡaḥ putraḥ sambhaviṣyāmi ¹kāṡyapāt /6.77/  

¹ ?M6 T G CE; M5 "ṡatakratau"; M1,7 "� � � �"  
² M5,6 T G; et aḷ "kaṡyapāt"

T G PPS Cv in. CE@837 (6 lines) here but M CE om.

> tatō rājyaṃ pradāsyāmi ṡakrāyāmitatējasē  
> dēvatāḥ sthāpayiṣyāmi svēṣu sthānēṣu nārada  
> baliṃ caiva kariṣyāmi pātāḻatalavāsinam /6.78/  

> trētāyugē bhaviṣyāmi rāmō bhṛgukulōdvahaḥ  
> kṣatraṃ cōtsādayiṣyāmi ¹pravṛddhabalavāhanam /6.79/  

¹ M T G; CE "samṛddha-"

> ¹sandhyāṃṡē samanuprāptē trētāyāṃ ²dvāparasya ca  
> rāmō dāṡarathir bhūtvā bhaviṣyāmi jagatpatiḥ /6.80/  

¹ M B Da Dn; T G CE "sandhau tu"  
² ‡all; M "dvāparāditaḥ"

> tritōpaghātād vairūpyam ēkatō’tha dvitas tathā  
> prāpsyatō vānaratvaṃ hi prajāpatisutāv ṛṣī /6.81/  

> tayōr yē tv anvayē jātā bhaviṣyanti vanaukasaḥ  
> tē sahāyā bhaviṣyanti surakāryē mama dvija /6.82/  

28 out of 32 manuscripts in. CE@840 (1 line) after /6.82L1/ but M CE om.

> tatō rakṣaḥpatiṃ ghōraṃ pulastyakulapāṃsanam  
> ¹hariṣyē rāvaṇaṃ ²dṛptaṃ sagaṇaṃ lōkakaṇṭakam /6.83/  

¹ M B; T G Dn CE "haniṣyē"  
² M; G "saṅghē"; B Dn "raudraṃ"; T1 "saṅkhē"; T2 CE "saṅkhyē"

> dvāparasya kalēṡ caiva sandhau paryavasānikē  
> prādurbhāvaḥ kaṃsahētōr ¹mathurāyāṃ bhaviṣyati /6.84/  

¹ ‡all; M "madhurāyāṃ"

> tatrāhaṃ dānavān hatvā subahūn dēvakaṇṭakān  
> kuṡasthalīṃ kariṣyāmi nivāsaṃ dvārakāṃ purīm /6.85/  

> vasānas tatra vai puryām aditēr vipriyaṅkaram  
> haniṣyē narakaṃ bhaumaṃ muraṃ pīṭhaṃ ca dānavam /6.86/  

> prāgjyōtiṣapuraṃ ramyaṃ nānādhanasamanvitam  
> kuṡasthalīṃ nayiṣyāmi hatvā vai dānavōttamān /6.87/  

> ¹ṡaṅkaraṃ samahāsēnaṃ bāṇapriya²hitēratam  
> parājēṣyāmy athōdyuktau ³dēvaulōkanamaskṛtau /6.88/  

¹ M T G; B Dn "mahēṡvara mahāsēnau"; CE "-raṃ ca mahā-"  
² M5 T G; M1,6,7 CE "-hitaiṣiṇam"  
³ M5 T G B Dn; M1,6,7 CE "dēvalōka-"

> tataḥ sutaṃ balēr jitvā bāṇaṃ bāhusahasriṇam  
> vināṡayiṣyāmi tataḥ sarvān saubhanivāsinaḥ /6.89/  

T G PPS Ku in. CE@844 (8 lines) here.

> yaḥ kālayavanaḥ khyātō gargatējōbhi¹saṃvṛtaḥ  
> bhaviṣyati vadhas tasya matta ēva dvijōttama /6.90/  

¹ all; B7,9 Da M1,6,7 "-sambhṛtaḥ"

> jarāsandhaṡ ca balavān sarvarājavirōdhakaḥ  
> bhaviṣyaty asuraḥ sphītō bhūmipālō girivrajē  
> mama buddhipari¹spandād vadhas tasya bhaviṣyati /6.91/  

¹ ‡all; M1,7 "-ṣyandād"; M5,6 "-ṣpandād"

All mss. (23) except K and M in. CE@846 (1 line) here.

> samāgatēṣu baliṣu pṛthivyāṃ sarvarājasu  
> vāsaviḥ susahāyō vai mama hy ēkō bhaviṣyati /6.92/  

> ēvaṃ lōkā vadiṣyanti naranārāyaṇāv ṛṣī  
> ¹udyatau dahataḥ kṣatraṃ lōkakāryārtham īṡvarau /6.93/  

¹ M T G; et al. "udyuktau"

> kṛtvā bhārāvataraṇaṃ vasudhāyā yathēpsitam  
> sarvasātvatamukhyānāṃ dvārakāyāṡ ca sattama  
> kariṣyē ¹kadanaṃ ghōram ātmajn̄ātivināṡanam /6.94/  

¹ M; T G CE "praḻayaṃ"

> karmāṇy aparimēyāni caturmūrtidharō hy aham  
> kṛtvā lōkān ¹gamiṣyāmi svān ²ēva brahma³satkṛtān /6.95/  

¹ all; M1 "bhaviṣ-"; M6,7 "bhajiṣ-"  
² M5 T G; et al. "ahaṃ"  
³ all; M1,6,7 "-saṃstutam"

> ¹simhaḥ kūrmaṡ ca matsyaṡ ca prādurbhāvā dvijōttama  
> yadā vēdaṡrutir naṣṭā ²tadā pratyāhṛtā mayā  
> savēdāḥ saṡrutīkāṡ ca kṛtāḥ pūrvaṃ kṛtē yugē  /6.96/  

¹ M; B Dn "hamsaḥ kū-"; T G CE "haṃsō hayaṡirāṡ caiva"  
² M T G; CE swap "mayā" and "tadā"

> atikrāntāḥ purāṇēṣu ṡrutās tē yadi vā kvacit  
> atikrāntāṡ ca bahavaḥ prādurbhāvā mamōttamāḥ  
> lōkakāryāṇi kṛtvā ca punaḥ svāṃ prakṛtiṃ gatāḥ /6.97/  

M T G K D (18 mss.) in. "Additional Passages -- Part 2" here.
PPS quotes them in the footnotes instead.

> na hy ētad brahmaṇā prāptam īdṛṡaṃ mama darṡanam  
> yat tvayā prāptam adyēha ēkāntagatabuddhinā /6.98/  

> ētat tē sarvam ākhyātaṃ brahman bhaktimatō mayā  
> purāṇaṃ ca ¹bhaviṣyac ca sarahasyaṃ ca sattama /6.99/  

¹ M T G; et al. "bhaviṣyaṃ"

>    ¹bhīṣma uvāca  
> ēvaṃ sa bhagavān dēvō viṡvamūrtidharō’vyayaḥ  
> ētāvad uktvā vacanaṃ tatraivān²taradhīyata /6.100/  

¹ all; CE om.  
² M5 T G CE; M1,6,7 B Da "-tardadhē prabhuḥ"

> nāradō’pi mahātējāḥ prāpyānugraham īpsitam  
> naranārāyaṇau draṣṭuṃ prādravad badarāṡramam /6.101/  

> idaṃ mahōpaniṣadaṃ caturvēdasamanvitam  
> sāṅkhyayōga¹kṛtāntēna pan̄carātrānuṡabditam /6.102/  

¹ M T G; CE "-kṛtaṃ tēna"

> nārāyaṇamukhōd¹gītaṃ nāradō’ṡrāvayat punaḥ  
> ²brahmaṇaḥ sadanē tāta yathā dṛṣṭaṃ yathā ṡrutam /6.103/  

¹ all; M5 "-gāraṃ"  
² all; M1,7 "brāhmaṇaḥ"

>    yudhiṣṭhira uvāca  
> ētad āṡcaryabhūtaṃ hi māhātmyaṃ tasya dhīmataḥ  
> kiṃ brahmā na vijānītē yataḥ ṡuṡrāva nāradāt /6.104/  

> pitāmahō hi bhagavāṃs tasmād dēvād anantaraḥ  
> kathaṃ sa na vijānīyāt prabhāvam amitaujasaḥ /6.105/  

>    bhīṣma uvāca  
> mahākalpasahasrāṇi mahākalpaṡatāni ca  
> samatītāni rājēndra sargāṡ ca praḻayāṡ ca ha /6.106/  

> ¹sarvasyādau smṛtō brahmā prajāsargakaraḥ prabhuḥ  
> ²jānāti dēvapravaraṃ bhūyaṡ cātō’dhikaṃ nṛpa  
> paramātmānam īṡānam ātmanaḥ prabhavaṃ ³tadā /6.107/  

¹ M T G; CE "sargasy-"  
² all; M1,6,7 "jānītē dēvapravarō"  
³ M; CE "tathā"; T G om. entire line

> yē tv anyē brahmasadanē siddhasaṅghāḥ ¹samāgatāḥ  
> tēbhyas tac chrāvayām āsa purāṇaṃ vēdasammitam /6.108/  

¹ M CE; T G "samāhitāḥ"

> tēṣāṃ sakāṡāt sūryaṡ ca ṡrutvā vai bhāvitātmanām  
> ¹ātmānugāmināṃ rājan̄ ṡrāvayām āsa ²tattvataḥ /6.109/  

¹ M5 B Dn; CE "-nāṃ brahma"; M1,6,7 "ātmān munigaṇān̄ brahma";
  T G "ātmānugān sthithān rājan̄"  
² M T G; CE "bhārata"

> ¹ṣaṭṣaṣṭiṃ hi sahasrāṇi ²munīnāṃ bhāvitātmanām  
> sūryasya tapatō lōkān nirmitā yē puraḥsarāḥ  
> tēṣām akathayat sūryaḥ sarvēṣāṃ bhāvitātmanām /6.110/  

¹ M; CE "ṣaṭṣaṣṭir"; T G "aṣṭāṡīti"  
² M T G; CE "ṛṣīṇāṃ"

M B Dn CE all say 66,000 but T G say 88,000.

> sūryānugāmibhis tāta ṛṣibhis tair mahātmabhiḥ  
> mērau samāgatā dēvāḥ ṡrāvitāṡ cēdam uttamam /6.111/  

> dēvānāṃ tu sakāṡād vai tataḥ ṡrutvāsitō dvijaḥ  
> ṡrāvayām āsa rājēndra ¹pitṝn vai munisattamaḥ /6.112/  

¹ M T G; CE "pitṝṇāṃ"

> mama cāpi pitā tāta kathayām āsa ṡantanuḥ  
> tatō mayaitac chrutvā ca kīrtitaṃ tava bhārata /6.113/  

> surair vā munibhir vāpi purāṇaṃ yair idaṃ ṡrutam  
> sarvē tē paramātmānaṃ pūjayanti punaḥ punaḥ /6.114/  

> idam ākhyānam ārṣēyaṃ pāramparyāgataṃ ¹nṛṣu  
> nāvāsudēvabhaktāya tvayā dēyaṃ kathan̄cana /6.115/  

¹ M T G; CE "nṛpa"

> mattō’nyāni ca tē rājann upākhyānaṡatāni vai  
> yāni ṡrutāni ¹dharmyāṇi tēṣāṃ sārō’yam uddhṛtaḥ /6.116/  

¹ all; G1,2 M1,7 "dharmāṇi"

> surāsurair yathā rājan nirmathyāmṛtam uddhṛtam  
> ēvam ētat purā vipraiḥ kathāmṛtam ihōddhṛtam /6.117/  

> yaṡ cēdaṃ paṭhatē nityaṃ yaṡ cēdaṃ ṡṛṇuyān naraḥ  
> ēkāntabhāvōpagata ēkāntē susamāhitaḥ /6.118/  

> prāpya ṡvētaṃ mahādvīpaṃ bhūtvā candraprabhō naraḥ  
> sa sahasrārciṣaṃ dēvaṃ praviṡēn nātra saṃṡayaḥ /6.119/  

> mucyēd ārtas tathā rōgāc chrutvēmām āditaḥ kathām  
> jijn̄āsur labhatē ¹kāmān bhaktō bhaktagatiṃ ²vrajēt /6.120/  

¹ M CE; T G "bhaktiṃ"  
² M5 B Dn CE; M1,6,7 T G "labhēt"

> tvayāpi satataṃ rājann abhyarcyaḥ puruṣōttamaḥ  
> sa hi mātā pitā caiva kṛtsnasya jagatō ¹guruḥ /6.121/  

¹ M CE; T G "bhavēt"

> brahmaṇyadēvō bhagavān prīyatāṃ tē sanātanaḥ  
> yudhiṣṭhira mahābāhō mahābāhur janārdanaḥ /6.122/  

T G in. CE@859 (/6.127/) here.

>    vaiṡampāyana uvāca  
> ṡrutvaitad ākhyānavaraṃ dharmarāḍ janamējaya  
> bhrātaraṡ cāsya tē sarvē nārāyaṇaparābhavan /6.123/  

> jitaṃ bhagavatā tēna puruṣēṇēti bhārata  
> nityaṃ japyaparā bhūtvā ¹sarasvatīm udīrayan /6.124/  

¹ ?M7 CE; M1,5,6 "sarasvatim"; T2 G1,2 PPS "sārasvatam"; T1 G3,6 "nārāyaṇam"

> yō hy asmākaṃ guruḥ ṡrēṣṭhaḥ kṛṣṇadvaipāyanō ¹muniḥ  
> sa jagau paramaṃ japyaṃ nārāyaṇam udīrayan /6.125/  

¹ M CE; T G "prabhuḥ"

> gatvāntarikṣāt satataṃ kṣīrōdam amṛtā¹layam  
> pūjayitvā ca dēvēṡaṃ punar āyāt svam āṡramam /6.126/  

¹ M T G; CE "-ṡayam"

After /6.126/ M B Dn in. CE@859 (/6.127/). T G in. the same after /6.122/.

>    bhīṣma uvāca  
> ētat tē sarvam ākhyātaṃ nāradōktam mayēritam  
> pāramparyāgataṃ hy ētat pitrā mē kathitaṃ purā /6.127/  

After this, 28 mss., excepting M CE, in. CE@860 (6 lines) given in "Additional Passages -- Part 3"

Thus ends chapter M1,6,7 160; G1 164; T1 G6 165; G3 160; T2 178; G2 190; M5 196.

It has 127 verses, 272 lines;
    CE 124 verses, 266 lines;
    GP 141 verses, 282 lines;
    PPS (77 + 91 = 168) verses, (154 + 183 = 337) lines;
    Ku (76 + 91 = 167) verses, (157 + 193 = 350) lines.

Chapter name:

M1,6,7 "bhagavad-darṡanam"  
T G6   "janma-rahasyam"  
G1     "avatāra-rahasyam"  
G2     "janma-jarā-rahasyam".

## Additional Passages -- Part 1

M in. these lines after /6.48/. See "Tabular Conspectus" on pg. 1851 and footonote of PPS
pg. 1944 (अ, क, ख, घ manuscripts). T G PPS also in. there plus a few more.

> pṛthivī vāyur ākāṡam āpō jyōtiṡ ca pan̄camam  
> tē ¹samētya mahātmānaḥ ṡarīram iti ⁰saṃjn̄itam /CE 32/  

¹ M T1 G2,3,6 PPS; T2 G1 CE "samētā"; Ku GP om.

> ¹tamāviṡati ²yō brahmann ³adṛṡyō laghuvikramaḥ  
> utpanna ēva bhavati ṡarīraṃ ⁴cēṣṭayaty uta /CE 33/  
> ṡubhāṡubhaiḥ karmabhir ca na sa lipyati nārada /CE 25cd/  

¹ M; CE "tad-"  
² M CE; T G "antarātmā"  
³ M CE; T G "hy adṛṡyō"  
⁴ M; T G CE "cēṣṭayan prabhuḥ"

> na vinā dhātusaṅghātaṃ ṡarīraṃ bhavati kvacit  
> na ca jīvaṃ vinā brahman dhātavaṡ cēṣṭayanty uta /CE 34/  

> pṛthagbhūtāṡ ca tē nityaṃ kṣētrajn̄aḥ pṛthag ēva ca  
> sattvaṃ rajas tamaṡ ¹cēti ³guṇān ētān pracakṣatē  
> ³hi tē pan̄casu bhūtēṣu ṡarīrasthēṣu kalpitāḥ /CE@832/  

¹ M; et al. "caiva"  
² M; T1 G2,6 CE "na guṇās tasya bhōjakāḥ"  
³ M; et al. "ētē"

> ētān̄ jīvō guṇān bhuṅktē na tv ¹ēti bhujyatē tu saḥ /CE 27ab/  
> paṡya jīvasya māhātmyaṃ ya ēvaṃ guṇabhōjakaḥ  
> ²ēṣa ³laghuparikrāntō na guṇās tasya bhōjakāḥ /CE@828/  

¹ M; T G "ēbhir"  
² M G1; et al. "īṣal"  
³ T1 G1,3,6 CE; T2 "laghvaparikrāntō"; G2 PPS "laghur ivātyantō"

> jagatpratiṣṭhā dēvarṣē pṛthivy apsu pralīyatē  
> jyōtiṣy āpaḥ pralīyantē jyōtir ¹vāyōḥ pralīyatē /CE 28/  

¹ M; et al. "vāyau"

> khē vāyuḥ praḻayaṃ yāti kālēpyākāṡam eva tu /CE 29ab/  
> manō hi paramaṃ bhūtaṃ tad avyaktē pralīyatē /CE 29cd/  

> nāsti tasmāt parataraṃ puruṣād vai sanātanāt /CE 30cd/  
> māṃ tu jānīhi brahmarṣē puruṣaṃ sarvagaṃ prabhum /CE@831/  

> māyā hy ēṣā mayā sṛṣṭā yan ¹mā paṡyasi nārada /CE 43ab/  
> sarvabhūtaguṇair yuktaṃ naivaṃ tvaṃ jn̄ātum arhasi /CE 43cd/  

¹ M; et al. "māṃ"

> nityaṃ hi nāsti jagati bhūtaṃ sthāvarajaṅgamam  
> ṛtē ¹mām ēkaṃ īṡānaṃ ²puruṣaṃ lōkajīvanam /CE 31/  

¹ M T G2,3,6 PPS  
² M T G PPS

## Additional Passages -- Part 2

After /6.96/ M in. "Additional Passages -- Part 2" (_adhika pāṭhaḥ_).  
Also repeated in footnotes of PPS pg. 1955.  
T G in. the same with more additions.

> nityaṃ hi ¹nāsmi jagati bhūtaṃ sthāvarajaṅgamam /CE 31ab/  

¹ M; T G "nāsti"

> manō hi paramaṃ bhūtaṃ tad avyaktē pralīyatē /CE 29cd/  
> avyaktaṃ puruṣē brahman niṣkriyē sampralīyatē /CE 30ab/  

> puruṣaḥ sarvam ēvēdam akṣayaṡ cāvyayaṡ ca ¹saḥ /CE@852/  

¹ M K; D T G "ha"

### Additional Passages -- Part 3

After /6.127/, 28 mss., excepting M CE, in. CE@860 (6 lines). The reading below follows T G (T2 G1).

>    sūta uvāca  
> ētat vaḥ sarvam ākhyātaṃ vaiṡaṃpāyanakīrtitam  
> janamējayēna yac chrutvā kṛtaṃ samyag yathāvidhi /6.128/  

> yūyaṃ hi taptatapasaḥ sarvē ca caritavratāḥ  
> sarvē vēdavidō mukhyā naimiṡāraṇyavāsinaḥ /6.129/  

> ṡaunakasya mahāsatraṃ prāptāḥ sarvē dvijōttamāḥ  
> yajadhvaṃ suhutair yajn̄air ātmānaṃ paramēṡvaram /6.130/  
