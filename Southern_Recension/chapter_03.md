# Chapter 03

>    bhīṣma uvāca  
> tatō’tītē mahākalpē utpannē’ṅgirasaḥ sutē  
> babhūvur nirvṛtā dēvā jātē dēvapurōhitē /3.1/  

> ¹mahad brahma bṛhac cēti ṡabdāḥ paryāyavācakāḥ  
> ēbhiḥ samanvitō rājan guṇair vidvān bṛhaspatiḥ /3.2/  

¹ M; et al. "bṛhad brahma mahac" (swap)

> tasya ṡiṣyō babhūvāgryō rājōparicarō vasuḥ  
> adhītavāṃs tadā ṡāstraṃ samyak citraṡikhaṇḍijam /3.3/  

> sa rājā bhāvitaḥ pūrvaṃ ¹dēvēna vidhinā ²nṛpa  
> pālayām āsa pṛthivīṃ divam ākhaṇḍalō yathā /3.4/  

¹ ?M1,6,7 T2 G1,2; M5 T1 G3,6 B Dn CE PPS Ku GP "daivēna"  
³ M T G1-3 PPS; G6 "tadā"; CE Ku GP "vasuḥ"

> tasya yajn̄ō mahān āsīd aṡvamēdhō mahātmanaḥ  
> bṛhaspatir upādhyāyas tatra hōtā babhūva ha /3.5/  

> prajāpatisutāṡ ¹cātra sadasyās ²tv abhavaṃs trayaḥ  
> ēkataṡ ca dvitaṡ caiva tritaṡ caiva maharṣayaḥ /3.6/  

¹ †all; M5 "cāsya"  
² †all; M5 G2 Dn "cābhavaṃs"

> dhanuṣākṣō’tha raibhyaṡ ca arvāvasuparāvasū  
> ṛṣir mēdhātithiṡ caiva tāṇḍyaṡ caiva mahān ṛṣiḥ /3.7/  

> ṛṣiḥ ¹ṡantir mahābhāgas tathā ²vēdaṡirāṡ ca yaḥ  
> kapilaṡ ca ³muniṡrēṣṭhaḥ ṡālihōtrapitāmahaḥ /3.8/  

¹ M B Da Dn; T G CE "ṡaktir"  
² all; M1,7 "vēdaṡirōc ca"  
³ M T G; CE "ṛṣi-"

> ¹ādyakas taittirīyaṡ ca vaiṡampāyanapūrvajaḥ  
> kaṇvō’tha dēvahōtraṡ ca ētē ṣōḍaṡa kīrtitāḥ  
> sambhṛtāḥ sarvasambhārās tasmin rājan mahākratau /3.9/  

¹ ?M; G1 "ādya kathas"; G2 PPS "ādyakāras tittiriṡ";
  T G1,3,6 B Dn CE "ādyaḥ kaṭhas taittiriṡ"  

3.9 explicitly says there were 16 (ṡōḍaṡa)participants:
Sixteeen participants (3.6 to 3.9): Ēkata, Dvita, Trita,
Dhanuṣākṣa, Raibhya, Arvāvasu, Parāvasu, Mēdhātithi, Tāṇḍya,
Ṡānti, Vēdaṡirās, Kapila the Muniṡrēṣṭha, Ṡālihōtra the grandfather,
Taittirīya the foremost (ādyakaḥ), Kāṇva, Dēvahotra

PPS mostly agrees with M when he says "Tittiri the foremost (ādyakāra)"

However, CE splits it into "Kapila who is the grandfather of Ṡālihōtra and Kaṭha the first (ādya
kaṭha)" -- doubtful because Kapila by himself is a muniṡrēṣṭha, so why glorify him as the
grandfather of the lesser-known Ṡālihōtra?

B Dn split is it as "Kapila the father of Ṡālihotra" (not grandfather!) and "Kaṭha the first".

> na tatra paṡughātō’bhūt sa rājaivaṃ ¹sthitō hy abhūt  
> ²ahiṃsārucir akṣudrō nirāṡīḥ karma³saṃskṛtaḥ  
> āraṇyakapadōdgītā bhāgās ⁴tatrānukalpitāḥ /3.10/  

¹ ?M5 T G; et al. "sthitō’bhavat"  
² M; T G "ahiṃsāratir"; B Dn CE "ahiṃsraḥ ṡucir"  
³ M1,5,7 T G1,3,6; G2 "-saṃsthitaḥ"; M6 B Dn CE "-saṃstutaḥ"  
⁴ M; T2 G1,2 PPS "tatra prakalpitāḥ"; T1 G1,3,6 B Dn CE "tatrōpakalpitāḥ"

"ahiṃṡārucir" = desirous of or longing for ahiṃsā; beauty or color of ahiṃsā.

> prītas tatō’sya bhagavān dēvadēvaḥ purātanaḥ  
> sākṣāt taṃ darṡayām āsa sō’dṛṡyō’nyēna kēna cit /3.11/  

> svayaṃ bhāgam upāghrāya purōḍāṡaṃ gṛhītavān  
> ¹adṛṡyēnāhṛtē bhāgē dēvēna harimēdhasā /3.12/  

¹ M G1,2; CE "adṛṡyēna hṛtō bhāgō"

> bṛhaspatis tataḥ kruddhaḥ ¹sruvam udyamya vēgitaḥ  
> ākāṡaṃ ghnan ²sruvōtpātai rōṣād ³aṡrūṇy avartayat /3.13/  

¹ †M7 T G CE PPS; M1,5,6 "srucam"  
² M; CE "sruvaḥ pātai"; T1 G2,3,6 "xxx"; T2 "xxx"; G1 "xxx"  
³ M5 T2 G1,3,6 CE PPS; M6 "-avartayan"; M1,7 "aṡrūny-"

"sruc" (fem.) and "sruva" (mas.) both mean a wooden ladle.
It must be "sruva" in ¹ also because M uniformly uses it in ².

"sruvōtpāta", a compound word, sruva + utpāta = jump/flying up of sruva.
In tṛtīyā vibhakti (plural), "sruvōtpātaiḥ" = by the sudden jumps of sruva.

> uvāca cōparicaraṃ ¹mayā bhāgō’yam udyataḥ  
> grāhyaḥ svayaṃ hi dēvēna matpratyakṣaṃ na saṃṡayaḥ /3.14/  

¹ all; M1,7 "mahā-"

> udyatā yajn̄abhāgā hi sākṣāt prāptāḥ surair iha  
> kimartham iha na prāptō darṡanaṃ ¹mē harir ²nṛpa /3.15/  

¹ M T2 G1,2; T1 G3,6 CE "sa"  
² M T G; CE "vibhuḥ" (see also /3.4/)

Before /3.15/ B Da Dn in. "yudhiṣṭhira uvāca" but M T G CE om.

CE makes it sounds like /3.15/ was spoken by Yudhiṣṭhira to Bhīsṃa. However, in the southern
recension, "mē harir nṛpa" clearly implies that Bṛhaspati ("mē") spoke /3.14/ and /3.15/ to King
Uparicara Vasu ("nṛpa").

> tataḥ sa taṃ samuddhūtaṃ bhūmipālō mahān vasuḥ  
> prasādayām āsa muniṃ sadasyās tē ca sarvaṡaḥ /3.16/  

Before /3.16/ B Da Dn in. "bhīṣma uvāca" but M T G CE om.

After /3.16/ T G PPS Ku in. CE@806 (12 lines) but M B Dn CE GP om.

> ūcuṡ cainam asambhrāntā na rōṣaṃ kartum arhasi  
> naiṣa dharmaḥ kṛtayugē ¹yaṃ tvaṃ rōṣam acīkṛthāḥ /3.17/  

¹ ?M5 T1 G3,6; M6 T2 G1,2 PPS "yat tvaṃ"; M1,7 "yat taṃ"; CE "yas tvaṃ"

> arōṣaṇō hy asau dēvō yasya bhāgō’yam udyataḥ  
> na sa ṡakyas tvayā draṣṭum asmābhir vā bṛhaspatē  
> yasya prasādaṃ kurutē sa ¹ēnaṃ draṣṭum arhati /3.18/  

¹ M5 T G1,3,6; M1,6,7 "cainaṃ"; G2 "hi taṃ"; CE "vai taṃ"

"ēnaṃ" is correct since M1,6,7 also use "ca + ēnaṃ".

>    ēkatadvitatritā ūcuḥ  
> vayaṃ hi brahmaṇaḥ putrā mānasāḥ parikīrtitāḥ  
> gatā niḥṡrēyasārthaṃ hi kadācid diṡam uttarām /3.19/  

> taptvā varṣasahasrāṇi catvāri tapa uttamam  
> ēkapādasthitāḥ samyak kāṣṭhabhūtāḥ samāhitāḥ /3.20/  

> mērōr uttarabhāgē tu kṣīrōdasyānukūlataḥ  
> sa dēṡō yatra nas taptaṃ tapaḥ paramadāruṇam  
> kathaṃ paṡyēmahi vayaṃ dēvaṃ nārāyaṇaṃ tv iti /3.21/  

> ¹atha vratasyāvabhṛthē vāg uvācāṡarīriṇī  
> sutaptaṃ vas tapō viprāḥ prasannēnāntarātmanā /3.22/  

¹ M T G Dn; CE "tatō"

> yūyaṃ jijn̄āsavō bhaktāḥ kathaṃ drakṣyatha taṃ prabhum  
> kṣīrōdadhēr ¹uttarataḥ ṡvētadvīpō mahā²prabhaḥ /3.23/  

¹ all; M1,7 "uttarasya"  
² all; T2 M1 "-prabhuḥ"

> ¹atra nārāyaṇaparā ⁰mānavāṡ candravarcasaḥ  
> ēkāntabhāvōpagatās tē bhaktāḥ puruṣōttamam /3.24/  

¹ M1,6,7 T G1,3,6; M5 B Dn CE "tatra"

> tē sahasrārciṣaṃ dēvaṃ praviṡanti sanātanam  
> atīndriyā ¹anāhārā ²aniṣyandāḥ sugandhinaḥ /3.25/  

¹ M T G B Da; CE "nirāhārā"  
² M T2 G1; CE "aniṣpandāḥ"

> ēkāntinas tē puruṣāḥ ṡvētadvīpanivāsinaḥ  
> gacchadhvaṃ tatra munayas tatrātmā mē prakāṡitaḥ /3.26/  

> atha ṡrutvā vayaṃ sarvē ¹vācaṃ tām aṡarīriṇīm  
> yathākhyātēna mārgēṇa ²dēṡaṃ taṃ pratipēdirē /3.27/  

¹ all; M1,6 "vācāṃ"  
² M5 T1 G3,6 B Da; M1,6,7 "dēvaṃ taṃ"; T2 G1 CE "taṃ dēṡaṃ" (swap)

> prāpya ṡvētaṃ mahādvīpaṃ taccittās ¹taddidṛkṣayā  
> sahasābhihatāḥ sarvē tējasā ²tasyābhigatāḥ  
> tatō ³tu dṛṣṭiviṣayas tadā pratihatō’bhavat /3.28/  

¹ M B8; et al. "taddidṛkṣavaḥ"  
² M; T G CE "tasya mohitāḥ"  
³ ?M1,5,7; M6 "tad"; B Da Dn "sma"; T G CE "nō"

M T G in. CE@811 (1 line) as /3.28L2/

> na ca paṡyāma puruṣaṃ tattējōhṛtadarṡanāḥ  
> tatō ¹naḥ prādurabhavad vijn̄ānaṃ dēvayōgajam /3.29/  

¹ all; M1,6,7 "na"

> na kilātaptatapasā ṡakyatē draṣṭum an̄jasā  
> tataḥ punar varṣaṡataṃ taptvā ¹tatkālikaṃ ²tapaḥ /3.30/  

¹ M K2 B0,9; T G Dn CE "tātkālikaṃ"  
² M K6 B7; T G Dn CE "mahat"

> ¹vratāvasānē suṡubhān narān dadṛṡirē vayam  
> ṡvētāṃṡ candra²prabhāṅgānsvān sarvalakṣaṇalakṣitān /3.31/  

¹ all; M5,6 "vratāvāsēna"  
² ?M5; M1,6,7 "-prabhānsvānsvān" (?); T G "-prabhānsvāsyān"; B Dn CE "-pratīkāṡān"

Reading of M1,6,7 is unclear though it seems to match T G.
Critical notes suggest "-prabhānsvān" but it is submetric.
M5 chosen as compromise since it matches the meaning of CE.

> nityān̄jali¹kṛtān brahma japataḥ prāgudaṅmukhān  
> mānasō nāma sa japō japyatē tair mahātmabhiḥ  
> tēnaikāgra²manas tēna prītō bhavati vai hariḥ /3.32/  

¹ all; M1,6,7 "-kṛtō"  
² M; T1 G3,6 B Dn CE "-manastvēna"; T2 "-manaskānāṃ"; G1 "-manaskēna";
  G2 PPS "-mānasēna"  

> ¹yāvatyō muniṡārdūla bhāḥ sūryasya yugakṣayē  
> ēkaikasya prabhā tādṛk sābhavan mānavasya ¹hi /3.33/  

¹ ?M5; M1,6,7 T2 G1 "yāvatī"; T1 G3,6 "yāvanti";
  G2 "yāvan nō"; PPS "yāvantyō"; B0,7 Dn "yā’bhavan"; CE "yā bhavēn"  
² ?M5 T1 G; M1,6,7 B Dn CE "ha"

All mss. agree on "bhāḥ" which is plural of feminine "bhā" meaning lustre.
However "yāvatī" is singular feminine of "yāvat" and "yāvanti" is neuter plural.
I want plural feminine "yāvataḥ". M5's reading "yāvatyaḥ + muni = yāvatyō muni-"
is the closest.

> tējōnivāsaḥ sa dvīpa iti vai mēnirē vayam  
> na tatrābhyadhikaḥ kaṡcit sarvē tē samatējasaḥ /3.34/  

> atha sūryasahasrasya prabhāṃ yugapad utthitām  
> sahasā dṛṣṭavantaḥ sma punar ēva bṛhaspatē /3.35/  

> sahitāṡ cābhyadhāvanta ¹tatas tē mānavā drutam  
> kṛtān̄jalipuṭā ²hṛṣṭā nama ity ēva vādinaḥ /3.36/  

¹ all; M1,7 "tapas"  
² all; M1,6,7 "hṛṣṭā"

> tatō’bhivadatāṃ tēṣām ¹aṡrauṣaṃ ²vipuladhvanim  
> baliḥ kilōpahriyatē tasya dēvasya tair naraiḥ /3.37/  

¹ M T G; CE "aṡrauṣma"  
² M T G; CE "vipulaṃ dhva-"

> vayaṃ tu tējasā tasya sahasā hṛtacētasaḥ  
> na kin̄cid api paṡyāmō ¹hṛtadṛṣṭibalēndriyāḥ  
> ēkas tu ṡabdō’virataḥ ṡrutō’smābhir udīritaḥ /3.38/  

¹ all; M1,7 Ke "hṛtadṛṣta-"

> jitaṃ tē puṇḍarīkākṣa namas tē viṡvabhāvana  
> ¹namas tē’stu hṛṣīkēṡa mahāpuruṣa²pūrvaja /3.39/  

¹ all; M1,6,7 "ōṃ namastē hṛ-"  
² all; M1,7 K4,7 "-pūrvajaḥ"

> iti ṡabdaḥ ṡrutō’smābhiḥ ¹ṡīkṣākṣarasamīritaḥ  
> ētasminn antarē vāyuḥ ²sarvagandhavahaḥ ṡuciḥ /3.40/  

¹ M; et al. "ṡikṣā-"  
² all; M1,6,7 "divyagandha-"

> divyāny uvāha puṣpāṇi karmaṇyāṡ cauṣadhīs tathā  
> tair iṣṭaḥ pan̄cakālajn̄air harir ēkāntibhir naraiḥ /3.41/  

> nūnaṃ tatrāgatō dēvō yathā tair vāg udīritā  
> vayaṃ tv ēnaṃ na paṡyāmō mōhitās tasya māyayā /3.42/  

> mārutē sannivṛttē ca balau ca pratipāditē  
> cintāvyākulitātmānō ¹jātāḥ smō’ṅgirasāṃ vara /3.43/  

¹ all; M1,7 "jātān"

> mānavānāṃ sahasrēṣu tēṣu vai ṡuddhayōniṣu  
> asmān na kaṡcin manasā cakṣuṣā ¹vāpy apūjayat /3.44/  

¹ all; M1,7 G2 B8 "cāpy"

> tē’pi svasthā munigaṇā ¹ēkaṃ bhāvaṃ anuvratāḥ  
> nāsmāsu dadhirē bhāvaṃ brahmabhāvam anuṣṭhitāḥ /3.45/  

¹ M T2 G1,2; T1 G3,6 CE "ēkabhāvam"

> tatō’smān supariṡrāntāṃs tapasā cāpi karṡitān  
> uvāca khasthaṃ kim api bhūtaṃ tatrāṡarīra¹gam /3.46/  

¹ M; et al. "-kam"

>    ¹dēva uvāca  
> dṛṣṭā vaḥ puruṣāḥ ṡvētāḥ ²pan̄cēndriyavivarjitāḥ  
> dṛṣṭō bhavati dēvēṡa ēbhir ³dṛṣṭair dvijōttamāḥ /3.47/  

¹ M5 B Da Dn K; M1,6,7 T G CE om.  
² M G2 Cs; T2 "xxx"; G1 "xxx"; CE "sarvēndriya-"  
³ all; M1,6,7 "dṛṣṭē"

> gacchadhvaṃ munayaḥ sarvē yathāgatam ¹itō’cirāt  
> na sa ṡakyō abhaktēna draṣṭuṃ dēvaḥ kathan̄cana /3.48/  

¹ all; M1,7 K1,2,4 "itō’virāṭ"  

> kāmaṃ kālēna mahatā ¹ēkāntitvāt samā²hitaiḥ  
> ṡakyō draṣṭuṃ sa bhagavān prabhāmaṇḍaladurdṛṡaḥ /3.49/  

¹ M T2 K7; et al. "ēkāntitvaṃ"  
² M T2 G1 K7; T1 G3,6 "-sthitaiḥ"; CE "-gataiḥ"

> mahat kāryaṃ tu kartavyaṃ yuṣmābhir dvijasattamāḥ  
> itaḥ kṛtayugē’tītē viparyāsaṃ gatē’pi ca /3.50/  

> vaivasvatē’ntarē viprāḥ prāptē ¹trētāntarē tataḥ  
> surāṇāṃ kāryasiddhyarthaṃ sahāyā vai bhaviṣyatha /3.51/  

¹ ?M5 T2 G1; et al. "trētāyugē"

> tatas tad adbhutaṃ vākyaṃ niṡamyaivaṃ sma ¹sōmapāḥ  
> tasya prasādāt prāptāḥ ²sma dēṡam īpsitam an̄jasā /3.52/  

¹ M T2; G1 CE "sōmapa"; B Da Dn "vai tadā"; T1 G3,6 "xxx"; G2 "xxx"  
² M K7; et al. "smō"

> ēvaṃ sutapasā caiva havyakavyais tathaiva ca  
> dēvō’smābhir na dṛṣṭaḥ sa kathaṃ tvaṃ draṣṭum arhasi  
> nārāyaṇō mahad bhūtaṃ viṡvasṛg ghavyakavyabhuk /3.53/  

>    bhīṣma uvāca  
> ēvam ēkatavākyēna dvitatritamatēna ca  
> anunītaḥ sadasyaiṡ ca bṛhaspatir udāradhīḥ  
> samānīya tatō yajn̄aṃ daivataṃ samapūjayat /3.54/  

> samāptayajn̄ō rājāpi prajāḥ pālitavān ⁰vasuḥ  
> brahmaṡāpād divō bhraṣṭaḥ pravivēṡa mahīṃ tataḥ /3.55/  

> antarbhūmigataṡ caiva satataṃ dharmavatsalaḥ  
> nārāyaṇaparō bhūtvā nārāyaṇa¹japaṃ jagau /3.56/  

¹ ?M5 T1 G3,6; M1,6,7 "-paraṃ jagau"; G2 "-padaṃ jagat";
  B Da Dn "-japaṃ japan"; T2 G1 CE "-padaṃ jagau"  

Seems M5 choose the middle-ground between Bengali and CE.

CE says, "he attained the abode (_padaṃ_) of Nārāyaṇa" but
then in the next verse it says again "he rose up from the
underworld (mahītala)" -- not possible after reaching Nārayaṇa-padam.

M5 means "he uttered (_jagau_) the Nārāyaṇa-japa" and in the next
verse he attained "parāṃ gatim".

> tasyaiva ca prasādēna punar ēvōtthitas tu saḥ  
> mahītalād gataḥ sthānaṃ brahmaṇaḥ ⁰samanantaram  
> parāṃ gatim anuprāpta iti naiṣṭhikam an̄jasā /3.57/  

Thus ends chapter M1,6,7 157; G1 160; T1 G6 161; G3 162; T2 174; G2 192; M5 193.

It has 57 verses, 124 lines;
    CE 57 verses, 123 lines;
    GP 65 verses, 130 lines;
    Ku 69 verses, 143 lines;
   PPS 67 verses, 135 lines.

Chapter name:

M6     "bṛhaspati-prasādanaṃ"
