# Chapter 13

>    vaiṡampāyana uvāca  
> ṡrutvaitan nāradō vākyaṃ naranārāyaṇēritam  
> atyantabhaktimān ¹dēva ēkāntitvam upēyivān /13.1/  

¹ ?M1,5 T G1,3,6 B6,8,9 Da; M6,7 G2 CE PPS "dēvē"

> ¹uṣitvā varṣasāhasraṃ tu naranārāyaṇāṡramē  
> ṡrutvā bhagavadākhyānaṃ ²dṛṣṭvā ca harim avyayam  
> ³jagāmi himavat kukṣāv āṡramaṃ svaṃ surārcitam /13.2/  

¹ M T G; CE "prōṣya varṣasahasraṃ"  
² all; M1,6,7 "āpṛṣtvā"  
³ M T G; CE "himavantaṃ jagāmāṡu yatrāsya svaka āṡramaḥ"

> tāv api khyāta¹yaṡasau naranārāyaṇāv ṛṣī  
> tasminn ēvāṡramē ²ramyē ³tēpatus tapa uttamam /13.3/  

¹ M T G; CE "-tapasau"  
² M5 T1 G CE; M1,6,7 T2 "puṇyē"  
³ all; M1,6,7 "tapas tēpatur" (swap)

> tvam apy amitavi¹krānta pāṇḍavānāṃ ²kulōdvaha  
> pāvitātmādya saṃvṛttaḥ ṡrutvēmām āditaḥ kathām /13.4/  

¹ M T G; CE "-krāntaḥ"  
² M T2 G1,2 PPS; CE "kulōdvahaḥ"

> naiva tasya parō lōkō nāyaṃ pārthivasattama  
> karmaṇā manasā vācā yō dviṣyād viṣṇum avyayam /13.5/  

> ¹majjanti pitaras tasya narakē ṡāṡvatīḥ samāḥ  
> yō dviṣyād vibudhaṡrēṣṭhaṃ dēvaṃ nārāyaṇaṃ harim /13.6/  

¹ all; M1,7 "majjanta"

> kathaṃ nāma bhavēd dvēṣya ⁰ātmā lōkasya kasya cit  
> ātmā hi puruṣavyāghra jn̄ēyō viṣṇur iti ¹ṡrutiḥ /13.7/  

¹ M T G; CE "sthitiḥ"

> ya ēṣa gurur asmākam ṛṣir gandhavatīsutaḥ  
> tēnaitat kathitaṃ tāta māhātmyaṃ paramātmanaḥ  
> tasmāc chrutaṃ mayā cēdaṃ kathitaṃ ca tavānagha /13.8/  

> kṛṣṇadvaipāyanaṃ vyāsaṃ viddhi nārāyaṇaṃ prabhum  
> kō hy anyaḥ ¹puṇḍrīkākṣān mahābhāratakṛd bhavēt  
> dharmān nānāvidhāṃṡ caiva kō brūyāt tam ṛtē prabhum /13.9/  

¹ M T G; CE "puruṣavyāghra"

This is the famous verse, "Who else other than Lord Puṇḍarīkākṣa could have composed the
Mahābharata?"

> vartatāṃ tē mahāyajn̄ō yathā saṅkalpitas tvayā  
> saṅkalpitāṡvamēdhas tvaṃ ṡruta¹dharmā ca tattvataḥ /13.10/  

¹ M D4,9; T G CE "-dharmaṡ"

> ētat tu mahad ākhyānaṃ ṡrutvā ¹bharata sattama  
> tatō yajn̄asamāptyarthaṃ kriyāḥ sarvāḥ ³samārabha /13.11/  

¹ M; B Da Dn "pārthiva sattamaḥ"; T G CE "pārikṣitō nṛpaḥ"  
² M; et al. "samārabhat"

Before /13.11/ T G1,3,6 in. "sūta uvāca" but B Da Dn in. "sautir uvāca"

T G CE clearly says that the king was Parīkṣit.  
GP implies the King was Janamējaya (instead of Parīkṣit).  
M says "the best of Bharatas" -- could be either Parīkṣit or Janamējaya.
But in M's time frame, the conversation is between Ṛṣi Vaiṡampāyana and Rāja Janamējaya, so it must
be refering to Parīkṣit who previously conducted 3 Aṡvamēdha yajn̄as (agrees with CE).

T G's reading is ambiguous because they have "sūta uvāca" and yet they explicitly say Parīkṣit,
bypassing Vaisampayana-Janamejaya altogether.

> nārāyaṇīyam ākhyānam ētat tē kathitaṃ mayā  
> nāradēna purā ¹rājan guravē ²mē nivēditam  
> ṛṣīṇāṃ pāṇḍavānāṃ ca ṡṛṇvatōḥ kṛṣṇabhīṣmayōḥ /13.12/  

¹ All 30 mss. (except M) subst. "yadvai" for "rājan"  
² 26 mss. subst. "tu" for "mē" and 3 subst. "vi-";
  B6 B8 (orig.) and M. agree with CE  

This is an attempt to move the converstatioon from Vaisampayana-Janamejaya to Sūta-Ṡaunaka level.

> sa hi ¹paramagurur ⁰bhuvanapatir  
>  dharaṇidharaḥ ṡamaniyamanidhiḥ  
> ṡrutivinayanidhir dvijaparamahitas  
>  tava bhavatu gatir harir ²amarahitaḥ /13.13/  

¹ 27 mss. (except M) subst. "paramarṣir"; 3 subst. "paramamunir"  
² all; M1,7 "amaravara"

> tapasāṃ nidhiḥ sumahatāṃ mahatō  
>  yaṡasaṡ ca bhājanam ariṣṭakahā  
> ēkāntināṃ ṡaraṇadō’bhayadō  
>  gatidō’stu vaḥ sa makhabhāgaharaḥ /13.14/  

> triguṇātigaṡ catuṣpan̄cadharaḥ  
>  pūrtēṣṭayōṡ ca phalabhāgaharaḥ  
> vidadhāti nityam ajitō’tibalō  
>  gatim ātmagāṃ sukṛtinām ṛṣiṇām /13.15/  

> taṃ lōkasākṣiṇam ¹ajaṃ puruṣaṃ  
>  ravivarṇam īṡvaragatiṃ bahuṡaḥ  
> ²praṇamadhvam ³ēkamatayō yatayaḥ  
>  salilōdbhavō’pi tam ṛṣiṃ praṇataḥ /13.16/  

¹ all; M1,6,7 "ajaraṃ"  
² all; M1,6,7 "praṇavamadhvam"  
³ M5 T G CE; M1,6,7 "ēkatayō"; B Dn "ēkamanasō"

> sa hi lōkayōnir amṛtasya padaṃ  
>  sūkṣmaṃ purāṇam acalaṃ paramam  
> tat sāṅkhyayōgibhir udāradhṛtaṃ  
>  buddhyā ¹yatātmaviditaṃ satatam ²◌ /13.17/  

¹ M; et al. "yatātmabhir-"  
² all; M5 D4 in. "iti"

Thus ends chapter M1,6,7 169; G1 172; T1 G6 173; G3 174; T2 186; G2 202; M5 204.

It has 17 verses, 38 lines;
    CE 17 verses, 38 lines;
    GP 21 verses, 42 lines;
    Ku 20 verses, 44 lines.
