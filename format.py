#!/usr/bin/env python3

import sys
import re

l_words = [ "pralay", "niṣkala", "pātāl", "prakṣāl", "kalēvara", "ṡītal",
            "nalin", "iḍōpa", "padmanāl", "sajaḍa", "kalatr" ]

detected = []
superscripts = "⁰¹²³⁴⁵⁶⁷⁸⁹"

is_south = (len(sys.argv) == 3)
infile = sys.argv[2] if is_south else sys.argv[1]

with open(infile) as f:
    print("Processing file " + infile)
    all_lines = f.readlines()

for i in range(len(all_lines) - 2):
    line = all_lines[i]
    next_line = all_lines[i + 1]
    next_to_next = all_lines[i + 2]

    if "<!" in line or "->" in line:
        print("Comment not deleted on line", i + 1)

    marker = line[0]
    if marker == '>':
        if not line.endswith("  \n"):
            print("Missing double spaces on verse line ", i + 1)

        # scan for uvaca lines, number of leading spaces must be 4
        if "uvāca" in line or "ūcuḥ" in line or "ūcatuḥ" in line:
            # must be only 2 words in line ("xxx uvaca")
            if len(line[1:].split()) == 2:
                if re.search('\S', line[1:]).start() != 4:
                    print("Number of spaces in 'uvaca' lines must be 4 on line", i + 1)

            if line[0:4] == ">   ":
                print("Replace four spaces by EM SPACE on 'uvaca' line", i + 1)
            elif line[0:3] == ">  ":
                print("Replace two spaces by EN SPACE on pada line", i + 1)

    if marker in superscripts:
        if line.endswith(";\n"):
            if not next_line.endswith("  \n") and next_to_next == '\n':
                print("Missing double spaces on superscript continuation line ", i + 1)
        else:
            if not line.endswith("  \n") and next_line != "\n":
                print("Missing double spaces on superscript line ", i + 1)

    if "et." in line:
        print("Did you mean et al. on line ", i + 1)

    # Check for Southern words
    if is_south:
        for word in l_words:
            if word in line:
                print("Missing l-kara in word ", word, " in line ", i + 1)

    # match either "/1.xx/  " or "/1.xx/¹  ". \d means digit and . is literal dot
    regex = "/([\d.]*)/[" + superscripts + "]*  $"
    number = re.search(regex, line)
    if number:
        assert(len(number.groups()) == 1)
        number = number.group(1).split('.')[1] # extract 72 from "1.72"
        detected.append(int(number))



# Check if verse numbers are continuous
last = detected[-1]
missing = [x for x in range(1, last + 1)  if x not in detected]

if missing:
    print("Missing verse numbers: ", missing)


