# Narayaniya

Nārāyaṇīya (also spelled Narayaniya or Narayaneeyam) is a Pān̄carātra text
occuring in the Mokṣadharma sub-parva in the Book 12 of the Mahābhārata. It
forms the last 18 (or 19) chapters of the Ṡānti Parva. It discusses the
supremacy of Lord Viṣṇu, His various *vyūhas* and the importance of
*jn̄āna-bhakti* towards Him.

This repository contains different recensions of the text derived from the
footnotes and appendices of the Mahabharata Critical Edition published by
the Bhandarkar Oriental Research Institute.

See README.md in corresponding directories for more info.
