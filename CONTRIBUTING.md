# Contributing

Contributors Wanted! As an open project, you are very invited to contribute to
it.

My knowledge of Sanskrit (especially Epic Sanskrit) grammar is lacking.
So, you are welcome to correct me.

## Reporting Bugs

As in any human undertaking, mistakes and errors creep in.
Please open a bug report in the issue tracker.

You can report any kind of bug, not limited to:

* Spelling, both Sanskrit and English
* Sandhi
* Verb-tense and noun-cardinality mismatch
* Any grammatical error
* Missing variant readings
* Verse numbering
* Footnote numbering
* Line splitting
* Formatting
* Other scribal mistakes (whitespace, typos, etc.)

## Submitting Patches

As a git repository, it is very easy to submit corrections.

You can `git format-patch` and attach it to the bug report.

Or you can submit a "Pull Request".

All your contributions are subject to the terms and conditions listed in
[[LICENSE.md]].
