# Chapter 14

>    ¹janamējaya uvāca  
> ṡrutaṃ bhagavatas tasya māhātmyaṃ paramātmanaḥ  
> janma dharmagṛhē caiva naranārāyaṇātmakam /14.1/  

¹ all; GP "ṡaunaka"

> mahāvarāhasṛṣṭā ca piṇḍōtpattiḥ purātanī  
> pravṛttau ca nivṛttau ca yō yathā parikalpitaḥ /14.2/  

> sa tathā naḥ ṡrutō brahman kathyamānas tvayānagha  
> yac ca tat kathitaṃ pūrvaṃ tvayā hayaṡirō mahat /14.3/  

> havyakavyabhujō viṣṇōr udakpūrvē mahōdadhau  
> tac ca dṛṣṭaṃ bhagavatā brahmaṇā paramēṣṭhinā /14.4/  

> kiṃ tad utpāditaṃ pūrvaṃ hariṇā lōkadhāriṇā  
> rūpaṃ ¹prabhāvaṃ mahatām apūrvaṃ dhīmatāṃ vara /14.5/  

¹ all; CE "prabhāvamaha-"

> dṛṣṭvā hi vibudhaṡrēṣṭham apūrvam amitaujasam  
> ¹tadāṡvaṡirasaṃ puṇyaṃ brahmā kim akarōn munē /14.6/  

¹ B6,8,9 Da V; B0 "yadaṡva-"; B7 CE GP "tad aṡva-"

> ētan naḥ saṃṡayaṃ brahman ¹purāṇaṃ jn̄ānasaṃbhavam  
> kathayasvōttamamatē mahāpuruṣanirmitam  
> pāvitāḥ sma tvayā brahman puṇyāṃ kathayatā kathām /14.7/  

¹ B0,6,8 V Da4 GP; B7,9 Da3 CE "purāṇajn̄ā-"

>    ¹vaiṡaṃpāyana uvāca  
> kathayiṣyāmi tē sarvaṃ purāṇaṃ vēdasaṃmitam  
> jagau yad bhagavān vyāsō rājn̄ō ⁰dharmasutasya vai /14.8/  

¹ all; GP MND "sūta"

> ṡrutvāṡvaṡirasō mūrtiṃ dēvasya harimēdhasaḥ  
> utpannasaṃṡayō rājā ¹ētad ēvam acōdayat /14.9/  

¹ all; CE "tam ēva samacōdayat"

>    ¹yudhiṣṭhira uvāca  
> yat tad ²dadṛṡivān brahmā dēvaṃ hayaṡirōdharam  
> kimarthaṃ tat samabhavat ³tan mamācakṣva sattama /14.10/  

¹ all; GP MND "janamējaya"  
² B6,7,8 Da; B0,9 CE GP "darṡitavān"  
³ all; CE "vapur dēvōpakalpitam"

>    ¹vyāsa uvāca  
> yat kiṃ cid iha lōkē vai ²dēhasattvaṃ viṡāṃ patē  
> sarvaṃ pan̄cabhir āviṣṭaṃ bhūtair īṡvara⁰buddhijaiḥ /14.11/  

¹ all; GP MND "vaiṡampāyana"  
² B GP; Da "dēhasattvē"; V "dēhabandhaṃ"; CE "dēhabaddhaṃ"

> īṡvarō hi jagatsraṣṭā prabhur nārāyaṇō virāṭ  
> bhūtāntarātmā varadaḥ saguṇō nirguṇō’pi ca /14.12/  

> bhūtapralayam ¹atyantaṃ ṡṛṇuṣva nṛpasattama  
> dharaṇyām atha līnāyām apsu caikārṇavē purā /14.13/  

¹ all; CE "avyaktaṃ"

> jyōtirbhūtē jalē cāpi līnē jyōtiṣi cānilē  
> vāyau cākāṡasaṃlīnē ākāṡē ca ¹manōnugē /14.14/  

¹ all; Ca "manōgatō"

> vyaktē manasi saṃlīnē vyaktē cāvyaktatāṃ ¹gatē  
> avyaktē puruṣaṃ yātē puṃsi sarvagatē’pi ca /14.15/  

¹ all; Ca "gatō"

> tama ēvābhavat sarvaṃ na prājn̄āyata kiṃ cana  
> tamasō brahma saṃbhūtaṃ ¹tamōmūlām ṛtātmakam /14.16/  

¹ ?B Da V; Ca CE GP "tamōmūlam"

> tad viṡvabhāva¹saṃjn̄āntaṃ pauruṣīṃ tanum ²āṡritam  
> sō’niruddha iti prōktas tat pradhānaṃ pracakṣatē /14.17/  

¹ Da CE; B0,6,9 "-saṃjn̄ātaṃ"; B8 "-samjn̄aṃ taṃ"; B7 om.  
² B0,6,7,8 Da V GP; B9 CE "āsthitam"

> tad avyaktam iti jn̄ēyaṃ triguṇaṃ nṛpasattama  
> vidyāsahāyavān dēvō viṣvaksēnō hariḥ prabhuḥ /14.18/  

> apsv ēva ṡayanaṃ cakrē nidrāyōgam upāgataḥ  
> jagataṡ cintayan sṛṣṭiṃ citrāṃ bahuguṇōdbhavām /14.19/  

> tasya cintayataḥ sṛṣṭiṃ mahān ātmaguṇaḥ smṛtaḥ  
> ahaṃkāras tatō jātō brahmā ṡubhacaturmukhaḥ /14.20/  

> hiraṇyagarbhō bhagavān sarvalōkapitāmahaḥ  
> padmē’ni¹ruddhaḥ saṃbhūtas tadā padmanibhēkṣaṇaḥ /14.21/  

¹ all; CE GP "-ruddhāt"  

> sahasrapatrē dyutimān upaviṣṭaḥ sanātanaḥ  
> dadṛṡē’dbhutasaṃkāṡē lōkān āpōmayān prabhuḥ /14.22/  

> sattvasthaḥ paramēṣṭhī sa tatō bhūtagaṇān ⁰sṛjat  
> pūrvam ēva ca padmasya patrē sūryāṃṡusaprabhē /14.23/  

> nārāyaṇakṛtau bindū apām āstāṃ guṇōttarau  
> tāv apaṡyat sa bhagavān anādinidhanō’cyutaḥ /14.24/  

> ēkas tatrābhavad bindur madhvābhō ¹ruciraprabhaḥ  
> sa tāmasō madhur jātas tadā nārāyaṇājn̄ayā /14.25/  

¹ B0,6,7,8 CE GP; B9 Da "rudhira-"

> kaṭhinas tv aparō binduḥ kaiṭabhō rājasas tu saḥ  
> tāv abhyadhāvatāṃ ṡrēṣṭhau tamōrajaguṇānvitau /14.26/  

> balavantau gadāhastau padmanālānusāriṇau  
> dadṛṡātē’ravindasthaṃ brahmāṇam amitaprabham /14.27/  

> sṛjantaṃ prathamaṃ vēdāṃṡ caturaṡ cāruvigrahān  
> tatō vigrahavantau tau vēdān dṛṣṭvāsurōttamau /14.28/  

> sahasā jagṛhatur vēdān brahmaṇaḥ paṡyatas tadā  
> atha tau dānavaṡrēṣṭhau vēdān gṛhya sanātanān /14.29/  

/14.29L1/ is hypermetric.

> rasāṃ viviṡatus tūrṇam ⁰udakpūrvē mahōdadhau  
> tatō hṛtēṣu vēdēṣu brahmā kaṡmalam āviṡat  
> tatō vacanam īṡānaṃ prāha vēdair vinākṛtaḥ /14.30/  

>    ¹brahmōvāca  
> vēdā mē paramaṃ cakṣur vēdā mē paramaṃ balam  
> vēdā mē paramaṃ dhāma vēdā mē brahma cōttamam /14.31/  

¹ all; CE om.

> mama vēdā hṛtāḥ sarvē dānavābhyāṃ balād itaḥ  
> andhakārā hi mē lōkā jātā vēdair vinākṛtāḥ /14.32/  

> ¹vēdān ṛtē hi kiṃ kuryāṃ ²lōkānāṃ sṛṣṭim udyatām  
> ahō bata mahad duḥkhaṃ vēdanāṡanajaṃ mama /14.33/  

¹ B8,9 Da CE GP; V B0,6,7 "vēdād ṛtē  
² B Da V; GP "lōkānāṃ sṛṣṭim uttamām"; CE "lōkān vai sraṣṭum udyataḥ"

> prāptaṃ dunōti hṛdayaṃ ¹tīvraṡōkapathān vayam  
> kō hi ṡōkārṇavē magnaṃ mām itō’dya samuddharēt  
> vēdāṃs tān ²cānayēn naṣṭān kasya cāhaṃ priyō bhavē /14.34/  

¹ B6,7,8,9 Da; V "-ṡōkaṃ pathān-"; GP "tīvraṃ ṡōkaparāyaṇam";
  B0 CE "tīvraṡōkāya randhayan"  
² B Da GP; V "anayēn" (sic); CE "ānayēn"

For ¹, 28 out of 34 mss give a different reading than CE and yet CE does not underline this!

>    ¹vyāsa uvāca  
> ity ēvaṃ bhāṣamāṇasya brahmaṇō nṛpasattama  
> harēḥ stōtrārtham udbhūtā buddhir buddhimatāṃ vara  
> tatō jagau paraṃ japyaṃ ²sān̄jalipragrahaḥ prabhuḥ /14.35/  

² B8; et al. om.  
¹ ?B9 Da4 CE; B0 GP "prān̄jali-"; B8 V "prānjaliḥ"; B6,7 Da3 "sān̄jaliḥ pragraha-"

>    ¹brahmōvāca  
> ²ōṃ namas tē brahmahṛdaya namas tē mama pūrvaja  
> lōkādya bhuvanaṡrēṣṭha sāṃkhyayōganidhē ⁰vibhō /14.36/  

¹ all; CE om.  
² all; V CE om.

> vyaktāvyaktakarācintya kṣēmaṃ panthānam ¹āsthitaḥ  
> viṡvabhuk sarvabhūtānām antarātmann ayōnija /14.37/  

¹ B0,6,7,8 Da; B9 V CE GP "āsthita"

> ahaṃ prasādajas tubhyaṃ ¹lōkadhāmasvayaṃbhuvaḥ  
> tvattō mē mānasaṃ janma prathamaṃ dvijapūjitam /14.38/  

¹ all; CE "lōkadhāmnē svayaṃbhuvē"

> cākṣuṣaṃ vai dvitīyaṃ mē janma cāsīt purātanam  
> tvatprasādāc ca mē janma tṛtīyaṃ vācikaṃ mahat /14.39/  

> tvattaḥ ṡravaṇajaṃ cāpi caturthaṃ janma mē vibhō  
> ¹nāsatyaṃ cāpi mē janma tvattaḥ pan̄camam ucyatē /14.40/  

¹ all; CE "nāsikyaṃ"

> aṇḍajaṃ cāpi mē janma tvattaḥ ṣaṣṭhaṃ vinirmitam  
> idaṃ ca saptamaṃ janma padmajaṃ ⁰mē’mitaprabha /14.41/  

> sargē sargē hy ahaṃ putras tava triguṇa¹varjitaḥ  
> ²prathamaḥ puṇḍarīkākṣa pradhānaguṇakalpitaḥ /14.42/  

¹ B0,6,7,8 V CE; B9 Da V "-varjita"  
² B Da3 V GP; Da4 "prathamaṃ"; CE "prathitaḥ"

> tvam ⁰īṡvarasvabhāvaṡ ca ¹karmabandhaḥ svayaṃbhuvaḥ  
> tvayā vinirmitō’haṃ vai vēdacakṣur vayōtigaḥ /14.43/  

¹ B6,7,8,9 Da; B0 GP "-bandhaṃ sva-"; CE "svayaṃbhūḥ puruṣōttamaḥ"

> tē mē vēdā hṛtāṡ cakṣur andhō jātō’smi jāgṛhi  
> dadasva ¹cakṣūṃṣi mama priyō’haṃ tē priyō’si mē /14.44/  

¹ B0,6,7,9 Da3 GP; B6(marg.) B8 V GP CE "dadasva cakṣuṣī mahyaṃ"

>    ¹vyāsa uvāca  
> ēvaṃ stutaḥ sa bhagavān puruṣaḥ sarvatōmukhaḥ  
> jahau nidrām atha tadā vēdakāryārtham ²utthitaḥ /14.45/  

¹ B8; et al. om.  
² B0,6,8,9 Da; B7 CE GP "udyataḥ"

> ¹aiṡvaryēṇa prayōgēṇa dvitīyāṃ tanum āsthitaḥ  
> sunāsikēna kāyēna bhūtvā candraprabhas tadā /14.46/  

¹ all; CE "aiṡvarēṇa"

> kṛtvā hayaṡiraḥ ṡubhraṃ vēdānām ālayaṃ prabhuḥ  
> tasya mūrdhā samabhavad dyauḥ sanakṣatratārakā /14.47/  

> kēṡāṡ cāsyābhavan dīrghā ravēr aṃṡusamaprabhāḥ  
> karṇāv ākāṡapātālē lalāṭaṃ bhūtadhāriṇī /14.48/  

> gaṅgā sarasvatī ¹ṡrōṇyau bhruvāv āstāṃ ²mahōdadhī  
> cakṣuṣī sōmasūryau ³tu nāsā sandhyā punaḥ smṛtā /14.49/  

¹ all; V "puṇyē"; CE "puṇyā"  
² all; V "mahādyutēḥ"; CE "mahānadī"  
³ ?B6,7,8 Da; B0,7,9 V CE GP "tē"

> ōṅkāras tv atha saṃskārō vidyuj jihvā ca nirmitā  
> dantāṡ ca pitarō rājan sōmapā iti viṡrutāḥ /14.50/  

> gōlōkō brahmalōkaṡca ōṣṭhāv āstāṃ mahātmanaḥ  
> grīvā cāsyābhavad rājan kālarātrir guṇōttarā /14.51/  

> ētad dhayaṡiraḥ kṛtvā nānāmūrtibhir āvṛtam  
> ¹antardadhau sa viṡvēṡō vivēṡa ca rasāṃ prabhuḥ /14.52/  

¹ B0,6,7,9 Da GP; B8 V CE "antardadhē"

> rasāṃ punaḥ praviṣṭaṡ ca yōgaṃ paramam āsthitaḥ  
> ¹ṡaikṣyaṃ svaraṃ samāsthāya ⁰ōm iti prāsṛjat svaram /14.53/  

¹ B6,7,8,9 Da; B0 "saikṣaṃ"; V CE "ṡaikṣaṃ"

> sa svaraḥ sānunādī ca ¹sarvagaḥ snigdha ēva ca  
> babhūvāntarmahībhūtaḥ sarvabhūta⁰guṇōditaḥ /14.54/  

¹ B7,9 Da V CE; B0,6 GP "sarvaṡaḥ"; B8 "sarvataḥ"

> tatas tāv asurau kṛtvā vēdān samayabandhanān  
> rasātalē vinikṣipya yataḥ ṡabdas tatō drutau /14.55/  

> ētasminn antarē rājan dēvō hayaṡirōdharaḥ  
> jagrāha vēdān akhilān rasātalagatān hariḥ /14.56/  

> prādāc ca brahmaṇē bhūyas tataḥ svāṃ prakṛtiṃ gataḥ  
> sthāpayitvā hayaṡira udakpūrvē mahōdadhau /14.57/  

> vēdānām ⁰ālayaṡ cāpi babhūvāṡvaṡirās tataḥ  
> atha kin̄cid apaṡyantau dānavau madhukaiṭabhau /14.58/  

> punar ājagmatus tatra vēgitau paṡyatāṃ ca tau  
> yatra vēdā vinikṣiptās tat sthānaṃ ṡūnyam ēva ca /14.59/  

> tata uttamam āsthāya vēgaṃ balavatāṃ varau  
> punar uttasthatuḥ ṡīghraṃ rasānām ālayāt tadā  
> dadṛṡātē ca puruṣaṃ tam ēvādikaraṃ prabhum /14.60/  

> ¹ṡvētaṃ candraviṡuddhābham aniruddhatanau sthitam  
> bhūyō’py amitavikrāntaṃ nidrāyōgam upāgatam /14.61/  

¹ ?B9 CE; B6 "taṃ ṡvētaṃ ṡuddhacandrābham"; B7 "taṃ ṡvētaṃ candraṡuddhābhām";
  B8 "ṡvētacandraviṡuddhāyām"; Da "taṃ ṡvētaṃ candrasaṃṡuddhām"  

> ātmapramāṇaracitē apām upari kalpitē  
> ṡayanē nāgabhōgāḍhyē jvālāmālāsamāvṛtē /14.62/  

> niṣkalmaṣēṇa sattvēna sampannaṃ ruciraprabham  
> taṃ dṛṣṭvā dānavēndrau tau mahāhāsam amun̄catām /14.63/  

> ūcatuṡ ca samāviṣṭau rajasā tamasā ca tau  
> ayaṃ sa puruṣaḥ ṡvētaḥ ṡētē nidrām upāgataḥ /14.64/  

> anēna nūnaṃ vēdānāṃ kṛtam āharaṇaṃ rasāt  
> kasyaiṣa kō nu khalv ēṣa kin̄ca svapiti bhōgavān /14.65/  

> ity uccāritavākyau tau bōdhayām āsatur harim  
> yuddhārthinau ¹hi vijn̄āya vibuddhaḥ puruṣōttamaḥ /14.66/  

¹ all; V "tau"; CE "tu"

Before /14.66/ B8 in. "vyāsa uvāca" but others om.

> nirīkṣya cāsurēndrau tau tatō yuddhē manō dadhē  
> atha yuddhaṃ samabhavat tayōr nārāyaṇasya ⁰ca /14.67/  

> rajastamōviṣṭatanū tāv ubhau madhukaiṭabhau  
> brahmaṇōpacitiṃ kurvan̄ jaghāna madhusūdanaḥ /14.68/  

> tatas tayōr vadhēnāṡu vēdāpaharaṇēna ca  
> ṡōkāpanayanaṃ cakrē brahmaṇaḥ puruṣōttamaḥ /14.69/  

> tataḥ parivṛtō brahmā ¹hariṇā vēdasatkṛtaḥ  
> nirmamē sa tadā lōkān kṛtsnān sthāvarajaṅgamān /14.70/  

¹ all; V CE "hatārir"

> dattvā pitāmahāyāgryāṃ ¹matiṃ lōkavisargikīm  
> tatraivāntardadhē dēvō yata ēvāgatō hariḥ /14.71/  

¹ all; CE "buddhiṃ"

> tau dānavau harir hatvā kṛtvā hayaṡiras tanum  
> punaḥ pravṛttidharmārthaṃ tām ēva vidadhē tanum /14.72/  

> ēvam ⁰ēṣa mahābhāgō babhūvāṡvaṡirā hariḥ  
> ¹purāṇam ētad ²prakhyātaṃ rūpaṃ varadam aiṡvaram /14.73/  

¹ B6,9 Da; B0,7,8 V CE GP "paurāṇam"  
² B0,6,7,9 Da GP; B8 V CE "ākhyātaṃ"  

> yō hy ētad brāhmaṇō nityaṃ ṡṛṇuyād ¹dhārayīta vā  
> na tasyādhyayanaṃ nāṡam upagacchēt kadācana /14.74/  

¹ all; CE "dhārayēta"

> ārādhya tapasōgrēṇa dēvaṃ hayaṡirōdharam  
> pān̄cālēna kramaḥ prāptō ¹dēvēna pathi dēṡitē /14.75/  

¹ all; CE "rāmēṇa"

> ētad dhayaṡirō rājann ākhyānaṃ tava kīrtitam  
> purāṇaṃ vēdasamitaṃ yan māṃ tvaṃ paripṛcchasi /14.76/  

> yāṃ yām icchēt tanuṃ dēvaḥ kartuṃ kāryavidhau kvacit  
> tāṃ tāṃ kuryād vikurvāṇaḥ svayam ātmānam ātmanā /14.77/  

> ēṣa vēdanidhiḥ ṡrīmān ēṣa vai tapasō nidhiḥ  
> ēṣa ¹yōgaṡ ca sāṅkhyaṃ ca brahma cāgryaṃ ²havir vibhuḥ /14.78/  

¹ B0,6,8 Da3 V CE GP; B7,9 Da3 "yōgaṃ"  
² B V GP; Da "harivi-"; CE "harir vi-"

> nārāyaṇaparā vēdā yajn̄ā nārāyaṇātmakāḥ  
> tapō nārāyaṇaparaṃ nārāyaṇaparā gatiḥ /14.79/  

> nārāyaṇaparaṃ satyam ṛtaṃ nārāyaṇātmakam  
> nārāyaṇaparō dharmaḥ punarāvṛttidurlabhaḥ /14.80/  

> pravṛttilakṣaṇaṡ caiva dharmō nārāyaṇātmakaḥ  
> nārāyaṇātmakō gandhō bhūmau ṡrēṣṭhatamaḥ smṛtaḥ /14.81/  

> apāṃ caiva ¹guṇā rājan ²rasā nārāyaṇ³ātmakāḥ  
> jyōtiṣāṃ ca ⁰guṇō rūpaṃ smṛtaṃ nārāyaṇātmakam /14.82/  

¹ all; V CE "guṇō"  
² all; V CE "rasō"  
³ all; V CE "-ātmakaḥ"

> nārāyaṇātmakaṡcāpi sparṡō vāyuguṇaḥ smṛtaḥ  
> nārāyaṇātmakaṡ⁰cāpi ṡabda ākāṡasambhavaḥ /14.83/  

> manaṡ cāpi tatō bhūtam avyaktaguṇalakṣaṇam  
> nārāyaṇaparaḥ kālō jyōtiṣām ayanaṃ ca ¹yat /14.84/  

¹ B0,6,8 V CE GP; B7,9 Da "tat"

> nārāyaṇaparā kīrtiḥ ṡrīṡ ca lakṣmīṡ ca dēvatāḥ  
> nārāyaṇaparaṃ sāṅkhyaṃ ¹yōgaṃ nārāyaṇātmakam /14.85/  

¹ B7,9 V Da; B0,6,8 GP CE "yōgō nārāyaṇātmakaḥ"

> kāraṇaṃ puruṣō ⁰yēṣāṃ pradhānaṃ cāpi kāraṇam  
> svabhāvaṡ ¹cāpi karmāṇi daivaṃ yēṣāṃ ca kāraṇam /14.86/  

¹ B6,7 Da V; B0,8,9 GP CE "caiva"

> pan̄cakāraṇasaṅkhyātō niṣṭhā sarvatra vai ¹harēḥ  
> tattvaṃ jijn̄āsamānānāṃ hētubhiḥ sarvatōmukhaiḥ /14.87/  

¹ B6,9 Da V; B0,7,8 CE GP "hariḥ"

> tattvam ēkō mahāyōgī harir nārāyaṇaḥ prabhuḥ  
> ¹brahmādīnāṃ salōkānām ṛṣīṇāṃ ca mahātmanām /14.88/  

¹ all; B0 CE "sabrahmakānāṃ lōkānām"

> sāṅkhyānāṃ yōgināṃ cāpi yatīnām ātmavēdinām  
> manīṣitaṃ vijānāti kēṡavō na tu tasya tē /14.89/  

> yē kē cit sarvalōkēṣu daivaṃ pitryaṃ ca kurvatē  
> dānāni ca prayacchanti ¹tapyantē ca tapō mahat /14.90/  

¹ B6,7,9 Da GP; B0,8 V CE "tapyanti"

> sarvēṣām āṡrayō viṣṇur aiṡvaraṃ vidhim āsthitaḥ  
> sarvabhūtakṛtāvāsō vāsudēvēti cōcyatē /14.91/  

> ayaṃ hi nityaḥ paramō maharṣir  
>  mahāvibhūtir ¹guṇavarjitākhyaḥ  
> guṇaiṡ ca saṃyōgam upaiti ṡīghraṃ  
>  kālō yathartāv ṛtusamprayuktaḥ /14.92/  

¹ B0,6,7,9 Da GP; B8 CE "guṇavān nirguṇākhyaḥ"

> naivāsya ¹budhyanti gatiṃ mahātmanō  
>  ²naivāgatiṃ kaṡcid ihānupaṡyati  
> jn̄ānātmakāḥ ³santi hi yē maharṣayaḥ  
>  paṡyanti nityaṃ puruṣaṃ guṇādhikam /14.93/  

¹ B6,7,9 Da V; B0,8 CE GP "vindanti"  
² B6,7 Da V; B0,8,9 CE GP "na cāgatiṃ"  
³ all; CE "saṃyaminō"

Thus ends chapter B7 171; B8 277; AB 349; GP 347; MND 348

It has 93 verses, 191 lines;
    CE 89 verses, 191 lines;
    GP 95 verses, 193 lines;
    Ku 94 verses, ??? lines.
