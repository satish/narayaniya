# Chapter 10

>    ¹ṡaunaka uvāca  
> ²saute sumahad ākhyānaṃ bhavatā parikīrtitam  
> yac chrutvā munayaḥ sarvē vismayaṃ paramaṃ gatāḥ /10.1/  

¹ B Da V T G GP; CE "janamējaya"  
² all; CE "brahman"

After /10.1/ B Da GP in. CE@App-I.32 (/10.2/ to /10.10/).
V in. the same after /10.16L1/.

> sarvāṡramābhigamanaṃ sarvatīrthāvagāhanam  
> na tathā phaladaṃ sautē nārāyaṇakathā yathā /10.2/  

> pāvitāṅgāḥ sma saṃvṛttāḥ ṡrutvēmam āditaḥ kathām  
> nārāyaṇāṡrayāṃ puṇyāṃ sarvapāpapramōcanīm /10.3/  

> durdarṡō bhagavān dēvaḥ sarvalōkanamaskṛtaḥ  
> ¹dēvaiḥ sabrahmakaiḥ kṛtsnair anyaiṡ caiva maharṣibhiḥ /10.4/  

¹ B6,7,9 Da V CE; B0,8 GP "sabrahmakaiḥ suraiḥ"

> dṛṣṭavān nāradō ¹yat tu dēvaṃ nārāyaṇaṃ harim  
> nūnam ētad dhy anumataṃ tasya dēvasya sūtaja /10.5/  

¹ B0,6,7,9 Da GP; B8 CE "yatra"

> yad dṛṣṭavān̄ jagannātham aniruddhatanau sthitam  
> yat prādravat punar bhūyō nāradō dēvasattamau  
> naranārāyaṇau draṣṭuṃ kāraṇaṃ tad bravīhi mē /10.6/  

>    ¹sūta uvāca  
> tasmin yajn̄ē vartamānē rājn̄aḥ pārikṣitasya vai  
> karmāntarēṣu vidhivad vartamānēṣu ṡaunaka /10.7/  

¹ B0,6,7,9 Da; B8 GP "sautir"

> kṛṣṇadvaipāyanaṃ vyāsam ṛṣiṃ vēdanidhiṃ prabhum  
> paripapraccha ¹rājēndraḥ pitāmahapitāmaham /10.8/  

¹ B0,6,8 Da3 CE GP; B7,9 Da4 "rājēndra"

>    janamējaya uvāca  
> ṡvētadvīpān nivṛttēna nāradēna surarṣiṇā  
> dhyāyatā bhagavadvākyaṃ cēṣṭitaṃ kim ataḥ param /10.9/  

> badaryāṡramam āgamya samāgamya ca tāv ṛṣī  
> kiyantaṃ kālam avasat kāṃ kathāṃ pṛṣṭavāṃṡ ca saḥ /10.10/  

> idaṃ ṡatasahasrād dhi bhāratākhyānavistarāt  
> ¹āmathya matimanthēna jn̄ānōdadhim anuttamam /10.11/  

¹ ?B9 Da CE; B0,6,7,8 GP "āmanthya"

> navanītaṃ yathā dadhnō malayāc candanaṃ yathā  
> āraṇyakaṃ ca vēdēbhya ōṣadhibhyō’mṛtaṃ yathā /10.12/  

> samuddhṛtam idaṃ brahman kathāmṛtam ¹idaṃ tathā  
> tapōnidhē tvayōktaṃ hi nārāyaṇakathāṡrayam /10.13/  

¹ B0,6,7,9 Da V GP; B8 CE "anuttamam"

> sa ⁰hīṡō bhagavān dēvaḥ sarvabhūtātmabhāvanaḥ  
> ahō nārāyaṇaṃ tējō durdarṡaṃ dvijasattama /10.14/  

> ¹yadāviṡanti kalpāntē sarvē brahmādayaḥ surāḥ  
> ṛṣayaṡ ca sagandharvā yac ca kin̄cic carācaram /10.15/  

¹ B Da; V CE GP "yatrāv-"

> na tatō’sti paraṃ manyē pāvanaṃ divi cēha ca  
> sarvāṡramābhigamanaṃ sarvatīrthāvagāhanam /10.16/  

> na tathā phaladaṃ cāpi nārāyaṇakathā yathā  
> sarvathā pāvitāḥ smēha ṡrutvēmām āditaḥ kathām /10.17/  

> harēr viṡvēṡvarasyēha sarvapāpapraṇāṡanīm  
> na citraṃ kṛtavāṃs tatra yad āryō mē dhanan̄jayaḥ /10.18/  

> vāsudēvasahāyō yaḥ prāptavān̄ jayam uttamam  
> na cāsya kin̄cid aprāpyaṃ manyē lōkēṣv api triṣu /10.19/  

> trailōkyanāthō viṣṇuḥ sa ¹yathā’sīt ²sahyakṛt ⁰sakhā  
> dhanyāṡ ca sarva ēvāsan brahmaṃs tē mama ³pūrvakāḥ /10.20/  

¹ ?B7,8,9 Da3 GP; B6,8 Da4 CE "yasyāsīt"  
² B Da Ca; V CE GP "sāhyakṛt"  
³ B0,6,7,8 CE; B9 "pūrvaṡaḥ" Da GP "pūrvajāḥ"

> hitāya ṡrēyasē caiva yēṣām āsīj janārdanaḥ  
> ¹tapasāpi na dṛṡyō hi bhagavāl̐ lōkapūjitaḥ /10.21/  

¹ ?CE; B0,8,9 "tapasāpya sudṛṡyō"; B7 GP "tapasātha sudṛṡyō";
  B6 "tapasāpy anudṛṡyō"; Da3 "tapasātha sadṛṡyō"; Da4 "tapasāpya sadṛṡyō"  

> yaṃ dṛṣṭavantas tē sākṣāc chrīvatsāṅkavibhūṣaṇam  
> tēbhyō dhanyataraṡ caiva nāradaḥ paramēṣṭhijaḥ /10.22/  

> na cālpatējasam ṛṣiṃ vēdmi nāradam avyayam  
> ṡvētadvīpaṃ samāsādya yēna dṛṣṭaḥ svayaṃ hariḥ /10.23/  

> dēvaprasādānugataṃ vyaktaṃ tat tasya darṡanam  
> yad dṛṣṭavāṃs tadā dēvam aniruddhatanau sthitam /10.24/  

> badarīm āṡramaṃ yat tu nāradaḥ prādravat punaḥ  
> naranārāyaṇau draṣṭuṃ kiṃ nu tatkāraṇaṃ munē /10.25/  

> ṡvētadvīpān nivṛttaṡ ca nāradaḥ paramēṣṭhijaḥ  
> badarīm āṡramaṃ prāpya samāgamya ca tāv ṛṣī /10.26/  

> kiyantaṃ kālam avasat ¹kathāḥ kāḥ ⁰pṛṣṭavāṃṡ ca ²ha  
> ṡvētadvīpād upāvṛttē tasmin vā sumahātmani /10.27/  

¹ all; CE "kāḥ kathāḥ"  
² all; CE "saḥ"

> kim abrūtāṃ mahātmānau naranārāyaṇāv ṛṣī  
> tad ētan mē yathātattvaṃ sarvam ākhyātum arhasi /10.28/  

>    vaiṡampāyana uvāca  
> namō bhagavatē tasmai vyāsāyāmitatējasē  
> yasya prasādād vakṣyāmi nārāyaṇakathām imām /10.29/  

> prāpya ṡvētaṃ mahādvīpaṃ dṛṣṭvā ca harim avyayam  
> nivṛttō nāradō rājaṃs tarasā mērum āgamat /10.30/  

> hṛdayēnōdvahan bhāraṃ yad uktaṃ paramātmanā  
> paṡcād asyābhavad rājann ātmanaḥ sādhvasaṃ mahat /10.31/  

> yad gatvā dūram adhvānaṃ kṣēmī punar ihāgataḥ  
> ¹mērōḥ pracakrāma tatō parvataṃ gandhamādanam /10.32/  

¹ all; CE "tatō mērōḥ pracakrāma" (swap)

> nipapāta ca khāt tūrṇaṃ viṡālāṃ badarīm anu  
> tataḥ sa dadṛṡē dēvau purāṇāv ṛṣisattamau /10.33/  

> tapaṡ carantau sumahad ātmaniṣṭhau mahāvratau  
> tējasābhyadhikau sūryāt sarvalōkavirōcanāt /10.34/  

> ṡrīvatsalakṣaṇau pūjyau jaṭāmaṇḍaladhāriṇau  
> jālapādabhujau tau tu pādayōṡ cakralakṣaṇau /10.35/  

> vyūḍhōraskau dīrghabhujau tathā muṣkacatuṣkiṇau  
> ṣaṣṭidantāv aṣṭa¹bhujau mēghaughasadṛṡasvanau  
> svāsyau pṛthulalāṭau ca suhanū subhrunāsikau /10.36/  

¹ B Da; CE GP "-daṃṣṭrau"

> ātapatrēṇa sadṛṡē ṡirasī dēvayōs tayōḥ  
> ēvaṃ lakṣaṇasampannau mahāpuruṣasan̄jn̄itau /10.37/  

> tau dṛṣṭvā nāradō hṛṣṭas tābhyāṃ ca pratipūjitaḥ  
> svāgatēnābhibhāṣyātha pṛṣṭaṡ cānāmayaṃ ¹tadā  
> babhūvāntargatamatir nirīkṣya puruṣōttamau /10.38/  

¹ B6,7,9 Da CE; B0,8 GP "tathā"

> sadōgatās tatra yē vai sarvabhūtanamaskṛtāḥ  
> ṡvētadvīpē mayā dṛṣṭās tādṛṡāv ṛṣisattamau /10.39/  

> iti san̄cintya manasā kṛtvā cābhipradakṣiṇam  
> upōpaviviṡē tatra pīṭhē kuṡamayē ṡubhē /10.40/  

> tatas tau tapasāṃ vāsau yaṡasāṃ tējasām api  
> ṛṣī ṡamadamōpētau kṛtvā ¹paurvāhnikaṃ vidhim /10.41/  

¹ B0,6,7,9 V; B8 Da4 "paurvāhnikaṃ"; Da3 GP "paurvāhṇikaṃ"; CE "pūrvāhṇikaṃ"

> paṡcān nāradam avyagrau pādyārghyābhyāṃ ¹athārcatām  
> pīṭhayōṡ cōpaviṣṭau tau kṛtātithyāhnikau nṛpa /10.42/  

¹ B6,8,9; Da "-cyatām" (s/cy/c/); B0 "-catuḥ"; "-citāṃ";
  CE "prapūjya ca"; V GP "athārcataḥ"  

> tēṣu tatrōpaviṣṭēṣu sa dēṡō’bhivyarājata  
> ājyāhutimahājvālair yajn̄avāṭō ¹yathā’gnibhiḥ /10.43/  

¹ all; CE "-ṭō’gnibhir yathā" (swap)

> atha nārāyaṇas tatra nāradaṃ vākyam abravīt  
> sukhōpaviṣṭaṃ viṡrāntaṃ kṛtātithyaṃ sukhasthitam /10.44/  

>    ¹naranārāyaṇāv ūcatuḥ  
> apīdānīṃ sa bhagavān paramātmā sanātanaḥ  
> ṡvētadvīpē tvayā dṛṣṭa āvayōḥ prakṛtiḥ parā /10.45/  

¹ B Da V MND; CE om.

>    nārada uvāca  
> dṛṣṭō mē puruṣaḥ ṡrīmān viṡvarūpadharō’vyayaḥ  
> sarvē hi lōkās tatrasthās tathā dēvāḥ saharṣibhiḥ /10.46/  

> adyāpi cainaṃ paṡyāmi yuvāṃ paṡyan sanātanau  
> yair lakṣaṇair upētaḥ sa harir avyaktarūpadhṛk /10.47/  

> tair lakṣaṇair upētau hi vyaktarūpadharau yuvām  
> dṛṣṭau ¹yuvāṃ mayā tatra tasya dēvasya pārṡvataḥ /10.48/  

¹ all; CE "mayā yuvāṃ" (swap)

> ¹ihaiva cāgatō’smy adya visṛṣṭaḥ paramātmanā  
> kō hi nāma bhavēt tasya tējasā yaṡasā ṡriyā /10.49/  

¹ all; CE "iha caivā-" (swap)

> sadṛṡas triṣu lōkēṣu ṛtē dharmātmajau yuvām  
> tēna mē ¹kathitaḥ kṛtsnō ²dharmaḥ kṣētrajn̄asaṃjn̄itaḥ /10.50/  

¹ all; CE "kathitaṃ pūrvaṃ"  
² all; CE "nāma kṣētrajn̄asan̄jn̄itam"

> prādurbhāvāṡ ca kathitā ¹bhaviṣyā iha yē yathā  
> tatra yē puruṣāḥ ṡvētāḥ pan̄cēndriyavivarjitāḥ /10.51/  

¹ all; CE "bhaviṣyanti hi"

> pratibuddhāṡ ca tē sarvē bhaktāṡ ca puruṣōttamam  
> tē’rcayanti sadā dēvaṃ taiḥ sārdhaṃ ramatē ca saḥ /10.52/  

> priyabhaktō hi bhagavān paramātmā dvijapriyaḥ  
> ramatē sō’rcyamānō hi sadā bhāgavatapriyaḥ /10.53/  

> viṡvabhuk sarvagō dēvō ¹bāndhavō bhaktavatsalaḥ  
> sa kartā kāraṇaṃ caiva kāryaṃ cātibaladyutiḥ /10.54/  

¹ B0,6,9 Da3 CE; B7,8 Da4 GP MND "mādhavō"

After /10.54/ B Da V GP in CE@887 (/10.55L1/).

> hētuṡ cājn̄āvidhānaṃ ca tattvaṃ caiva mahāyaṡāḥ  
> tapasā yōjya sō’’tmānaṃ ṡvētadvīpāt paraṃ hi yat  
> tēja ity abhivikhyātaṃ svayambhāsāvabhāsitam /10.55/  

> ṡāntiḥ sā triṣu lōkēṣu ¹vihitā bhāvitātmanā  
> ētayā ṡubhayā buddhyā naiṣṭhikaṃ vratam āsthitaḥ /10.56/  

¹ all; CE "siddhānāṃ bhāvitātmanām"

> na tatra sūryas tapati na sōmō’bhivirājatē  
> na vāyur vāti dēvēṡē tapaṡ carati duṡcaram /10.57/  

> vēdīm aṣṭa¹talōtsēdhāṃ bhūmāv āsthāya viṡva²kṛt  
> ēkapādasthitō dēva ūrdhvabāhur udaṅmukhaḥ /10.58/  

¹ B0,6,8 Da Ca CE; B9 GP "-nalōtsēdhāṃ"; B7 "-talōtsādhaṃ"  
² all; CE "-bhuk"

> sāṅgān āvartayan vēdāṃs tapas tēpē suduṡcaram  
> yad brahmā ṛṣayaṡ caiva svayaṃ paṡupatiṡ ca yat /10.59/  

> ṡēṣāṡ ca vibudhaṡrēṣṭhā daityadānavarākṣasāḥ  
> nāgāḥ ¹suparṇā gandharvāḥ siddhā rājarṣayaṡ ca yē /10.60/  

¹ B7,9 Da CE GP; B0,6,8 "suparṇagan-"

> havyaṃ kavyaṃ ca satataṃ vidhi¹yuktaṃ prayun̄jatē  
> ²kṛtsnaṃ tat tasya dēvasya caraṇāv ³upatiṣṭhatē /10.61/  

¹ all; CE "-pūrvaṃ"  
² ?B6 Da CE; B0 "kṛtsnasya"; B7,8,9 V GP "kṛtsnaṃ tu"  
³ B6,7,9 Da V; B0,8 GP "upatiṣṭhataḥ"; CE "upatiṣṭhati"

> yāḥ kriyāḥ samprayuktās ¹ca ēkāntagatabuddhibhiḥ  
> tāḥ sarvāḥ ṡirasā dēvaḥ pratigṛhṇāti vai svayam /10.62/  

¹ all; CE "tu"

> na tasyānyaḥ priyataraḥ pratibuddhair mahātmabhiḥ  
> vidyatē triṣu lōkēṣu ¹tatō’smy aikāntikaṃ gataḥ  
> iha caivāgatas tēna visṛṣṭaḥ paramātmanā /10.63/  

¹ B7,9 Da CE; B0,6,8 GP "tatō’sy aikāntikaṃ"

> ēvaṃ mē bhagavān dēvaḥ svayam ākhyātavān hariḥ  
> ¹āṡiṣyē tatparō bhūtvā yuvābhyāṃ saha nityaṡaḥ /10.64/  

¹ B6,7,8,9 Da; V "taṃ parō"; B0 CE GP "āsiṣyē"

Thus ends chapter B7 167; B8 268 and also 273; AB 345; GP 343; MND 344

It has 64 verses, 133 lines;
    CE 52 verses, 113 lines;
    GP 63 verses, 133 lines;
    Ku 67 verses, 139 lines.
