# Chapter 07

>    ¹ṡaunaka uvāca  
> kathaṃ sa bhagavān dēvō yajn̄ēṣv agraharaḥ prabhuḥ  
> yajn̄adhārī ca satataṃ vēdavēdāṅgavit tathā /7.1/  

¹ all; CE "janamējaya"

> nivṛttaṃ cāsthitō dharmaṃ ¹kṣēmī ²bhāgavataḥ prabhuḥ  
> ³nivṛttidharmān vidadhē sa ēva bhagavān prabhuḥ /7.2/  

¹ B0,6,7,9 Da4 CE; B8 "kṣēmaṃ"; GP "kṣamī"  
² all; CE "bhāgavatapriyaḥ"  
³ all; CE "pravṛ-"

> kathaṃ pravṛttidharmēṣu bhāgārhā dēvatāḥ kṛtāḥ  
> kathaṃ nivṛttidharmāṡ ca kṛtā vyāvṛttabuddhayaḥ /7.3/  

> ētaṃ naḥ saṃṡayaṃ ¹sūta chindhi guhyaṃ sanātanam  
> tvayā nārāyaṇakathā ṡrutā vai dharma²saṃhitāḥ /7.4/  

¹ B6,7,9 B8 (orig.) Da4 AG GP; B0 B6 (marg.) CE "vipra"  
² all; CE "saṃhitā"

After /7.4/ B Da AB GP MND in. CE@861 (/7.5/ to /7.6/).

>    ¹sūta uvāca  
> janamējayēna yat pṛṣṭaḥ ṡiṣyō vyāsasya dhīmataḥ  
> tat tē’haṃ ²kīrtayiṣyāmi paurāṇaṃ ³ṡaunakōttamam /7.5/  

¹ B6,7,8,9 Da; AB GP "sautir-"; CE om.  
² B6,7 Da CE; B0,8,9 AB GP "kathayiṣyāmi"  
³ all; CE "ṡaunakōttama"

> ṡrutvā māhātmyam ētasya dēhināṃ paramātmanaḥ  
> janamējayō mahāprājn̄ō vaiṡampāyanam abravīt /7.6/  

>    ¹janamējaya uvāca  
> imē sabrahmakā lōkāḥ sasurāsuramānavāḥ  
> kriyāsv abhyudayōktāsu ²saktā dṛṡyanti sarvaṡaḥ /7.7/  

¹ all; CE om.  
² B6,7 Da CE AB GP; B0,8 "ṡaktā"; B9 "ṡaktyā"

> mōkṣaṡ cōktas tvayā brahman nirvāṇaṃ paramaṃ sukham  
> yē ¹tu muktā bhavantīha puṇyapāpavivarjitāḥ  
> tē sahasrārciṣaṃ dēvaṃ praviṡant²īha ṡuṡruma /7.8/  

¹ all; CE "ca"  
² all; CE "-īti ṡuṡrumaḥ"

> ¹ahō hi duranuṣṭhēyō mōkṣadharmaḥ sanātanaḥ  
> yaṃ hitvā dēvatāḥ sarvā havyakavyabhujō’bhavan /7.9/  

¹ B6-9 Da CE; B0 AB GP "ayaṃ"

> kiṃ ²tad brahmā ca rudraṡ ca ṡakraṡ ca balabhit prabhuḥ  
> sūryas tārādhipō vāyur agnir varuṇa ēva ca /7.10/  

¹ B Da; CE "nu"; AB GP "ca"

> ākāṡaṃ jagatī caiva yē ca ṡēṣā divaukasaḥ  
> pralayaṃ na vijānanti ātmanaḥ parinirmitam /7.11/  

> tatas tē nāsthitā mārgaṃ dhruvam ¹akṣayam avyayam  
> ²smṛtiṃ kālaparīmāṇaṃ pravṛttiṃ yē ³samāsthitāḥ /7.12/  

¹ B Da4; Da3 AB GP "akṣaram"  
² B Da3; Da4 AB GP "smṛtikāla-"; CE "smṛtvā"  
³ ?B0,6,8 CE AB GP; B7,9 Da "samāṡritāḥ"

> dōṣaḥ kālaparī¹māṇō mahān ēṣa kriyāvatām  
> ētan mē saṃṡayaṃ vipra hṛdi ṡalyam ivārpitam  
> chindhītihāsakathanāt paraṃ kautūhalaṃ hi mē /7.13/  

¹ all; CE "-māṇē"

> kathaṃ bhāgaharāḥ prōktā dēvatāḥ kratuṣu dvija  
> kimarthaṃ cādhvarē brahmann ijyantē tridivaukasaḥ /7.14/  

> yē ca bhāgaṃ ¹pragṛhṇanti yajn̄ēṣu dvijasattama  
> tē yajantō mahāyajn̄aiḥ kasya bhāgaṃ dadanti vai /7.15/  

¹ B6,7,9 Da3 CE AB GP; B0,8 Da4 "na gṛhṇa-"

>    vaiṡampāyana uvāca  
> ahō gūḍhatamaḥ praṡnas tvayā pṛṣṭō janēṡvara  
> nātaptatapasā hy ēṣa nāvēdaviduṣā tathā /7.16/  

> nāpurāṇavidā ¹caiva ṡakyō vyāhartum an̄jasā  
> hanta tē kathayiṣyāmi yan mē pṛṣṭaḥ purā guruḥ  
> kṛṣṇadvaipāyanō vyāsō vēdavyāsō mahān ṛṣiḥ /7.17/  

¹ all; CE "cāpi"

> sumantur jaiminiṡ caiva pailaṡ ca sudṛḍhavrataḥ  
> ahaṃ caturthaḥ ṡiṣyō vai pan̄camaṡ ca ṡukaḥ smṛtaḥ /7.18/  

> ētān samāgatān sarvān pan̄ca ṡiṣyān damānvitān  
> ṡaucācārasamāyuktān̄ jitakrōdhān̄ jitēndriyān  
> vēdān adhyāpayām āsa mahābhāratapan̄camān /7.19/  

> mērau girivarē ramyē siddhacāraṇasēvitē  
> tēṣām abhyasyatāṃ vēdān kadā cit saṃṡayō’bhavat /7.20/  

> ēṣa vai yas tvayā pṛṣṭas tēna tēṣāṃ prakīrtitaḥ  
> tataḥ ṡrutō mayā cāpi tavākhyēyō’dya bhārata /7.21/  

> ṡiṣyāṇāṃ vacanaṃ ṡrutvā sarvājn̄ānatamōnudaḥ  
> parāṡarasutaḥ ṡrīmān vyāsō vākyam ¹uvāca ha /7.22/  

¹ B6-9 Da CE; B0 AB GP "athābravīt"

>    ¹vyāsa uvāca  
> mayā hi sumahat taptaṃ tapaḥ paramadāruṇam  
> bhūtaṃ bhavyaṃ ²bhaviṣyaṃ ca jānīyām iti sattamāḥ /7.23/  

¹ B8,9; et al. om.  
² B AB GP; CE "bhaviṣyac"; Da om. L2.

> tasya mē taptatapasō nigṛhītēndriyasya ca  
> nārāyaṇaprasādēna kṣīrōdasyānukūlataḥ  
> traikālikam idaṃ jn̄ānaṃ prādurbhūtaṃ yathēpsitam /7.24/  

> tac chṛṇudhvaṃ yathā¹nyāyaṃ vakṣyē saṃṡayam uttamam  
> yathā vṛttaṃ hi kalpādau dṛṣṭaṃ mē jn̄ānacakṣuṣā /7.25/  

¹ all; CE "-jn̄ānaṃ"

> paramātmēti yaṃ prāhuḥ sāṅkhyayōgavidō janāḥ  
> mahāpuruṣasan̄jn̄āṃ sa labhatē svēna karmaṇā  
> tasmāt prasūtam avyaktaṃ pradhānaṃ ¹taṃ vidur budhāḥ /7.26/  

¹ all; CE "tad"

> avyaktād vyaktam utpannaṃ lōkasṛṣṭyartham īṡvarāt  
> aniruddhō hi ¹lōkēṣu mahān ātmēti kathyatē /7.27/  

¹ B0,7,8,9 CE AB GP; B6 Da "lōkēṡa"  

> yō’sau vyaktatvam āpannō nirmamē ca pitāmaham  
> sō’haṅkāra iti prōktaḥ sarva¹tējōgatō hi saḥ /7.28/  

¹ B0,6,8,9 Ds; Da"-tējagatō"; B7 CE AB GP "-tējōmayō"

> pṛthivī vāyur ākāṡam āpō jyōtiṡ ca pan̄camam  
> ahaṅkāraprasūtāni mahābhūtāni ¹pan̄cadhā /7.29/  

¹ all; Ds "bhāgataḥ"; CE "bhārata"

> mahābhūtāni ¹sṛṣṭvātha ²tānguṇān nirmamē punaḥ  
> bhūtēbhyaṡ caiva niṣpannā ³mūrtimantō’ṣṭa tān̄ ṡṛṇu /7.30/  

¹ B Da CE; Ds AB GP "sṛṣṭvaiva"  
² all; CE "tadgu-"  
³ B6,7,9 B8 (orig.) Da CE; B0 B8 (marg.) Ds AB GP "mūrtimantaṡ ca"

> marīcir aṅgirāṡ cātriḥ pulastyaḥ pulahaḥ kratuḥ  
> vasiṣṭhaṡ ca mahātmā vai manuḥ svāyambhuvas tathā  
> jn̄ēyāḥ prakṛtayō’ṣṭau tā yāsu lōkāḥ pratiṣṭhitāḥ /7.31/  

> ¹vēdavēdāṅgasaṃyuktān ²yajn̄ayajn̄āṅgasaṃyutān  
> nirmamē lōkasiddhyarthaṃ brahmā lōkapitāmahaḥ  
> aṣṭābhyaḥ prakṛtibhyaṡ ³ca jātaṃ viṡvam idaṃ jagat /7.32/  

¹ all; CE "vēdān vēdāṅga-"  
² B0,8,9 Da3 Ds AB GP; B6,7 Da4 CE "yajn̄ān yaj-"  
³ B0,6,8,9 CE; B7 Da "tu"

> rudrō rōṣātmakō jātō daṡānyān sō’sṛjat svayam  
> ēkādaṡaitē rudrās tu ¹vikārāḥ ²pauruṣāḥ smṛtāḥ /7.33/  

¹ B6,7,8,9 Da4 CE; B0 Da3 GP "vikārapuru-"  
² B6,7,9 Da; B0,8 CE GP "puruṣāḥ"

> tē rudrāḥ prakṛtiṡ caiva sarvē caiva surarṣayaḥ  
> utpannā lōkasiddhyarthaṃ brahmāṇaṃ samupasthitāḥ /7.34/  

>    ¹rudrā ūcuḥ  
> vayaṃ ²hi sṛṣṭā bhagavaṃs tvayā ³ca prabhaviṣṇunā  
> yēna yasminn adhīkārē vartitavyaṃ pitāmaha /7.35/  

¹ Da3; Da4 "rudrōvāca"; et al. om.  
² B6,9 Da CE; B0,8 Ds GP "sṛṣtā hi" (swap); B7 "hi sṛṣtvā"  
³ all; CE "vai"

> yō’sau ¹tvayā’bhinirdiṣṭō ²adhi³kārārthacintakaḥ  
> paripālyaḥ kathaṃ tēna ⁴sāhaṅkārēṇa kartṛṇā /7.36/  

¹ all; CE "tvayā vinir-"  
² B6,7,8,9 Da4 CE; B0 Da3 GP "hy adhi-"  
³ B6,8,9 Da4; B0,7 Da3 CE GP "-kārō’rthacin-"  
⁴ all; CE "sō’dhikārō’dhikāriṇā"

> pradiṡasva balaṃ tasya yō’dhikārārthacintakaḥ  
> ēvam uktō mahādēvō dēvāṃs tān idam abravīt /7.37/  

>    ¹brahmōvāca  
> sādhv ahaṃ jn̄āpitō dēvā yuṣmābhir bhadram astu vaḥ  
> mamāpy ēṣā samutpannā cintā yā bhavatāṃ matā /7.38/  

¹ B Da GP; CE om.

> ¹lōkatantrasya kṛtsnasya kathaṃ kāryaḥ parigrahaḥ  
> kathaṃ balakṣayō na syād yuṣmākaṃ hy ātmanaṡ ca mē /7.39/  

¹ B Da CE; AB GP "lōkatrayasya"

> itaḥ sarvē’pi gacchāmaḥ ṡaraṇaṃ lōkasākṣiṇam  
> mahāpuruṣam avyaktaṃ sa nō vakṣyati yad dhitam /7.40/  

>    ¹vyāsa uvāca  
> tatas tē brahmaṇā sārdham ṛṣayō vibudhās tathā  
> kṣīrōdasyōttaraṃ kūlaṃ jagmur lōkahitārthinaḥ /7.41/  

¹ B8,9; et al. om.

> tē tapaḥ samupātiṣṭhan brahmōktaṃ vēdakalpitam  
> sa mahāniyamō nāma tapaṡcaryā sudāruṇā /7.42/  

> ¹ūrdhvā dṛṣṭir bāhavaṡ ca ēkāgraṃ ca manō’bhavat  
> ²ēkapādaḥ sthitāḥ ³samyak kāṣṭhabhūtāḥ samāhitāḥ /7.43/  

¹ all; CE "ūrdhvaṃ"  
² all; CE "ēkapādasthithāḥ"  
³ B6,7 Da; B0,8,9 Ds GP "sarvē"

> divyaṃ varṣasahasraṃ tē tapas taptvā ¹tad uttamam  
> ṡuṡruvur madhurāṃ vāṇīṃ vēdavēdāṅgabhūṣitām /7.44/  

¹ B6,7,8,9 Da CE; B0 Ds GP "sudāruṇaṃ"

>    ¹ṡrībhagavān uvāca  
> bhō ²bhōḥ sabrahmakā dēvā ṛṣayaṡ ca tapōdhanāḥ  
> svāgatēnārcya vaḥ sarvān̄ ṡrāvayē vākyam uttamam /7.45/  

¹ B Da GP MND; CE om.  
² B0,6,7 Da3 CE GP; B8,9 Da4 "bhō"

> vijn̄ātaṃ vō mayā kāryaṃ tac ca lōkahitaṃ mahat  
> ¹pravṛttiyuktaṃ kartavyaṃ yuṣmatprāṇōpabṛṃhaṇam /7.46/  

¹ ?B0,6,8 CE GP; B7,9 Da "prabhūti-"

> sutaptaṃ ¹vas tapō dēvā mamārādhanakāmyayā  
> bhōkṣyathāsya mahāsattvās tapasaḥ phalam uttamam /7.47/  

¹ B6,7,9 Da4 CE; B0 Da3 GP "ca"; B8 "yat"

> ēṣa brahmā lōkaguruḥ ¹mahā̐llōkapitāmahaḥ  
> yūyaṃ ca vibudhaṡrēṣṭhā māṃ yajadhvaṃ samāhitāḥ /7.48/  

¹ B GP; Da "mahātmā sa"; CE "sarvalōka-"

> sarvē bhāgān kalpayadhvaṃ yajn̄ēṣu mama nityaṡaḥ  
> ¹tataḥ ²ṡrēyō vidhāsyāmi yathādhīkāram īṡvarāḥ /7.49/  

¹ ?B6,7 Da; B0,8,9 CE AB GP "tathā"  
² B0,6 Da CE; B7,8,9 Ds GP "ṡrēyō’bhidhā-"

>    ¹vaiṡampāyana uvāca  
> ṡrutvaitad dēvadēvasya vākyaṃ hṛṣṭatanūruhāḥ  
> tatas tē vibudhāḥ sarvē brahmā tē ca maharṣayaḥ /7.50/  

¹ B6,7,8,9 GP; B0 Da "vyāsa"; CE om.

> vēdadṛṣṭēna vidhinā vaiṣṇavaṃ kratum āharan  
> tasmin satrē ¹tadā brahmā svayaṃ bhāgam akalpayat /7.51/  

¹ B Da CE; GP "sadā"

> dēvā dēvarṣayaṡ caiva ¹sarvē bhāgān akalpayan  
> tē kārtayugadharmāṇō bhāgāḥ paramasatkṛtāḥ  
> ²prāhur āditya³varṇābhaṃ puruṣaṃ tamasaḥ param /7.52/  

¹ B0,9 Da CE; B6,7,8 "svayaṃ bhāgam"; GP "svaṃ svaṃ bhāgam"  
² B0,6,8,9 Ds GP; B7 Da CE "prāpur"  
³ B6,7,9 Da; B0,8 CE GP "-varṇaṃ taṃ"  

> bṛhantaṃ sarvagaṃ dēvam īṡānaṃ ¹sarvadaṃ prabhum  
> tatō’tha varadō dēvas tān sarvān amarān sthitān  
> aṡarīrō babhāṣēdaṃ vākyaṃ khasthō mahēṡvaraḥ /7.53/  

¹ B6,7,9 Da4; B0,8 CE GP "varadaṃ"; Da3 "sarvadēvaṃ"

> yēna yaḥ kalpitō bhāgaḥ sa tathā ¹māmupāgataḥ  
> prītō’haṃ pradiṡāmy adya phalam āvṛttilakṣaṇam /7.54/  

¹ all; CE "samupāgataḥ"

Before /7.54/ B8 "ṡrībhagavān uvāca" but others om.

> ētad vō lakṣaṇaṃ dēvā mat¹prabhāvasamudbhavam  
> ²yūyaṃ yajn̄air ³ijyamānāḥ samāptavaradakṣiṇaiḥ /7.55/  

¹ B6,7,8,9; B0 Da CE AB GP "-prasādasam-"  
² B Da CE; Ds AB GP "svayaṃ"  
³ B7,9 CE; B0,6,8 GP "yajamānāḥ"; Da3 "ījamānāḥ"; Da4 "ījyamānāḥ";
  it is clear that Da start with "īja-" or "ījya-" instead of "yaja-"  

> yugē yugē bhaviṣyadhvaṃ pravṛttiphala¹bhāginaḥ  
> yajn̄air yē cāpi yakṣyanti sarvalōkēṣu vai surāḥ /7.56/  

¹ B0,6,7,8 GP; B9 Da CE "-bhōginaḥ"

> kalpayiṣyanti vō bhāgāṃs ¹tāṃs tān vai vēdakalpitān  
> yō ²vai yathā kalpitavān bhāgam asmin mahākratau /7.57/  

¹ B6,7; Da "tān vai" (hypometric); B0,8,9 CE GP "tē narā";
  B6,7 Da3,a4 all contain "tān vai"  
² B6,7,9 Da; B0,8 CE GP "mē"

> sa tathā yajn̄abhāgārhō vēdasūtrē mayā kṛtaḥ  
> yūyaṃ lōkān ¹bhāvayadhvaṃ yajn̄abhāga²phalōditāḥ /7.58/  

¹ all; CE "dhārayadhvaṃ"  
² B6,7,8,9 Da3 CE; B0 Ds GP "-phalōcitāḥ"; Da4 "-phalōditaḥ"

> sarvārthacintakā lōkē yathādhīkāranirmitāḥ  
> yāḥ kriyāḥ pracariṣyanti pravṛtti¹guṇakalpitāḥ /7.59/  

¹ B6,7,9 Da; B0,8 CE GP "-phalasatkṛtāḥ"

> ¹tābhir āpyāyitabalā lōkān vai dhārayiṣyatha  
> yūyaṃ hi bhāvitā ²yajn̄aiḥ ³sarvayajn̄ēṣu mānavaiḥ /7.60/  

¹ B Da CE; Ds GP "ābhir"  
² all; CE "lōkē"  
³ B0,6,7,8 GP CE; B9 Da "svar lōkē suramānavaiḥ"

> māṃ tatō bhāvayiṣyadhvam ēṣā vō bhāvanā mama  
> ¹ityarthaṃ nirmitā vēdā yajn̄āṡ cauṣadhibhiḥ saha /7.61/  

¹ all; Da "ityēvaṃ"

> ēbhiḥ samyak prayuktair hi prīyantē dēvatāḥ kṣitau  
> nirmāṇam ētad yuṣmākaṃ pravṛttiguṇakalpitam /7.62/  

> mayā kṛtaṃ suraṡrēṣṭhā yāvat kalpakṣayād ¹iha  
> cintayadhvaṃ lōkahitaṃ yathādhīkāram īṡvarāḥ /7.63/  

¹ all; CE "iti"

> marīcir ¹aṅgirāthātriḥ pulastyaḥ pulahaḥ kratuḥ  
> vasiṣṭha iti saptaitē ²manasā nirmitā hi ³vai /7.64/  

¹ B6,7,9 Da; B0 CE GP "aṅgirāṡ cātriḥ"; B8 "aṅgirāḥ atriḥ"  
² B6,7,8,9; B0 Da CE GP "mānasā"  
³ all; GP "tē"

> ētē vēdavidō mukhyā vēdācāryāṡ ca kalpitāḥ  
> pravṛttidharmiṇaṡ caiva ¹prājāpatyavikalpitāḥ /7.65/  

¹ B6,7,9 Da; B0 Ds GP "prājāpatyē ca kal-"; B8 CE "prājāpatyēna kal-"

> ayaṃ kriyāvatāṃ panthā vyaktībhūtaḥ sanātanaḥ  
> aniruddha iti ¹prōktō lōkasargakaraḥ prabhuḥ /7.66/  

¹ B0,6,8 Da3 CE GP; B7,9 Da4 "khyātō"

sanaḥ ¹sanīkaḥ sanakaḥ ²caturthaṡ ca sanandanaḥ  
sanatkumāraḥ kapilaḥ saptamaṡ ca sanātanaḥ /7.67/  

¹ B0,6,8,9 Da3; Da4 "sanākaḥ"; B7 "sanaṅkiḥ"; CE GP "sanatsujātaṡ ca"  
² B6,7,8,9 Da; B0 "sanakarmā"; CE GP "sanakaḥ sasanandanaḥ"

The second name, Sanīka, is not found in MW or Böhtlingk.

> saptaitē mānasāḥ prōktā ṛṣayō brahmaṇaḥ sutāḥ  
> svayamāgatavijn̄ānā ¹nivṛttaṃ dharmam ²āsthitāḥ /7.68/  

¹ B6,7 Da CE; B0,8,9 Ds GP "nivṛttiṃ"  
² B0,6,7 Da CE GP; B8,9 "āṡritāḥ"; Da4 "āsthitaḥ"

> ētē yōgavidō mukhyāḥ ¹sāṅkhyajn̄ānaviṡāradāḥ  
> ācāryā ²mōkṣaṡāstrēṣu mōkṣadharmapravartakāḥ /7.69/  

¹ all; CE "sāṅkhyadharmavidas tathā"  
² B7,9 Da; B6 "mokṣadharmēṣu"; B0,8 GP "dharmaṡāstrēṣu"; CE "mōkṣaṡāstrē ca"

> yatō’haṃ prasṛtaḥ pūrvam avyaktāt triguṇō mahān  
> tasmāt paratarō yō’sau kṣētrajn̄a iti kalpitaḥ /7.70/  

> sō’haṃ kriyāvatāṃ panthāḥ punarāvṛttidurlabhaḥ  
> yō yathā nirmitō jantur yasmin yasmiṃṡ ca karmaṇi /7.71/  

> pravṛttau vā nivṛttau vā tatphalaṃ sō’ṡnutē ¹mahat  
> ēṣa lōkagurur brahmā jagadādikaraḥ prabhuḥ /7.72/  

¹ all; CE "-’vaṡaḥ"

> ēṣa mātā pitā caiva yuṣmākaṃ ca pitāmahaḥ  
> mayānuṡiṣṭō bhavitā sarvabhūtavarapradaḥ /7.73/  

> asya ¹caivātmajō rudrō lalāṭād yaḥ samutthitaḥ  
> brahmānuṡiṣṭō bhavitā ²sarvabhūtavaraḥ prabhuḥ /7.74/  

¹ ?B6 (marg.) B8,9 GP; B6 (orig.) Da4 "caivānvayō";
  B7 "caivānvayē"; Da3 "caivānvajō"; CE "caivānujō"  
² B6,7,9 Da; B0 GP "-bhūtadharaḥ pra-"; B8 "sarvalōkaharaḥ pra-";
  CE "sarvatrasavarapradaḥ"  

> gacchadhvaṃ svān adhīkārāṃṡ cintayadhvaṃ yathāvidhi  
> pravartantāṃ kriyāḥ sarvāḥ sarvalōkēṣu māciram /7.75/  

> ¹pradṛṡyantāṃ ca karmāṇi prāṇināṃ gatayas tathā  
> pari²niṣṭhitakālāni ³āyūṃṣīha surōttamāḥ /7.76/  

¹ ?B6,7 CE; B0,8,9 GP "pradiṡyantāṃ"; Da "adṛṡyantāṃ"  
² B0,6,7,8 Da3 GP; Da4 "-tiṣṭhita-"; CE "-nirmita-"  
³ all; CE "āyūṃṣi ca"

> idaṃ kṛtayugaṃ nāma ¹kālaṡrēṣṭhaḥ ²pravartitaḥ  
> ahiṃsyā yajn̄apaṡavō yugē’smin ³naitad anyathā /7.77/  

¹ B6,7,8,9 Da3; B0 Da3 GP CE "kālaḥ ṡrē-"  
² all; CE "pravartatē"  
³ B7,8,9 Da4 CE; Da3 GP "na tad"; B0,6 "vaitad"

> catuṣpāt sakalō dharmō bhaviṣyaty atra vai surāḥ  
> tatas trētāyugaṃ nāma trayī yatra bhaviṣyati /7.78/  

> prōkṣitā yatra paṡavō vadhaṃ prāpsyanti vai makhē  
> ¹tatra pādacaturthō vai dharmasya na bhaviṣyati /7.79/  

¹ B6,7,8,9 Da CE; B0 GP "yatra"

> tatō vai dvāparaṃ nāma miṡraḥ kālō bhaviṣyati  
> dvipādahīnō dharmaṡ ca yugē tasmin bhaviṣyati /7.80/  

> tatas tiṣyē’tha samprāptē yugē kalipuraskṛtē  
> ēkapādasthitō dharmō yatra tatra bhaviṣyati /7.81/  

> dēvā dēvarṣayaṡ cōcuḥ tam ēvaṃvādinaṃ gurum  
> ēkapādasthitē dharmē yatrakvacanagāmini  
> kathaṃ kartavyam asmābhir bhagavaṃs tad vadasva naḥ /7.82/  

CE subst. "dēvā ūcuḥ" for CE@866 (/7.81L1/).

>    ṡrībhagavān uvāca  
> yatra vēdāṡ ca yajn̄āṡ ca tapaḥ satyaṃ damas tathā  
> ahiṃsādharmasaṃyuktāḥ pracarēyuḥ surōttamāḥ  
> sa ¹vō dēṡaḥ sēvitavyō ²yō vai dharmaṃ sadā spṛṡēt /7.83/  

¹ all; CE "vai"  
² ?B6,7 Da; B0,8 GP CE "mā vō’dharmaḥ padā spṛṡēt"

>    vyāsa uvāca  
> tē’nuṡiṣṭā bhagavatā dēvāḥ sarṣigaṇās tathā  
> namaskṛtvā bhagavatē jagmur dēṡān yathēpsitān /7.84/  

B6,8 end a chapter here and begin a new one next.

> gatēṣu tridivaukaḥsu brahmaikaḥ paryavasthitaḥ  
> didṛkṣur bhagavantaṃ tam aniruddhatanau sthitam /7.85/  

> taṃ dēvō darṡayām āsa kṛtvā hayaṡirō mahat  
> sāṅgān āvartayan vēdān kamaṇḍalu¹gaṇitradhṛk /7.86/  

¹ B6,7,9 Da Ca CE; B0,8 GP "-tridaṇḍadhṛk"

> tatō’ṡvaṡirasaṃ dṛṣṭvā taṃ dēvam amitaujasam  
> lōkakartā prabhur brahmā lōkānāṃ hitakāmyayā /7.87/  

Before /7.87/ B0,6,8 Da in. "vyāsa uvāca" but CE GP om.
It's unnecessary because Vyāsa is already the speaker since /7.84/

> mūrdhnā praṇamya varadaṃ tasthau prān̄jalir agrataḥ  
> sa pariṣvajya dēvēna vacanaṃ ṡrāvitas tadā /7.88/  

>    ¹ṡrībhagavān uvāca  
> lōkakāryagatīḥ sarvās tvaṃ cintaya yathāvidhi  
> dhātā tvaṃ sarvabhūtānāṃ tvaṃ prabhur jagatō guruḥ  
> tvayy āvēṡitabhārō’haṃ dhṛtiṃ prāpsyāmy athān̄jasā /7.89/  

¹ B0,6 Da4 GP MND; et al. om.

> yadā ca surakāryaṃ tē aviṣahyaṃ bhaviṣyati  
> prādurbhāvaṃ gamiṣyāmi tadātmajn̄ānadēṡikaḥ /7.90/  

>    ¹vyāsa uvāca  
> ēvam uktvā hayaṡirās tatraivāntaradhīyata  
> tēnānuṡiṣṭō brahmāpi svaṃ lōkam acirād gataḥ /7.91/  

¹ B8; et al. om.

> ēvam ēṣa ¹mahābhāgaḥ padmanābhaḥ ²sanātanaḥ  
> yajn̄ēṣv agraharaḥ prōktō yajn̄adhārī ca nityadā /7.92/  

¹ B6,7,8 Da CE; B0,9 GP "mahābhāga"  
² B0,6,8 Da4 CE GP; B7,9 Da3 "surōttama"

> ¹nivṛttiṃ cāsthitō dharmaṃ gatim akṣayadharmiṇām  
> pravṛttidharmān vidadhē kṛtvā lōkasya citratām /7.93/  

¹ all; B7 Da4 "nivṛttiṃ"

> sa ādiḥ sa madhyaḥ sa cāntaḥ prajānāṃ  
>  sa dhātā sa dhēyaḥ sa kartā sa kāryam  
> yugāntē sa suptaḥ ¹sa saṅkṣipya lōkān  
>  yugādau prabuddhō jagad dhy utsasarja /7.94/  

¹ B Da4; Da3 CE GP "susaṅkspiya"

> tasmai namadhvaṃ dēvāya nirguṇāya ¹mahātmanē  
> ajāya viṡvarūpāya dhāmnē sarvadivaukasām /7.95/  

¹ all; CE "guṇātmanē"

> mahābhūtādhipatayē rudrāṇāṃ patayē tathā  
> ādityapatayē caiva vasūnāṃ patayē tathā /7.96/  

> aṡvibhyāṃ patayē caiva marutāṃ patayē tathā  
> vēdayajn̄ādhipatayē vēdāṅgapatayē’pi ca /7.97/  

> samudravāsinē nityaṃ harayē mun̄jakēṡinē  
> ¹ṡāntāya sarvabhūtānāṃ mōkṣa²dharmānubhāṣiṇē /7.98/  

¹ B6,7,8 Da GP; B9 CE "ṡāntayē"; B0 om.  
² B7,8,9 CE GP; B6 Da "-dharmārtha-"; B0 om.

> tapasāṃ tējasāṃ caiva patayē ¹yaṡasām api  
> ²vacasāṃ patayē nityaṃ saritāṃ patayē tathā /7.99/  

¹ B6,7,8,9 Da GP; B0 CE "yaṡasō’pi ca"  
² B6,7,8,9 Da GP; B0 CE "vācaṡ ca"

> kapardinē varāhāya ēkaṡṛṅgāya dhīmatē  
> vivasvatē’ṡvaṡirasē caturmūrtidhṛtē sadā /7.100/  

> guhyāya jn̄ānadṛṡyāya akṣarāya kṣarāya ca  
> ēṣa dēvaḥ san̄carati sarvatragatir avyayaḥ /7.101/  

> ¹ēṣa caitat paraṃ brahma jn̄ēyō vijn̄ānacakṣuṣā  
> kathitaṃ tac ca ²vai sarvaṃ mayā pṛṣṭēna tattvataḥ /7.102/  

¹ all; CE GP (repeat) "ēvam ētat purā dṛṣṭaṃ mayā vai jn̄ānacakṣuṣā"  
² all; CE "vaḥ"

> kriyatāṃ madvacaḥ ṡiṣyāḥ sēvyatāṃ harir īṡvaraḥ  
> gīyatāṃ vēdaṡabdaiṡ ca pūjyatāṃ ca yathāvidhi /7.103/  

>    vaiṡampāyana uvāca  
> ¹vēdavyāsēna vyāsēna ēvamuktās tatō vayaṃ  
> sarvē ṡiṣyāḥ sutaṡ cāsya ṡukaḥ paramadharmavit /7.104/  

¹ B6,7,8,9 Da; B0 CE GP "ity uktās tu vayaṃ tēna vēdavyāsēna dhīmatā"

> sa cāsmākam upādhyāyaḥ sahāsmābhir viṡāṃ patē  
> caturvēdōdgatābhis ¹tam ṛgbhis ²samabhituṣṭuvē /7.105/  

¹ all; CE "ca"  
² all; CE "tam abhi-"

> ētat tē sarvam ākhyātaṃ yan māṃ tvaṃ paripṛcchasi  
> ēvaṃ mē’kathayad rājan purā dvaipāyanō guruḥ /7.106/  

> yaṡ ¹cainaṃ ṡṛṇuyān nityaṃ yaṡ ²cainaṃ parikīrtayēt  
> namō bhagavatē kṛtvā samāhita⁰manā naraḥ /7.107/  

¹ B6,7,9 Da; B0 CE GP "cēdaṃ"  
² B0,7,8,9 GP; B0 Da CE "cēdaṃ"

> bhavaty arōgō ⁰dyutimān balarūpasamanvitaḥ  
> āturō mucyatē rōgād baddhō mucyēta bandhanāt  
> ¹kāmānkāmī labhēt kāmaṃ dīrgham ²cāyur avāpnuyāt /7.108/  

¹ B0,6,9 Da GP; B7,8 "kāmātkāmī"; CE "kāmakāmī"  
² B0,6,7,9 Da GP; B8 CE "āyur"

> brāhmaṇaḥ sarvavēdī syāt kṣatriyō vijayī bhavēt  
> vaiṡyō vipulalābhaḥ syāc chūdraḥ sukham avāpnuyāt /7.109/  

> aputrō labhatē putraṃ kanyā caivēpsitaṃ patim  
> lagnagarbhā vimucyēta garbhiṇī janayēt sutam  
> vandhyā prasavam āpnōti putrapautrasamṛddhimat /7.110/  

> kṣēmēṇa gacchēd adhvānam idaṃ yaḥ paṭhatē pathi  
> yō yaṃ ¹kāmayatē kāmaṃ sa tam āpnōti ca dhruvam /7.111/  

¹ B6,7,8,9 Da3; B0 Da4 CE GP "kāmaṃ kāmayatē" (swap)

> idaṃ maharṣēr vacanaṃ viniṡcitaṃ  
>  mahātmanaḥ puruṣavarasya ¹kīrtitam  
> samāgamaṃ carṣidivaukasām imaṃ  
>  niṡamya bhaktāḥ susukhaṃ labhantē /7.112/  

¹ all; CE "kīrtanam"

Thus ends chapter B7 164; B8 266; AB 342; GP 340; MND 341

It has 112 verses, 237 lines;
    CE 108 verses, 234 lines;
    GP 119 verses, 238 lines;
    Ku 117 verses, ??? lines.
