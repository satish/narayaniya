# Chapter 17

>    janamējaya uvāca  
> bahavaḥ puruṣā brahmann utāhō ēka ēva tu  
> kō hy atra puruṣaḥ ṡrēṣṭhaḥ kō vā yōnir ihōcyatē /17.1/  

>    vaiṡampāyana uvāca  
> bahavaḥ puruṣā lōkē sāṅkhyayōga¹vicāraṇē  
> ²naitad icchanti puruṣam ēkaṃ kurukulōdvaha /17.2/  

¹ all; V "-vicāriṇē"; CE "-vicāriṇam"  
² ?B0,9 V CE GP; B6,7,8 "naivam"; Da "naiva gacchanti"

> bahūnāṃ puruṣāṇāṃ ca yathaikā yōnir ucyatē  
> tathā taṃ puruṣaṃ viṡvaṃ vyākhyāsyāmi guṇādhikam /17.3/  

> namaskṛtvā ¹ca guravē ²vyāsāya viditātmanē  
> tapōyuktāya dāntāya vandyāya paramarṣayē /17.4/  

¹ all; CE "tu"  
² all; CE "vyāsāyāmitatējasē"

> idaṃ puruṣasūktaṃ hi sarvavēdēṣu pārthiva  
> ṛtaṃ satyaṃ ca vikhyātam ṛṣisiṃhēna cintitam /17.5/  

> utsargēṇāpavādēna ṛṣibhiḥ kapilādibhiḥ  
> adhyātmacintām āṡritya ṡāstrāṇy uktāni bhārata /17.6/  

> samāsatas tu yad vyāsaḥ puruṣaikatvam uktavān  
> tat tē’haṃ sampravakṣyāmi prasādād amitaujasaḥ /17.7/  

> atrāpy udāharantīmam itihāsaṃ purātanam  
> brahmaṇā saha saṃvādaṃ tryambakasya viṡāṃ patē /17.8/  

> kṣīrōdasya samudrasya madhyē hāṭakasaprabhaḥ  
> vaijayanta iti khyātaḥ parvatapravarō nṛpa /17.9/  

> tatrādhyātmagatiṃ dēva ēkākī pravicintayan  
> vairājasadanē nityaṃ vaijayantaṃ niṣēvatē /17.10/  

> atha tatrāsatas tasya caturvaktrasya dhīmataḥ  
> lalāṭaprabhavaḥ putraḥ ṡiva āgād yadṛcchayā  
> ¹ākāṡēna mahāyōgī purā trinayanaḥ prabhuḥ /17.11/  

¹ all; CE "ākāṡēnaiva yōgīṡaḥ"

> tataḥ khān nipapātāṡu dharaṇīdharamūrdhani  
> agrataṡ cābhavat prītō vavandē cāpi pādayōḥ /17.12/  

> taṃ pādayōr nipatitaṃ dṛṣṭvā savyēna pāṇinā  
> utthāpayām āsa tadā prabhur ēkaḥ prajāpatiḥ /17.13/  

>    ¹pitāmaha uvāca  
> uvāca cainaṃ bhagavāṃṡ cirasyāgatam ātmajam  
> svāgataṃ tē mahābāhō diṣṭyā prāptō’si mē’ntikam /17.14/  

¹ all; CE om.

> kaccit tē kuṡalaṃ putra svādhyāyatapasōḥ sadā  
> nityam ugratapās tvaṃ hi tataḥ pṛcchāmi tē punaḥ /17.15/  

>    rudra uvāca  
> tvatprasādēna bhagavan svādhyāyatapasōr mama  
> kuṡalaṃ cāvyayaṃ caiva sarvasya jagatas ⁰tathā /17.16/  

> ciradṛṣṭō hi bhagavān vairājasadanē mayā  
> tatō’haṃ parvataṃ prāptas tv imaṃ tvatpādasēvitam /17.17/  

> kautūhalaṃ cāpi hi mē ēkāntagamanēna tē  
> naitat kāraṇam alpaṃ hi bhaviṣyati pitāmaha /17.18/  

> kiṃ nu tat sadanaṃ ṡrēṣṭhaṃ kṣutpipāsāvivarjitam  
> surāsurair adhyuṣitam ṛṣibhiṡ cāmitaprabhaiḥ /17.19/  

> gandharvair apsarōbhiṡ ca satataṃ sanniṣēvitam  
> utsṛjyēmaṃ girivaram ēkākī prāptavān asi /17.20/  

>    brahmōvāca  
> vaijayantō girivaraḥ satataṃ sēvyatē mayā  
> atraikāgrēṇa manasā puruṣaṡ cintyatē virāṭ /17.21/  

>    rudra uvāca  
> bahavaḥ puruṣā brahmaṃs tvayā sṛṣṭāḥ svayambhuvā  
> sṛjyantē cāparē brahman sa caikaḥ puruṣō virāṭ /17.22/  

> kō hy asau cintyatē brahmaṃs ¹tvayā vai puruṣōttamaḥ  
> ētan mē saṃṡayaṃ brūhi mahat kautūhalaṃ hi mē /17.23/  

¹ B6,7,8 Da CE; B0,9 GP "tvayaikaḥ"

>    brahmōvāca  
> bahavaḥ puruṣāḥ putra yē tvayā samudāhṛtāḥ  
> ēvam ¹ētad atikrāntaṃ draṣṭavyaṃ naivam ity api /17.24/  

¹ all; Ca "ēnat"

> ādhāraṃ tu pravakṣyāmi ēkasya puruṣasya tē  
> bahūnāṃ puruṣāṇāṃ sa yathaikā yōnir ucyatē /17.25/  

> tathā taṃ puruṣaṃ viṡvaṃ ¹paramaṃ sumahattamam  
> nirguṇaṃ ²nirguṇībhūtvā praviṡanti sanātanam /17.26/  

¹ all; Da "paramaṃ sukham uttamam"  
² all; CE "nirguṇā bhūtvā"

Thus ends chapter B7 175; B8 277; AB 352; GP 350; MND 351

It has 26 verses, 53 lines;
    CE 25 verses, 53 lines;
    GP 27 verses, 53 lines;
    Ku 25 verses, 53 lines.
