# Chapter 08

>    janamējaya uvāca  
> astauṣīd yair imaṃ vyāsaḥ saṡiṣyō madhusūdanam  
> nāmabhir vividhair ēṣāṃ niruktaṃ bhagavan mama /8.1/  

> vaktum arhasi ṡuṡrūṣōḥ prajāpatipatēr harēḥ  
> ṡrutvā bhavēyaṃ yat pūtaḥ ṡaraccandra ivāmalaḥ /8.2/  

>    vaiṡampāyana uvāca  
> ṡṛṇu rājan yathācaṣṭa ¹phālgunasya harir ²vibhuḥ  
> prasannātmātmanō nāmnāṃ niruktaṃ guṇakarmajam /8.3/  

¹ all; CE "phalgunasya"  
² B7,9 Da CE; B0,6,8 Ds GP "prabhuḥ"

> nāmabhiḥ kīrtitais ¹tasya kēṡavasya mahātmanaḥ  
> pṛṣṭavān kēṡavaṃ rājan ¹phālgunaḥ paravīrahā /8.4/  

¹ B0,6,8,9 CE GP; B7 Da "tatra"  
² all; CE "phalgunaḥ"

>    arjuna uvāca  
> bhagavan bhūtabhavyēṡa sarvabhūtasṛg avyaya  
> ¹lōkadhāman̄ jagannātha lōkānāṃ ²prabhavāvyaya /8.5/  

¹ B6,7 Da; B0,8,9 CE GP "lōkadhāma"  
² B6,7,8,9 Da; B0 CE GP "abhayaprada"

> yāni nāmāni tē dēva kīrtitāni maharṣibhiḥ  
> vēdēṣu sapurāṇēṣu yāni guhyāni karmabhiḥ /8.6/  

> tēṣāṃ niruktaṃ tvattō’haṃ ṡrōtum icchāmi kēṡava  
> na hy anyō ¹varṇayēn nāmnāṃ niruktaṃ tvām ṛtē prabhō /8.7/  

¹ all; CE "vartayēn"

>    ṡrībhagavān uvāca  
> ṛgvēdē sayajurvēdē tathaivātharvasāmasu  
> purāṇē sōpaniṣadē tathaiva jyōtiṣē’rjuna /8.8/  

> sāṅkhyē ca yōgaṡāstrē ca āyurvēdē tathaiva ca  
> bahūni mama nāmāni kīrtitāni maharṣibhiḥ /8.9/  

> gauṇāni tatra nāmāni karmajāni ¹tvaṃ kānicit  
> niruktaṃ karmajānāṃ ca ṡṛṇuṣva prayatō’nagha /8.10/  

¹ all; CE "ca"

> kathyamānaṃ mayā tāta tvaṃ hi mē’rdhaṃ smṛtaḥ purā  
> namō’tiyaṡasē tasmai dēhināṃ paramātmanē /8.11/  

> nārāyaṇāya viṡvāya nirguṇāya guṇātmanē  
> yasya prasādajō brahmā rudraṡ ca krōdhasambhavaḥ /8.12/  

> yō’sau yōnir hi sarvasya sthāvarasya carasya ca  
> aṣṭādaṡaguṇaṃ yat tat sattvaṃ sattvavatāṃ vara /8.13/  

> prakṛtiḥ sā parā mahyaṃ rōdasī yōgadhāriṇī  
> ṛtā satyāmarājayyā lōkānām ātmasan̄jn̄itā /8.14/  

> ¹yasyāḥ sarvāḥ pravartantē sargapralayavikriyāḥ  
> tatō yajn̄aṡ ca yaṣṭā ca purāṇaḥ puruṣō virāṭ  
> aniruddha iti prōktō lōkānāṃ ²prabhavāvyayaḥ /8.15/  

¹ B6,7,9 Da; B0,8 "yasmāt"; CE GP "tasmāt"  
² B Da; CE GP "prabhavāpyayaḥ"

> ¹brāhmē rātrikṣayē prāptē tasya hy amitatējasaḥ  
> prasādāt prādurabhavat padmaṃ padmanibhēkṣaṇa /8.16/  

¹ all; Ca "brahmarātri-, 'brahma' nāmadhēyasya paramātmanō mahāpralayāvasānē"

> ¹tatō brahmā samabhavat sa tasyaiva prasādajaḥ  
> ahnaḥ kṣayē lalāṭāc ca sutō dēvasya vai tathā /8.17/  

¹ all; CE "tatra"

> krōdhāviṣṭasya san̄jajn̄ē rudraḥ saṃhārakārakaḥ  
> ētau dvau vibudhaṡrēṣṭhau prasādakrōdhajav ¹ubhau /8.18/  

¹ all; CE "smṛtau"

> tadādēṡitapanthānau sṛṣṭisaṃhārakārakau  
> nimittamātraṃ tāv atra sarvaprāṇivarapradau /8.19/  

> kapardī jaṭilō muṇḍaḥ ṡmaṡānagṛhasēvakaḥ  
> ugravrata⁰dharō rudrō yōgī ¹paramadāruṇaḥ /8.20/  

¹ all; CE "tripuradā-"

> dakṣakratuharaṡ caiva bhaganētraharas tathā  
> nārāyaṇātmakō jn̄ēyaḥ pāṇḍavēya yugē yugē /8.21/  

> tasmin hi pūjyamānē vai dēvadēvē mahēṡvarē  
> sampūjitō bhavēt pārtha dēvō nārāyaṇaḥ prabhuḥ /8.22/  

> aham ātmā hi lōkānāṃ viṡvānāṃ pāṇḍunandana  
> tasmād ātmānam ēvāgrē rudraṃ sampūjayāmy aham /8.23/  

> yady ahaṃ nārcayēyaṃ vai īṡānaṃ varadaṃ ṡivam  
> ātmānaṃ nārcayēt kaṡcid iti mē ⁰bhāvitaṃ manaḥ /8.24/  

> mayā pramāṇaṃ hi kṛtaṃ lōkaḥ samanuvartatē  
> pramāṇāni hi pūjyāni tatas taṃ pūjayāmy aham /8.25/  

> yas taṃ vētti sa māṃ vētti yō’nu taṃ sa hi mām anu  
> rudrō nārāyaṇaṡ caiva sattvam ēkaṃ dvidhākṛtam /8.26/  

> lōkē carati kauntēya vyaktisthaṃ sarvakarmasu  
> na hi mē kēna cid dēyō varaḥ pāṇḍavanandana /8.27/  

> iti san̄cintya manasā purāṇaṃ ¹rudram īṡvaram  
> putrārtham ārādhitavān ²aham ātmānam ātmanā /8.28/  

¹ all; CE "viṡvam"  
² B GP; Da CE "ātmānam aham" (swap)

> na hi viṣṇuḥ praṇamati kasmai cid vibudhāya tu  
> ṛta ātmānam ēvēti tatō rudraṃ bhajāmy aham /8.29/  

> sabrahmakāḥ sarudrāṡ ca sēndrā dēvāḥ saharṣibhiḥ  
> arcayanti suraṡrēṣṭhaṃ dēvaṃ nārāyaṇaṃ harim /8.30/  

> bhaviṣyatāṃ vartatāṃ ca bhūtānāṃ caiva bhārata  
> sarvēṣām agraṇīr viṣṇuḥ sēvyaḥ pūjyaṡ ca nityaṡaḥ /8.31/  

> ¹namasva havyadaṃ viṣṇuṃ tathā ṡaraṇadaṃ ²nama  
> varadaṃ ³namasva kauntēya havyakavyabhujaṃ ⁴nama /8.32/  

¹ ?B0 Da CE GP; B6 "namasyaṃ"; B7,8,9 "namasya"  
² ?B7,9 CE; B0,6,8 GP "namaḥ"; Da3 "mama"; Da4 "manaḥ"  
³ ?B0 Da3 CE GP; B6 "nama"; B7,8,9 "namasya"; Da4 "nāma"  
⁴ ?B0,6,7 CE; B8,9 Da3 GP "namaḥ"; Da4 "-gamaḥ" (sic)

> caturvidhā mama janā bhaktā ⁰ēvaṃ hi ¹mē ṡrutam  
> tēṣām ēkāntinaḥ ṡrēṣṭhās ²yē caivānanyadēvatāḥ /8.33/  

¹ all; CE "tē"  
² all; CE "tē"

> aham ēva gatis tēṣāṃ nirāṡīḥkarmakāriṇām  
> yē ca ṡiṣṭās trayō bhaktāḥ phalakāmā hi tē matāḥ /8.34/  

> sarvē cyavana¹dharmās tē pratibuddhas tu ṡrēṣṭhabhāk  
> brahmāṇaṃ ṡitikaṇṭhaṃ ca yāṡ cānyā dēvatāḥ smṛtāḥ /8.35/  

¹ all; CE "-dharmāṇaḥ"

> ¹prabuddhavaryāḥ sēvantē mām ēvaiṣyanti yat param  
> bhaktaṃ prati viṡēṣas tē ēṣa pārthānukīrtitaḥ /8.36/  

¹ ?CE; Da3 GP "prabuddhacaryāḥ sēvantō"; B0,7,9 "pratibuddhavaryāḥ sēvantō" (hypermetric);
  B6 "prabuddha� � � � � "; B8 "prabuddhavarjaṃ sēvantō"; Da4 "prabuddhavaryā sēvantō"  

It is clear that the second word must be "sēvantō" (not "sēvantē").

> tvaṃ caivāhaṃ ca kauntēya naranārāyaṇau smṛtau  
> bhārāvataraṇārthaṃ ⁰hi praviṣṭau mānuṣīṃ tanum /8.37/  

> jānāmy adhyātmayōgāṃṡ ca yō’haṃ yasmāc ca bhārata  
> nivṛttilakṣaṇō dharmas tathābhyudayikō’pi ca /8.38/  

> ¹narāṇām ayanaṃ khyātam ²aham ēkaḥ sanātanaḥ  
> āpō nārā iti prōktā āpō vai narasūnavaḥ  
> ayanaṃ mama tat pūrvam atō nārāyaṇō hy aham /8.39/  

¹ ?B6,7,8 CE GP; B9 Da3 "-khyātō"; B0 Da4 "nārāyaṇamayaṃ khyātam"  
² B0,7,8,8 CE GP; B9 Da "tv aham"  

> chādayāmi jagad viṡvaṃ bhūtvā sūrya ivāṃṡubhiḥ  
> sarvabhūtādhivāsaṡ ca vāsudēvas tatō hy aham /8.40/  

> gatiṡ ca sarvabhūtānāṃ ¹prajānāṃ cāpi bhārata  
> vyāptā mē rōdasī pārtha kāntiṡ cābhyadhikā mama /8.41/  

¹ B6,7,9 B8 (orig.) CE; B0 B8 (marg.) Da GP "prajanaṡ"

> adhibhūtāni ¹cāntē’haṃ tad ²icchaṡ cāsmi bhārata  
> ³kramēṇa cāpy ahaṃ pārtha viṣṇur ity abhisan̄jn̄itaḥ /8.42/  

¹ B6,7,9 Da CE; B0,8 GP "cāntēṣu"  
² B6,9 Da; B0,7,8 CE GP "icchaṃṡ"  
³ B6,7,9 Da; B0,8 CE GP "kramaṇāc"

> damāt siddhiṃ parīpsantō māṃ janāḥ kāmayanti ¹ha  
> divaṃ cōrvīṃ ca madhyaṃ ca tasmād dāmōdarō hy aham /8.43/  

¹ B0,8,9 Da3 GP; B6,7 Da4 CE "hi"

> pṛṡnir ity ucyatē cānnaṃ ¹vēdā ²āpō’dhṛtaṃ tathā  
> mamaitāni sadā ²garbhaḥ pṛṡnigarbhas tatō hy aham /8.44/  

¹ B0,6,9 Da CE; B7,8 GP "vēda"  
² B6,8,9 Da3; Da4 "-dhanaṃ"; CE GP "āpō’mṛtaṃ"  
³ all; CE "garbhē"

> ṛṣayaḥ prāhur ēvaṃ māṃ ¹tritaṃ ²kūpābhipātinam  
> pṛṡnigarbha tritaṃ pāhīty ēkatadvitapātitam /8.45/  

¹ all; CE "tritakū-"  
² B0,7,9 B6(orig.) B8(orig.) Da3; B6(marg.) B8(marg.) Da4 CE "-pātitam"; GP "kūpānipātitam"

> tataḥ sa brahmaṇaḥ putra ādyō ¹hy ṛṣivaras tritaḥ  
> ²santatāradapānād vai pṛṡnigarbhānukīrtanāt /8.46/  

¹ all; CE "ṛṣi-"  
² B6,7,9 Da; B0,8 CE GP "uttatārōpā-"

> sūryasya tapatō lōkān agnēḥ sōmasya cāpy uta  
> aṃṡavō ¹yat prakāṡantē mama tē kēṡasan̄jn̄itāḥ  
> sarvajn̄āḥ kēṡavaṃ tasmān mām āhur dvijasattamāḥ /8.47/  

¹ all; CE "yē"

> svapatnyām ¹sādhitō garbha utathyēna mahātmanā  
> utathyē’ntarhitē caiva kadā cid dēvamāyayā  
> bṛhaspatir athāvindat ²patnīṃ tasya mahātmanaḥ /8.48/  

¹ B6,7,8,9 Da; B0 CE GP "āhitō"  
² all; CE "tāṃ patnīṃ tasya bhārata"

> tatō vai tam ṛṣiṡrēṣṭhaṃ maithunōpagataṃ ¹tadā  
> uvāca garbhaḥ kauntēya pan̄cabhūtasamanvitaḥ /8.49/  

¹ B0,7,9 Da4; B6,8 Da3 GP CE "tathā"

> pūrvāgatō’haṃ varada nārhasy ambāṃ prabādhitum  
> ētad bṛhaspatiḥ ṡrutvā cukrōdha ca ṡaṡāpa ca /8.50/  

> ¹maithunāyāgatō yasmāt tvayāhaṃ vinivāritaḥ  
> tasmād andhō ²yāsyasi tvaṃ macchāpān nātra saṃṡayaḥ /8.51/  

¹ all; CE "maithunōpagatō"  
² all; CE "jāsyasi"

> sa ṡāpād ṛṣimukhyasya dīrghaṃ tama upēyivān  
> sa hi dīrghatamā nāma nāmnā hy āsīd ṛṣiḥ purā /8.52/  

> vēdān avāpya caturaḥ sāṅgōpāṅgān sanātanān  
> prayōjayām āsa tadā nāma guhyam idaṃ mama  
> ānupūrvyēṇa vidhinā kēṡavēti punaḥ punaḥ /8.53/  

> sa cakṣuṣmān samabhavad ⁰gautamaṡ cābhavat punaḥ  
> ēvaṃ hi varadaṃ nāma kēṡavēti mamārjuna  
> dēvānām atha sarvēṣām ṛṣīṇāṃ ca mahātmanām /8.54/  

> agniḥ sōmēna saṃyukta ¹ēkayōnitvamāgataḥ  
> ²agnīṣōmamayaṃ tasmāj jagat kṛtsnaṃ carācaram /8.55/  

¹ all; CE "ēkayōni mukhaṃ kṛtam"  
² B0,8 Da GP; B6,8 "agnīsōmātmakaṃ"; B7 CE "agnīṣōmātmakaṃ"

> api hi purāṇē bhavati (1) ¹ēkayōgātmakam ²agnīṣōmaṃ (2)  
> dēvāṡ cāgnimukhā iti (3) ēkayōnitvāc ca parasparam ³arhantō lōkān ⁴dhārayata ⁵◌ (4) /8.56/  

¹ B6,7,9 Da4; B0 Da3 "ēkayōgātmakāv"; B8 CE GP "ēkayōnyātmakāv"  
² B6,7,8,9 Da; B0 "agnīsōmau"; CE GP "agnīṣōmau"  
³ all; CE "mahayantō"  
⁴ ?B0,8,9 Da3 CE; B6,7 Da4 GP "dhārayanta"  
⁵ B0,6,7,9 Da; B8 CE GP in. "iti"

Thus ends chapter B7 165; B8 267; AB 343; GP 341; MND 342

It has 56 verses, 116 lines, 1 paragraph of 4 sentences;
    CE 52 verses, 116 lines, 1 paragraph of 4 sentences;
    GP 58 verses, 116 lines, 1 paragraph of 4 sentences;
    Ku 58 verses, 116 lines, 1 paragraph of 4 sentences.
