# Chapter 13

>    vaiṡampāyana uvāca  
> ṡrutvaitan nāradō vākyaṃ naranārāyaṇēritam  
> ⁰atyantabhaktimān ¹dēva ēkāntitvam upēyivān /13.1/  

¹ B6,8,9 Da; B0,7 V CE GP "dēvē"

> prōṣya varṣasahasraṃ ¹ca naranārāyaṇāṡramē  
> ṡrutvā bhagavadākhyānaṃ dṛṣṭvā ca harim avyayam /13.2/  

¹ all; CE "tu"

> himavantaṃ jagāmāṡu yatrāsya svaka āṡramaḥ  
> tāv api khyātatapasau naranārāyaṇāv ṛṣī /13.3/  

> tasminn ēvāṡramē ramyē tēpatus tapa uttamam  
> tvam apy amitavikrāntaḥ pāṇḍavānāṃ kulōdvahaḥ /13.4/  

> pāvitātmādya saṃvṛttaḥ ṡrutvēmām āditaḥ kathām  
> naiva ⁰tasya parō lōkō nāyaṃ pārthivasattama /13.5/  

> karmaṇā manasā vācā yō dviṣyād viṣṇum avyayam  
> majjanti pitaras tasya narakē ṡāṡvatīḥ samāḥ /13.6/  

> yō dviṣyād vibudhaṡrēṣṭhaṃ dēvaṃ nārāyaṇaṃ harim  
> kathaṃ nāma bhavēd dvēṣya ātmā lōkasya kasya cit /13.7/  

> ātmā hi puruṣavyāghra jn̄ēyō viṣṇur iti sthitiḥ  
> ya ēṣa gurur asmākam ṛṣir gandhavatīsutaḥ /13.8/  

> tēnaitat kathitaṃ tāta māhātmyaṃ ¹paramavyayam  
> tasmāc chrutaṃ mayā cēdaṃ kathitaṃ ca tavānagha /13.9/  

¹ all; V CE "paramātmanaḥ"

After /13.9/ B Da V GP read CE 12.336.50a-51a followed by 12.336.49bcd (/13.10/).

> nāradēna tu samprāptaḥ sarahasyaḥ sasaṅgrahaḥ  
> ēṣa dharmō jagannāthāt sa tē pūrvaṃ nṛpōttama  
> kathitō harigītāsu samāsavidhikalpitaḥ /13.10/  

> kṛṣṇadvaipāyanaṃ vyāsaṃ viddhi nārāyaṇaṃ ¹bhuvi  
> kō hy anyaḥ puruṣavyāghra mahābhāratakṛd bhavēt  
> dharmān nānāvidhāṃṡ caiva kō brūyāt tam ṛtē prabhum /13.11/  

¹ all MND; V "vibhum"; CE "prabhum"

/13.11L2/ is a famous line. D7 T G M read "kō hy anyaḥ puṇḍarīkākṣān".

> vartatāṃ tē mahāyajn̄ō yathā saṅkalpitas tvayā  
> saṅkalpitāṡvamēdhas tvaṃ ṡrutadharmaṡ ca tattvataḥ /13.12/  

>    ¹sūta uvāca  
> ētat tu mahad ākhyānaṃ ṡrutvā ²pārthivasattama  
> tatō yajn̄asamāptyarthaṃ kriyāḥ sarvāḥ samārabhat /13.13/  

¹ all; CE om.  
² all; CE "pārikṣitō nṛpaḥ"

> nārāyaṇīyam ākhyānam ētat tē kathitaṃ mayā  
> pṛṣṭēna ṡaunakādyēha naimiṣāraṇyavāsiṣu /13.14/  

After /13.14L1/ B Da V GP in CE@892 (/13.14L2/).

> nāradēna purā ¹yadvai guravē ²tu nivēditam  
> ṛṣīṇāṃ pāṇḍavānāṃ ca ṡṛṇvatōḥ kṛṣṇabhīṣmayōḥ /13.15/  

¹ all; CE "rājan"  
² all; CE "mē"

> sa hi ¹paramarṣir ⁰bhuvanapatir  
>  ⁰dharaṇidharaḥ ²ṡrutivinayavidhiḥ  
> ṡrutiniyamaparō ³dvijavarasahitas  
>  tava ⁴◌ bhavatu gatir harir amarahitaḥ /13.16/  

¹ all; CE "paramagurur"  
² ?B6,8,9 "ṡrutivinayavidhir ṡrutiniyamaparō";
  Da3     "ṡrutavinayanidhir ṡrutiniyamaparō";
  Da4     "ṡrutavinayanidhir ṡamaniyamanidhiḥ";
  CE      "ṡamaniyamanidhiḥ ṡrutivinayanidhir";
  B0      "ṡrutivinayanidhir ṡamaniyamanidhiḥ";
  B7      "ṡrutiniyamaparō" (sic);
³ all; CE "dvijaparamahitas"  
⁴ B6,9 Da3 CE; B0,7,8 GP in. "ca"; Da4 in. "tu"

> asuravadhakaraḥ tapasāṃ nidhiḥ  
>  sumahatāṃ yaṡasāṃ ca bhājanaṃ  
> madhukaiṭabhahā kṛtadharmavidāṃ gatidō  
>  ¹abhayadō’stu tē ṡaraṇaṃ triguṇādhiguṇaḥ /13.17/  

¹ ?B6,9 Da; B0,8 "tu ṡaraṇaṃ sa tē triguṇōviguṇaḥ";
  GP "abhayadō makhabhāgaharō’stu ṡaraṇaṃ sa tē"; B7 damaaged  

This verse is very different in CE 12.334.14.

> triguṇātigaṡ ¹caturātmadhara  
>  pūrtēṣṭayōṡ ca phalabhāgaharaḥ  
> ²vidadhātu nityam ajitō’tibalō  
>  gatim ātma³nāṃ sukṛtinām ṛṣiṇām /13.18/  

¹ all; CE "catuṣpan̄cadharaḥ"  
² all; CE "vidadhāti"  
³ B6,7,9 Da; B8 "-vatāṃ"; B0 CE "-gāṃ"

> taṃ lōkasākṣi¹kam ajaṃ ²puruṣaṃ  
>  ravivarṇam īṡvaragatiṃ bahuṡaḥ  
> praṇamadhvam ³ēkamanasō yatayaḥ  
>  salilōdbhavō’pi tam ṛṣiṃ praṇataḥ /13.19/  

¹ B6,7,8,9 Da; B0 V CE "-ṇam"  
² B6,9 Da4 V CE; B0,7 Da3 GP "puruṣaṃ purāṇaṃ"; B8 "purāṇaṃ puruṣam"  
³ B0,6,7,9 Da GP; B8 CE "ēkamatayō"

> sa hi lōkayōnir amṛtasya padaṃ  
>  sūkṣmaṃ ¹parāyaṇaṃ acalaṃ ²hi param  
> tat sāṅkhyayōgibhir udāradhṛtaṃ  
>  buddhyā yatātmabhir ³idaṃ sanātanam /13.20/  

¹ B0,6,9 Da GP; CE "purāṇam"  
² B6,9 Da V; B0,7,8 GP "hi padam"; CE "paramam"  
³ all; CE "viditaṃ satatam"

Thus ends chapter B7 170; B8 276; AB 348; GP 346; MND 347

It has 20 verses, 42 lines;
    CE 17 verses, 38 lines;
    GP 21 verses, 42 lines;
    Ku 20 verses, 44 lines.
