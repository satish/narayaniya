# Chapter 15

>    janamējaya uvāca  
> ahō hy ēkāntinaḥ sarvān prīṇāti bhagavān hariḥ  
> vidhiprayuktāṃ pūjāṃ ca gṛhṇāti bhagavān svayam /15.1/  

> yē tu dagdhēndhanā lōkē puṇyapāpavivarjitāḥ  
> tēṣāṃ tvayābhinirdiṣṭā pāramparyāgatā gatiḥ /15.2/  

> caturthyāṃ caiva tē gatyāṃ gacchanti puruṣōttamam  
> ēkāntinas tu puruṣā gacchanti paramaṃ padam /15.3/  

> nūnam ēkāntadharmō’yaṃ ṡrēṣṭhō nārāyaṇapriyaḥ  
> agatvā gatayas tisrō ¹yad gacchanty avyayaṃ harim /15.4/  

¹ B6,Da3 GP CE; B7,8,9 Da4 "yad gacchaty"; B0 "niyacchanti"

> sahōpaniṣadān vēdān yē viprāḥ samyag āsthitāḥ  
> paṭhanti vidhim āsthāya yē cāpi yatidharmiṇaḥ /15.5/  

> tēbhyō viṡiṣṭāṃ jānāmi gatim ēkāntināṃ nṛṇām  
> kēnaiṣa dharmaḥ kathitō dēvēna ¹ṛṣiṇāpi vā /15.6/  

¹ B0,6,7,8 V CE GP; B9 Da "ṛṣīn purā"

> ēkāntināṃ ca kā caryā kadācōtpāditā vibhō  
> ētan mē saṃṡayaṃ chindhi paraṃ kautūhalaṃ hi mē /15.7/  

>    vaiṡampāyana uvāca  
> samupōḍhēṣv anīkēṣu kurupāṇḍavayōr mṛdhē  
> arjunē vimanaskē ¹vai gītā bhagavatā svayam /15.8/  

¹ B Da4; Da3 V CE GP "ca"

> ¹agatiṡ ca gatiṡ caiva pūrvaṃ tē kathitā mayā  
> gahanō hy ēṣa dharmō vai durvijn̄ēyō’kṛtātmabhiḥ /15.9/  

¹ all; CE "āgatiṡ"

> sammitaḥ sāmavēdēna puraivādiyugē kṛtaḥ  
> dhāryatē svayam īṡēna rājan nārāyaṇēna ¹ca /15.10/  

¹ all; CE "ha"

> ¹ētad arthaṃ mahārāja pṛṣṭaḥ pārthēna nāradaḥ  
> ṛṣimadhyē mahābhāgaḥ ṡṛṇvatōḥ kṛṣṇabhīṣmayōḥ /15.11/  

¹ all; CE "ētam"

> guruṇā ca mamāpy ēṣa kathitō nṛpasattama  
> yathā tu ⁰kathitas tatra nāradēna tathā ṡṛṇu /15.12/  

> yadāsīn mānasaṃ janma nārāyaṇamukhōdgatam  
> brahmaṇaḥ pṛthivīpāla tadā nārāyaṇaḥ svayam /15.13/  

> tēna dharmēṇa kṛtavān daivaṃ pitryaṃ ca bhārata  
> phēnapā ṛṣayaṡ caiva taṃ dharmaṃ pratipēdirē /15.14/  

> vaikhānasāḥ phēnapēbhyō dharmam ētaṃ prapēdirē  
> vaikhānasēbhyaḥ sōmas tu tataḥ sō’ntardadhē punaḥ /15.15/  

> yadāsīc cākṣuṣaṃ janma dvitīyaṃ brahmaṇō nṛpa  
> ¹tataḥ ²pitāmahēnā’yaṃ sōmād ³dharmaḥ pariṡrutaḥ /15.16/  

¹ B6,7,8 Da3 V; B0,9 CE "tadā"; Da4 "yataḥ"; GP "yadā"  
² B6,7,8,9 Da; B0 GP "pitāmahēnaiva" CE "pitāmahāt"  
³ all; V CE "ētaṃ dharmam ajānata"

> nārāyaṇātmakaṃ rājan rudrāya pradadau ca ¹tam  
> tatō yōgasthitō rudraḥ purā kṛtayugē nṛpa /15.17/  

¹ all; V CE "saḥ"

> vālakhilyān ṛṣīn sarvān dharmam ⁰ētam ¹apādayat  
> antardadhē tatō bhūyas tasya dēvasya māyayā /15.18/  

¹ B6,7,8,9 Da; V "apāṡrayat"; CE GP "apāṭhayat"

> tṛtīyaṃ brahmaṇō janma yadāsīd vācikaṃ mahat  
> tatraiṣa dharmaḥ sambhūtaḥ svayaṃ nārāyaṇān nṛpa /15.19/  

> suparṇō nāma tam ṛṣiḥ prāptavān puruṣōttamāt  
> tapasā vai sutaptēna damēna niyamēna ca /15.20/  

> triḥ parikrāntavān ētat suparṇō dharmam uttamam  
> yasmāt tasmād vrataṃ hy ētat trisauparṇam ihōcyatē /15.21/  

> ṛg¹vēdapāṭhapaṭhitaṃ vratam ētad dhi duṡcaram  
> suparṇāc cāpy adhigatō dharma ēṣa sanātanaḥ /15.22/  

¹ B0 V CE GP; B6,7,8,9 Da "-vāda-"

> vāyunā dvipadāṃ ṡrēṣṭha ⁰prathitō ¹jagadāyuṣā  
> vāyōḥ sakāṡāt prāptaṡ ca ṛṣibhir vighasāṡibhiḥ /15.23/  

¹ ?B0,8 V CE GP; B6,7,9 "jagadāyunā"; Da "jagadātmanā"

> ⁰tēbhyō mahōdadhiṡ ¹caiva prāptavān dharmam uttamam  
> ²antardadhē tatō bhūyō nārāyaṇasamā³hṛtaḥ /15.24/  

¹ all; V CE "cainaṃ"  
² all; V CE "tataḥ sō’ntardadhē" (swap)  
³ B6,7,8,9; Da "-hataḥ"; CE GP "-hitaḥ"

> yadā bhūyaḥ ṡravaṇajā sṛṣṭir āsīn mahātmanaḥ  
> brahmaṇaḥ puruṣavyāghra tatra kīrtayataḥ ṡṛṇu /15.25/  

> jagat sraṣṭumanā dēvō harir nārāyaṇaḥ svayam  
> cintayām āsa puruṣaṃ jagatsargakaraṃ ¹prabhum /15.26/  

¹ B6,7,8,9 Da V GP; B0 CE "prabhuḥ"

> atha cintayatas tasya karṇābhyāṃ puruṣaḥ ¹smṛtaḥ  
> prajāsargakarō brahmā tam uvāca jagatpatiḥ /15.27/  

¹ B0,6,7,8 V GP; Da "tataḥ"; B9 CE "sṛtaḥ"

> sṛja prajāḥ putra sarvā mukhataḥ pādatas tathā  
> ṡrēyas tava vidhāsyāmi balaṃ tējaṡ ca suvrata /15.28/  

> dharmaṃ ca mattō gṛhṇīṣva sātvataṃ nāma nāmataḥ  
> tēna ¹sṛṣṭaṃ kṛtayugaṃ sthāpayasva yathāvidhi /15.29/  

¹ ?B7,9 Da GP; B6 "sṛṣṭaḥ"; B0,8 V CE "sarvaṃ"

> tatō brahmā namaṡcakrē dēvāya harimēdhasē  
> dharmaṃ cāgryaṃ sa jagrāha sarahasyaṃ sasaṅgraham /15.30/  

> āraṇyakēna sahitaṃ nārāyaṇamukhōd¹gatam  
> upadiṡya tatō dharmaṃ brahmaṇē’mitatējasē /15.31/  

¹ ?B6,7,9 V CE; B0,8 Da GP "-bhavam"

> ⁰taṃ kārtayugadharmāṇaṃ nirāṡīḥkarmasan̄jn̄itam  
> jagāma tamasaḥ pāraṃ yatrāvyaktaṃ vyavasthitam /15.32/  

> tatō’tha varadō dēvō brahmalōkapitāmahaḥ  
> asṛjat sa ¹tatō lōkān kṛtsnān sthāvarajaṅgamān /15.33/  

¹ all; CE "tadā"

> tataḥ prāvartata tadā ādau kṛtayugaṃ ṡubham  
> tatō hi sātvatō dharmō vyāpya lōkān avasthitaḥ /15.34/  

> tēnaivādyēna dharmēṇa brahmā lōkavisargakṛt  
> pūjayām āsa dēvēṡaṃ hariṃ nārāyaṇaṃ prabhum /15.35/  

> dharmapratiṣṭhāhētōṡ ca manuṃ svārōciṣaṃ tataḥ  
> adhyāpayām āsa tadā lōkānāṃ hitakāmyayā /15.36/  

> tataḥ svārōciṣaḥ putraṃ svayaṃ ṡaṅkhapadaṃ nṛpa  
> adhyāpayat purāvyagraḥ sarvalōkapatir ¹vibhuḥ /15.37/  

¹ B0,8,9 V CE GP; B6,7 Da3 "babhau"; Da4 "vibhau"

After /15.37L1/ B6,7,8,9 Da V repeat /15.38L2/ but GP CE don't.

> tataḥ ṡaṅkhapadaṡ cāpi putram ātmajam aurasam  
> ¹diṡāṃ pālaṃ ²suvarṇābham adhyāpayata bhārata /15.38/  

¹ ?B0,6,8 Da3 V GP; B7,9 Da4 CE "diṡāpālaṃ"  
² B6,7,8,9 V GP; B0 "suparṇābham"; Da CE "sudharmāṇam"

> ¹tatō antardadhē bhūyaḥ prāptē trētāyugē punaḥ  
> ²nāsatyē janmani purā brahmaṇaḥ pārthivōttama /15.39/  

¹ ?B6 Da; B0 GP "sō’ntardadhē tatō"; B7 "� tatō’ntarda � � �";
  B9 "� tatō’ntardadhē"; B8 V CE "tataḥ sō’ntardadhē"  
² B0,6,8,9 GP "nāsikyajan-"; Da "nāsatya-"; V "nāsikē jan-"; B7 "�"

> dharmam ētaṃ svayaṃ dēvō harir nārāyaṇaḥ prabhuḥ  
> ¹ujjagādāravindākṣō brahmaṇaḥ paṡyatas tadā /15.40/  

¹ B0,9 Da V; B6,7,8 GP "tajjagādāravin-"; CE "ujjagārāravin-"

> sanatkumārō bhagavāṃs tataḥ prādhītavān nṛpa  
> sanatkumārād api ca vīraṇō vai prajāpatiḥ /15.41/  

> kṛtādau kuruṡārdūla dharmam ¹ētad adhītavān  
> vīraṇaṡ cāpy adhītyainaṃ ²raibhyāya ³munayē dadau /15.42/  

¹ all; B8 CE "ētam"  
² all; CE "raucyāya"  
³ B0,6,8 Da4 GP; B9 Da3 "tanayē"; CE "manavē"

> ¹raibhyaḥ putrāya ṡuddhāya suvratāya sumēdhasē  
> ²kukṣināmnē sa pradadau diṡāṃ pālāya dharmiṇē /15.43/  

¹ all; CE "raucyaḥ"  
² B6,7,8,9 Da V; B0 GP "kukṣināmāya"; CE "kukṣināmnē’tha"

> ¹tatō antardadhē bhūyō nārāyaṇamukhōd²bhavaḥ  
> aṇḍajē janmani punar brahmaṇē hariyōnayē /15.44/  

¹ B6,7,9 Da; B0 GP "tatō’py antardadhē"; B8 "sa tatō’ntardadhē"; V CE "tataḥ sō’ntardadhē"  
² all; B8 CE "-gataḥ"

> ēṣa dharmaḥ samudbhūtō nārāyaṇamukhāt ¹prabhō  
> gṛhītō brahmaṇā rājan prayuktaṡ ca yathāvidhi /15.45/  

¹ B6,7,9 Da V; B0,8 CE GP "punaḥ"

> adhyāpitāṡ ca munayō nāmnā barhiṣadō nṛpa  
> barhiṣadbhyaṡ ca ¹samprāptaḥ sāmavēdāntagaṃ dvijam /15.46/  

¹ all; CE "saṅkrāntaḥ"

> jyēṣṭhaṃ ¹nāmābhivikhyātaṃ jyēṣṭhasāmavratō hariḥ  
> jyēṣṭhāc cāpy anusaṅkrāntō rājānam avikampanam /15.47/  

¹ all; V CE "nāmnābhi"

> antardadhē tatō rājann ēṣa dharmaḥ ⁰prabhōr harēḥ  
> yad idaṃ saptamaṃ janma padmajaṃ brahmaṇō nṛpa /15.48/  

> tatraiṣa dharmaḥ kathitaḥ svayaṃ nārāyaṇēna ¹ca  
> pitāmahāya ṡuddhāya yugādau ⁰lōkadhāriṇē /15.49/  

¹ B6,7,8,9 Da V; B0 GP "ha"; CE "hi"

> ¹pitāmahaṡ ca dakṣāya dharmam ētaṃ purā dadau  
> tatō jyēṣṭhē tu dauhitrē prādād dakṣō nṛpōttama /15.50/  

¹ B0,6,8 Da4 V CE GP; B7,9 Da3 "pitāmahasya dakṣasya"

> ādityē savitur jyēṣṭhē vivasvān̄ jagṛhē tataḥ  
> trētāyugādau ca ¹tatō vivasvān manavē dadau /15.51/  

¹ all; CE "punar"

> manuṡ ca lōka¹sthityarthaṃ sutāyēkṣvākavē dadau  
> ²ikṣvākunā ca kathitō vyāpya lōkān avasthitaḥ /15.52/  

¹ B6,7,9 Da; V "-vṛttyarthaṃ"; B0,8 CE GP "-bhūtyarthaṃ"  
² all; CE "ikṣvākuṇā"

> gamiṣyati kṣayāntē ca punar nārāyaṇaṃ nṛpa  
> ⁰vratināṃ cāpi yō dharmaḥ sa tē pūrvaṃ nṛpōttama /15.53/  

> kathitō harigītāsu ¹samāsavidhikalpitaḥ  
> nāradēna ⁰tu samprāptaḥ sarahasyaḥ sasaṅgrahaḥ /15.54/  

¹ B0,6,7,8 V CE GP; B9 Da "samādhividhi-"

> ēṣa dharmō jagannāthāt sākṣān nārāyaṇān nṛpa  
> ēvam ēṣa mahān dharma ādyō rājan sanātanaḥ /15.55/  

> durvijn̄ēyō duṣkaraṡ ca sātvatair dhāryatē sadā  
> dharmajn̄ānēna caitēna suprayuktēna karmaṇā /15.56/  

> ahiṃsādharmayuktēna prīyatē harir īṡvaraḥ  
> ēkavyūhavibhāgō vā kvacid ¹dvivyūhasan̄jn̄itaḥ /15.57/  

¹ B0,9 Da V CE; B6,7,8 GP "dvirvyū-"

> ¹trivyūhaṡ cāpi saṅkhyātaṡ caturvyūhaṡ ca dṛṡyatē  
> harir ēva hi kṣētrajn̄ō nirmamō niṣkalas tathā /15.58/  

¹ ?B0,7,9 V CE; B6,8 Da GP "trirvyū-"

> jīvaṡ ca sarvabhūtēṣu pan̄cabhūtaguṇātigaḥ  
> manaṡ ca prathitaṃ rājan pan̄cēndriyasamīraṇam /15.59/  

> ēṣa lōka¹vidhir dhīmān ēṣa lōkavisargakṛt  
> akartā caiva kartā ca kāryaṃ kāraṇam ēva ca /15.60/  

¹ B0,7,8,9 Da4 GP; B6 V CE "-nidhir"

> yathēcchati tathā rājan krīḍatē puruṣō’vyayaḥ  
> ēṣa ¹ēkāntidharmas tē kīrtitō nṛpasattama /15.61/  

¹ B6,7 Da V CE; B0,8,9 GP "ēkānta-"

> mayā guruprasādēna durvijn̄ēyō’kṛtātmabhiḥ  
> ēkāntinō hi puruṣā durlabhā bahavō nṛpa /15.62/  

> yady ēkāntibhir ākīrṇaṃ jagat syāt kurunandana  
> ahiṃsakair ātmavidbhiḥ sarvabhūtahitē rataiḥ  
> bhavēt kṛtayugaprāptir āṡīḥkarmavivarjitaiḥ /15.63/  

> ēvaṃ sa bhagavān vyāsō gurur mama viṡāṃ patē  
> kathayām āsa dharmajn̄ō dharmarājn̄ē dvijōttamaḥ /15.64/  

> ṛṣīṇāṃ sannidhau rājan̄ ṡṛṇvatōḥ kṛṣṇabhīṣmayōḥ  
> tasyāpy akathayat pūrvaṃ nāradaḥ sumahātapāḥ /15.65/  

> dēvaṃ paramakaṃ brahma ṡvētaṃ candrābham acyutam  
> yatra caikāntinō yānti nārāyaṇaparāyaṇāḥ /15.66/  

>    janamējaya uvāca  
> ēvaṃ bahuvidhaṃ dharmaṃ pratibuddhair niṣēvitam  
> na kurvanti kathaṃ viprā anyē nānāvratē sthitāḥ /15.67/  

>    vaiṡampāyana uvāca  
> tisraḥ prakṛtayō rājan dēhabandhēṣu nirmitāḥ  
> sāttvikī rājasī caiva tāmasī ¹caiva bhārata /15.68/  

¹ all; CE "cēti"

> dēhabandhēṣu ¹puruṣaḥ ṡrēṣṭhaḥ kurukulōdvaha  
> sāttvikaḥ puruṣavyāghra bhavēn ²mōkṣāyaniṡcitaḥ /15.69/  

¹ B0,8,9 V CE GP; B6,7 Da "purataḥ"  
² all; CE "mōkṣārtha-"

> atrāpi sa vijānāti puruṣaṃ brahma¹vittamam  
> nārāyaṇaparō mōkṣas tatō vai sāttvikaḥ smṛtaḥ /15.70/  

¹ B0,6,8 Da GP "vartinam"

> manīṣitaṃ ca prāpnōti cintayan puruṣōttamam  
> ēkāntabhaktiḥ satataṃ nārāyaṇaparāyaṇaḥ /15.71/  

> manīṣiṇō hi yē kē cid yatayō mōkṣa¹dharmiṇaḥ  
> tēṣāṃ ²vai chinnatṛṣṇānāṃ yōgakṣēmavahō hariḥ /15.72/  

¹ all; CE "-kāṅkṣiṇaḥ"  
² B7,9 Da CE; B0,6,8 GP "vichinna-"; V1 om.

> jāyamānaṃ hi puruṣaṃ yaṃ paṡyēn madhusūdanaḥ  
> sāttvikas tu sa vijn̄ēyō bhavēn mōkṣē ca niṡcitaḥ /15.73/  

> sāṅkhyayōgēna tulyō hi dharma ēkāntasēvitaḥ  
> nārāyaṇātmakē mōkṣē tatō yānti parāṃ gatim /15.74/  

> nārāyaṇēna dṛṣṭaṡ ca pratibuddhō bhavēt pumān  
> ēvam ātmēcchayā rājan pratibuddhō na jāyatē /15.75/  

> rājasī tāmasī caiva vyāmiṡrē prakṛtī smṛtē  
> tadātmakaṃ hi puruṣaṃ jāyamānaṃ viṡāṃ patē  
> pravṛttilakṣaṇair yuktaṃ nāvēkṣati hariḥ svayam /15.76/  

> paṡyaty ēnaṃ jāyamānaṃ brahmā lōkapitāmahaḥ  
> rajasā tamasā caiva ⁰mānuṣaṃ samabhiplutam /15.77/  

> kāmaṃ dēvāṡ ca ṛṣayaḥ sattvasthā nṛpasattama  
> hīnāḥ sattvēna sūkṣmēṇa tatō vaikārikāḥ smṛtāḥ /15.78/  

>    janamējaya uvāca  
> kathaṃ vaikārikō gacchēt puruṣaḥ puruṣōttamam  
> vada sarvaṃ yathādṛṣṭaṃ pravṛttiṃ ca yathākramam /15.79/  

B Da V GP in. /15.79L2/ (=CE@901) but CE om.

>    vaiṡampāyana uvāca  
> ¹susūkṣmaṃ ⁰sattvasaṃyuktaṃ saṃyuktaṃ tribhir akṣaraiḥ  
> puruṣaḥ puruṣaṃ gacchēn niṣkriyaḥ pan̄ca²viṃṡakaḥ /15.80/  

¹ all; V CE "susūkṣmasattva-"  
² all; CE "-viṃṡakam"

> ēvam ēkaṃ sāṅkhyayōgaṃ vēdāraṇyakam ēva ca  
> parasparāṅgāny ētāni pan̄carātraṃ ca kathyatē  
> ēṣa ēkāntināṃ dharmō nārāyaṇaparātmakaḥ /15.81/  

> yathā samudrāt prasṛtā jalaughās  
>  tam ēva rājan punar āviṡanti  
> imē tathā jn̄ānamahājalaughā  
>  nārāyaṇaṃ vai punar āviṡanti /15.82/  

> ēṣa tē kathitō dharmaḥ ¹sātvataḥ kurunandana  
> kuruṣvainaṃ yathānyāyaṃ yadi ²ṡaknōṣi bhārata /15.83/  

¹ all; V CE "sātvatō yadubāndhava"  
² B6,7,9 V CE; B8 Da "ṡaktōṣi"; GP "ṡaktōsi"

> ēvaṃ hi ⁰sumahābhāgō nāradō guravē mama  
> ṡvētānāṃ ¹yatinām ²cāha ēkāntagatim avyayām /15.84/  

¹ ?B0,8 V CE GP; B7,9 Da "yatatāṃ"; B6 "� � taṃ"  
² all; V CE "āha"

> vyāsaṡ cākathayat prītyā dharmaputrāya dhīmatē  
> sa ēvāyaṃ mayā tubhyam ākhyātaḥ prasṛtō gurōḥ /15.85/  

> itthaṃ hi duṡcarō dharma ēṣa pārthivasattama  
> yathaiva tvaṃ tathaivānyē ¹bhajantīha vimōhitāḥ /15.86/  

¹ ?B6,7,9; B0,8 V GP "bhavantīha"; Da "na bhajantīha mōhi-"; CE "na bhajantīha vimō-"

> kṛṣṇa ēva hi lōkānāṃ bhāvanō mōhanas tathā  
> saṃhārakārakaṡcaiva kāraṇaṃ ca viṡāṃ patē /15.87/  

Thus ends chapter B7 172; B8 274; AB 350; GP 348; MND 349

It has 87 verses, 177 lines;
    CE 82 verses, 176 lines;
    GP 88 verses, 177 lines;
    Ku 88 verses, ??? lines.
