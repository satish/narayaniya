# Chapter 16

>    janamējaya uvāca  
> ¹sāṅkhyayōgaṃ pan̄carātraṃ vēdāraṇyakam ēva ca  
> jn̄ānāny ētāni brahmarṣē lōkēṣu pracaranti ha /16.1/  

¹ B0,7,8,9; B6 Da V CE GP "sāṅkhyaṃ yōgaṃ"  

> kim ētāny ēkaniṣṭhāni pṛthaṅniṣṭhāni vā munē  
> prabrūhi vai mayā pṛṣṭaḥ pravṛttiṃ ca yathākramam /16.2/  

>    vaiṡampāyana uvāca  
> jajn̄ē bahujn̄aṃ param atyudāraṃ  
>  yaṃ dvīpamadhyē sutam ātma¹yōgāt  
> parāṡarād ²satyavatī maharṣiṃ  
>  tasmai namō’jn̄ānatamōnudāya /16.3/  

¹ all; CE "-vantam"  
² all; CE "gandhavatī"

> pitāmahādyaṃ pravadanti ṣaṣṭhaṃ  
>  maharṣim ārṣēyavibhūtiyuktam  
> nārāyaṇasyāṃṡajam ēkaputraṃ  
>  dvaipāyanaṃ vēdamahānidhānam /16.4/  

> tam ādikālēṣu mahāvibhūtir  
>  nārāyaṇō brahma¹nidhānamādyam  
> sasarja putrārtham udāratējā  
>  vyāsaṃ mahātmānam ⁰ajaḥ purāṇaḥ /16.5/  

¹ B6,7,8,9 Da; B0,8 CE GP "-mahānidhānam"

>    janamējaya uvāca  
> tvayaiva ¹kathitaḥ pūrvaṃ ²sambhavō dvijasattama  
> vasiṣṭhasya sutaḥ ṡaktiḥ ṡaktēḥ putraḥ parāṡaraḥ  
> parāṡarasya dāyādaḥ kṛṣṇadvaipāyanō muniḥ /16.6/  

¹ B0,6,7 Da V CE GP; B8,9 "kathitaṃ"  
² B0,6,7 Da CE GP; B8,9 V "sambhavē"

> bhūyō nārāyaṇasutaṃ tvam ēvainaṃ prabhāṣasē  
> kim ataḥ pūrvajaṃ janma vyāsasyāmitatējasaḥ  
> kathayasvōttamamatē janma nārāyaṇōdbhavam /16.7/  

>    vaiṡampāyana uvāca  
> vēdārthān vēttukāmasya dharmiṣṭhasya tapōnidhēḥ  
> gurōr mē jn̄ānaniṣṭhasya himavatpāda āsataḥ /16.8/  

> kṛtvā bhāratam ākhyānaṃ tapaḥṡrāntasya dhīmataḥ  
> ṡuṡrūṣāṃ tatparā rājan kṛtavantō vayaṃ tadā /16.9/  

> sumantur jaiminiṡ caiva pailaṡ ca sudṛḍhavrataḥ  
> ahaṃ caturthaḥ ṡiṣyō vai ṡukō vyāsātmajas tathā /16.10/  

> ēbhiḥ parivṛtō vyāsaḥ ṡiṣyaiḥ pan̄cabhir uttamaiḥ  
> ṡuṡubhē himavatpādē bhūtair bhūtapatir yathā /16.11/  

> vēdān āvartayan sāṅgān bhāratārthāṃṡ ca sarvaṡaḥ  
> tam ēkamanasaṃ dāntaṃ yuktā vayam upāsmahē /16.12/  

> kathāntarē’tha kasmiṃṡ cit pṛṣṭō’smābhir dvijōttamaḥ  
> vēdārthān bhāratārthāṃṡ ca janma nārāyaṇāt tathā /16.13/  

> ¹sa pūrvam uktvā vēdārthān bhāratārthāṃṡ ca tattvavit  
> nārāyaṇād idaṃ janma vyāhartum upacakramē /16.14/  

¹ B0,6,9 V CE CP; B7,8 Da "pūrvam uktvātha"

> ṡṛṇudhvam ākhyānavaram ¹idam ārṣēyam uttamam  
> ādikālōdbhavaṃ viprās tapasādhigataṃ mayā /16.15/  

¹ all; V CE "ētad"

Before /16.15/ MND in "vyāsa uvāca" but B Da CE GP om.

> prāptē prajāvisargē vai saptamē padmasambhavē  
> nārāyaṇō mahāyōgī ṡubhāṡubhavivarjitaḥ /16.16/  

> sasṛjē nābhitaḥ ¹pūrvaṃ brahmāṇam amitaprabham  
> tataḥ sa prādurabhavad athainaṃ vākyam abravīt /16.17/  

¹ all; V CE "putraṃ"

> mama tvaṃ nābhitō jātaḥ prajāsargakaraḥ prabhuḥ  
> sṛja prajās tvaṃ vividhā brahman sajaḍapaṇḍitāḥ /16.18/  

> sa ēvam uktō vimukhaṡ cintāvyākulamānasaḥ  
> praṇamya varadaṃ dēvam uvāca harim īṡvaram /16.19/  

> kā ṡaktir mama dēvēṡa prajāḥ sraṣṭuṃ namō’stu tē  
> aprajn̄āvān ahaṃ dēva vidhatsva yad anantaram /16.20/  

> sa ēvam uktō bhagavān bhūtvāthāntarhitas tataḥ  
> cintayām āsa dēvēṡō buddhiṃ buddhimatāṃ varaḥ /16.21/  

> svarūpiṇī tatō buddhir upatasthē hariṃ prabhum  
> yōgēna ¹caiva niryōgē svayaṃ niyuyujē tadā /16.22/  

¹ B Da; V CE GP "caināṃ niryōgaḥ"

> sa tām aiṡvaryayōgasthāṃ buddhiṃ ¹gatimatīṃ satīm  
> uvāca vacanaṃ dēvō buddhiṃ vai prabhur avyayaḥ /16.23/  

¹ all; CE "ṡaktimatīṃ"

> brahmāṇaṃ praviṡasvēti lōkasṛṣṭyarthasiddhayē  
> tatas tam īṡvarādiṣṭā buddhiḥ kṣipraṃ vivēṡa sā /16.24/  

> ¹athainaṃ buddhisaṃyuktaṃ punaḥ sa dadṛṡē hariḥ  
> bhūyaṡ ⁰cainaṃ vacaḥ prāha sṛjēmā vividhāḥ prajāḥ /16.25/  

¹ B0,8,9 V CE GP; B6,7 Da4 "tathainaṃ"; Da3 "tathaitaṃ"

> bāḍham ity ēva kṛtvā ⁰sa yathājn̄āṃ ṡirasā ¹dadhē  
> ēvam uktvā sa bhagavāṃs tatraivāntaradhīyata /16.26/  

¹ B6,7,8,9 Da; B0 CE GP "harēḥ"; V "tadā"

All in. CE@902 as /16.26L1/.

> prāpa ⁰caiva muhūrtēna ¹saṃsthānaṃ dēvasan̄jn̄itam  
> tāṃ ²vai bhagavatīṃ prāpya ³ēkībhāvagatō’bhavat /16.27/  

¹ ?B6,9 Da; B0,7,8 CE GP "svasthānaṃ"  
² B6,7,8,9 Da; B0 V CE GP "caiva prakṛtiṃ"  
³ ?B0 V CE GP; B6,7,8 "matiṃ bhāvagatō’bhavat"; B9 Da "matiṃ bhāgavatō’bhavat"

> athāsya buddhir abhavat punar anyā tadā kila  
> sṛṣṭā ¹prajāḥ imāḥ sarvā brahmaṇā paramēṣṭhinā /16.28/  

¹ all; V CE "imāḥ prajāḥ" (swap)

> daityadānavagandharvarakṣōgaṇasamākulāḥ  
> jātā hīyaṃ vasumatī bhārākrāntā tapasvinī /16.29/  

> bahavō balinaḥ pṛthvyāṃ daityadānavarākṣasāḥ  
> bhaviṣyanti tapōyuktā varān prāpsyanti cōttamān /16.30/  

> avaṡyam ēva taiḥ sarvair varadānēna darpitaiḥ  
> bādhitavyāḥ suragaṇā ṛṣayaṡ ca tapōdhanāḥ /16.31/  

> tatra nyāyyam idaṃ kartuṃ bhārāvataraṇaṃ mayā  
> atha nānāsamudbhūtair vasudhāyāṃ yathākramam /16.32/  

> nigrahēṇa ca pāpānāṃ sādhūnāṃ pragrahēṇa ca  
> ¹iyaṃ tapasvinī satyā dhārayiṣyati mēdinī /16.33/  

¹ all; V CE "imāṃ tapasvinīṃ satyāṃ dhārayiṣyāmi mēdinīm"

> mayā hy ēṣā hi dhriyatē pātālasthēna bhōginā  
> mayā dhṛtā dhārayati jagad¹viṡvaṃ carācaram  
> tasmāt pṛthvyāḥ paritrāṇaṃ kariṣyē sambhavaṃ gataḥ /16.34/  

¹ all; CE "hi sa"

> ēvaṃ sa cintayitvā tu bhagavān madhusūdanaḥ  
> rūpāṇy anēkāny asṛjat prādurbhāvabhavāya saḥ /16.35/  

> vārāhaṃ nārasiṃhaṃ ca vāmanaṃ mānuṣaṃ tathā  
> ēbhir mayā nihantavyā durvinītāḥ surārayaḥ /16.36/  

> atha bhūyō jagatsraṣṭā bhōḥṡabdēnānunādayan  
> sarasvatīm uccacāra tatra sārasvatō’bhavat /16.37/  

> apāntaratamā nāma sutō vāksambhavō ¹prabhuḥ  
> bhūtabhavyabhaviṣyajn̄aḥ satyavādī ²dṛḍhavrataḥ /16.38/  

¹ B Da GP; V "prabhōḥ"; CE "vibhōḥ"  
² ?B0,7,9 V CE GP; B6,7,8 Da "jitēndriyaḥ"

> tam uvāca nataṃ mūrdhnā dēvānām ādir avyayaḥ  
> vēdākhyānē ṡrutiḥ kāryā tvayā matimatāṃ vara /16.39/  

Before /16.39L2/ B6 in. "ṡrībhagavān uvāca"

> tasmāt kuru yathājn̄aptaṃ ¹mamaitad vacanaṃ munē  
> tēna bhinnās tadā vēdā manōḥ svāyambhuvē’ntarē /16.40/  

¹ all; CE "mayaitad"

> tatas tutōṣa bhagavān haris tēnāsya karmaṇā  
> tapasā ca sutaptēna yamēna niyamēna ca /16.41/  

>    ¹ṡrībhagavān uvāca  
> manvantarēṣu putra tvam ²ēvam eva pravartakaḥ  
> bhaviṣyasy acalō brahmann apradhṛṣyaṡ ca nityaṡaḥ /16.42/  

¹ CE; B Da V GP MND om.  
² B6,8,9 Da4 GP; B0,7 "ēvam ēvaṃ pra-"; CE "ēvaṃ lōkapra-"; Da3 om.

> punas tiṣyē ca samprāptē kuravō nāma bhāratāḥ  
> bhaviṣyanti mahātmānō rājānaḥ prathitā bhuvi /16.43/  

> tēṣāṃ tvattaḥ prasūtānāṃ kulabhēdō bhaviṣyati  
> parasparavināṡārthaṃ tvām ṛtē dvijasattama /16.44/  

> tatrāpy anēkadhā vēdān bhētsyasē tapasānvitaḥ  
> kṛṣṇē yugē ca samprāptē kṛṣṇavarṇō bhaviṣyasi /16.45/  

> dharmāṇāṃ vividhānāṃ ca kartā jn̄ānakaras tathā  
> bhaviṣyasi tapōyuktō na ca rāgād vimōkṣyasē /16.46/  

> vītarāgaṡ ca putras tē paramātmā bhaviṣyati  
> mahēṡvaraprasādēna naitad vacanam anyathā /16.47/  

> yaṃ mānasaṃ vai pravadanti ¹viprāḥ  
>  pitāmahasyōttamabuddhiyuktam  
> vasiṣṭham agryaṃ ²tapasō nidhānaṃ  
>  ³yasyāpisūryaṃ vyati⁴ricyatē bhāḥ /16.48/  

¹ all; V CE "putraṃ"  
² B6,7,8,9 CE; B0 GP "ca tapō"; Da "tapasā"  
³ B GP; Da "yasyābhisūr-"; V CE "yaṡ cāpi sūr-"  
⁴ all; V CE "-ricya bhāti"

> tasyānvayē cāpi tatō maharṣiḥ  
>  parāṡarō nāma mahāprabhāvaḥ  
> pitā sa tē vēdanidhir variṣṭhō  
>  mahātapā vai tapasō nivāsaḥ  
> kānīnagarbhaḥ pitṛkanyakāyāṃ  
>  tasmād ṛṣēs tvaṃ bhavitā ca putraḥ /16.49/  

> bhūtabhavyabhaviṣyāṇāṃ chinnasarvārthasaṃṡayaḥ  
> yē hy atikrāntakāḥ pūrvaṃ sahasrayuga¹paryayāt /16.50/  

¹ B Da; V CE GP "-paryayāḥ"

> tāṃṡ ca sarvān mayōddiṣṭān drakṣyasē tapasānvitaḥ  
> punar drakṣyasi cānēkasahasrayuga¹paryayāt /16.51/  

¹ B Da; V CE GP "-paryayān"

> anādinidhanaṃ lōkē ¹cakrabandhaṃ mahāmunē  
> anudhyānān mama munē naitad vacanam anyathā /16.52/  

¹ B; Da V CE GP "cakrahastaṃ ca māṃ munē"

After /16.52/ B Da V GP in. CE@904 (/16.53L1/).

> bhaviṣyati mahāsattva khyātiṡ cāpy atulā tava  
> ṡanaiṡcaraḥ sūryaputrō bhaviṣyati manur mahān /16.53/  

> tasmin manvantarē caiva ¹manvādigaṇapūrvakaḥ  
> tvam ēva bhavitā vatsa matprasādān na saṃṡayaḥ /16.54/  

¹ B0,6,9 Da GP; B0,8 CE "saptarṣi-"; V "sa saptagaṇapūrvakaḥ"

After /16.54/ B Da V GP in CE@905 (/16.55/).

> yat kin̄cid vidyatē lōkē sarvaṃ tan madvicēṣṭitam  
> anyō hy anyaṃ cintayati svacchandaṃ vidadhāmy aham /16.55/  

>    ¹vyāsa uvāca  
> ēvaṃ sārasvatam ṛṣim apāntaratamaṃ ⁰tadā  
> uktvā vacanam īṡānaḥ sādhayasvēty athābravīt /16.56/  

¹ CE; B Da V GP om.

> sō’haṃ tasya prasādēna dēvasya harimēdhasaḥ  
> apāntaratamā nāma tatō jātō’’jn̄ayā harēḥ  
> punaṡ ca jātō vikhyātō vasiṣṭhakulanandanaḥ /16.57/  

> tad ētat kathitaṃ janma mayā pūrvakam ātmanaḥ  
> nārāyaṇaprasādēna tathā nārāyaṇāṃṡajam /16.58/  

> mayā hi sumahat taptaṃ tapaḥ paramadāruṇam  
> purā matimatāṃ ṡrēṣṭhāḥ paramēṇa samādhinā /16.59/  

> ētad vaḥ kathitaṃ sarvaṃ yan māṃ ⁰pṛcchatha putrakāḥ  
> pūrvajanma bhaviṣyaṃ ca bhaktānāṃ snēhatō mayā /16.60/  

Here B7,8,9 Da end chapter; B7 173; B8 275

>    vaiṡampāyana uvāca  
> ēṣa tē kathitaḥ ¹pūrvaṃ sambhavō’smadgurōr nṛpa  
> vyāsasyākliṣṭamanasō yathā pṛṣṭaḥ punaḥ ṡṛṇu /16.61/  

¹ B0,9 V CE GP; B6,7,8 "pūrvaḥ"; Da "pūrvasam-"

Here B6 ends chapter.

> sāṅkhyaṃ yōgaṃ pan̄carātraṃ vēdāḥ pāṡupataṃ tathā  
> jn̄ānāny ētāni rājarṣē viddhi nānāmatāni vai /16.62/  

> sāṅkhyasya vaktā kapilaḥ paramarṣiḥ sa ucyatē  
> hiraṇyagarbhō yōgasya vēttā nānyaḥ purātanaḥ /16.63/  

> apāntaratamāṡ caiva vēdācāryaḥ sa ucyatē  
> prācīnagarbhaṃ tam ṛṣiṃ pravadantīha kē cana /16.64/  

> umāpatir bhūtapatiḥ ṡrīkaṇṭhō brahmaṇaḥ sutaḥ  
> uktavān idam avyagrō jn̄ānaṃ pāṡupataṃ ṡivaḥ /16.65/  

> pan̄carātrasya kṛtsnasya vēttā tu bhagavān svayam  
> sarvēṣu ca nṛpaṡrēṣṭha jn̄ānēṣv ētēṣu dṛṡyatē /16.66/  

> yathā¹gataṃ ²yathājn̄ānaṃ niṣṭhā nārāyaṇaḥ prabhuḥ  
> na cainam ēvaṃ jānanti tamōbhūtā viṡāṃ patē /16.67/  

¹ B6,7,8,9 Da; B0,8 V CE GP "-gamaṃ"  
² ?B0,7,9 V CE GP; B6,8 Da "tathā jn̄ānaṃ"  

> tam ēva ṡāstra¹kartāraḥ pravadanti manīṣiṇaḥ  
> niṣṭhāṃ nārāyaṇam ṛṣiṃ nānyō’stīti ²vacō mama /16.68/  

¹ all; CE "-kartāraṃ"  
² all; V CE "ca vādinaḥ"

> niḥsaṃṡayēṣu sarvēṣu nityaṃ ¹sa vasatē hariḥ  
> sasaṃṡayān hētubalān nādhyāvasati mādhavaḥ /16.69/  

¹ B6,7,8,9 Da4 V; B0 CE GP "vasati vai"

> pan̄carātravidō yē tu yathākramaparā nṛpa  
> ēkāntabhāvōpagatās tē hariṃ praviṡanti vai /16.70/  

> sāṅkhyaṃ ca yōgaṃ ca sanātanē dvē  
>  vēdāṡ ca sarvē nikhilēna rājan  
> sarvaiḥ samastair ṛṣibhir niruktō  
>  nārāyaṇō viṡvam idaṃ purāṇam /16.71/  

> ṡubhāṡubhaṃ karma samīritaṃ yat  
>  pravartatē sarvalōkēṣu kin̄cit  
> tasmād ṛṣēs tad bhavatīti vidyād  
>  divy ¹antarīkṣē bhuvi ⁰cāpsu ²caiva /16.72/  

¹ B6,7,8,9 V; B0 Da CE GP "antarikṣē"  
² B6,7,8,9 Da; B0 V GP "cēti"; CE "cāpi"

Thus ends chapter B7 174; B8 276; AB 351; GP 349; MND 350

It has 72 verses, 149 lines;
    CE 69 verses, 145 lines;
    GP 74 verses, 149 lines;
    Ku 74 verses, ??? lines.
