# Chapter 09

>    arjuna uvāca  
> agnīṣōmau kathaṃ pūrvam ēkayōnī pravartitau  
> ēṣa mē saṃṡayō jātas taṃ chindhi madhusūdana /9.1/  

>    ṡrībhagavān uvāca  
> hanta tē vartayiṣyāmi purāṇaṃ pāṇḍunandana  
> ātma¹tējōdbhavaṃ pārtha ṡṛṇuṣvaikamanā mama /9.2/  

¹ B0,6 Da CE GP; B7,8,9 "-tējobhavaṃ"

> samprakṣālanakālē’tikrāntē ¹caturthē yugasahasrāntē  
> avyaktē sarvabhūtapralayē ²sarvabhūta sthāvarajaṅgamē /9.3/  

¹ B6,7 Da CE; B0,8,9 GP "caturyuga-"  
² all; CE om.

> jyōtirdharaṇivāyu¹virahitē’ndhē tamasi jalaikārṇavē lōkē  
> ²mamāpa ity ēvaṃ brahmabhūtasaṃjn̄akē’dvitīyē pratiṣṭhitē /9.4/  

¹ B0,6,9 Da; B7,8 CE GP "-rahitē-"  
² B Da; GP "āpa ity-"; CE "tama ity ēvābhibhūtē’saṃjn̄akē-"

> ¹na ca rātryāṃ na divasē na sati nāsati na ²cāpy avyaktē nāvyaktē vyavasthitē /9.5/  

¹ all; CE "naiva"  
² all; CE om.

> ⁰ētasyām ⁰avasthāyāṃ nārāyaṇaguṇāṡrayād ¹ajarāmarād anindriyād  
>  agrāhyād asambhavāt ²◌ ahiṃsrāl lalāmād vividhapravṛttiviṡēṣāt  
> ³avairād akṣayād ⁴amarād ajarād ⁵amūrtitaḥ sarvavyāpinaḥ sarvakartuḥ ⁶ṡāṡvatas  
>  tamasaḥ puruṣaḥ prādurbhūtō harir avyayaḥ /9.6/  

¹ B GP; Da "ajarād"; CE "akṣayād ajarād"  
² B7,9 Da; B0,6,8 CE GP in. "satyād"  
³ all; CE om.  
⁴ all; CE "ajarāmarād"  
⁵ B0,6,7,8 CE GP; B9 Da "amūrtimataḥ"  
⁶ B0,6,8,9 Da4 GP; B7 Da3 CE "ṡāṡvatāt"

> nidarṡanam api hy atra bhavati  
> nāsīd ahō na rātrir āsīt  
> na sad āsīn nāsad āsīt /9.7/  

> tama ēva purastād abhavad viṡvarūpam  
> sā ¹viṡvasya ²jananīty ēvam asyārthō’nu³bhāṣyaḥ /9.8/  

¹ B6,8 Da CE; B0,7,8 GP "viṡvarūpasya"  
² B0,6 Da CE; B7 GP "rajanī hy"; B8 "rajanīty"; B9 "jananī hy"  
³ all; CE "-bhāṣyatē"

> tasyēdānīṃ ¹tamasaḥ sambhavasya puruṣasya ²brahmayōnēr brahmaṇaḥ prādurbhāvē  
>  sa puruṣaḥ prajāḥ sisṛkṣamāṇō nētrābhyām agnīṣōmau sasarja  
> tatō bhūta³sargēṣu sṛṣṭēṣu prajākramavaṡād brahmakṣatram upātiṣṭhat  
> yaḥ sōmas tad brahma yad brahma tē brāhmaṇāḥ  
> yō’gnis tat kṣatraṃ kṣatrād brahma balavattaram  
> kasmād iti lōkapratyakṣaguṇam ētat ⁴yathā  
> brāhmaṇēbhyaḥ paraṃ bhūtaṃ nōtpannapūrvam  
> dīpyamānē’gnau ⁰juhōtīti kṛtvā bravīmi  
> bhūtasargaḥ kṛtō brahmaṇā bhūtāni ca pratiṣṭhāpya trailōkyaṃ dhāryata iti  
> mantravādō’pi hi bhavati /9.9/  

¹ B0,6,8,9 GP; B7 Da CE "tamaḥsam-"  
² all; CE "padmayōnēr"  
³ all; CE "-sargē pravṛttē"  
⁴ B; Da CE GP "tad yathā"

> tvam agnē yajn̄ānāṃ hōtā viṡvēṣām  
> ¹hitō dēvānāṃ mānuṣāṇāṃ ca jagata iti /9.10/  

¹ B6,9 GP MND; B0,7 om. "jagata"; Da "-ca yajata iti";
  B8 "-dēvānāṃ mukhamimaṃ mānuṣyāṇāṃ ca-"; CE "hitō dēvēbhir mānuṣē janē iti"  

> nidarṡanaṃ cātra bhavati  
> viṡvēṣām agnē yajn̄ānāṃ ¹tvaṃ hōtēti  
> ²hitō dēvair ³manuṣyair ⁴jagata iti /9.11/  

¹ all; CE om.  
² ?CE Ca; B7,8,9 Da "abhihitō"; B0,6 GP "tvaṃ hitō"  
³ all; CE "mānuṣair"  
⁴ B0,6,7,9 CE GP; B8 Da "yajata"

> agnir hi yajn̄ānāṃ hōtā kartā  
> sa cāgnir brahma /9.12/  

> na hy ṛtē ¹mantrān havanam asti  
> na vinā puruṣaṃ tapaḥ sambhavati  
> havir mantrāṇāṃ sampūjā vidyatē dēva²mānuṣaṛṣīṇāṃ ³anēna tvaṃ hōtēti ⁴niyuktaḥ  
> yē ca ⁵mānuṣahōtrādhikārās tē ca  
> brāhmaṇasya hi yājanaṃ vidhīyatē na kṣatravaiṡyayōr dvijātyōḥ  
> tasmād brāhmaṇā hy agnibhūtā yajn̄ān udvahanti  
> yajn̄ā dēvāṃs tarpayanti dēvāḥ pṛthivīṃ bhāvayanti  
> ṡatapathē’pi hi ⁶brāhmaṇaṃ bhavati /9.13/  

¹ B0,6,9 Da; B7,8 GP "mantrāṇāṃ"; CE "mantrādd"  
² all; CE "manuṣyāṇām"  
³ ?B7,8 CE GP; Da3 "na"; Da4 "tat"; B0,6,9 om.  
⁴ ?B0,6,8 CE GP; B7,9 Da "yuktaḥ"  
⁵ all; CE "mānuṣā hō-"  
⁶ ?B6,9 CE; Da "brāhmaṇō"; B0,7 GP "brāhmaṇamukhē"

> agnau samiddhē ¹◌ ²juhutē yō vidvān ³brāhmaṇamukhē dānāhutiṃ juhōti /9.14/  

¹ B Da; CE GP in. "sa"  
² B0,6,7 Da; B9 "juhuti"; B8 CE GP "juhōti"  
³ B6,8,9 Da CE; B0,7 GP "brāhmaṇamukhēnāhutiṃ"

> ēvam apy agnibhūtā brāhmaṇā vidvāṃsō’gniṃ bhāvayanti  
> agnir viṣṇuḥ sarvabhūtāny anupraviṡya prāṇān dhārayati  
> api cātra sanatkumāragītāḥ ṡlōkā bhavanti /9.15/  

> ¹brahmā viṡvaṃ sṛjat pūrvaṃ sarvādir ²niravaskṛtam  
> brahmaghōṣair divaṃ ²gacchanty amarā brahmayōnayaḥ  
> brāhmaṇānāṃ matir vākyaṃ karma ṡraddhā tapāṃsi ca  
> dhārayanti mahīṃ dyāṃ ca ⁴ṡaikyō vāg amṛtaṃ sadā /9.16/  

¹ all; CE "viṡvaṃ brahmāsṛ-" (swap)  
² all; CE "niravaskaram"  
³ all; CE "tiṣṭhanty"  
⁴ ?B6 "ṡikṣā vāg-"; B8 "ṡaikṣād vāg-"; Da "ṡaityā vāg-";
  B0,GP "-amṛtaṃ tathā"; B9 CE "ṡaityād vāry amṛtaṃ yathā"  

It is clear that the ending must be "vāg amṛtaṃ sadā".

> nāsti satyāt parō dharmō nāsti mātṛsamō guruḥ  
> brāhmaṇēbhyaḥ paraṃ nāsti prētya cēha ca bhūtayē /9.17/  

> naiṣām ukṣā ¹vardhatē nōta vāhā  
>  na gargarō ²mathyati sampradānē  
> apadhvastā dasyubhūtā bhavanti  
>  yēṣāṃ rāṣṭrē brāhmaṇā vṛttihīnāḥ /9.18/  

¹ B8,9 Da3 Ca CE; B6,7 "vahatē"; B0 Da4 GP "vahati"  
² all; CE "mathyati"

> vēdapurāṇētihāsaprāmāṇyān nārāyaṇamukhōdgatāḥ  
>  sarvātmānaḥ sarvakartāraḥ sarva¹bhāvāṡ ca brāhmaṇāḥ /9.19/  

¹ all; CE "-bhāvanāṡ ca"

> ¹vāksaṃyamakālē hi tasya dēvasya varapradasya brāhmaṇāḥ  
>  prathamaṃ prādurbhūtā brāhmaṇēbhyaṡ ca ṡēṣā varṇāḥ prādurbhūtāḥ /9.20/  

¹ all; CE "vāksamakālaṃ"

> itthaṃ ca surāsuraviṡiṣṭā brāhmaṇā ¹yatra mayā brahmabhūtēna purā  
>  svayam ēvōtpāditāḥ surāsuramaharṣayō bhūtaviṡēṣāḥ sthāpitā nigṛhītāṡ ca /9.21/  

¹ B Da; CE "yadā"; GP "ya ēva"

> ahalyādharṣaṇanimittaṃ hi gautamād dhariṡmaṡrutām indraḥ prāptaḥ  
> kauṡikanimittaṃ cēndrō muṣkaviyōgaṃ mēṣavṛṣaṇatvaṃ cāvāpa /9.22/  

> aṡvinōr grahapratiṣēdhō¹dyatavajrasya purandarasya cyavanēna ²stambhitau bāhū /9.23/  

¹ B7,9 Da CE GP; B0,6,8 "-dyatasya savajrasya"  
² all; CE "stambhitō bāhuḥ"

> kratuvadhaprāptamanyunā ca dakṣēṇa bhūyas tapasā  
>  cātmānaṃ saṃyōjya nētrākṛtir anyā lalāṭē rudrasyōtpāditā /9.24/  

> tripuravadhārthaṃ dīkṣām ¹abhyupagatasya  
>  rudrasyōṡanasā ²jaṭāḥ ṡirasa utkṛtya prayuktāḥ  
> ³tatra prādurbhūtā bhujagāḥ  
> tair asya bhujagaiḥ pīḍyamānaḥ kaṇṭhō nīlatām ⁴upagataḥ  
> pūrvē ca manvantarē svāyambhuvē nārāyaṇahasta⁵bandhagrahaṇān nīlakaṇṭhatvam ēva ⁶vā /9.25/  

¹ B6,7 Da CE; B0,9 "apyupa-"; B8 GP "upagatasya"  
² all; CE "jaṭā ṡirasō"  
³ ?B7,9 Da; B0,6,8 CE GP "tataḥ"  
⁴ all; CE "upanītaḥ"  
⁵ B6,9 Da CE; B0,8 "-bandhanagraha-"; B7 GP om. "-bandha-"  
⁶ all; CE "ca"

> ¹amṛtōtpāda puraṡcaraṇatām upagatasyāṅgirasō  
>  bṛhaspatēr upaspṛṡatō na prasādaṃ gatavatyaḥ kilāpaḥ  
> atha bṛhaspatir apāṃ cukrōdha  
> yasmān mamōpaspṛṡataḥ kaluṣībhūtā ²◌ tasmād  
>  adyaprabhṛti jhaṣamakara³kacchapa⁰jantusaṅkīrṇāḥ kaluṣībhavatēti  
> tadāprabhṛty āpō yādōbhiḥ saṅkīrṇāḥ ⁴saṃvṛttāḥ /9.26/  

¹ B6,7,9 Da4; B0,8 Da3 "-pādana pura-"; CE "amṛtōtpādanē pura-"  
² ?B7,9 Da; B0,6 CE GP in. "na ca prasādam upagatās"; B8 "ca mat prasādam upagatās"  
³ all; CE "-matsyakacch-"  
⁴ B6,9 Da3 CE; B0,8 Da4 GP "sampravṛttāḥ"; B7 "sambhūtāḥ"

> viṡvarūpō vai tvāṣṭraḥ purōhitō dēvānām āsīt svasrīyō’surāṇām  
> sa pratyakṣaṃ dēvēbhyō bhāgam ¹adāt parōkṣam asurēbhyaḥ /9.27/  

¹ all; CE "adadat"

> atha hiraṇyakaṡipuṃ puraskṛtya  
>  viṡvarūpamātaraṃ svasāram asurā varam ayācanta  
> hē svasar ayaṃ tē putras tvāṣṭrō viṡvarūpas triṡirā dēvānāṃ  
>  purōhitaḥ pratyakṣaṃ dēvēbhyō bhāgam ¹adāt parōkṣam asmākam  
> tatō dēvā vardhantē vayaṃ kṣīyāmaḥ  
> tad ēnaṃ tvaṃ vārayitum arhasi tathā yathāsmān bhajēd iti /9.28/  

¹ all; CE "adadat"

> atha viṡvarūpaṃ nandanavanam upagataṃ mātōvāca  
> putra kiṃ parapakṣavardhanas tvaṃ mātulapakṣaṃ nāṡayasi  
> nārhasy ēvaṃ kartum iti  
> sa viṡvarūpō mātur vākyam anatikramaṇīyam  
>  iti matvā sampūjya hiraṇyakaṡipum agāt /9.29/  

> hairaṇyagarbhāc ca vasiṣṭhād dhiraṇyakaṡipuḥ ṡāpaṃ prāptavān  
> yasmāt tvayānyō vṛtō hōtā tasmād asamāptayajn̄as  
>  tvam apūrvāt sattvajātād vadhaṃ prāpsyasīti  
> tacchāpadānād dhiraṇyakaṡipuḥ prāptavān vadham /9.30/  

> ¹atha viṡvarūpō mātṛpakṣavardhanō’tyarthaṃ tapasy abhavat  
> tasya vratabhaṅgārtham indrō bahvīḥ ṡrīmatyō’psarasō niyuyōja  
> tāṡ ca dṛṣṭvā manaḥ kṣubhitaṃ tasyābhavat tāsu cāpsaraḥsu nacirād ²iva saktō’bhavat  
> saktaṃ cainaṃ jn̄ātvāpsarasa ūcur gacchāmahē vayaṃ yathāgatam iti /9.31/  

¹ all; CE om.  
² B6,7,8 Da4; B0,9 Da3 CE GP "ēva"

> tās tvāṣṭra uvāca  
> kva gamiṣyatha āsyatāṃ tāvan mayā saha ṡrēyō bhaviṣyatīti  
> tās tam abruvan  
> vayaṃ dēvastriyō’psarasa indraṃ ¹dēvaṃ varadaṃ purā prabhaviṣṇuṃ vṛṇīmaha iti /9.32/  

¹ all; CE om.

> atha tā viṡvarūpō’bravīd adyaiva sēndrā dēvā na bhaviṣyantīti  
> tatō mantrān̄ jajāpa  
> tair mantraiḥ ⁰prāvardhata triṡirāḥ  
> ēkēnāsyēna sarvalōkēṣu ¹yathāvad dvijaiḥ kriyāvadbhir  
>  yajn̄ēṣu suhutaṃ sōmaṃ papāv ēkēnāpa ēkēna sēndrān dēvān  
> athēndras taṃ vivardhamānaṃ sōmapānāpyāyitasarvagātraṃ dṛṣṭvā cintām āpēdē ²saha vai /9.33/  

¹ all; CE om.  
² all; CE om.

> ¹tē dēvāḥ sēndrā brahmāṇam abhijagmuḥ ²ta ūcur   
> viṡvarūpēṇa sarvayajn̄ēṣu suhutaḥ sōmaḥ pīyatē  
> vayam abhāgāḥ saṃvṛttāḥ  
> asurapakṣō vardhatē vayaṃ kṣīyāmaḥ  
> tad arhasi nō vidhātuṃ ³ṡrēyō’anantaram iti /9.34/  

¹ all; CE "dēvāṡ ca tē sahēndrēṇa"  
² all; CE "ūcuṡ ca"  
³ all; CE "ṡrēyō yad anan-"

> tān brahmōvāca ṛṣir bhārgavas tapas tapyatē dadhīcaḥ  
> sa yācyatāṃ ⁰varaṃ ¹◌ yathā kalēvaraṃ ⁰jahyāt  
> tasyāsthibhir vajraṃ kriyatām iti /9.35/  

¹ B0,6,9 Da4 CE; B7,8 Da3 GP in. "sa"

> ¹tatō dēvās tatrā²gacchanta yatra dadhīcō bhagavān ṛṣis tapas tēpē  
> sēndrā dēvās ³tam tathā abhigamyōcur bhagavaṃs ⁴tapaḥ sukuṡalam avighnaṃ cēti /9.36/  

¹ all; CE om.  
² B6,7,9 Da4; B0,8 Da3 CE GP "-gacchan yat-"  
³ B6,7,8 Da4 GP; B0,9 Da3 om. "tam"; CE om. "tathā"  
⁴ ?B6,7,9 GP; B0,8 "tapaḥ sakuṡa-"; Da3 "tapaḥ kuṡa-"; Da4 "tapas tē kuṡa-"; CE "tapasaḥ kuṡa-"

> tān dadhīca uvāca svāgataṃ bhavadbhyaḥ kiṃ kriyatām ¹iti  
> yad vakṣyatha tat kariṣyām⁰īti /9.37/  

¹ all; CE om.

> tē tam abruvan̄ ṡarīraparityāgaṃ  
>  lōkahitārthaṃ ¹bhagavān kartum arhatīti /9.38/  

¹ B7,9 Da4 CE GP; B0 "bhagavan"; B6,8 Da3 "bhavān" (sic.)

> atha dadhīcas tathaivāvimanāḥ sukhaduḥkhasamō  
>  mahāyōgī ātmānaṃ samādhāya ṡarīraparityāgaṃ cakāra /9.39/  

> tasya paramātmany avasṛtē tāny asthīni dhātā saṅgṛhya vajram akarōt  
> tēna vajrēṇābhēdyēnāpradhṛṣyēṇa  
>  brahmāsthisambhūtēna viṣṇupraviṣṭēnēndrō viṡvarūpaṃ jaghāna  
> ṡirasāṃ cāsya chēdanam akarōt  
> tasmād anantaraṃ viṡvarūpagātramathanasambhavaṃ  
>  ¹tvāṣṭrōtpāditam ēvāriṃ vṛtram indrō jaghāna /9.40/  

¹ all; CE "tvaṣṭrō-"

> tasyāṃ dvaidhībhūtāyāṃ brahmavadhyāyāṃ bhayād indrō dēvarājyaṃ  
>  ¹paryatyajad apsu sambhavāṃ ²ca ṡītalāṃ mānasasarōgatāṃ nalinīṃ ³pratipēdē  
> tatra caiṡvaryayōgād aṇumātrō bhūtvā bisagranthiṃ pravivēṡa /9.41/  

¹ all; CE "parityajya"  
² all; CE om.  
³ all; CE "prapēdē"

> atha brahmavadhyābhayapranaṣṭē trailōkyanāthē ṡacīpatau jagad anīṡvaraṃ babhūva  
> dēvān rajas tamaṡ cāvivēṡa  
> mantrā na prāvartanta maharṣīṇām  
> rakṣāṃsi prādurabhavan  
> brahma cōtsādanaṃ jagāma  
> anindrāṡ cābalā lōkāḥ supradhṛṣyā babhūvuḥ /9.42/  

> atha dēvā ṛṣayaṡ cāyuṣaḥ putraṃ nahuṣaṃ nāma ¹dēvarājyē’bhiṣiṣicuḥ  
> ²nahuṣaḥ pan̄cabhiḥ ṡatair jyōtiṣāṃ lalāṭē  
>  jvaladbhiḥ ³sarvabhūtatējōharais ⁴tripiṣṭapaṃ pālayāṃ babhūva /9.43/  

¹ B0,6,7,8 GP; B9 Da CE "dēvarājatvē’bhi-"  
² all; B8,9 "tatō nahu-"  
³ B0,6,7,9 Da3; B8 Da4 GP CE "sarvatējoharais-"  
⁴ B Da4; Da3 CE GP "triviṣṭapaṃ"

> atha lōkāḥ prakṛtim āpēdirē svasthāṡ ca ¹hṛṣṭāṡ ca babhūvuḥ /9.44/  

¹ all; CE om.

> athōvāca nahuṣaḥ  
> sarvaṃ māṃ ṡakrōpabhuktam upasthitam ṛtē ṡacīm iti  
> sa ēvam uktvā ṡacīsamīpam agamad uvāca cainām  
> subhagē’ham indrō dēvānāṃ bhajasva mām iti  
> taṃ ṡacī pratyuvāca  
> prakṛtyā tvaṃ dharmavatsalaḥ sōmavaṃṡōdbhavaṡ ca  
> nārhasi parapatnīdharṣaṇaṃ kartum iti /9.45/  

> tām athōvāca nahuṣaḥ  
> aindraṃ padam adhyāsyatē mayā  
> aham indrasya rājyaratnaharō nātrādharmaḥ kaṡcit tvam ¹indrabhuktēti  
> sā tam uvāca  
> asti mama kin̄cid vratam aparyavasitam  
> tasyāvabhṛthē tvām upagamiṣyāmi kaiṡ cid ēvāhōbhir iti  
> sa ²ṡacyaivam abhihitō ³◌ jagāma /9.46/  

¹ B6,8 Da CE; B0,7,9 GP "indrōpabhuk-"  
² ?B0,7,8 CE GP; B6,9 Da "ēvam"  
³ all; CE in. "nahuṣō"

> atha ṡacī duḥkhaṡōkārtā bhartṛdarṡanalālasā nahuṣabhayagṛhītā bṛhaspatim upāgacchat  
> sa ca tām ¹atyudvignāṃ dṛṣṭvaiva dhyānaṃ praviṡya bhartṛkāryatatparāṃ jn̄ātvā bṛhaspatir uvāca  
> anēnaiva vratēna tapasā cānvitā dēvīṃ varadām upaṡrutim āhvaya  
> ²sā ³tē indraṃ darṡayiṣyatīti /9.47/  

¹ all; CE "abhigatāṃ"  
² B6,9 Da CE; B0,7,8 GP "tadā sā -"  
³ all; CE "tavēndraṃ"

> sātha mahā¹niyamasthitā dēvīṃ varadām upaṡrutiṃ mantrair ⁰āhvayat  
> sōpaṡrutiḥ ṡacīsamīpam agāt  
> uvāca cainām iyam ²asmīti ³tvayāhūtōpasthitā  
> kiṃ tē priyaṃ karavāṇīti  
> tāṃ mūrdhnā praṇamyōvāca ṡacī bhagavaty arhasi mē bhartāraṃ darṡayituṃ tvaṃ satyā ⁴ṛtā cēti  
> saināṃ mānasaṃ sarō’nayat  
> tatrēndraṃ bisagranthigatam adarṡayat /9.48/  

¹ all; CE "-niyamam āsthitā"  
² B0,7,9 Da3 GP; B6,8 Da4 CE "asmi"  
³ all; CE "tvayōpahū-"  
⁴ B GP; Da "dhātā"; CE "matā"

> tām ¹atha patnīṃ kṛṡāṃ glānāṃ ²cēndrō dṛṣṭvā cintayāṃ babhūva  
> ahō mama ³◌ duḥkham idam upagatam  
> naṣṭaṃ hi mām iyam ⁵anviṣya yat patny abhyagamad duḥkhārtēti  
> tām indra uvāca kathaṃ ⁶vartayasīti  
> sā tam uvāca  
> nahuṣō mām ⁷āhvayati ⁸patnīm kartuṃ  
> kālaṡ cāsya mayā kṛta iti /9.49/  

¹ all; CE "indraḥ"  
² all; CE "ca"  
³ all; CE in. "mahad"  
⁴ B GP; Da CE "adyōpagatam"  
⁵ all; CE "anviṣyōpāgamad duḥ-"  
⁶ B0,7,8,9 CE GP; B6 Da "vartasīti"  
⁷ B0,7,8,9 CE GP; B6 Da "ārabhatē"  
⁸ all; CE om.

> tām indra uvāca  
> gaccha  
> nahuṣas tvayā vācyō’pūrvēṇa mām ṛṣiyuktēna yānēna tvam adhirūḍha udvahasva ¹iti  
> indrasya ²◌ mahānti vāhanāni manasaḥ ⁰priyāṇy adhirūḍhāni mayā  
> tvam anyēnōpayātum arhasīti  
> saivam uktā hṛṣṭā jagāma  
> indrō’pi bisagranthim ēvāvivēṡa bhūyaḥ /9.50/  

¹ all; CE om.  
² all; CE in. "hi"  

> athēndrāṇīm abhyāgatāṃ dṛṣṭvā ¹tām uvāca nahuṣaḥ pūrṇaḥ sa kāla iti  
> taṃ ṡacy abravīc chakrēṇa yathōktam  
> sa maharṣiyuktaṃ vāhanam adhirūḍhaḥ ṡacīsamīpam upāgacchat /9.51/  

¹ all; CE om.

> atha maitrāvaruṇiḥ kumbhayōnir agastyō ¹ṛṣivarō  
>  maharṣīn ²dhik kriyamāṇāṃs tān nahuṣēṇāpaṡyat  
> padbhyāṃ ca ³ca spṛṡyamānān  
> tataḥ sa nahuṣam abravīd akāryapravṛtta pāpa patasva mahīm  
> sarpō bhava yāvad bhūmir girayaṡ ca tiṣṭhēyus tāvad iti  
> sa maharṣivākyasamakālam ēva tasmād yānād avāpatat /9.52/  

¹ B0,7,8,9 Da3 GP; B6 Da4 CE om.  
² B0,7,9 Da GP; B6,8 CE "vikriya-"  
³ B6,7,9 Da; B0 GP "ca tamaspṛṡat"; B8 "cainān aspṛṡat"; CE "tēnāspṛṡyata"

> athānindraṃ punas trailōkyam abhavat  
> tatō dēvā ṛṣayaṡ ca bhagavantaṃ viṣṇuṃ ṡaraṇam indrārthē’bhijagmuḥ  
> ūcuṡ cainaṃ bhagavann indraṃ brahma¹hatyābhibhūtaṃ trātum arhasīti  
> tataḥ sa varadas tān abravīd aṡvamēdhaṃ yajn̄aṃ vaiṣṇavaṃ ṡakrō’bhi²yajatu  
> tataḥ ³svaṃ sthānaṃ prāpsyatīti /9.53/  

¹ all; CE "-vadhyā-"  
² ?B6 CE; B0 "-jayatāṃ" (sic); B7,8 GP "-yajatāṃ"; B9 "-yajata"; Da "-yajanta"  
³ ?B6,9 CE; B0,7 Da GP "svasthā-"

> tatō dēvā ṛṣayaṡ cēndraṃ nāpaṡyan yadā tadā ṡacīm ūcur gaccha subhagē indram ānayasvēti  
> sā punas tat saraḥ samabhyagacchat  
> indraṡ ca tasmāt sarasaḥ ⁰samutthāya bṛhaspatim abhijagāma  
> bṛhaspatiṡ cāṡvamēdhaṃ mahākratuṃ ṡakrāyāharat  
> ¹tatra kṛṣṇa²sāraṃ mēdhyam aṡvam utsṛjya vāhanaṃ tam ēva  
>  kṛtvā indraṃ marutpatiṃ bṛhaspatiḥ ³svaṃ sthānaṃ prāpayām āsa /9.54/  

¹ all; CE "tataḥ"  
² B6,7,9 Da; B8 "-sāramē-"; B0 GP CE "-sāraṅgaṃ mē-"  
³ B6,7,8,9 Da GP; B0 CE "svasthā-"

> tataḥ sa dēvarāḍ dēvair ṛṣibhiḥ stūyamānas triviṣṭapasthō niṣkalmaṣō babhūva  
> brahmavadhyāṃ caturṣu sthānēṣu vanitāgnivanaspatigōṣu vyabhajat  
> ēvam indrō brahmatējaḥprabhāvōpabṛṃhitaḥ ṡatruvadhaṃ kṛtvā ¹svaṃ sthānaṃ prāpitaḥ /9.55/  

¹ all; CE "svasthā-"

B6,8 end chapter 168 here.

> ākāṡagaṅgāgataṡ ca purā bharadvājō maharṣir ¹upāspṛṡat  
>  trīn ²kramān kramatā viṣṇunābhyāsāditaḥ  
> sa bharadvājēna sasalilēna pāṇinōrasi  
>  tāḍitaḥ salakṣaṇōraskaḥ saṃvṛttaḥ /9.56/  

¹ all; CE "upāspṛṡaṃs"  
² B0,6,7,9 CE GP; B8 Da "vikramān"

> bhṛguṇā maharṣiṇā ṡaptō’gniḥ sarvabhakṣatvam ¹upānītaḥ /9.57/  

¹ all; CE "upanītaḥ"

> aditir vai dēvānām annam apacad ētad bhuktvāsurān haniṣyantīti  
> tatra budhō vratacaryāsamāptāv āgacchat  
> aditiṃ ¹cāvōcad bhikṣāṃ dēhīti  
> tatra dēvaiḥ pūrvam ētat prāṡyaṃ nānyēnēty aditir bhikṣāṃ nādāt  
> atha bhikṣāpratyākhyānaruṣitēna budhēna ⁰brahmabhūtēna  
>   vivasvatō ²dvitīyajanmany aṇḍasan̄jn̄itasyāṇḍaṃ māritam adityāḥ  
> sa mārtaṇḍō vivasvān abhavac chrāddhadēvaḥ /9.58/  

¹ ?B8,9 CE GP; B6 Da "cāyācata" B0,7 "cāyācad"  
² B7,8,9 Da GP; B0 "janma dvitīyasya saṃjn̄itasyā-"; B6 CE "dvitīyē jan-"

> dakṣasya ¹◌ vai duhitaraḥ ṣaṣṭir āsan  
> tābhyaḥ kaṡyapāya trayōdaṡa prādād daṡa dharmāya daṡa manavē saptaviṃṡatim indavē  
> tāsu tulyāsu nakṣatrākhyāṃ gatāsu sōmō rōhiṇyām ²abhyadhikaṃ prītim ānabhūt  
> tatas tāḥ ⁰ṡēṣāḥ patnya īrṣyāvatyaḥ pituḥ samīpaṃ gatvēmam arthaṃ ṡaṡaṃsuḥ  
> bhagavann asmāsu tulyaprabhāvāsu sōmō rōhiṇīm ³abhyadhikaṃ bhajatīti  
> sō’bravīd yakṣmainam ⁴āvēkṣyatīti  
> dakṣaṡāpāt ⁵ca sōmaṃ rājānaṃ yakṣmāvivēṡa  
> sa yakṣmaṇāviṣṭō dakṣam ⁶upāgacchat  
> dakṣaṡ cainam abravīn na samaṃ ⁷vartayasīti  
> tatrarṣayaḥ sōmam abruvan kṣīyasē yakṣmaṇā  
> paṡcimasyāṃ diṡi samudrē hiraṇyasarastīrtham  
> tatra gatvātmānam abhiṣēcayasvēti  
> athāgacchat sōmas tatra hiraṇyasarastīrtham  
> gatvā ⁸cātmanaḥ ⁰snapanam akarōt  
> snātvā cātmānaṃ pāpmanō mōkṣayām āsa  
> tatra cāvabhāsitas tīrthē yadā sōmas tadāprabhṛti  
>  ⁹ca tīrthaṃ tat prabhāsam iti nāmnā khyātaṃ babhūva  
> tacchāpād adyāpi kṣīyatē sōmō’māvāsyāntarasthaḥ  
> paurṇamāsīmātrē’dhiṣṭhitō mēghalēkhāpraticchannaṃ vapur darṡayati  
> mēghasadṛṡaṃ varṇam agamat tad asya ṡaṡalakṣma vimalam abhavat /9.59/  

¹ B0,6,9 Da4 CE; B7 Da3 GP in. "yā"  
² all; CE "abhyadhikāṃ prītim akarōt"  
³ B6,7,9 Da4; B0 Da3 GP "pratyadhikaṃ"; CE "adhikaṃ"  
⁴ ?Da3 CE; B7 GP "āviṡyata iti"; B6 "āvēkṣyata iti"; B0 "āvikṣata iti";
  B9 "āvēkṣata iti"; B8 "bhajati iti" (sic); Da4 "āvēkṣati iti"  
⁵ B6,8,9 Da; B0,7 CE GP om.  
⁶ B6,7,9 Da4; Da3 GP "agāt"; B0,8 CE "agamat"  
⁷ all; CE "vartasa iti"  
⁸ ?B6,7,8 GP CE; B9 Da "tatrātmanaḥ"; B0 "cātmānaḥ"  
⁹ B0,6,8 Da GP; B7,9 CE om.

> sthūlaṡirā maharṣir mērōḥ prāguttarē ¹digvibhāgē tapas tēpē  
> ²tataḥ tasya tapas tapyamānasya sarvagandhavahaḥ  
>   ṡucir vāyur vivāyamānaḥ ṡarīram aspṛṡat  
> sa tapasā tāpitaṡarīraḥ kṛṡō vāyunōpavījyamānō ³hṛdayē paritōṣam agamat  
> tatra ⁴kila tasyānilavyajanakṛtaparitōṣasya sadyō vanaspatayaḥ puṣpaṡōbhāṃ   
>  ⁵nidarṡitavanta iti sa ētān̄ ṡaṡāpa na sarvakālaṃ puṣpavantō bhaviṣyathēti /9.60/  

¹ all; CE "digbhāgē"  
² B0,7,8 Da GP; B6,9 CE om.  
³ all; CE "hṛdayapari-"  
⁴ all; CE om.  
⁵ all; V CE "na darṡa-"

B8 ends chapter 266 here.

> nārāyaṇō lōkahitārthaṃ vaḍavāmukhō nāma ¹maharṣiḥ purā’babhūva  
> tasya mērau tapas tapyataḥ samudra āhūtō nāgataḥ  
> tēnāmarṣitēnātmagātrōṣmaṇā samudraḥ stimitajalaḥ ²kṛtaḥ  
> svēdaprasyandanasadṛṡaṡ cāsya lavaṇabhāvō janitaḥ  
> uktaṡ ³cāpyapēyō bhaviṣyasi  
> ētac ca ⁴tē tōyaṃ vaḍavāmukhasan̄jn̄itēna pīyamānaṃ madhuraṃ bhaviṣyati ⁵iti  
> tad ētad adyāpi vaḍavāmukhasan̄jn̄i⁶tēnānuvartinā tōyaṃ ⁷sāmudrāt pīyatē /9.61/  

¹ B Da; GP "purā maharṣirba-" (swap); CE "-purābhavat"  
² ?B0,8 Da3 CE GP; B6,7,9 Da4 om.  
³ all; CE "cāpēyō"  
⁴ B0,6,8,9 CE GP; B7 Da V "tvat tō-"  
⁵ all; CE om.  
⁶ B6,7,8,9 CE GP; B0 Da V "-tēnānivartinā"  
⁷ all; CE "samudraṃ"

> himavatō girēr duhitaram umāṃ ¹kanyāṃ rudraṡ cakamē  
> bhṛgur api ca maharṣir himavantam ⁰āgamyābravīt kanyām ²imāṃ mē dēhīti  
> tam abravīd dhimavān ⁰abhilaṣitō ³mē varō rudra iti  
> tam abravīd bhṛgur yasmāt tvayāhaṃ kanyāvaraṇakṛtabhāvaḥ  
>  pratyākhyātas tasmān na ratnānāṃ bhavān bhājanaṃ bhaviṣyatīti /9.62/  

¹ all; CE om.  
² all; CE "umāṃ"  
³ B0,9 Da V; B6,7,8 CE GP om.

> adyaprabhṛty ētad avasthitam ṛṣivacanam  
> tad ēvaṃvidhaṃ māhātmyaṃ brāhmaṇānām  
> kṣatram api ¹ca ṡāṡvatīm avyayāṃ ²ca pṛthivīṃ patnīm ³adhigamya bubhujē  
> tad ētad brahmāgnīṣōmīyam  
> tēna jagad dhāryatē /9.63/  

¹ B6,7,8 Da GP; B0,9 V CE om.  
² all; CE om.  
³ B0,7,9 Da4 V; B6,8 Da3 CE GP "abhigamya"

B6,8 CE end chapter here. CE 12.329; B6 169; B8 270; GP 65 paragraphs; MND 63 paragraphs.
B0,7,9 Da V GP MND om. colophon here and continue.

> ¹ucyatē  
> sūryācandramasau ²cakṣuḥ kēṡair mē aṃṡusan̄jn̄itaiḥ  
> bōdhayaṃs tāpayaṃṡ caiva jagad ³uttiṣṭhatē pṛthak /9.64/  

¹ B0,6,8,9 Da3 GP; B7 V Da4 "ucyatē ca"; CE "ṡrībhagavān uvāca"  
² all; CE "ṡaṡvat"  
³ all; CE "uttiṣṭhataḥ"

> bōdhanāt tāpanāc caiva jagatō harṣaṇaṃ bhavēt  
> agnīṣōmakṛtair ēbhiḥ karmabhiḥ pāṇḍunandana  
> hṛṣīkēṡō’ham īṡānō varadō lōkabhāvanaḥ /9.65/  

> ¹iḍōpahūtayōgēna harē bhāgaṃ kratuṣv aham  
> varṇaṡ ca mē hariṡrēṣṭhas tasmād dharir ahaṃ smṛtaḥ /9.66/  

¹ ?B8 Da4 CE; B0,7 "ilōpahūtaṃ yō-"; B6,9 Da3 "ilōpa-"; GP "iḻōpa-"

> dhāma sārō hi lōkānām ṛtaṃ caiva ¹avicāritam  
> ṛtadhāmā tatō vipraiḥ ²satyaṡ cāhaṃ prakīrtitaḥ /9.67/  

¹ B Da3 V; Da4 CE GP MND "vicāritam"  
² B0,9 Da Ca CE MND; B6,7,8 GP "sadyaṡ"

> naṣṭāṃ ca dharaṇīṃ pūrvam avindaṃ vai guhāgatām  
> gōvinda iti ¹tēnāhaṃ dēvair vāgbhir abhiṣṭutaḥ /9.68/  

¹ all; CE "māṃ dēvā vāgbhiḥ samabhituṣṭuvuḥ"

> ṡipiviṣṭēti cākhyāyāṃ hīnarōmā ca yō bhavēt    
> tēnāviṣṭaṃ ¹tu yat kin̄cic ²chipiviṣṭō hi sa smṛtaḥ /9.69/  

¹ all; CE "hi"  
² B6,9 Da; B0,8 CE GP "chipiviṣṭaṃ hi tat smṛtam"; B7 "-viṣṭēti ca smṛtaḥ"

> ¹yāskō mām ṛṣir avyagrō naikayajn̄ēṣu gītavān  
> ṡipiviṣṭa iti ²hy asmād guhyanāmadharō hy aham /9.70/  

¹ B7,8,9 CE GP; B0 Da V "yaskō" (sic); B6 "jaskō"  
² ?B0,8,9 CE GP; B6,7 Da "hyāṡu"

> stutvā māṃ ¹ṡipiviṣṭēti yāskō ṛṣir udāradhīḥ  
> matprasādād ¹adhō naṣṭaṃ niruktam abhijagmivān /9.71/  

¹ B7,8,9 CE GP; B0,6 V "ṡivipiṣṭa"  
² B0,6,8 CE GP; B7,9 Da4 "athō"

> na hi jātō na ¹jāyēmaṃ na janiṣyē kadā cana  
> kṣētrajn̄aḥ sarvabhūtānāṃ tasmād aham ajaḥ smṛtaḥ /9.72/  

¹ B0,7,8,9 GP; B6 Da CE "jāyē’haṃ"

> nōktapūrvaṃ mayā kṣudram aṡlīlaṃ vā kadācana  
> ṛtā brahmasutā sā mē satyā dēvī sarasvatī /9.73/  

> sac cāsac caiva kauntēya mayāvēṡitam ātmani  
> pauṣkarē brahmasadanē satyaṃ mām ṛṣayō viduḥ /9.74/  

> sattvān na cyutapūrvō’haṃ sattvaṃ vai viddhi matkṛtam  
> janma¹nīhā bhavēt sattvaṃ paurvikaṃ mē dhanan̄jaya /9.75/  

¹ ?B8,9 Da GP; B0,6,7 CE "nīhābhavat satt-"

> nirāṡīḥkarma¹saṃyuktaḥ ²sātvataṃ cāpy akalmaṣaṃ  
> sātvatajn̄ānadṛṣṭō’haṃ ³sātvatair iti sātvataḥ /9.76/  

¹ B0,6,8,9 GP; B7 Da V CE "saṃyuktaṃ"  
² B6,7 Da; B0,8,9 GP "sattvataṡ cāpy akalmaṣaḥ"; CE "sātvataṃ māṃ prakalpaya"  
³ B0,6,7 Da V; B8,9 GP "satvatām iti sātvataḥ"; CE "sātvataḥ sātvatāṃ patiḥ"

> kṛṣāmi mēdinīṃ pārtha bhūtvā kārṣṇāyasō mahān  
> kṛṣṇō varṇaṡ ca mē yasmāt tasmāt kṛṣṇō’ham arjuna /9.77/  

> mayā saṃṡlēṣitā bhūmir adbhir vyōma ca vāyunā  
> vāyuṡ ca tējasā sārdhaṃ vaikuṇṭhatvaṃ tatō ¹matam /9.78/  

¹ B6,7,9 Da; B0,8 CE GP "mama"

> nirvāṇaṃ paramaṃ ¹brahma dharmō’sau para ucyatē  
> tasmān na cyutapūrvō’ham acyutas tēna karmaṇā /9.79/  

¹ all; CE "saukhyaṃ"

> pṛthivīnabhasī cōbhē viṡrutē viṡva¹tōmukhē  
> tayōḥ sandhāraṇārthaṃ hi mām adhōkṣajam an̄jasā /9.80/  

¹ all; CE "-laukikē"

> niruktaṃ vēdaviduṣō ⁰yē ca ṡabdārthacintakāḥ  
> tē māṃ gāyanti prāgvaṃṡē adhōkṣaja iti sthitiḥ /9.81/  

> ṡabda ¹ēkapadair ²ēṣa vyāhṛtaḥ paramarṣibhiḥ  
> nānyō hy adhōkṣajō lōkē ṛtē nārāyaṇaṃ prabhum /9.82/  

¹ B0,8,9 Ca GP; B6,7 Da "ēkapadē"; CE "ēkamatair"  
² B0,8,9 CE GP; B6,7 Da "caiva"

> ghṛtaṃ mamārciṣō lōkē jantūnāṃ prāṇa¹dhāraṇē  
> ghṛtārcir aham avyagrair vēdajn̄aiḥ parikīrtitaḥ /9.83/  

¹ B7,9 Da V; B0,6,8 CE GP "-dhāraṇam"

> trayō hi dhātavaḥ khyātāḥ karmajā iti ¹yē smṛtāḥ  
> pittaṃ ṡlēṣmā ca vāyuṡ ca ēṣa saṅghāta ucyatē /9.84/  

¹ B Da; GP "tē"; CE "ca"

> ētaiḥ ¹tu dhāryatē jantur ētaiḥ kṣīṇaiṡ ²tu kṣīyatē  
> āyurvēdavidas tasmāt tridhātuṃ māṃ pracakṣatē /9.85/  

¹ B6,7,9 Da; B0,8 CE GP "ca"  
² B7,9 Da; B0,6,8 CE GP "ca"

> vṛṣō hi bhagavān dharmaḥ khyātō lōkēṣu bhārata  
> ¹naighaṇṭuka²padākhyānē viddhi māṃ vṛṣam uttamam /9.86/  

¹ ?B7,9 Da3 CE GP; B0 "nirghaṇṭuka-"; B6 "nairghaṇṭaka-"; B8 "nirghaṇṭaka"; Da4 "nidyuṇṭuka-"  
² B0,6,7,9 Da GP; B8 "-padākhyānair"; B6 (marg.) CE "-padākhyātaṃ"

> kapir varāhaḥ ṡrēṣṭhaṡ ca dharmaṡ ca vṛṣa ucyatē  
> tasmād vṛṣākapiṃ prāha kaṡyapō māṃ prajāpatiḥ /9.87/  

> na cādiṃ na madhyaṃ tathā ¹naiva cāntaṃ  
>  kadācid vidantē surāṡ cāsurāṡ ca  
> anādyō hy amadhyas tathā cāpy anantaḥ  
>  pragītō’ham īṡō vibhur lōkasākṣī /9.88/  

¹ B0,6 Da CE; B7,8,9 GP "caiva nāntaṃ" (swap)

> ṡucīni ṡravaṇīyāni ṡṛṇōmīha dhanan̄jaya  
> na ca pāpāni gṛhṇāmi tatō’haṃ vai ṡuciṡravāḥ /9.89/  

> ēkaṡṛṅgaḥ purā bhūtvā varāhō ¹nandivardhanaḥ  
> imām ²ca uddhṛtavān bhūmim ēkaṡṛṅgas tatō hy aham /9.90/  

¹ B0,7,8,9 GP; B6 Da V CE "divyadarṡanaḥ"  
² all; CE om.

> tathaivāsaṃ trikakudō vārāhaṃ rūpam āsthitaḥ  
> trikakut tēna vikhyātaḥ ṡarīrasya tu māpanāt /9.91/  

> virin̄ca iti ¹yat prōktaṃ ²kapilaṃ jn̄ānacintakaiḥ  
> sa prajāpatir ēvāhaṃ ³rēcanāt sarvalōkakṛt /9.92/  

¹ B0,6,7,9 Da GP; B8 "yatprōktaḥ"; CE "yaḥ prōktaḥ"  
² B0,7,9 Da GP; B6,8 "kāpila-"; CE "kapila-"  
³ B0,6,7,9; B8 "rētanāt"; Da CE GP "cētanāt"

> vidyāsahāyavantaṃ mām ādityasthaṃ sanātanam  
> kapilaṃ prāhur ācāryāḥ sāṅkhyā niṡcitaniṡcayāḥ /9.93/  

> hiraṇyagarbhō dyutimān ēṣa ¹yac chandasi stutaḥ  
> yōgaiḥ ²saṃyujyatē nityaṃ sa ēvāhaṃ ³bhuvi smṛtaḥ /9.94/  

¹ B0,7,9 Da3; B6,8 Da4 CE "yaṡ"; GP om.  
² B0,6,8 Da; B7,9 CE GP "sampūjyatē"  
³ B0,6,7,9 B8 (orig.) GP; B8 (marg.) V Da CE "vibhuḥ"

> ēkaviṃṡati⁰ṡākhaṃ ca ṛgvēdaṃ māṃ pracakṣatē  
> sahasraṡākhaṃ yat sāma yē vai vēdavidō janāḥ  
> gāyanty āraṇyakē viprā madbhaktās ¹tē’pi durlabhāḥ /9.95/  

¹ B6,9 Da4 CE; B0,7,8 Da3 GP "tē hi"

> ṣaṭpan̄cāṡatam aṣṭau ca saptatriṃṡatam ity uta  
> yasmin̄ ṡākhā yajurvēdē sō’ham ādhvaryavē smṛtaḥ /9.96/  

> pan̄cakalpam ¹tv atharvāṇaṃ kṛtyābhiḥ paribṛṃhitam  
> kalpayanti hi māṃ viprā atharvāṇavidas tathā /9.97/  

¹ B Da; V CE GP om.

> ṡākhābhēdāṡ ca yē kē cid yāṡ ca ṡākhāsu gītayaḥ  
> svaravarṇasamuccārāḥ sarvāṃs tān viddhi matkṛtān /9.98/  

> yat tad dhayaṡiraḥ pārtha samudēti varapradam  
> sō’ham ēvōttarē bhāgē kramākṣaravibhāgavit /9.99/  

> ¹vāmādēṡitamārgēṇa matprasādān mahātmanā  
> pān̄cālēna kramaḥ prāptas tasmād bhūtāt sanātanāt  
> bābhravyagōtraḥ sa babhau ²prathamaṃ kramapāragaḥ /9.100/  

¹ ?B GP; V Da CE "rāma-"  
² all; CE "prathamaḥ"

> nārāyaṇād varaṃ labdhvā prāpya yōgam anuttamam  
> kramaṃ praṇīya ṡikṣāṃ ca praṇayitvā sa gālavaḥ /9.101/  

> kaṇḍarīkō’tha rājā ca brahmadattaḥ pratāpavān  
> jātīmaraṇajaṃ duḥkhaṃ smṛtvā smṛtvā punaḥ punaḥ  
> saptajātiṣu mukhyatvād yōgānāṃ sampadaṃ gataḥ /9.102/  

> purāham ātmajaḥ pārtha prathitaḥ kāraṇāntarē  
> dharmasya kuruṡārdūla tatō’haṃ dharmajaḥ smṛtaḥ /9.103/  

> naranārāyaṇau pūrvaṃ tapas tēpatur avyayam  
> dharmayānaṃ samārūḍhau parvatē gandhamādanē /9.104/  

> tatkāla¹samayaṃ caiva dakṣayajn̄ō babhūva ha  
> na caivākalpayad bhāgaṃ dakṣō rudrasya bhārata /9.105/  

¹ B6,7,9 Da4 CE; B0,8 Da3 GP "-samayē"

> tatō ¹dadhīcivacanād dakṣayajn̄am apāharat  
> sasarja ṡūlaṃ ²krōdhēna prajvalantaṃ muhur muhuḥ /9.106/  

¹ B8 Da CE GP; B0,7,9 "dadhīca-"; B6 damaged  
² B9 Da CE; B0,7,8 GP "kōpēna"; B6 damaged

> tac chūlaṃ bhasmasāt kṛtvā dakṣayajn̄aṃ savistaram  
> āvayōḥ sahasāgacchad badaryāṡramam antikāt /9.107/  

> vēgēna mahatā pārtha patan nārāyaṇōrasi  
> tataḥ svatējasāviṣṭāḥ kēṡā nārāyaṇasya ha /9.108/  

> babhūvur mun̄javarṇās ¹tu tatō’haṃ mun̄jakēṡavān  
> tac ca ṡūlaṃ vinirdhūtaṃ huṅkārēṇa mahātmanā /9.109/  

¹ B0,7,8 CE GP; B9 Da "ca"; B6 damaged

> jagāma ṡaṅkarakaraṃ nārāyaṇasamāhatam  
> atha rudra upādhāvat tāv ṛṣī tapasānvitau /9.110/  

> tata ēnaṃ samuddhūtaṃ kaṇṭhē jagrāha pāṇinā  
> nārāyaṇaḥ sa viṡvātmā tēnāsya ṡitikaṇṭhatā /9.111/  

> atha rudravighātārtham iṣīkāṃ ¹nara uddharan  
> mantraiṡ ca saṃyuyōjāṡu sō’bhavat paraṡur mahān /9.112/  

¹ B0,6,8,9 Da GP; B7 CE "jagṛhē naraḥ"

> kṣiptaṡ ca sahasā ¹tēna khaṇḍanaṃ prāptavāṃs tadā  
> tatō’haṃ khaṇḍaparaṡuḥ smṛtaḥ paraṡukhaṇḍanāt /9.113/  

¹ all; CE "rudrē"

B8 ends chapter 271 here.

>    arjuna uvāca  
> ¹tasmin yuddhē tu vārṣṇēya trailōkya⁰mathanē tadā  
> ²kō jayaṃ prāptavāṃs tatra ṡaṃsaitan mē janārdana /9.114/  

¹ B6,8,9 Da V; B0,7 CE GP "asmin"  
² B0,6,7,9 Da GP; B8 "kaṃ jayaṃ"; CE "jayaṃ kaḥ" (swap)

>    ṡrībhagavān uvāca  
> tayōḥ saṃlagnayōr yuddhē rudranārāyaṇātmanōḥ  
> udvignāḥ sahasā kṛtsnā ¹sarvē lōkās tadā’bhavan /9.115/  

¹ all; CE "lōkāḥ sarvē’bhavaṃs tadā" (swap)

> nāgṛhṇāt pāvakaḥ ṡubhraṃ makhēṣu suhutaṃ haviḥ  
> vēdā na pratibhānti sma ṛṣīṇāṃ bhāvitātmanām /9.116/  

> dēvān rajas tamaṡ caiva samāviviṡatus tadā  
> vasudhā san̄cakampē ¹ca nabhaṡ ca vipaphāla ha /9.117/  

¹ B0,6,8,9 V GP; B7 Da CE "-’tha"

> ¹niṣprabhāni ca tējāṃsi brahmā caivāsanāc cyutaḥ  
> agāc chōṣaṃ samudraṡ ca himavāṃṡ ca vyaṡīryata /9.118/  

¹ B V; Da CE GP "niṣprabhāṇi"

> tasminn ēvaṃ samutpannē nimittē pāṇḍunandana  
> brahmā vṛtō dēvagaṇair ṛṣibhiṡ ca mahātmabhiḥ /9.119/  

> ājagāmāṡu taṃ dēṡaṃ yatra yuddham avartata  
> ⁰sān̄jalipragrahō bhūtvā caturvaktrō niruktagaḥ /9.120/  

> uvāca vacanaṃ rudraṃ lōkānām astu vai ṡivam  
> nyasyāyudhāni viṡvēṡa jagatō hitakāmyayā /9.121/  

> yad akṣaram athāvyaktam īṡaṃ lōkasya ¹bhārata  
> kūṭasthaṃ kartṛnirdvandvam akartēti ca yaṃ viduḥ /9.122/  

¹ B Da; CE GP "bhāvanam"

> vyaktibhāvagatasyāsya ēkā mūrtir iyaṃ ¹ṡubhā  
> narō nārāyaṇaṡ caiva jātau dharmakulōdvahau /9.123/  

¹ all; V CE "ṡivā"

> tapasā mahatā yuktau dēvaṡrēṣṭhau mahāvratau  
> ahaṃ prasādajas tasya kasmiṃṡ cit kāraṇāntarē /9.124/  

> tvaṃ caiva krōdhajas tāta pūrvasargē sanātanaḥ  
> mayā ca sārdhaṃ ⁰varadaṃ vibudhaiṡ ca maharṣibhiḥ /9.125/  

> prasādayāṡu lōkānāṃ ṡāntir bhavatu māciram  
> brahmaṇā tv ēvam uktas tu rudraḥ krōdhāgnim utsṛjan /9.126/  

After /9.126L1/ B6,8 in. "ṡrībhagavān uvāca"

> prasādayām āsa tatō dēvaṃ nārāyaṇaṃ prabhum  
> ṡaraṇaṃ ca jagāmādyaṃ varēṇyaṃ varadaṃ ¹vibhuṃ /9.127/  

¹ B0,6,7,9 Da; B8 GP "prabhuṃ"; CE "harim"

> tatō’tha varadō dēvō jitakrōdhō jitēndriyaḥ  
> prītimān abhavat tatra rudrēṇa saha saṅgataḥ /9.128/  

> ṛṣibhir brahmaṇā caiva vibudhaiṡ ca supūjitaḥ  
> uvāca dēvam īṡānam īṡaḥ sa jagatō hariḥ /9.129/  

> yas tvāṃ vētti sa māṃ vētti yas tvām anu sa mām anu  
> nāvayōr antaraṃ kin̄cin mā tē bhūd buddhir anyathā /9.130/  

Before /9.130/ B8 in. "harir uvāca"

> adya prabhṛti ṡrīvatsaḥ ṡūlāṅkō’yaṃ bhavatv ayam  
> mama pāṇyaṅkitaṡ cāpi ṡrīkaṇṭhas tvaṃ bhaviṣyasi /9.131/  

> ēvaṃ lakṣaṇam utpādya parasparakṛtaṃ tadā  
> sakhyaṃ caivātulaṃ kṛtvā rudrēṇa sahitāv ṛṣī /9.132/  

Before /9.132/ B0,8 GP in. "ṡrībhagavān uvāca"

> tapas tēpatur avyagrau visṛjya tridivaukasaḥ  
> ēṣa tē kathitaḥ pārtha nārāyaṇajayō mṛdhē /9.133/  

> nāmāni caiva guhyāni niruktāni ca bhārata  
> ṛṣibhiḥ kathitānīha yāni saṅkīrtitāni tē /9.134/  

> ēvaṃ bahuvidhai rūpaiṡ carāmīha vasundharām  
> brahmalōkaṃ ca kauntēya gōlōkaṃ ca sanātanam  
> mayā tvaṃ rakṣitō yuddhē mahāntaṃ prāptavān̄ jayam /9.135/  

> yas tu tē sō’gratō yāti yuddhē sampraty upasthitē  
> taṃ viddhi rudraṃ kauntēya dēvadēvaṃ kapardinam /9.136/  

> kālaḥ sa ēva kathitaḥ krōdhajēti mayā tava  
> ¹nihatās tēna vai pūrvaṃ hatavān asi ²yān ripūn /9.137/  

¹ B0,6,7,9 Da GP; B8 CE "nihatāṃs"  
² B0,6,7,9 Da GP; B8 CE "vai"

> apramēyaprabhāvaṃ taṃ dēvadēvam umāpatim  
> namasva dēvaṃ prayatō viṡvēṡaṃ ¹haram ²avyayam /9.138/  

¹ B0,7,8 Da4 CE GP; B6,9 Da3 "param"  
² B0,6,7 Da CE; B8,9 GP "akṣayam"

After /9.138/ B Da V GP in. CE@882 (/9.139/).

> ¹yaḥ sa tē kathitaḥ pūrvaṃ krōdhajēti punaḥ punaḥ  
> tasya ²prabhāva ³ēvāgrē yac chrutaṃ tē dhanan̄jaya /9.139/  

¹ B6,7,9 Da3 CE; B0,8 "yasya"; Da4 GP "yaṡ ca"  
² B6,7,8,9 Da V GP; B0 CE "prabhāvaṃ"  
³ B0,6,7,8 GP; B9 Da "ēvāyaṃ"; CE "ēvāgryaṃ"

Thus ends chapter B7 166; B8 268; AB 344; GP 342; MND 343

It has 139 units comprising (2 + 76 = 78) verses in (4 + 157 = 161) lines and 61 passages in 228 lines;
    GP 141 units comprising (2 + 76 = 78) verses in (4 + 157 = 161) lines and 63 passages in 228 lines;
    CE (50 + 71 = 121) units comprising (2 + 71 = 73) verses in (4 + 155 = 159) lines and 48 passages in 228 lines;
    Ku ??? verses, ??? lines.
