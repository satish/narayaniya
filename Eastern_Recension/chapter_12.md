# Chapter 12

>    vaiṡampāyana uvāca  
> kasya cit tv atha kālasya nāradaḥ paramēṣṭhijaḥ  
> daivaṃ kṛtvā yathānyāyaṃ pitryaṃ cakrē tataḥ param /12.1/  

> tatas taṃ vacanaṃ prāha jyēṣṭhō dharmātmajaḥ prabhuḥ  
> ka ijyatē dvijaṡrēṣṭha daivē pitryē ca kalpitē /12.2/  

> tvayā matimatāṃ ṡrēṣṭha tan mē ṡaṃsa yathāgamam  
> kim ētat kriyatē karma phalaṃ ⁰cāsya kim iṣyatē /12.3/  

>    nārada uvāca  
> tvayaitat kathitaṃ pūrvaṃ daivaṃ kartavyam ity api  
> daivataṃ ca parō yajn̄aḥ paramātmā sanātanaḥ /12.4/  

> tatas tadbhāvitō nityaṃ yajē vaikuṇṭham avyayam  
> tasmāc ca prasṛtaḥ pūrvaṃ brahmā lōkapitāmahaḥ /12.5/  

> mama vai pitaraṃ prītaḥ paramēṣṭhy apy ajījanat  
> ahaṃ saṅkalpajas tasya putraḥ prathamakalpitaḥ /12.6/  

> yajāmi ¹vai pitṝn sādhō nārāyaṇavidhau kṛtē  
> ēvaṃ sa ēva bhagavān pitā mātā pitāmahaḥ /12.7/  

¹ all; V CE "ahaṃ"

> ijyatē pitṛyajn̄ēṣu ¹tathā nityaṃ jagatpatiḥ  
> ṡrutiṡ cāpy aparā ²dēvī putrān hi pitarō’yajan /12.8/  

¹ all; V CE "mayā"  
² all; CE "dēva"

> vēdaṡrutiḥ ¹pranaṣṭā ca punar adhyāpitā sutaiḥ  
> tatas tē mantradāḥ putrāḥ pitṛtvam upapēdirē /12.9/  

¹ B0,7,8,9 Da; V "pranaṣṭāḥ"; B6 CE "praṇaṣṭā"

> nūnaṃ ¹puraitad viditaṃ yuvayōr bhāvitātmanōḥ  
> putrāṡ ca pitaraṡ caiva parasparam apūjayan /12.10/  

¹ B0,6,7 B8(marg.) Da3 CE; B9 "purē tad"; Da4 "puraistad"; GP "suraistad"

> trīn piṇḍān nyasya vai pṛthvyāṃ pūrvaṃ dattvā kuṡān iti  
> kathaṃ tu piṇḍasan̄jn̄āṃ tē pitarō lēbhirē purā /12.11/  

>    naranārāyaṇāv ūcatuḥ  
> imāṃ hi dharaṇīṃ pūrvaṃ naṣṭāṃ sāgaramēkhalām  
> gōvinda ujjahārāṡu vārāhaṃ rūpam āṡritaḥ /12.12/  

> sthāpayitvā tu dharaṇīṃ svē sthānē puruṣōttamaḥ  
> jalakardamaliptāṅgō lōkakāryārtham udyataḥ /12.13/  

> prāptē cāhnikakālē ¹tu ²madhyadēṡagatē ravau  
> daṃṣṭrāvilagnāṃs ¹trīnpiṇḍān vidhūya sahasā prabhuḥ /12.14/  

¹ B0,6,7,9 Da GP; B8 V "ca"; CE "sa"  
² all; V CE "madhyandina-"  
³ all; CE "mṛt-"

> sthāpayām āsa vai pṛthvyāṃ kuṡān āstīrya nārada  
> sa tēṣv ātmānam uddiṡya pitryaṃ cakrē yathāvidhi /12.15/  

> saṅkalpayitvā trīn piṇḍān svēnaiva vidhinā prabhuḥ  
> ātmagātrōṣmasambhūtaiḥ snēhagarbhais tilair api /12.16/  

> prōkṣyāpavargaṃ dēvēṡaḥ prāṅmukhaḥ kṛtavān svayam  
> maryādāsthāpanārthaṃ ca tatō vacanam uktavān /12.17/  

>    ¹vṛṡākapir uvāca  
> ahaṃ hi pitaraḥ sraṣṭum udyatō lōkakṛt svayam  
> ⁰tasya cintayataḥ sadyaḥ pitṛkārya²vidhīn parān /12.18/  

¹ all; CE om.  
² B0,6,7 Da GP; B8 "-vishīn purā"; B9 "-vidhān parān"; V CE "-vidhiṃ param"

> daṃṣṭrābhyāṃ pravinirdhūtā ¹mamaitē dakṣiṇāṃ diṡam  
> āṡritā dharaṇīṃ ²pīḍya tasmāt pitara ēva tē /12.19/  

¹ B0,7,9 Da4 V CE GP; B6 B8(marg.) Da3 "mṛtpiṇḍā"  
² B Da3; Da4 V CE GP "piṇḍās"

> trayō mūrtivihīnā vai ¹catvāras tu samūrtayaḥ  
> bhavantu pitarō lōkē mayā sṛṣṭāḥ sanātanāḥ /12.20/  

¹ B6,7,9 Da V; B0,8 CE GP "piṇḍamūrtidharās tv imē"

> pitā pitāmahaṡ caiva tathaiva prapitāmahaḥ  
> aham ēvātra vijn̄ēyas triṣu piṇḍēṣu saṃsthitaḥ /12.21/  

> nāsti mattō’dhikaḥ kaṡcit kō ¹vānyō’rcyō mayā svayam  
> kō vā mama pitā lōkē aham ēva pitāmahaḥ /12.22/  

¹ all; CE "vābhyarcyō"

> pitāmahapitā caiva aham ēvātra kāraṇam  
> ity ¹ētad uktvā vacanaṃ dēvadēvō vṛṣākapiḥ /12.23/  

¹ B0,6,7,9 Da V GP; B8 CE "ēvam"

Before /12.23L2/ B8 in. "naranārāyaṇāv ūcatuḥ".

> ¹vārāhaparvatē vipra dattvā piṇḍān savistarān  
> ātmānaṃ pūjayitvaiva tatraivādarṡanaṃ gataḥ /12.24/  

¹ ?B6,9 Da V; B0,7,8 CE GP "varāha-"

> ¹ēṣā tasya sthitir vipra pitaraḥ piṇḍasan̄jn̄itāḥ  
> labhantē satataṃ pūjāṃ vṛṣākapivacō yathā /12.25/  

¹ all; CE "ētadarthaṃ ṡubhamatē"

> yē yajanti pitṝn dēvān gurūṃṡ caivātithīṃs tathā  
> gāṡ caiva dvijamukhyāṃṡ ca pṛthivīṃ mātaraṃ ⁰tathā /12.26/  

> karmaṇā manasā vācā viṣṇum ēva yajanti tē  
> antargataḥ sa bhagavān sarvasattvaṡarīragaḥ /12.27/  

> samaḥ sarvēṣu bhūtēṣu īṡvaraḥ sukhaduḥkhayōḥ  
> mahān mahātmā sarvātmā nārāyaṇa iti ṡrutaḥ /12.28/  

Thus ends chapter B7 170; B8 275; AB 347; GP 345; MND 346

It has 28 verses, 56 lines;
    CE 25 verses, 56 lines;
    GP 28 verses, 56 lines;
    Ku 27 verses, 56 lines.
