# Chapter 1  

>    yudhiṣṭhira uvāca  
> gṛhasthō brahmacārī vā vānaprasthōʼtha bhikṣukaḥ  
> ya icchēt siddhimāsthātuṃ dēvatāṃ kāṃ yajēta saḥ /1.1/  

> kutō hyasya dhruvaḥ svargaḥ kutō niḥṡrēyasaṃ param  
> vidhinā kēna juhuyād daivaṃ pitryam ¹athāpi ca /1.2/  

¹ B6-9 Da; B0 CE "tathaiva"

> muktaṡca ¹kāṃ gatiṃ gacchēn mōkṣaṡcaiva kimātmakaḥ  
> svargataṡcaiva kiṃ kuryād yēna na cyavatē divaḥ /1.3/  

¹ B CE; Da "saṅgatiṃ"

> dēvatānāṃ ca kō dēvaḥ pitṝṇāṃ ca ¹pitā tathā  
> tasmāt parataraṃ yacca tanmē brūhi pitāmaha /1.4/  

¹ B Da GP Ku; CE "tathā pitā"  

>    bhīṣma uvāca  
> gūḍhaṃ māṃ praṡnavit praṡnaṃ ¹pṛcchasi tvamihānagha  
> na ²hy ētat tarkayā ³ṡakyō vaktuṃ varṣaṡatairapi /1.5/  

¹ B6,7,9 Da; B0 CE "pṛcchasē"  
² B0,6,8,9 Da GP Ku; CE "hy ēṣa"   
³ B6,7,8 Da CE GP Ku; B0,9 "ṡakyaṃ"  

*tarkayā* should probably read *tarkēṇa* because *tarka* is masculine and the
former is a feminine form. Sastri, therefore, renders it as *anyathā ṡakyaṃ*

> ṛtē dēvaprasādād vā rājan̄ jn̄ānāgamēna vā  
> gahanaṃ hyētadākhyānaṃ vyākhyātavyaṃ tavārihan /1.6/  

> atrāpyudāharantīmam itihāsaṃ purātanam  
> nāradasya ca saṃvādam ṛṣērnārāyaṇasya ca /1.7/  

> nārāyaṇō hi viṡvātmā caturmūrtiḥ sanātanaḥ  
> dharmātmajaḥ sambabhūva pitaivaṃ mēʼbhyabhāṣata /1.8/  

> kṛtē yugē mahārāja purā svāyambhuvēʼntarē  
> narō nārāyaṇaṡcaiva hariḥ kṛṣṇas ¹svayambhuvaḥ /1.9/  

¹ B Da; CE "tathaiva ca"

> ¹teṣāṃ nārāyaṇanarau tapastēpaturavyayau  
> badaryāṡramamāsādya ṡakaṭē kanakāmayē /1.10/  

¹ B Da GP Ku; CE "tebhyō"

> aṣṭacakraṃ hi tadyānaṃ bhūtayuktaṃ manōramam  
> ¹yatrādau lōkanāthau tau kṛṡau dhamanisantatau /1.11/  

¹ B0,8,9 Da; CE "tatrā-"

> tapasā tējasā caiva ¹durnirīkṣyau surairapi  
> yasya prasādaṃ kurvātē sa dēvau draṣṭumarhati /1.12/  

¹ B6-9 Da3 GP Ku; B0 Da4 CE "durnirīkṣau"

> nūnaṃ tayōranumatē hṛdi hṛcchayacōditaḥ  
> mahāmērōḥ girēḥ ṡṛṅgāt pracyutō gandhamādanam /1.13/  

> nāradaḥ sumahadbhūtaṃ ¹sarvalōkān acīcarat  
> taṃ dēṡamagamadrājan badaryāṡramamāṡugaḥ /1.14/  

¹ B Da GP Ku; CE "lōkān sarvān-"  

> tayōrāhnikavēlāyāṃ tasya kautūhalaṃ tvabhūt  
> ¹idaṃ tadāspadaṃ kṛtsnaṃ yasmil̐lōkāḥ pratiṣṭhitāḥ /1.15/  

¹ B CE; Da "imaṃ"

> sadēvāsuragandharvāḥ ¹sakinnaramahōragāḥ  
> ēkā mūrtiriyaṃ pūrvaṃ jātā bhūyaṡcaturvidhā /1.16/  

¹ B Da GP Ku; CE "saṛṣikiṃnaralelihāḥ"

> dharmasya kulasantānē ¹dharmādēbhir vivardhitaḥ  
> ahō hyanugṛhītōʼdya dharma ēbhiḥ surairiha  
> naranārāyaṇābhyāṃ ca kṛṣēṇa hariṇā tathā /1.17/  

¹ B6,7,9 Da GP Ku; B0 "dharmādibhir"; B8 CE "mahānebhir"

> ¹atra kṛṣṇō hariṡcaiva kasminṡcit kāraṇāntarē  
> sthitau ²dharmōttarau hy ētau tathā tapasi ³viṣṭhitau /1.18/  

¹ B Da GP Ku; CE "tatra"  
² B0,8,9 Da CE; B6,7 "-ttamau"  
³ B Da; CE "dhiṣṭhitau"

> ētau hi paramaṃ dhāma kānayōr ¹āhnikakriyā  
> pitarau sarvabhūtānāṃ daivataṃ ca yaṡasvinau  
> kāṃ dēvatāṃ ²tu yajataḥ pitṝn va kān mahāmatī /1.19/  

¹ B CE; Da "āhnikī-"  
² B0,7-9 Da; B6 "ca"; CE "nu"  

> iti san̄cintya manasā bhaktyā nārāyaṇasya ¹tu  
> sahasā prādurabhavat samīpē dēvayōstadā /1.20/  

¹ B GP Ku; Da "ca"; CE "ha"

> kṛtē daivē ca pitryē ca tatastābhyāṃ nirīkṣitaḥ  
> pūjitaṡcaiva vidhinā yathāprōktēna ṡāstrataḥ /1.21/  

> ¹tad dṛṣṭvā mahadāṡcaryam ²apūrvaṃ vidhivistaram  
> upōpaviṣṭaḥ suprītō nāradō bhagavānṛṣiḥ /1.22/  

¹ B6-9 Da3 GP Ku; B0 Da4 CE "taṃ"  
² B6,8 Da CE; B0,7,9 "apūrva"

> nārāyaṇaṃ saṃnirīkṣya prasannēnāntarātmanā  
> namaskṛtvā mahādēvam idaṃ vacanamabravīt /1.23/  

>    nārada uvāca  
> vēdēṣu sapurāṇēṣu sāṅgōpāṅgēṣu gīyasē  
> tvamajaḥ ṡāṡvatō dhātā ¹matō’mṛtam anuttamam  
> pratiṣṭhitam bhūtabhavyaṃ tvayi sarvamidaṃ jagat /1.24/  

¹ ?B0,6,7,9 CE; B8 Da GP Ku MND "mātā’mṛtam"

Before /1.24/ B Da GP in. "nārada uvāca" but CE om.

> catvārō hyāṡramā dēvā sarvē gārhasthamūlakāḥ  
> yajantē tvāmaharar nānāmūrtisamāsthitam /1.25/  

> pitā mātā ca sarvasya jagataḥ ṡāṡvatō guruh  
> kaṃ tvadya yajasē dēvaṃ pitaraṃ kaṃ na vidmahē /1.26/  

>    ṡrībhagavān uvāca  
> avācyamētad vaktavyam ātmaguhyaṃ sanātanam  
> tava bhaktimatō brahman vakṣyāmi tu yathātatham /1.27/  

> yat tat sūkṣmamavijn̄ēyam avyaktamacalaṃ dhruvam  
> indriyairindriyārthaiṡca sarvabhūtaiṡca varjitam /1.28/  

> sa hyantarātmā bhūtānāṃ kṣētrajn̄aṡcēti kathyatē  
> triguṇavyatiriktōʼsau puruṣaṡcēti kalpitaḥ /1.29/  

> tasmādavyaktamutpannaṃ triguṇaṃ dvijasattama  
> avyaktāvyaktabhāvasthā yā sā prakṛtiravyayā /1.30/  

> tāṃ yōnimāvayōrvṛddhi yōʼsau sadasadātmakaḥ  
> āvābhyāṃ ¹pūjyatē sō hi daivē pitryē ca ²kalpitē /1.31/  

¹ ?B6-9; B0 Da CE GP "pūjyatē’sau"  
² ?B0 Da4 CE; B8,9 GP "kalpyatē"; B6,7 Da3 "kalpatē"

> nāsti tasmāt parōʼnyō hi pitā dēvō’tha vā ¹dvija  
> ātmā hi nau sa vijn̄ēyaḥ tatastaṃ pūjayāvahē /1.32/  

¹ B GP Ku; Da CE "dvijaḥ"  

> tēnaiva sthāpitā brahman maryādā lōkabhāvinī  
> daivaṃ pitryaṃ ca kartavyam iti tasyānuṡāsanam /1.33/  

> brahmā sthāṇurmanurdakṣō bhṛgurdharmas ¹tathā yamaḥ  
> marīcir aṅgirāṡcātriḥ pulastyaḥ pulahaḥ kratuḥ /1.34/  

¹ B0,6,9 Da; B7 "tathā mayaḥ" (sic); B8 "tathā damaḥ"; CE "tapō damaḥ"

> vasiṣṭhaḥ paramēṣṭhī ca vivasvān sōma ēva ca  
> kardamaṡcāpi yaḥ prōktaḥ krōdhō vikrīta ēva ca /1.35/  

vikṛti and vikrīta are the same (as per Monier Williams).
*krōdhō vikrīta* is wrong because krōdha is not mentioned as Prajāpati anywhere.

*ēkatō dvita eva ca* is wrong because what about Trita? He's also one of the
 three Āptyas.

However, Kṛta is one of the Viśvēdēvas, also name of father of Uparicara, etc.

> ēkaviṃṡatirutpannāḥ tē prajāpatayaḥ smṛtāḥ  
> tasya dēvasya maryādāṃ ¹pūjayantaḥ sanātanīm /1.36/  

¹ B Da3; Ba4 CE "pūjayanti"

> daivaṃ pitryaṃ ca satataṃ tasya vijn̄āya tattvataḥ  
> ātmaprāptāni ca tatō jānanti dvijasattamāḥ /1.37/  

> svargasthā api yē kēcit ¹taṃ namasyanti dēhinaḥ  
> tē tatprasādād gacchanti tēnādiṣṭaphalāṃ gatim /1.38/  

¹ B6,8 Da CE; B0,7,9 GP "tān"

> yē hīnāḥ saptadaṡabhir guṇaiḥ karmabhirēva ca  
> kalāḥ pan̄cadaṡa ¹tyaktās tē muktā iti niṡcayaḥ /1.39/  

¹ ?B6,7,8 Da3; B0 "prōktās"; B9 Da4 CE GP "tyaktvā"

> muktānāṃ tu gatirbrahman kṣētrajn̄a iti ¹kalpitaḥ  
> sa hi sarva¹gataṡcaiva nirguṇaṡcaiva kathyatē /1.40/  

¹ B6,7,9 Da4 CE; B0,8 "kalpitāḥ"; Da4 GP "kalpitā"  
¹ B0,6,9 Da3 CE; B7,8 "-gatiṡ-"; Da4 GP "-guṇaṡ-"

> dṛṡyatē jn̄ānayōgēna āvāṃ ca prasṛtau tataḥ  
> ēvaṃ jn̄ātvā tamātmānaṃ pūjayāvaḥ sanātanam /1.41/  

> taṃ vēdāṡcāṡramāṡcaiva nānā¹matasamāsthitāḥ  
> bhaktyā sampūjayantyādyaṃ gatiṃ caiṣāṃ dadāti saḥ /1.42/  

¹ B Da GP; CE "-tanusa-"

> yē tu tadbhāvitā lōkē ¹ēkāntitvaṃ samāṡritāḥ  
> ētad abhyadhikaṃ tēṣāṃ yat tē taṃ praviṡantyuta /1.43/  

¹ B6,7,9 Da4 CE; B0,8 Da3 GP Ku "hy ēkāntitvaṃ"

> iti guhyasamuddēṡaḥ tava nārada kīrtitaḥ  
> bhaktyā prēmṇā ca viprarṣē asmad bhaktyā ca tē ṡrutaḥ /1.44/  

Thus ends chapter B7 155; B8 261.

It has 44 verses, 91 lines;
    GP 45 verses, 91 lines;
    CE 43 verses, 91 lines;
    Ku 46 verses, ?? lines
