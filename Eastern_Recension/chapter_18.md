# Chapter 18

>     brahmōvāca  
> ṡṛṇu putra yathā hy ēṣa puruṣaḥ ṡāṡvatō’vyayaḥ  
> akṣayaṡ cāpramēyaṡ ca sarvagaṡ ca nirucyatē /18.1/  

> na sa ṡakyas tvayā draṣṭuṃ mayānyair vāpi sattama  
> ¹saguṇair ²nirguṇō viṡvō jn̄ānadṛṡyō hy asau smṛtaḥ /18.2/  

¹ B0,6,7,8 Da GP; B9 V CE "saguṇō"  
² B0,9 Da CE; B6,7,8 GP "nirguṇair"

> aṡarīraḥ ṡarīrēṣu sarvēṣu nivasaty asau  
> vasann api ṡarīrēṣu na sa lipyati karmabhiḥ /18.3/  

> mamāntarātmā tava ca yē cānyē dēhasan̄jn̄itāḥ  
> sarvēṣāṃ sākṣibhūtō’sau na grāhyaḥ kēna cit kvacit /18.4/  

> viṡvamūrdhā viṡvabhujō viṡvapādākṣināsikaḥ  
> ēkaṡcarati kṣētrēṣu svairacārī yathāsukham /18.5/  

> kṣētrāṇi hi ṡarīrāṇi ¹bījaṃ cāpi ²ṡubhāṡubham  
> tāni vētti sa yōgātmā tataḥ kṣētrajn̄a ucyatē /18.6/  

¹ all; V CE "bījāni ca"  
² all; CE "ṡubhāṡubhē"

> nāgatir na gatis tasya jn̄ēyā ¹bhūtēṣu kēna cit  
> sāṅkhyēna vidhinā caiva yōgēna ca yathākramam /18.7/  

¹ all; CE "bhūtēna"

> cintayāmi gatiṃ cāsya na gatiṃ vēdmi ¹cōttarām  
> yathājn̄ānaṃ tu vakṣyāmi puruṣaṃ taṃ sanātanam /18.8/  

¹ all; V CE "cōttamām"

> tasyaikatvaṃ mahattvaṃ ¹hi sa caikaḥ puruṣaḥ smṛtaḥ  
> mahāpuruṣaṡabdaṃ sa bibharty ēkaḥ sanātanaḥ /18.9/  

¹ B0,6,8 V CE; B7,9 GP "ca"; Da "vai"

> ēkō hutāṡō bahudhā ¹samidhyatē  
>  ēkaḥ sūryas ²tapasō yōnir ēkā  
> ēkō vāyur bahudhā vāti lōkē  
>  mahōdadhiṡ cāmbhasāṃ yōnir ēkaḥ  
> puruṣaṡ caikō nirguṇō viṡvarūpas  
>  taṃ nirguṇaṃ puruṣaṃ cāviṡanti /18.10/  

¹ B0,9 Da V CE GP; B6,7,8 "samiddha"  
² B6,7,8,9 Da GP; B0 "tapatē"; V "tējasō"; CE "tapasāṃ"

> hitvā guṇamayaṃ sarvaṃ karma hitvā ṡubhāṡubham  
> ubhē satyānṛtē tyaktvā ēvaṃ bhavati nirguṇaḥ /18.11/  

> acintyaṃ cāpi taṃ jn̄ātvā bhāvasūkṣmaṃ catuṣṭayam  
> vicarēd yō ⁰yatir yattaḥ sa gacchēt puruṣaṃ ⁰prabhum /18.12/  

> ¹ēkaṃ hi paramātmānaṃ kē cid icchanti paṇḍitāḥ  
> ēkātmānaṃ tathātmānam ²aparē jn̄ānacintakāḥ /18.13/  

¹ all; V CE "ēvaṃ"  
² B6,7,8,9 Da GP; B0 V "aparē dhyāna-"; CE "aparē’dhyātma-"

> tatra yaḥ paramātmā hi sa nityaṃ nirguṇaḥ smṛtaḥ  
> sa hi nārāyaṇō jn̄ēyaḥ sarvātmā puruṣō hi saḥ /18.14/  

> na lipyatē phalaiṡ cāpi padmapatram ivāmbhasā  
> karmātmā tv aparō yō’sau mōkṣabandhaiḥ sa yujyatē /18.15/  

> sasaptadaṡakēnāpi rāṡinā yujyatē ¹ca saḥ  
> ēvaṃ bahuvidhaḥ prōktaḥ puruṣas tē yathākramam /18.16/  

¹ B V GP; Da CE "hi"

> yat tat kṛtsnaṃ lōkatantrasya dhāma  
>  vēdyaṃ paraṃ bōdhanīyaḥ ¹sa bōddhā  
> mantā mantavyaṃ ²rasitā rasanīyaṃ  
>  ghrātā ghrēyaṃ sparṡitā sparṡanīyam /18.17/  

¹ all; CE "-yaṃ sabōddhṛ"  
² B6,7,8,9 V; B0 "prāsitā prāsanīyaṃ"; Da "ṡrāvitā ṡrāvaṇīyaṃ"; GP CE "prāṡitā prāṡitavyaṃ"

> draṣṭā draṣṭavyaṃ ṡrāvitā ṡrāvaṇīyaṃ  
>  jn̄ātā jn̄ēyaṃ saguṇaṃ nirguṇaṃ ca  
> yad vai prōktaṃ ¹guṇasampat pradhānaṃ  
>  nityaṃ caitac chāṡvataṃ cāvyayaṃ ca /18.18/  

¹ B6,7,8,9 Da; B0 GP "tāta samyak"; V CE "guṇasāmyaṃ"

> yad vai sūtē ¹dhattē cādyaṃ ²vidhānaṃ  
>  tad vai viprāḥ pravadantē’niruddham  
> yad vai lōkē vaidikaṃ karma sādhu  
>  āṡīryuktaṃ tad dhi ³tasyaiva bhāvyam /18.19/  

¹ B0,8 Da; B6,7,9 "dhatticādyaṃ"; CE GP "dhātur ādyaṃ"  
² B Da GP; CE "nidhānaṃ"  
³ all; CE "tasyōpabhōjyam"

> dēvāḥ sarvē munayaḥ sādhu ⁰dāntās  
>  taṃ prāg ⁰yajn̄air yajn̄abhāgaṃ ⁰yajantē  
> ahaṃ brahmā ādya īṡaḥ prajānāṃ  
>  tasmāj jātas tvaṃ ca mattaḥ prasūtaḥ  
> mattō jagaj jaṅgamaṃ sthāvaraṃ ca  
>  sarvē vēdāḥ sarahasyā hi putra /18.20/  

> caturvibhaktaḥ puruṣaḥ sa krīḍati yathēcchati  
> ēvaṃ sa ¹bhagavān svēna jn̄ānēna pratibōdhitaḥ /18.21/  

¹ all; CE "ēva bhagavān̄"

> ētat tē kathitaṃ putra yathāvad anupṛcchataḥ  
> sāṅkhyajn̄ānē tathā yōgē yathāvad anuvarṇitam /18.22/  

Thus ends chapter B7 176; B8 278; AB 353; GP 351; MND 352

It has 22 verses, 53 lines;
    CE 21 verses, 53 lines;
    GP 23 verses, 53 lines;
    Ku 23 verses, 53 lines.
