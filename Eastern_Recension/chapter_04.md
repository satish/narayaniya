# Chapter 04

>    yudhiṣṭhira uvāca  
> yadā ¹bhāgavatō’tyartham āsīd rājā mahāvasuḥ  
> kimarthaṃ sa paribhraṣṭō vivēṡa vivaraṃ bhuvaḥ /4.1/  

¹ all but CE "bhaktō bhagavata"

>    bhīṣma uvāca  
> atrāpy udāharantīmam itihāsaṃ purātanam  
> ṛṣīṇāṃ caiva saṃvādaṃ tridaṡānāṃ ca bhārata /4.2/  

> ajēna yaṣṭavyam iti ¹dēvāḥ prāhur dvijōttamān  
> sa ca ²chāgō’py ajō jn̄ēyō nānyaḥ paṡur iti sthitiḥ /4.3/  

¹ B6,7,9 Da CE; B0,8 AB GP "prahur dēvāḥ" (swap)  
² B0,6,7 Da AB GP; B8,9 CE "chāgō hy"  

>    ṛṣaya ūcuḥ  
> bījair yajn̄ēṣu yaṣṭavyam iti vai vaidikī ṡrutiḥ  
> ajasan̄jn̄āni bījāni chāgaṃ na ²hantum arhatha /4.4/  

¹ B8,9 Da; B0,6,7 AB GP "nō"  
² all but CE "ghnantum"

> naiṣa dharmaḥ satāṃ dēvā yatra vadhyēta vai paṡuḥ  
> idaṃ kṛtayugaṃ ṡrēṣṭhaṃ kathaṃ vadhyēta vai paṡuḥ /4.5/  

>    bhīṣma uvāca  
> tēṣāṃ saṃvadatām ēvam ṛṣīṇāṃ vibudhaiḥ saha  
> mārgāgatō nṛpaṡrēṣṭhas taṃ dēṡaṃ prāptavān vasuḥ /4.6/  

> ¹antarīkṣacaraḥ ṡrīmān samagrabalavāhanaḥ  
> taṃ dṛṣṭvā sahasāyāntaṃ vasuṃ tē tv ²antarīkṣagam /4.7/  

¹ B0,7,8,9 Da AB; B6 GP CE "antari-"  
² B0,8,9 AB; B6,7 Da CE GP "antari-"

Note that in both places it must match (either "antari-" or "antarī-").

> ūcur dvijātayō dēvān ēṣa chētsyati saṃṡayam  
> yajvā dānapatiḥ ṡrēṣṭhaḥ sarvabhūtahitapriyaḥ  
> kathaṃ svid anyathā brūyād ¹ēṣa vākyaṃ mahān vasuḥ /4.8/  

¹ all but CE "vākyam ēṣa"

/4.8/ is spoken by the dēvas and ṛṣis among themselves.

> ēvaṃ tē saṃvidaṃ kṛtvā vibudhā ṛṣayas tathā  
> apṛcchan ¹sahasābhyētya vasuṃ rājānam antikāt /4.9/  

¹ ?B7 AB CE; B0,6,8,9 GP "sahitābhyētya"; Da "sahasā prētya"

Before /4.9/ B9 in. "bhīṣma uvāca".

> bhō rājan kēna yaṣṭavyam ajēnāhōsvid auṣadhaiḥ  
> ētan naḥ saṃṡayaṃ chindhi pramāṇaṃ nō bhavān mataḥ /4.10/  

Before /4.10/ B8 in. "ṛṣaya ūcuḥ".

> sa tān kṛtān̄jalir bhūtvā paripapraccha vai vasuḥ  
> kasya ¹vaḥ kō mataḥ ²kāmō brūta satyaṃ ³dvijōttamāḥ /4.11/  

¹ B0,6,7,9 CE; B8 Da AB GP "vai"  
² all but CE "pakṣō"  
³ all but CE "samāgatāḥ"  

Before /4.11/ B6,8,9 in. "bhīṣma uvāca".

>    ṛṣaya ūcuḥ  
> ¹bījair yaṣṭavyam ity ²ēṣa pakṣō’smākaṃ narādhipa  
> dēvānāṃ tu paṡuḥ pakṣō matō rājan vadasva naḥ /4.12/  

¹ ?B6,7,8 Da4 (marg.); B0,9 Da3 CE AB GP "dhānyair"  
² all but AB GP "ēva"

>    bhīṣma uvāca  
> dēvānāṃ tu mataṃ jn̄ātvā vasunā pakṣasaṃṡrayāt  
> chāgēnājēna yaṣṭavyam ēvam uktaṃ vacas tadā /4.13/  

> kupitās tē tataḥ sarvē munayaḥ sūryavarcasaḥ  
> ūcur vasuṃ vimānasthaṃ dēvapakṣārthavādinam /4.14/  

> surapakṣō gṛhītas tē yasmāt tasmād divaḥ pata  
> adya prabhṛti tē rājann ākāṡē vihatā gatiḥ  
> asmacchāpābhighātēna mahīṃ bhittvā pravēkṣyasi /4.15/  

Before /4.15/ B9 in. "ṛṣaya ūcuḥ".

> tatas tasmin muhūrtē’tha rājōparicaras tadā  
> adhō vai saṃbabhūvāṡu bhūmēr vivaragō nṛpaḥ  
> smṛtis tv ēnaṃ na ¹prajahau tadā nārāyaṇājn̄ayā /4.16/  

¹ all but AB GP "hi jahau"

Before /4.16/ B9 in. "bhīṣma uvāca".

> dēvās tu sahitāḥ sarvē vasōḥ ṡāpavimōkṣaṇam  
> cintayām āsur avyagrāḥ sukṛtaṃ hi nṛpasya tat /4.17/  

> anēnāsmatkṛtē rājn̄ā ṡāpaḥ prāptō mahātmanā  
> asaya pratipriyaṃ kāryaṃ sahitair nō divaukasaḥ /4.18/  

> iti buddhyā vyavasyāṡu gatvā niṡcayam īṡvarāḥ  
> ūcus ¹taṃ hṛṣṭamanasō rājōparicaraṃ tadā /4.19/  

¹ all but AB GP "saṃhṛṣṭa-"

> ¹brahmaṇyadēvaṃ tvaṃ bhaktaḥ surāsuraguruṃ harim  
> kāmaṃ sa tava tuṣṭātmā kuryāc chāpavimōkṣaṇam /4.20/  

¹ all but AB GP "brahmaṇyadēvabhaktas tvaṃ"

Before /4.20/ B8,9 in. "dēvā ūcuḥ".

> mānanā tu dvijātīnāṃ kartavyā vai mahātmanām  
> avaṡyaṃ tapasā tēṣāṃ phalitavyaṃ nṛpōttama /4.21/  

> yatas tvaṃ sahasā bhraṣṭa ākāṡān mēdinītalam  
> ēkaṃ tv anugrahaṃ tubhyaṃ ¹dadmō vai nṛpasattama /4.22/  

¹ ?B0,6 CE AB GP; B7 Da "dadma vai"; B8,9 "dadāma"

> yāvat tvaṃ ṡāpadōṣēṇa kālam āsiṣyasē’nagha  
> bhūmēr vivaragō bhūtvā ¹tāvat tvaṃ kālam āpsyasi /4.23/  

¹ B0,6,7,9 AB GP; B8 Da CE "tāvantaṃ"

> yajn̄ēṣu suhutāṃ viprair vasōr dhārāṃ ¹samāhitaiḥ  
> prāpsyasē’smadanudhyānān mā ca tvāṃ glānir ²aspṛṡat /4.24/  

¹ all but CE "mahātmabhiḥ"  
² B AB GP; Da "āviṡat"; CE "āspṛṡēt"

> na kṣutpipāsē rājēndra bhūmēṡ chidrē bhaviṣyataḥ  
> vasōr ¹dhārāsupītatvāt tējasāpyāyitēna ca  
> sa dēvō’smadvarāt prītō brahmalōkaṃ hi nēṣyati /4.25/  

¹ B6,7 Da; B0,8 AB GP "dhārābhi-"; B9 CE "dhārānu-"

> ēvaṃ dattvā varaṃ rājn̄ē sarvē ¹tē ca divaukasaḥ  
> gatāḥ svabhavanaṃ dēvā ṛṣayaṡ ca tapōdhanāḥ /4.26/  

¹ B0,6,7,8 Da3; B9 Da4 CE "tatra"

Before /4.26/ B6 in. "bhīṣma uvāca".

> cakrē ¹vasus tataḥ pūjāṃ viṣvaksēnāya bhārata  
> japyaṃ jagau ca satataṃ nārāyaṇamukhōdgatam /4.27/  

¹ all but CE "ca satataṃ"

This verse explicitly mentions "Viṣvaksēna pūja".

> tatrāpi pan̄cabhir yajn̄aiḥ pan̄cakālān ariṃdama  
> ayajad dhariṃ surapatiṃ bhūmēr vivaragō’pi san /4.28/  

> tatō’sya tuṣṭō bhagavān bhaktyā nārāyaṇō hariḥ  
> ananyabhaktasya satas tatparasya jitātmanaḥ /4.29/  

> varadō bhagavān viṣṇuḥ samīpasthaṃ dvijōttamam  
> garutmantaṃ mahāvēgam ¹ābabhāṣē’psitaṃ tadā /4.30/  

¹ all but CE "ābhabhāṣē smayann iva"

>    ṡrībhagavān uvāca  
> dvijōttama mahābhāga ¹gamyatāṃ vacanān mama  
> samrāḍ rājā vasur nāma dharmātmā ²māṃ samāṡritaḥ /4.31/  

¹ all but AB GP "paṡyatāṃ"  
² B6,9 Da CE; B7 "māṃ samāsthitaḥ"; B0,8 AB GP "saṃṡitavrataḥ"

Before /4.31/ B9 in. "bhagavān uvāca" but all others om.

This verse explicitly talks about "samāṡrayaṇa" in Bengali manuscripts but not
in the Vulgate.

> brāhmaṇānāṃ prakōpēna praviṣṭō vasudhātalam  
> mānitās tē tu viprēndrās tvaṃ tu gaccha dvijōttama /4.32/  

> bhūmēr vivarasaṃguptaṃ garuḍēha mamājn̄ayā  
> adhaṡcaraṃ nṛpaṡrēṣṭhaṃ khēcaraṃ kuru māciram /4.33/  

>    bhīṣma uvāca  
> garutmān atha vikṣipya pakṣau mārutavēgavān  
> vivēṡa vivaraṃ bhūmēr yatrāstē ¹pārthivō vasuḥ /4.34/  

¹ all but CE "vāgyatō"

> tata ēnaṃ samutkṣipya sahasā vinatāsutaḥ  
> utpapāta nabhas tūrṇaṃ tatra cainam amun̄cata /4.35/  

> ¹tasmin muhūrtē saṃjajn̄ē rājōparicaraḥ punaḥ  
> saṡarīrō gataṡ caiva brahmalōkaṃ nṛpōttamaḥ /4.36/  

¹ B6,8,9 Da CE AB; B0,7 GP "asmin"

> ēvaṃ tēnāpi kauntēya vāgdōṣād dēvatājn̄ayā  
> prāptā gatir ¹adhastāt tu dvijaṡāpān mahātmanā /4.37/  

¹ all but Ca "ayajn̄ārhā"; CE "ayajvārhā"

> kēvalaṃ puruṣas tēna sēvitō harir īṡvaraḥ  
> tataḥ ṡīghraṃ jahau ṡāpaṃ brahmalōkam avāpa ca /4.38/  

> ētat tē sarvam ākhyātaṃ ¹sambhūtā mānavā yathā  
> nāradō’pi yathā ṡvētaṃ dvīpaṃ sa ²gatavān ṛṣiḥ  
> tat tē sarvaṃ pravakṣyāmi ṡṛṇuṣvaikamanā nṛpa /4.39/  

¹ B0,7 Da AB GP; B6,8,9 CE "tē bhūtā"  
² B0,7,8,9 CE AB GP; B6 Da "bhagavān"  

Before /4.39/ except CE, all others in. "bhīṣma uvāca" which is redundant
because the speaker changed to Bhīṣma already in /4.34/, as is evident by
Bhīṣma addressing "kauntēya" in /4.37/.

Thus ends chapter B7 161; B8 264; AB 339;

It has 39 verses, 83 lines;
    GP 41 verses, 83 lines;
    CE 39 verses, 83 lines;
    Ku 47 verses, 97 lines
