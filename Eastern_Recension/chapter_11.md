# Chapter 11

>    naranārāyaṇāv ūcatuḥ  
> dhanyō’sy anugṛhītō’si yat tē dṛṣṭaḥ svayaṃ prabhuḥ  
> na hi taṃ dṛṣṭavān kaṡcit ¹padmayōnir api svayam /11.1/  

¹ B0,6,8 CE GP MND; B7,9 Da "ṛtē hy āvāṃ dvijōttama"

The reading of B7,9 Da is a mistake; it's a repeat of the last half of /11.4L2/.

> avyaktayōnir bhagavān durdarṡaḥ puruṣōttamaḥ  
> nāradaitad dhi ¹nau satyaṃ vacanaṃ samudāhṛtam /11.2/  

¹ all; V CE "tē"

> nāsya ¹bhaktāt priyatarō lōkē kaṡcana vidyatē  
> tataḥ svayaṃ darṡitavān svam ātmānaṃ dvijōttama /11.3/  

¹ B6,7,8,9 Da GP; B0 "bhaktaḥ"; CE "bhaktaiḥ"

> tapō hi tapyatas tasya yat sthānaṃ paramātmanaḥ  
> na tat samprāpnutē kaṡcid ṛtē hy āvāṃ dvijōttama /11.4/  

> yā hi sūryasahasrasya samastasya bhavēd dyutiḥ  
> sthānasya sā bhavēt tasya svayaṃ tēna virājatā /11.5/  

> tasmād uttiṣṭhatē vipra dēvād viṡvabhuvaḥ patēḥ  
> kṣamā kṣamāvatāṃ ṡrēṣṭha yayā bhūmis tu yujyatē /11.6/  

> tasmāc cōttiṣṭhatē dēvāt sarvabhūta¹hitād rasaḥ  
> āpō ⁰yēna hi yujyantē dravatvaṃ prāpnuvanti ca /11.7/  

¹ all; CE "-hitō"

> tasmād ēva samudbhūtaṃ tējō ¹bhūtaguṇātmakam  
> yēna ²saṃyujyatē sūryas tatō ³lōkē virājatē /11.8/  

¹ B6,7,9 Da3; B0,8 Da4 CE GP "rūpagu-"  
² B Da3 V; Da4 CE GP "sma yuj-"  
³ all; CE "lōkān"

> tasmād dēvāt samudbhūtaḥ sparṡas tu puruṣōttamāt  
> yēna sma yujyatē vāyus tatō lōkān vivāty asau /11.9/  

> tasmāc cōttiṣṭhatē ṡabdaḥ sarvalōkēṡvarāt prabhōḥ  
> ākāṡaṃ yujyatē yēna tatas tiṣṭhaty ¹asaṃṡayam /11.10/  

¹ B6,7,9 Da; B0,8 CE GP "asaṃvṛtam"

> tasmāc cōttiṣṭhatē ¹viṡvaṃ sarvabhūtagataṃ manaḥ  
> candramā yēna saṃyuktaḥ ²ākāṡaguṇadhāraṇaḥ /11.11/  

¹ B Da; CE GP "dēvāt"  
² B0,7,9 Da3 V; B8 Da4 CE GP "prakāṡagu-"

> ¹ṣaḍbhūtōtpādakaṃ nāma tat sthānaṃ vēdasan̄jn̄itam  
> vidyāsahāyō yatrāstē bhagavān havyakavyabhuk /11.12/  

¹ Ca Da CE; B0,7,9 B0(orig.) GP "sad-"; B6(marg.) B8 "yad-"

> yē hi ¹niṣkaluṣā lōkē puṇyapāpavivarjitāḥ  
> tēṣāṃ vai kṣēmam adhvānaṃ gacchatāṃ dvijasattama /11.13/  

¹ all; V CE "niṣkalmaṣā"

After /11.14L1/ B6,8 V MND in. CE@889 but B0,7,9 Da CE GP om.

> sarva⁰lōkatamōhantā ādityō dvāram ucyatē  
> ādityadagdhasarvāṅgā adṛṡyāḥ kēna cit kvacit /11.14/  

> paramāṇubhūtā ¹ca paraṃ taṃ dēvaṃ praviṡanty uta  
> tasmād api ²ca nirmuktā aniruddhatanau sthitāḥ /11.15/  

¹ B6,8,9 Da V; B0,7 CE GP "bhūtvā tu"  
² all; V CE "vinir-"

/11.15L1/ is hypermetric (17 syllables instead of 16).

> manōbhūtās tatō ¹bhūtvā pradyumnaṃ praviṡanty uta  
> pradyumnāc cāpi nirmuktā jīvaṃ saṅkarṣaṇaṃ ²tataḥ /11.16/  

¹ all; V CE "bhūyaḥ"  
² all; V CE "tathā"

> viṡanti viprapravarāḥ sāṅkhyā bhāgavataiḥ saha  
> tatas traiguṇyahīnās tē paramātmānam an̄jasā /11.17/  

> praviṡanti ¹dvijaṡrēṣṭhāḥ kṣētrajn̄aṃ nirguṇātmakam  
> sarvāvāsaṃ vāsudēvaṃ kṣētrajn̄aṃ viddhi tattvataḥ /11.18/  

¹ all; CE "dvijaṡrēṣṭha"

> samāhitamanaskāṡ ca niyatāḥ saṃyatēndriyāḥ  
> ēkāntabhāvōpagatā vāsudēvaṃ viṡanti tē /11.19/  

> āvām api ca dharmasya gṛhē jātau dvijōttama  
> ramyāṃ viṡālām āṡritya tapa ugraṃ samāsthitau /11.20/  

> yē tu tasyaiva dēvasya prādurbhāvāḥ surapriyāḥ  
> bhaviṣyanti trilōkasthās tēṣāṃ svastīty ¹athō dvija /11.21/  

¹ ?B0,6 Da GP; B7,8,9 V CE "atō"

> vidhinā svēna yuktābhyāṃ yathāpūrvaṃ dvijōttama  
> āsthitābhyāṃ sarvakṛcchraṃ vrataṃ samyak ¹tad uttamam /11.22/  

¹ B6,7,9 Da CE; B0,8 V GP "anuttamam"

> āvābhyām api dṛṣṭas tvaṃ ṡvētadvīpē tapōdhana  
> samāgatō bhagavatā ⁰san̄jalpaṃ kṛtavān yathā /11.23/  

> sarvaṃ hi nau saṃviditaṃ trailōkyē sacarācarē  
> yad bhaviṣyati vṛttaṃ vā vartatē vā ṡubhāṡubham  
> sarvaṃ sa tē kathitavān dēvadēvō mahāmunē /11.24/  

After /11.24L2/ B Da V GP in. CE@891 (/11.24L3/)

>    vaiṡampāyana uvāca  
> ētac chrutvā tayōr vākyaṃ tapasy ¹ugrē’bhyavartata  
> nāradaḥ prān̄jalir bhūtvā nārāyaṇaparāyaṇaḥ /11.25/  

¹ ?B6,9 CE; B8 GP "ugrē ca vartatōḥ"; B0 "ugrē ca vartatē";  
  B7 "ugre’bhyavartatōḥ"; Da3 "ugrē sma vartata"; Da4 "ugrē sma vartatē"

> jajāpa vidhivan mantrān nārāyaṇagatān bahūn  
> divyaṃ varṣasahasraṃ hi naranārāyaṇāṡramē /11.26/  

> avasat sa mahātējā nāradō bhagavān ṛṣiḥ  
> tam ēvābhyarcayan dēvaṃ naranārāyaṇau ca tau /11.27/  

Thus ends chapter B7 169; B8 274; AB 346; GP 344; MND 345

It has 27 verses, 55 lines;
    CE 26 verses, 54 lines;
    GP 27 verses, 55 lines;
    Ku 28 verses, 57 lines.
