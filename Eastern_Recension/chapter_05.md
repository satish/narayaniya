# Chapter 05

>    bhīṣma uvāca  
> prāpya ṡvētaṃ mahādvīpaṃ nāradō bhagavān ṛṣiḥ  
> dadarṡa tān ēva narān̄ ṡvētāṃṡ ¹candraprabhān̄ ṡubhān /5.1/  

¹ B6,7,9 CE; Da "candraprabhānanān"; B0,8 AB GP "candrasamaprabhān"

> pūjayām āsa ṡirasā manasā taiṡ ca pūjitaḥ  
> didṛkṣur japyaparamaḥ sarvakṛcchra¹rataḥ sthitaḥ /5.2/  

¹ B6,8 Da; B0,7,9 AB GP "-gataḥ"; CE "-dharaḥ"

> bhūtvaikāgramanā vipra ūrdhvabāhur ¹samāhitaḥ  
> stōtraṃ jagau sa viṡvāya nirguṇāya ²guṇātmanē /5.3/  

¹ all but CE "mahāmuniḥ"  
² all but CE "mahātmanē"

>    nārada uvāca  
> namas tē ¹dēvadēvēṡa (1) niṣkriya (2) nirguṇa (3) lōkasākṣin (4) ²kṣētrajn̄a (5)  
> ananta (6=116) puruṣa (7) ²mahāpuruṣa (8) triguṇa (9) pradhāna (10)  

¹ all but CE "dēvadēva"  
² after this B0 GP AB MND in. "puruṣōttama" (in both places)

> ¹amṛta (11) vyōma (12) sanātana (13) sadasadvyaktāvyakta (14) ṛtadhāman (15)  
> ²pūrvādidēva (16) vasuprada (17) prajāpatē (18) suprajāpatē (19) vanaspatē (20)  

¹ after this GP in. "amṛtākhya anantākhya"; AB in. "amṛtākṣa anantākhya"; B0 in. "anantākhya"; B8,9 in. "amṛtākhya"  
² all but AB GP "ādidēva"  

> ¹mahāprajāpatē (21) ūrjaspatē (22) ²vācaspatē (23) ³manaspatē (24) ⁴jagatpatē (25)  
> ⁵divaspatē (26) marutpatē (27) salilapatē (28) pṛthivīpatē (29) dikpatē (30)  

¹ B6,9 om.  
² B8,9 om.  
³ B6 om. After this B7 in. "yaṡaspatē"  
⁴ B6 om. B0 repeats twice  
⁵ B8 Da om.

> ¹pūrvanivāsa (31) ²guhya (32) brahmapurōhita (33) ³brahmakāyika (34) mahārājika (35)  
> ⁴cāturmahārājika (36) ⁵ābhāsura (37) ⁶mahābhāsura (38) ⁷saptamahābhāga (39) yāmya (40)  

¹ all but Da4 "dikpūrvanivāsa"  
² B6,8,9 Da4 AB GP; B0 subst. "purōhita"; B7 Da3 CE om.  
³ after this CE in. "mahākāyika" but B Da3 AB GP om.  
⁴ B AB GP; CE "caturmahārājika"; Da om.  
⁵ ?B6 CE AB GP; B0,9 "ābhāsvara"; B7 "bhāsvara"; B8 "bhābhāsvara"  
⁶ Da CE AB GP; B0,7,8,9 "mahābhāsvara"; B6 om.  
⁷ all but CE "saptamahābhāsura"

As per MW ābhāsura and ābhāsvara are equivalent (meaning, "shining").

> mahāyāmya (41) san̄jn̄āsan̄jn̄a (42) tuṣita (43) mahātuṣita (44) ¹pramardana (45)  
> parinirmita (46) ²vaṡavartin (47) ³aparinirmita (48) yajn̄a (49) mahāyajn̄a (50)  

¹ B0,6,7,9 AB GP; Da4 "pramardina"; B8 Da3 CE "pratardana"  
² After this AB GP in. "aparinindita; aparimita; vaṡavartin"; B0 AB GP cont. in. "avaṡavartin"   
³ B6,7 Da om.

-1

> yajn̄asambhava (51) yajn̄ayōnē (52) yajn̄agarbha (53) yajn̄ahṛdaya (54) yajn̄astuta (55)  
> yajn̄abhāgahara (56) pan̄cayajn̄adhara (57) ¹pan̄cakālakartṛpatē (58) pan̄carātrika (59) vaikuṇṭha (60)  

¹ B7,8 Da AB GP; B6 "pan̄cakartṛtē"; B0,9 CE "-kartṛgatē"

> aparājita (61) ¹mānasika (62) ²parasvāmin (63) susnāta (64) ³haṃsa (65)   
> paramahaṃsa (66) paramayājn̄ika (67) ⁴sāṅkhyayōga (68) amṛtēṡaya (69) hiraṇyēṡaya (70)  

¹ all but B9 "nāmasika"; Da "nādhamika". After this B0 AB GP in. "nāmanāmika"  
² all but CE "paramasvāmin"  
³ after this B8 Da AB GP in. "mahāhaṃsa"  
⁴ after thus B0 AB GP in. "sāṅkhyamūrtē"

+1

> ¹dēvēṡaya (71) kuṡēṡaya (72) brahmēṡaya (73) padmēṡaya (74) ²viṡvēṡvara (75)  
> ³tvaṃ jagadanvayaṡ ca (76) tvaṃ jagatprakṛtiḥ (77) ⁴tavāgnir āsyam (78) vaḍavāmukhō’gniḥ (79) tvam āhutiḥ (80)  

¹ ?B0,6,7 Da4 AB GP; B8,9 Da3 CE "vēdēṡaya"  
² B6,7 Da om. After this B0 AB GP in "viṣvaksēna"  
³ After this B8,9 in. "tasya rūpasya"; Da in. "tavāsya rūpasya"  
⁴ B6,9 Da om.

+1
-1

> ¹sārathiḥ (81) tvaṃ vaṣaṭkāraḥ (82) tvam ōṅkāraḥ (83) tvaṃ manaḥ (84) ²tvaṃ tapaḥ () tvaṃ candramāḥ (85)  
> tvaṃ cakṣur ³ājyam (86) tvaṃ sūryaḥ (87) tvaṃ diṡāṃ ⁴gajaḥ (88) ⁵digbhānō (89) ⁶vidigbhānō () hayaṡiraḥ (90)  

¹ all but CE "tvaṃ sā-"  
² B9 CE om.  
³ ?B6,8 Da3 AB; B0 Da4 CE "ādyaṃ"; B7 "ākṣyaṃ"; B9 GP "ādityaṃ"  
⁴ ?B8,9 CE AB GP; B0 "jagat"; B6 "patiḥ"; B7 Da3 "jagatī"; Da4 "jagati";
  It is clear that it must have "-ti-" hence "gajaḥ" is ruled out.
⁵ B8,9 Da CE AB; B0,6,7 GP "tvaṃ dig-"  
⁶ B0,7,8,9 AB GP; Da CE om.

+2 guaranteed

> ¹prathamatrisauparṇadhara (91) pan̄cāgnē (92) triṇācikēta (93) ²ṣaḍaṅgavidhāna (94) ³prāgjyōtiṣa (95)   
> ⁴jyēṣṭhasāmaga (96) ⁵vratadhara (97) ⁶atharvaṡiraḥ (98) pan̄camahākalpa (99) phēnapācārya (100)  

¹ B6,9 Da; B0 "-trisauvarṇa"; B7 "-trisauparṇō"; B8 CE AB GP "-sauparṇa". After this, B0 Da4 AB GP in. "varṇadharaḥ"  
² all but CE "-nidhāna"  
³ B6,7 Da om.  
⁴ ?B0,8,9 CE AB GP; Da3 "prāgjyēṣṭhamāsaga"; Da4 "prāgjyēṣṭhamāsika"; B6,7 "prāgjyēṣṭha"  
⁵ ?B0,8 Da; B7 CE GP "sāmikavrata-"; B6 AB "māsikavrata-"; B9 "sāmāsivrata-"  
⁶ B0,6,9 Da CE; B7,8 AB GP "-ṡirāḥ"

From ³⁴⁵ it seems that one of them is omitted. Most likely they are "prāgjyēṣṭha" and "māsikavratadhara"

-1 guaranteed

> vālakhilya (101) vaikhānasa (102) abhagnayōga (103) abhagnaparisaṅkhyāna (104) yugādē (105)  
> yugamadhya (106) yuganidhana (107) ākhaṇḍala (108) prācīnagarbha (109) kauṡika (110)  

> puruṣṭuta (111) ¹puruhūta (112) viṡvarūpa (113) anantagatē (114) anantabhōga (115)  
> ²ananta (116=6) anādē (117) amadhya (118) ³avyaktamadhya (119) avyaktanidhana (120)  

¹ After this, B0,8 AB GP in. "viṡvakṛt"  
² B0,6,8,9 CE AB GP; B7 Da om.  
³ B6,8,9 Da3 CE AB GP; B0,7 Da4 om.

> ¹vratāvāsa (121) samudrādhivāsa (122) yaṡōvāsa (123) ²damāvāsa () tapōvāsa (124) lakṣmyāvāsa (125)   
> vidyāvāsa (126) kīrtyāvāsa (127) ³kāntyāvāsa () ṡrīvāsa (128) sarvāvāsa (129) vāsudēva (130)  

¹ B0,7,8 CE AB GP; B6,9 Da "vratavāsa" (sic)  
² all; CE om.  
³ B6,8,9 Da; B0,7 CE AB GP om.  

+2 guaranteed

> sarvacchandaka (131) harihaya (132) harimēdha (133) mahāyajn̄abhāgahara (134) ¹varaprada (135=157)   
> ²yamaniyamamahāniyamakṛcchrātikṛcchramahākṛcchrasarvakṛcchraniyamadhara (136)   
> ³nivṛttadharmapravacanagatē (137) pravṛttavēdakriya (138) aja (139) sarvagatē (140)  

¹ After this B0 Da3 AB GP in. "sukhaprada; dhanaprada"; B8 in. "mēdhāhara; sukhaprada"; B7 only "dhanaprada"  
² GP split this into 8 names  
³ ?B6,8 CE; B0,7 AB GP "-gata"; B9 "-dharmanagatē" (sic); Da3 "nirvṛtta*gata"; Da4 "nirvṛttakarma-"

+7 guaranteed

> sarvadarṡin (141) agrāhya (142) acala (143) mahāvibhūtē (144) māhātmyaṡarīra (145)   
> pavitra (146) mahāpavitra (147) ¹hiraṇyamaya (148) bṛhat (149) apratarkya (150)  

¹ B6,7,8,9 Da3 GP; B0 Da4 AB CE "hiraṇmaya"  

> ¹avijn̄ēya (151) brahmāgrya (152) prajāsargakara (153) prajānidhanakara (154) mahāmāyādhara (155)   
> citraṡikhaṇḍin (156) varaprada (157=135) purōḍāṡabhāgahara (158) ²gatādhvan (159) chinnatṛṣṇa (160)  

¹ After this B6,7 in. "pṛṡnigarbha"  
² B6,7,9 Da4 CE; B0,8 Da3 AB GP "gatādhvara"  

> chinnasaṃṡaya (161) ¹sarvatōnivṛtta (162) ²brāhmaṇarūpa (163) ³brāhmaṇapriya (164) viṡvamūrtē (165)   
> ⁴mahāmūrtē (166) bāndhava (167) bhaktavatsala (168) brahmaṇyadēva (169) bhaktō’haṃ tvāṃ didṛkṣuḥ (170)   
> ēkāntadarṡanāya namō namaḥ (171) /5.4/  

¹ B0 AB GP split this into two "sarvatōvṛtta; nivṛttarūpa"  
² After this B8 in. "brahmarūpa; brāhmaṇavrata"  
³ After this B9 in. "pavitra"  
⁴ After this B8 in. "pavitra"; B9 om. this  

Thus ends chapter B7 162; B8 265; AB 340;

It has 4 verses, 6 lines, 1 statement containing 181? names;
    GP 4 verses, 6 lines, 1 statement containing 200 names;
    CE 4 verses, 6 lines, 1 statement containing 171 names;
    Ku 4 verses, 6 lines, 1 statement containing ??? names.
