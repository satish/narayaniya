# Narayaniya

Nārāyaṇīya (also spelled Narayaniya or Narayaneeyam) is a Pān̄carātra text
occuring in the Mokṣadharma sub-parva in the Book 12 of the Mahābhārata. It
forms the last 18 (or 19) chapters of the Ṡānti Parva. It discusses the
supremacy of Lord Viṣṇu, His various *vyūhas* and the importance of
*jn̄āna-bhakti* towards Him.

This repository contains the Sanskrit text derived from the Eastern recension
(Bengali and Nepali/Maithili manuscripts and Arjuna Mishra's readings) as
recorded in the footnotes and appendices of the Critical Edition (CE) of the
Mahabharata published by Bhandarkar Oriental Research Institute (BORI).

## Why?

Because, why not? As Sukthankar himself says:

> For, who and what is to prevent him from constituting his own text from this
> critical edition?

Because the renowned Vedic scholar and author Dr. K.S.Narayanacharya
[mentioned][ORIG] that the Eastern/Bengali recension is closer to the authentic
version of Mahabharata. Though, he himself recommends the Gita Press edition,
itself based on Nilakantha's "vulgate" (which comes with its own issues
highlighted in Prolegomena to the CE). So, here is an attempt to reconstruct a
typical Eastern recension with some help from M.N.Dutt and Ganguli (both Bengali
themselves).

[ORIG]: https://youtu.be/VJMrfnaSyW0?t=2710

* BORI's editors have a fetish for Kashmiri, esp. when in bed with Malayalam.
  Instead of "critical edition", they could've called it KK (Kashmir & Kerala)
  recension, given their utter disparagement and condescension towards
  everything that lies *in-between* Kashmir and Kerala: the Devanagari and
  Telugu-Grantha recensions, Nilakantha's and PPS Sastri's.
* Kumbhakonam edition suffers from [FOMO](https://en.wikipedia.org/wiki/FOMO) --
  it includes all verses from all possible scripts lest you forget an obscure
  verse that no one will ask you.
* PPS Sastri's southern edition follows *one* chosen manuscript (G2) and is full
  of spurious interpolations and omissions -- even when it is inconsistent with
  the rest of TG manuscripts.

### What do I actually want?

I want a version which is Vaiṣṇava in spirit -- doesn't matter whether it is
northern or southern or composite or interpolated. A version that sage Lord
Vedavyasa approves.

Basing this on the Bengali and Arjuna Miṡra's versions removes the
"roughness" of Nilakantha's vulgate and the over-zealous scissors of the
CE.

Plus, it is fun to reconstruct a recension :)

From the [prolegomena][plg] to Ādi Parva (Critical Edition):

[plg]: http://gretil.sub.uni-goettingen.de/gretil_elib/Suk9441__Sukthankar_MemorialEd_1_CritStud_Mbh_1944.pdf

> The Bengali version is closely allied to the Vulgate, but is unquestionably
> superior to the latter in so far that it is happily free from a large number of
> late accretions which encumber the Vulgate.
>
> Nīlakaṇṭha's text has acquired in modern times an importance out of all
> proportion to its critical value, to the utter neglect of far superior texts,
> such as the Kaṡmīrī or Bengali.
>
> Arjuna has treated the Harivamṡa as an integral part of the epic, elaborately
> defending this position; his commentary, therefore, embraces the Harivamṡa
> (12,000 verses) also.
>
> A commentary older and more important than the Arthadīpikā of Arjunamiśra, and
> one more neglected still, is the Jñānadīpikā of Devabodha... He is, therefore,
> most likely, the earliest commentator of the Mahābhārata hitherto known, and,
> in my opinion, also the **best**.
>
> Arjuna has in fact based his commentary largely on that of his predecessor. He
> has copied very large portions of Devabodha's commentary, sometimes verbatim,
> sometimes in extract. Moreover even when the two commentaries differ, the
> influence of Devabodha is plainly discernible. In fact, the Arthadīpikā may be
> considered as a revised and enlarged edition of the Jñānadīpikā. The
> similarity of the names is suggestive and worthy of note.
>
> The *editio princeps* (Calcutta 1836) remains the best edition of the Vulgate,
> after the lapse of nearly a century. The well-known pothī-form Bombay editions
> (Gopal Narayan in 1913 and others), which include Nīlakaṇṭha's scholium, are
> supposed to represent Nīlakaṇṭha's text; but they contain many readings and
> lines which are not to be found in the Nīlakaṇṭha manuscripts, and are
> therefore not wholly reliable.

## Concordance

Since there are different recensions of Mahābhārata, here I enumerate the
relavant sections:

* BORI critical edition: Chapters 12.321 to 12.339 (19 chapters)
* P.P.S Sastri's South Indian recension: Chapters 12.321 to 12.339 (19 chapters)
* Nilakantha's Bhavadipa (North Indian recension): Chapters 12.334 to 12.351 (18 chapters)
* Gita Press, Gorakhpur edition: follows same readings as Nilakantha's.
* [Asiatic Society of Bengal][AB], Calcutta edition (1830s): 12.336 to 12.353
  (18 chapters). Only [Vol.3][AB] and [Vol.4][AB4] are available. Note that this
  is **not** a Bengali recension but that of Nilakantha's.
* Kumbakonam edition (South Indian composite recension): 12.342 to 12.361 (20 chapters)
* [Dvaita critical edition][dv] (Palimaru Matha):
  Chapters 166 to 184 of Mokṣadharma, i.e., 12.334 to 12.352 (19 chapters) are
  identical to BORI except for 16 additional verses in 12.336 (starred passages
  837, 846, 848, App. II No. 31 are present but verse 82, 108, 109 missing in BORI)
* Kisari Mohan Ganguli's translation: [12.335][km1] to [12.352][km2] (18 chapters)
* Manmatha Nath Dutt's translation: 12.335 to 12.352 (18 chapters)

[AB]: http://dspace.wbpublibnet.gov.in:8080/jspui/handle/10689/5939
[AB4]: http://dspace.wbpublibnet.gov.in:8080/jspui/handle/10689/5715
[km1]: http://www.sacred-texts.com/hin/m12/m12c034.htm
[km2]: http://www.sacred-texts.com/hin/m12/m12c051.htm
[dv]: https://play.google.com/store/books/details/?id=QSGADwAAQBAJ

The conversation starts with the great King Yudhiṣṭhira questioning grandsire
Bhīṣma lying on a bed of arrows. It ends with Caturmukha-Brahmā answering Rudra
about the Supreme Puruṣa. The first verse of the first chapter of Nārāyaṇīya is
thus (Yudhiṣṭhira addressing Bhīṣma):

>    yudhiṣṭhira uvāca  
> gṛhasthō brahmacārī vā vānaprasthōʼtha bhikṣukaḥ  
> ya icchēt siddhimāsthātuṃ dēvatāṃ kāṃ yajēta saḥ  

The first verse of the last chapter is thus (Brahmā addressing Rudra):

>    brahmōvāca  
> ṡṛnu putra yathā hyēkaḥ puruṣaḥ ṡāṡvatōʼvyayaḥ  
> akṣayaṡcāpramēyaṡca sarvagaṡca nirucyatē  

The entire Ṡānti Parva ends with the last chapter of Nārāyaṇīya (12.339) in PPS
Sastri's edition. However, all the other editions continue with many more
chapters after the Nārāyaṇīya (Ucchravṛtti-upākhyāna, etc). Not only that, even
the starting chapters are different. BORI and Sastri explicitly mention the
start of the Nārāyaṇīya with the Yudhiṣṭhira's verse, as above. Kumbakonam
edition, however, suggests its 12.344 chapter as the first chapter of Nārāyaṇīya
(it ends with ... *nārāyaṇīyē* ... unlike 12.343). Similarly, Gita Press
suggests its 12.335 as the first chapter of Nārāyaṇīya (it also ends the chapter
with ... *nārāyaṇīyē* ... unlike 12.334). BORI, Gita Press and Nilakantha
explicitly mention that the last chapter of Nārāyaṇīya is the Brahma-Rudra
conversation, as above.


| Gita Press | Verses    | BORI    | Verses | Kumbakonam | Verses | PPS Sastri | Verses        | Dvaita  | Verses | M.N.Dutt | Verses |
|:-----------|:----------|:--------|:-------|:-----------|:-------|:-----------|:--------------|:--------|:-------|:---------|-------:|
| Ch. 334    | 45  (½)   | Ch. 321 | 43     | Ch. 342    | 46     | Ch. 321    | 46            | Mo. 166 | 43     | Ch. 335  |     44 |
| Ch. 335    | 56  (1)   | Ch. 322 | 52     | Ch. 343    | 56     | Ch. 322    | 56            | Mo. 167 | 52     | Ch. 336  |     53 |
| Ch. 336    | 65        | Ch. 323 | 57     | Ch. 344    | 69     | Ch. 323    | 67            | Mo. 168 | 57     | Ch. 337  |     62 |
| Ch. 337    | 41  (1)   | Ch. 324 | 39     | Ch. 345    | 47     | Ch. 324    | 49            | Mo. 169 | 39     | Ch. 338  |     39 |
| Ch. 338    | 4         | Ch. 325 | 4      | Ch. 346    | 4      | Ch. 325    | 4  (3 + 1)    | Mo. 170 | 4      | Ch. 339  |      4 |
| Ch. 339    | 141 (15½) | Ch. 326 | 124    | Ch. 347    | 76     | Ch. 326    | 77            | Mo. 171 | 140    | Ch. 340  |    136 |
| -＂-       | -＂-      | -＂-    | -＂-   | Ch. 348    | 91     | Ch. 327    | 91            | -＂-    | -＂-   | -＂-     |   -＂- |
| Ch. 340    | 119       | Ch. 327 | 107    | Ch. 349    | 117    | Ch. 328    | 119           | Mo. 172 | 107    | Ch. 341  |    112 |
| Ch. 341    | 51        | Ch. 328 | 53     | Ch. 350    | 59     | Ch. 329    | 60 (58 + 2)   | Mo. 173 | 53     | Ch. 342  |     56 |
| Ch. 342    | 142 (2)   | Ch. 329 | 50     | Ch. 351    | 66     | Ch. 330    | 194 (7 + 187) | Mo. 174 | 50     | Ch. 343  |    140 |
| -＂-       | -＂-      | Ch. 330 | 71     | Ch. 352    | 76     | Ch. 331    | 58            | Mo. 175 | 71     | -＂-     |   -＂- |
| Ch. 343    | 66        | Ch. 331 | 52     | Ch. 353    | 67     | Ch. 332    | 69            | Mo. 176 | 52     | Ch. 344  |     64 |
| Ch. 344    | 27        | Ch. 332 | 26     | Ch. 354    | 28     | Ch. 333    | 28            | Mo. 177 | 26     | Ch. 345  |     27 |
| Ch. 345    | 28        | Ch. 333 | 25     | Ch. 355    | 27     | Ch. 334    | 28            | Mo. 178 | 25     | Ch. 346  |     28 |
| Ch. 346    | 22        | Ch. 334 | 17     | Ch. 356    | 20     | Ch. 335    | 19            | Mo. 179 | 17     | Ch. 347  |     20 |
| Ch. 347    | 96        | Ch. 335 | 89     | Ch. 357    | 94     | Ch. 336    | 94            | Mo. 180 | 89     | Ch. 348  |     93 |
| Ch. 348    | 88        | Ch. 336 | 82     | Ch. 358    | 88     | Ch. 337    | 87            | Mo. 181 | 82     | Ch. 349  |     88 |
| Ch. 349    | 74        | Ch. 337 | 69     | Ch. 359    | 74     | Ch. 338    | 97            | Mo. 182 | 69     | Ch. 350  |     73 |
| Ch. 350    | 27        | Ch. 338 | 25     | Ch. 360    | 25     | -＂-       | -＂-          | Mo. 183 | 25     | Ch. 351  |     25 |
| Ch. 351    | 23        | Ch. 339 | 21     | Ch. 361    | 23     | Ch. 339    | 24            | Mo. 184 | 21     | Ch. 352  |     23 |
| TOTAL      | 1115 (20) |         | 1006   |            | 1153   |            | 1267          |         | 1022   |          |   1087 |


## Choice of Manuscripts

Bhandarkar's Critical Edition mentions the following manuscripts for Ṡānti
parva, especially the Mokṣadharma:

* Bengali manuscripts B0 (Paris), B6 (Shantiniketan), B7,8 (Dacca), B9
  (Calcutta).
  - B0,B7,B8 begin with "ōṃ namō bhagavatē vāsudēvāya". B9 with "ōṃ namō
    gaṇēṡāya". B6 unknown. B9 seems to be in its own group. B6,8 have sometimes
    a rare agreement with T G M. B0,6-9 is collectively denoted "B".
    B6 and B9 are especially agree with CE more than others.
* Devanagari manuscripts of Arjuna Miṡra Da3,a4 (Poona), which is closely
  aligned with Bengali recension.
* Maithili (Videha) manuscript V1 (Nepal), dated 1516 CE.
* Devanagari composite manuscripts (eight): D2-9. All except D4, D6, D9 begin
  with "ṡrī gaṇēṡāya namaḥ". In the "nārāyaṇaṃ namaskṛtya" mantra D8 reads
  "vyāsaṃ" but others D2,3,5,7 read "caiva".
  - D2,3,8 esp. D2,3,5,8, form a set, often aligning with Bengali.
  - D4,9 esp. with D4,5,7,9 go well with T G (esp. D7 T G), but never
    together with Bengali. D8 is explictly Ṡaiva (see CE12*0870_01)

The readings are chosen to strongly align with the B manuscripts.

The final chosen text must be supported by at least **four** manuscripts out of
seven (B0,6-9 Da3,4) -- this avoids any manuscript-specific peculiarities. If
not, a question mark (?) is inserted just before the first citation in the
footnotes.

The verses are consistently checked for [Anuṣṭubh metre][mtr] or any other metre
as applicable. This usually helps to weed out spurious readings.

[mtr]: https://sanskritmetres.appspot.com/

### Example



## Formatting

* All the verse lines begin with ">" and end with two spaces.
* All the "uvāca" and "ūcuḥ" lines begin with ">    " (that's not SPACE \x20 but
  Unicode EM QUAD \x2001).
* Long pādas are separated to next line starting with ">  " (that's not SPACE
  \x20 but Unicode EN SPACE \x2002).
* To count the number of lines in Emacs, M-s o, enter "> ".
* Numbering of the verses follows MND when possible.

## Abbreviations

* � -- denotes missing reading in ms
* ◌  -- denote omissions in constituted text but not others
* AB -- Asiatic Society of Bengal (Calcutta Edition)
* Ca -- Arjuna Mishra's commentary (Bhāratārtha-pradīpikā)
* Cv -- Vadiraja's commentary (Lakṣālaṅkāra)
* CE -- BORI critical edition
* Dv -- Dvaita (Palimaru Matha) edition
* GP -- Gita Press edition
* in. -- inserts, inserted
* KMG -- Kisāri Mohan Ganguli
* Ku -- Kumbhakonam edition
* L -- Line, as in L1 for the first line, L2 for the second line.
* marg -- margin text
* MND -- Manmatha Nath Dutt
* MW -- Monier-Williams dictionary
* om. -- omits, omitted
* orig. -- original text
* PPS -- PPS Sastri's edition
* (sic) -- incorrect reading, but as given in manuscript anyway
* V -- Maithili manuscript (only V1 is used for the Narayaniya)

"B" is shorthand for "B0,6-9" (five manuscripts).  
"Da" is shorthand for "Da3,Da4" (two manuscripts).

Starred passages in CE are numbered like CE@809.  
Uncertain and inconclusive readings are denoted by ?.

TODO

* Prefix or suffix hyphen when superscript numbers occur in between words.
* Check for "uvāca"/"ūcuḥ" sequence repetitions
* Check for missing verse references, like //
* Check end-of-chapter verse counts of GP, Ku
