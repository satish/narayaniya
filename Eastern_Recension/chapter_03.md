# Chapter 03

>    bhīṣma uvāca  
> tatō’tītē mahākalpē utpannē’ṅgirasaḥ sutē  
> babhūvur nirvṛtā dēvā jātē dēvapurōhitē /3.1/  

> bṛhad brahma mahac cēti ṡabdāḥ paryāyavācakāḥ  
> ēbhiḥ samanvitō rājan guṇair vidvān bṛhaspatiḥ /3.2/  

> tasya ṡiṣyō babhūvāgryō rājōparicarō vasuḥ  
> adhītavāṃs tadā ṡāstraṃ samyak citraṡikhaṇḍijam /3.3/  

> sa rājā bhāvitaḥ pūrvaṃ daivēna vidhinā ³nṛpaḥ  
> pālayām āsa pṛthivīṃ divam ākhaṇḍalō yathā /3.4/  

³ B7,8 Da; B0,6,9 CE AB GP "vasuḥ"

> tasya yajn̄ō mahān āsīd aṡvamēdhō mahātmanaḥ  
> bṛhaspatir upādhyāyas tatra hōtā babhūva ha /3.5/  

> prajāpatisutāṡ cātra sadasyās ¹tv abhavaṃs trayaḥ  
> ēkataṡ ca dvitaṡ caiva tritaṡ caiva maharṣaya /3.6/  

¹ B6-9 Da CE; B0 GP AB "cābha-"

> ¹dhanuṣākṣō’tha raibhyaṡ ca arvāvasuparāvasū  
> ṛṣir mēdhātithiṡ caiva tāṇḍyaṡ caiva mahān ṛṣiḥ /3.7/  

¹ ?B7-9 CE; B0 Da AB GP "dhanuṣākhyō’tha"; B6 om.

> ṛṣiḥ ¹ṡāntir mahābhāgas tathā vēdaṡirāṡ ca yaḥ  
> ²ṛṣiḥ ṡrēṣṭhaṡ ca kapilaḥ ṡālihōtra³pitāmahaḥ /3.8/  

¹ B0,6,7,9 Da AB GP; B8 "sakathir" (sic); CE "ṡaktir"  
² B0,6,8 Da3; B9 Da4 AB GP "ṛṣiṡrēṣṭhaṡ ca kapilaḥ";
  B7 "ṛṣiṡ ca ṡrēṣṭha kapilaḥ"; CE "kapilaṡ ca ṛṣi ṡrēṣṭhaḥ"  
³ ?B6-9 Da CE; B0 AB GP "-pitāsmṛtaḥ"

Note that Arjuna Miṡra comments in Ca, "kapilaḥ - ṡālihōtrasya pitā", so
favoring GP's "-pitāsmṛtaḥ".

> ādyaḥ ¹kaṭhas taittiriṡ ca vaiṡaṃpāyanapūrvajaḥ  
> kaṇvō’tha dēvahōtraṡ ca ētē ṣōḍaṡa kīrtitāḥ /3.9/  

¹ B Ca CE AB GP; Da "kacas"

> ¹saṃbhūtāḥ sarvasaṃbhārās tasmin rājan mahākratau  
> na tatra paṡughātō’bhūt sa rājaivaṃ sthitō’bhavat /3.10/  

¹ B AB GP; Da CE "saṃbhṛtāḥ"  

> ahiṃsraḥ ṡucir akṣudrō nirāṡīḥ karmasaṃstutaḥ  
> āraṇyaka¹padōdbhūta bhāgās tatrōpakalpitāḥ /3.11/  

¹ B Da AB GP; CE "padōdgītā"

> prītas tatō’sya bhagavān dēvadēvaḥ purātanaḥ  
> sākṣāt ¹tu darṡayām āsa sō’dṛṡyō’nyēna ²vai divi /3.12/  

¹ B6-9 Da; B0 CE AB GP "taṃ"  
² B6,7,9 Da; B0,8 CE AB GP "kēna cit"

> svayaṃ bhāgam upāghrāya purōḍāṡaṃ gṛhītavān  
> adṛṡyēna hṛtō bhāgō dēvēna harimēdhasā /3.13/  

> bṛhaspatis tataḥ kruddhaḥ ¹ṡrucam udyamya vēgitaḥ  
> ākāṡaṃ ghnan ²ṡrucaḥ pātai rōṣād aṡrūṇy avartayat /3.14/  

¹ B6-9; B0 "ṡruvam"; Da3 GP AB "srucam"; Da4 "sravam"; CE "sruvam"  
² B6-9; Da GP AB "srucaḥ"; B0 CE "sruvaḥ"

"sruva" means ladle. MW says "ṡruc" is alternative spelling of "sruc", both
meaning ladle (as well).

> uvāca cōparicaraṃ mayā bhāgō’yam udyataḥ  
> grāhyaḥ svayaṃ hi dēvēna matpratyakṣaṃ na saṃṡayaḥ /3.15/  

>    yudhiṣṭhira uvāca  
> udyatā yajn̄abhāgā hi sākṣāt prāptāḥ surair iha  
> kimartham iha na prāptō darṡanaṃ sa harir vibhuḥ /3.16/  

>    bhīṣma uvāca  
> tataḥ sa taṃ samuddhūtaṃ bhūmipālō mahān vasuḥ  
> prasādayām āsa muniṃ sadasyās tē ca sarvaṡaḥ /3.17/  

> ūcuṡ cainam asambhrāntā na rōṣaṃ kartum arhasi  
> naiṣa dharmaḥ kṛtayugē ¹yas tvaṃ rōṣam acīkṛthāḥ /3.18/  

¹ ?B0,6,8 CE AB GP; B7 "na"; B9 Da "yat"

> arōṣaṇō hy asau dēvō yasya bhāgō’yam udyataḥ  
> ¹na sa ṡakyas tvayā draṣṭum asmābhir vā bṛhaspatē  
> yasya prasādaṃ kurutē sa vai taṃ draṣṭum arhati /3.19/  

¹ B6-8 Da CE; B0 AB GP "na ṡakyaḥ sa"; B9 "sa na ṡakyā"

>    ¹ēkatadvitatritā ūcuḥ  
> vayaṃ hi brahmaṇaḥ putrā mānasāḥ parikīrtitāḥ  
> gatā niḥṡrēyasārthaṃ hi kadācid diṡam uttarām /3.20/  

¹ B6,7,9 Da CE; B0,8 GP AB MND substitute CE@808 "ēkatadvitatritaṡ cōcus tataṡ citraṡikhaṇḍinaḥ"  

> taptvā varṣasahasrāṇi ¹caritvā tapa uttamam  
> ²ēkapādasthitāḥ samyak kāṣṭhabhūtāḥ samāhitāḥ /3.21/  

¹ B Da AB GP; CE "catvāri"  
² B6-9 CE AB GP; B0 Da "ēkapādāḥ sthi-"

> mērōr uttara¹bhāgēna kṣīrōdasyānukūlataḥ  
> sa dēṡō yatra nas taptaṃ tapaḥ paramadāruṇam /3.22/  

¹ B6-9; B0 Da CE AB GP "bhāgē tu"

After /3.23L1/ B6-9 Da AB GP in. CE@809 (/3.23L2/).

> kathaṃ paṡyēmahi vayaṃ dēvaṃ ¹nārāyaṇaṃ tv iti  
> varēṇyaṃ varadaṃ taṃ vai dēvadēvaṃ sanātanam /3.23/  

¹ B Da CE; AB GP "nārāyaṇatmakam"

After /3.24L1/ B6-9 Da AB GP in. CE@810 (/3.24L2/).

> ¹tatō vratasyāvabhṛthē vāg uvācāṡarīriṇī  
> snigdhagambhīrayā vācā praharṣaṇakarī vibhō /3.24/  

¹ B6,7,9 Da CE; B0,8 AB GP "atha"

> sutaptaṃ vas tapō viprāḥ prasannēnāntarātmanā  
> yūyaṃ jijn̄āsavō bhaktāḥ kathaṃ drakṣyatha taṃ ¹prabhum /3.25/  

¹ B Da CE MND; AB GP "vibhum"

> kṣīrōdadhēr uttarataḥ ṡvētadvīpō mahāprabhaḥ  
> tatra nārāyaṇaparā mānavāṡ candravarcasaḥ /3.26/  

> ēkāntabhāvōpagatās tē bhaktāḥ puruṣōttamam  
> tē sahasrārciṣaṃ dēvaṃ praviṡanti sanātanam /3.27/  

> ¹anindriyā ²anāhārā ³aniṣpandāḥ sugandhinaḥ  
> ēkāntinas tē puruṣāḥ ṡvētadvīpanivāsinaḥ  
> gacchadhvaṃ tatra munayas tatrātmā mē prakāṡitaḥ /3.28/  

¹ B Da AB GP; CE "atīndriyā"  
² B Da; CE GP "nirāhārā"  
³ B0,8 CE GP; B6,7,9 Da "avispandāḥ"; AB "anispandāḥ"

B0,8 CE GP consistently use "niṣpa-" and AB "nispa-". As per MW, both are same
(search for nispanda). Anyway it must match with /2.9/ and /2.13/.

> atha ṡrutvā vayaṃ sarvē vācaṃ tām aṡarīriṇīm  
> yathākhyātēna mārgēṇa ¹dēṡaṃ taṃ pratipēdirē /3.29/  

¹ B0,6,7,9 Da; B8 CE AB GP "taṃ dēṡaṃ"

> prāpya ṡvētaṃ mahādvīpaṃ taccittās taddidṛkṣavaḥ  
> ¹tatō’smad dṛṣṭiviṣayas tadā pratihatō’bhavat /3.30/  

¹ B0,6,7,9 Da AB GP; B8 CE "tatō nō"

> na ca paṡyāma puruṣaṃ tattējōhṛtadarṡanāḥ  
> tatō naḥ prādurabhavad vijn̄ānaṃ dēvayōgajam /3.31/  

> na kilātaptatapasā ṡakyatē draṣṭum an̄jasā  
> tataḥ punar varṣaṡataṃ taptvā tātkālikaṃ mahat /3.32/  

> vratāvasānē ¹ca ṡubhān narān dadṛṡirē vayam  
> ṡvētāṃṡ candrapratīkāṡān sarvalakṣaṇalakṣitān /3.33/  

¹ ?B8 Da AB GP; B0,6,7 CE "suṡubhān"; B9 "tu ṡubhān"

> nityān̄jalikṛtān brahma japataḥ prāgudaṅmukhān  
> mānasō nāma sa japō japyatē tair mahātmabhiḥ /3.34/  

> tēnaikāgramanastvēna prītō bhavati vai hariḥ  
> ¹yābhavan muniṡārdūla bhāḥ sūryasya yugakṣayē /3.35/  

¹ ?B0,7 Da GP AB; B6,8,9 CE "yā bhavēn"

> ēkaikasya prabhā tādṛk sābhavan mānavasya ha  
> tējōnivāsaḥ sa dvīpa iti vai mēnirē vayam /3.36/  

> na tatrābhyadhikaḥ kaṡ cit sarvē tē samatējasaḥ  
> atha sūryasahasrasya prabhāṃ yugapad utthitām  /3.37/  

> sahasā dṛṣṭavantaḥ sma punar ēva bṛhaspatē  
> sahitāṡ cābhyadhāvanta tatas tē mānavā drutam /3.38/  

> kṛtān̄jalipuṭā hṛṣṭā nama ity ēva vādinaḥ  
> tatō ¹hi vadatāṃ tēṣām ²aṡrauṣma ³vipulaṃ dhvanim /3.39/  

¹ B0,6,7,9 Da AB GP; B8 CE "’bhivadatāṃ"  
² B6,9 Da CE AB GP; B0,8 "aṡrauṣaṃ"; B7 "asrausma"  
³ B0,9 Da CE AB GP; B6,7,8 "vipuladhv-"  

> baliḥ kilōpa¹hriyatē tasya dēvasya tair naraiḥ  
> vayaṃ tu tējasā tasya sahasā hṛtacētasaḥ /3.40/  

¹ B6,8 Da CE GP; B0,7,9 AB "-kriyatē"

> na kin̄cid api paṡyāmō hṛtadṛṣṭibalēndriyāḥ  
> ēkas tu ṡabdō ¹vitataḥ ṡrutō’smābhir udīritaḥ /3.41/  

¹ B AB GP; Da CE "’virataḥ"  

> jitantē puṇḍarīkākṣa namas tē viṡvabhāvana  
> namas tē’stu hṛṣīkēṡa mahāpuruṣapūrvaja /3.42/  

> iti ṡabdaḥ ṡrutō’smābhiḥ ṡikṣākṣara¹samīritaḥ  
> ētasminn antarē vāyuḥ sarvagandhavahaḥ ṡuciḥ /3.43/  

¹ B7,8 Da CE; B0,6,9 GP "-samanvitaḥ"

> divyāny uvāha puṣpāṇi karmaṇyāṡ cauṣadhīs tathā  
> tair iṣṭaḥ pan̄cakālajn̄air harir ēkāntibhir naraiḥ /3.44/  

After /3.44/ B Da AB GP in. CE@813 (/3.45L1/).

> bhaktyā paramayā yuktair manōvāk karmabhis ¹tathā  
> nūnaṃ tatrāgatō dēvō yathā tair vāg udīritā  
> vayaṃ tv ēnaṃ na paṡyāmō mōhitās tasya māyayā /3.45/  

¹ B7,8,9 Da4; B0,6 Da3 CE GP AB "tadā"

> mārutē saṃnivṛttē ca balau ca pratipāditē  
> cintāvyākulitātmānō jātāḥ smō’ṅgirasāṃ vara /3.46/  

> mānavānāṃ sahasrēṣu tēṣu vai ṡuddhayōniṣu  
> ¹na cāsmān kaṡcin manasā cakṣuṣā vāpy apūjayat /3.47/  

¹ B6,7 Da; CE AB GP "asmān na"

> tē’pi svasthā munigaṇā ēkabhāvam anuvratāḥ  
> nāsmāsu dadhirē bhāvaṃ brahmabhāvam anuṣṭhitāḥ /3.48/  

> tatō’smān supariṡrāntāṃs tapasā ¹cāti ²karṣitān  
> uvāca khasthaṃ kim api bhūtaṃ ³tatrāṡarīrakam /3.49/  

¹ all but CE "cāpi"  
² all but CE "karṡitān"  
³ B0,6,7,9 CE AB GP; B8 "-sarīrikam"; Da "-ṡarīrajam"

>    ¹dēva uvāca  
> dṛṣṭā vaḥ puruṣāḥ ṡvētāḥ sarvēndriyavivarjitāḥ  
> dṛṣṭō bhavati dēvēṡa ēbhir dṛṣṭair ²dvijōttamaiḥ /3.50/  

¹ B0,6-8 Da AB GP; B9 "bhūta uvāca"; CE MND om.  
² all but CE "dvijōttamāḥ"

> gacchadhvaṃ munayaḥ sarvē yathāgatam itō’cirāt  
> na sa ¹ṡakyas tv abhaktēna draṣṭuṃ dēvaḥ kathan̄cana /3.51/  

¹ B AB GP; Da "ṡakyō hy abha-"; CE "ṡakyō abha-"

> kāmaṃ kālēna mahatā ēkāntitvaṃ ¹samāgataiḥ  
> ṡakyō draṣṭuṃ sa bhagavān prabhāmaṇḍaladurdṛṡaḥ /3.52/  

¹ B Da CE; AB GP "upāgataiḥ"

> mahat kāryaṃ ¹ca kartavyaṃ yuṣmābhir dvijasattamāḥ  
> ²tataḥ kṛtayugē’tītē viparyāsaṃ gatē’pi ³vā /3.53/  

¹ all but CE "tu"  
² B6,7,8,9 Da; B0 CE AB GP "itaḥ"  
³ B6,7,9 Da; B0,8 CE AB GP "ca"

> vaivasvatē’ntarē viprāḥ prāptē trētāyugē ¹punaḥ  
> surāṇāṃ kāryasiddhyarthaṃ sahāyā vai bhaviṣyatha /3.54/  

¹ all but CE "tataḥ"

>    ¹ēkatadvitatritā ūcuḥ  
> tatas tad adbhutaṃ vākyaṃ ²niṡamyaivāmrtōpamam  
> tasya prasādāt prāptāḥ smō dēṡam īpsitam an̄jasā /3.55/  

¹ B6,8; et al. om.  
² all but CE "niṡamyaivaṃ sma sōmapa"  

> ēvaṃ sutapasā caiva havyakavyais tathaiva ca  
> dēvō’smābhir na dṛṣṭaḥ sa kathaṃ tvaṃ draṣṭum arhasi /3.56/  

B Da GP in. CE@814 (/3.57L2/).

> nārāyaṇō mahad bhūtaṃ viṡvasṛg ghavyakavyabhuk  
> anādinidhanō’vyaktō devadānavapūjitaḥ /3.57/  

>    bhīṣma uvāca  
> ēvam ēkatavākyēna dvitatritamatēna ca  
> anunītaḥ sadasyaiṡ ca bṛhaspatir udāradhīḥ /3.58/  

> ¹samānīya tatō yajn̄aṃ daivataṃ samapūjayat  
> samāptayajn̄ō rājā’pi ²prajāṃ pālitavān vasuḥ /3.59/  

¹ B Da CE; AB GP "samāpayat"  
² all but CE "prajāḥ"  

> brahmaṡāpād divō bhraṣṭaḥ pravivēṡa mahīṃ tataḥ  
> sa rājā rājaṡārdūla satyadharmaparāyaṇaḥ  
> antarbhūmigataṡ caiva satataṃ dharmavatsalaḥ /3.60/  

B Da AB GP in. CE@815 (/3.60L2/).

> nārāyaṇaparō bhūtvā nārāyaṇa¹japaṃ japan  
> tasyaiva ca prasādēna punar ēvōtthitas tu saḥ /3.61/  

¹ all but CE "-padaṃ jagau"

> mahītalād gataḥ sthānaṃ brahmaṇaḥ samanantaram  
> parāṃ gatim anuprāpta iti naiṣṭhikam an̄jasā /3.62/  

Thus ends chapter B7 160; B8 263; AB 338;

It has 62 verses, 129 lines;
    GP 65 verses, 130 lines;
    CE 57 verses, 123 lines;
    Ku 69 verses, 143 lines
