# Chapter 06

>    bhīṣma uvāca  
> ēvaṃ stutaḥ sa bhagavān guhyais tathyaiṡ ca nāmabhiḥ  
> taṃ muniṃ darṡayām āsa nāradaṃ viṡvarūpadhṛk /6.1/  

> kin̄cic ¹candrādviṡuddhātmā kin̄cic candrād viṡēṣavān  
> kṛṡānuvarṇaḥ kin̄cic ca kin̄cid dhiṣṇyākṛtiḥ prabhuḥ /6.2/  

¹ all; CE "candra-"

> ṡukapatra¹nibhaḥ kin̄cit ²kin̄cic ca sphaṭikaprabhaḥ  
> nīlān̄janacayaprakhyō jātarūpaprabhaḥ kvacit /6.3/  

¹ B AB GP; Da CE "-varṇaḥ"  
² B6,7 Da; B0,8 AB GP "ca kin̄cit sphaṭikasannibhaḥ", CE "-sphaṭikasaprabhaḥ"

Note that the text of AB GP CE has one extra syllable (33 syllables,
hypermetric), so they are not in Anuṣṭup. However, the constituent text above
(B6,7 Da) is correct (32 syllables).

> pravālāṅkuravarṇaṡ ca ṡvētavarṇaḥ ¹kvacid babhau  
> kvacit suvarṇavarṇābhō ²vaiḍūryasadṛṡaḥ kvacit /6.4/  

¹ B6,7,8 Da CE; B0,9 AB GP "tathā kvacit"  
² Da CE AB GP; B "vaidūrya"  

As per MW "vaidūrya" is wrong wording for "vaiḍūrya".

> nīlavaiḍūryasadṛṡa indranīlanibhaḥ kvacit  
> mayūragrīvavarṇābhō muktāhāranibhaḥ kvacit /6.5/  

Just like in /6.4/, here also B "nīlavaidūrya-"

> ētān ¹bahuvidhān varṇān ²rūpān bibhrat sanātanaḥ  
> sahasranayanaḥ ṡrīmān̄ ṡataṡīrṣaḥ sahasrapāt /6.6/  

¹ all; CE "-varṇān bahuvidhān"  
² B0,6,7,8 Da3; B9 AB GP "rūpair"; Da4 CE "rūpē"

> sahasrōdarabāhuṡ ca avyakta iti ca kvacit  
> ōṅkāram udgiran vaktrāt sāvitrīṃ ca tadanvayām /6.7/  

> ṡēṣēbhyaṡ caiva vaktrēbhyaṡ ¹caturvēdān giran bahūn  
> āraṇyakaṃ jagau dēvō harir nārāyaṇō vaṡī /6.8/  

¹ ?B0,8 AB GP; B6,7 "-giran vasuḥ"; Da "-giran vapuḥ"; CE "caturvēdōdgataṃ vasu";
  B9 subst. "udgiraṃṡ caturō vēdān̄ saṡēṣān vasudhādhipa"  

It is quite clear that the majority (B6,7 Da CE) have "vasu" or "vapu" at the
end.

> vēdīṃ kamaṇḍaluṃ ¹ṡubhrān maṇirūpān¹ahau kuṡān  
> ajinaṃ daṇḍakāṣṭhaṃ ca jvalitaṃ ca hutāṡanam /6.9/  

¹ all; CE "darbhān"  
² all; CE " athōpalān"

> dhārayām āsa dēvēṡō hastair yajn̄apatis tadā  
> taṃ prasannaṃ prasannātmā nāradō dvijasattamaḥ /6.10/  

> vāgyataḥ ¹praṇatō bhūtvā vavandē paramēṡvaram  
> tam uvāca nataṃ mūrdhnā dēvānām ādir avyayaḥ /6.11/  

¹ all; CE "prayatō"

>    ¹ṡrībhagavān uvāca  
> ēkataṡ ca dvitaṡ caiva tritaṡ caiva maharṣayaḥ  
> imaṃ dēṡam anuprāptā mama darṡanalālasāḥ /6.12/  

¹ B0,7,8,9 AB GP; B6 Da CE om.

> na ca māṃ tē dadṛṡirē na ca drakṣyati kaṡcana  
> ṛtē hy ēkāntikaṡrēṣṭhāt tvaṃ ¹caivaikāntikōttamaḥ /6.13/  

¹ B0,7,9 Da AB GP; B6,8 "-kōttama"; CE "caivaikāntikō mataḥ"

> mamaitās tanavaḥ ṡrēṣṭhā jātā dharmagṛhē dvija  
> tās tvaṃ bhajasva satataṃ sādhayasva yathāgatam /6.14/  

> vṛṇīṣva ca varaṃ vipra mattas tvaṃ ¹yad ihēcchasi  
> prasannō’haṃ tavādyēha viṡvamūrtir ihāvyayaḥ /6.15/  

¹ B0,6,8,9 AB GP; CE "yam";
  B7, Da read L1 as "-varaṃ mattō viprēha tvaṃ yadīcchasi"  

>    nārada uvāca  
> adya mē tapasō dēva yamasya niyamasya ca  
>sadyaḥ phalam avāptaṃ vai dṛṣṭō yad bhagavān mayā /6.16/  

> vara ēṣa mamātyantaṃ dṛṣṭas tvaṃ yat sanātanaḥ  
> ¹bhagavān viṡvadṛk siṃhaḥ sarvamūrtir ¹mahānprabhuḥ /6.17/  

¹ B0,6,9 Da4 CE; B7,8 Da3 AB GP "bhagavan"  
² all; CE "mahāpra-"

>    bhīṣma uvāca  
> ēvaṃ sandarṡayitvā tu nāradaṃ paramēṣṭhi¹jam  
> uvāca vacanaṃ bhūyō gaccha nārada māciram /6.18/  

¹ B CE; Da AB GP "-nam"

>    ¹ṡrībhagavān uvāca  
> imē hy anindriyāhārā madbhaktāṡ candravarcasaḥ  
> ēkāgrāṡ cintayēyur māṃ naiṣāṃ vighnō bhavēd iti /6.19/  

¹ No manuscript in. this, but needed so that the speaker changes to "bhīṣma  
  uvāca" again /6.105/ (which is in. by all manuscripts).

> siddhāṡ ¹caitē mahābhāgāḥ purā hy ēkāntinō’bhavan  
> tamōrajō¹bhinirmuktā māṃ pravēkṣyanty asaṃṡayam /6.20/  

¹ B6,9 Da CE; B0,7,8 AB GP "hyētē"  
¹ all; CE "-vinirmuktā"

> na dṛṡyaṡ cakṣuṣā yō’sau na spṛṡyaḥ sparṡanēna ca  
> na ghrēyaṡ caiva gandhēna rasēna ca vivarjitaḥ /6.21/  

> sattvaṃ rajas tamaṡ caiva na guṇās taṃ bhajanti vai  
> yaṡ ca sarvagataḥ sākṣī lōkasyātmēti kathyatē /6.22/  

> bhūtagrāmaṡarīrēṣu naṡyatsu na vinaṡyati  
> ajō nityaḥ ṡāṡvataṡ ca nirguṇō niṣkalas tathā /6.23/  

> dvirdvādaṡēbhyas tattvēbhyaḥ khyātō yaḥ pan̄caviṃṡakaḥ  
> puruṣō niṣkriyaṡ caiva jn̄ānadṛṡyaṡ ca kathyatē /6.24/  

> yaṃ praviṡya bhavantīha muktā vai dvija¹sattamāḥ  
> sa vāsudēvō vijn̄ēyaḥ paramātmā sanātanaḥ /6.25/  

¹ all; CE "-sattama"

> ¹paṡya dēvasya māhātmyaṃ mahimānaṃ ca nārada  
> ṡubhāṡubhaiḥ karmabhir yō na lipyati kadā cana /6.26/  

¹ B0,8 Da CE AB GP; B6,7,8 "yasya"  

After /6.26L1/ B9 Da in. CE@827 (na vidur...munipuṅgava) but B0,6,7,8 CE AB GP MND om.

> sattvaṃ rajas tamaṡ ¹cēti guṇān ētān pracakṣatē  
> ²ētē sarvaṡarīrēṣu tiṣṭhanti vicaranti ca /6.27/  

¹ all; Da CE "caiva"  
² B0,8,9 Da4 CE AB GP; B6,7 Da3 "ētēraiva ṡarī-"

> ētān guṇāṃs tu kṣētrajn̄ō bhuṅktē naibhiḥ sa bhujyatē  
> nirguṇō guṇabhuk caiva guṇasraṣṭā guṇādhikaḥ /6.28/  

> jagatpratiṣṭhā dēvarṣē pṛthivy apsu pralīyatē  
> jyōtiṣy āpaḥ pralīyantē jyōtir vāyau pralīyatē /6.29/  

> khē vāyuḥ pralayaṃ yāti manasy ākāṡam ēva ca  
> manō hi paramaṃ bhūtaṃ tad avyaktē pralīyatē /6.30/  

> avyaktaṃ puruṣē brahman niṣkriyē sampralīyatē  
> nāsti tasmāt ¹parataraḥ puruṣād vai sanātanāt /6.31/  

¹ all; CE "parataraṃ"

> nityaṃ hi nāsti jagati bhūtaṃ sthāvarajaṅgamam  
> ṛtē tam ēkaṃ puruṣaṃ vāsudēvaṃ sanātanam /6.32/  

> sarvabhūtātmabhūtō hi vāsudēvō mahābalaḥ  
> pṛthivī vāyur ākāṡam āpō jyōtiṡ ca pan̄camam /6.33/  

> tē samētā mahātmānaḥ ṡarīram iti san̄jn̄itam  
> tadāviṡati yō brahmann ¹adṛṡyō laghuvikramaḥ /6.34/  

¹ B0,8 Da CE AB GP; B6,7,9 "adṛṡyaṃ"

> utpanna ēva bhavati ṡarīraṃ cēṣṭayan prabhuḥ  
> na vinā dhātusaṅghātaṃ ṡarīraṃ bhavati kvacit /6.35/  

> na ca jīvaṃ vinā brahman ¹vāyavaṡ cēṣṭayanty uta  
> sa jīvaḥ parisaṅkhyātaḥ ṡēṣaḥ saṅkarṣaṇaḥ prabhuḥ /6.36/  

¹ B Da3 AB GP; Da4 CE "dhātavaṡ"

> tasmāt sanatkumāratvaṃ ¹yō’labhatsvēna karmaṇā  
> yasmiṃṡ ca sarvabhūtāni pralayaṃ yānti saṅkṣayē  
> sa manaḥ sarvabhūtānāṃ pradyumnaḥ paripaṭhyatē /6.37/  

¹ B0,8,9 Da3 AB GP; B6,7 Da4 "-labhētsvēna" (s/ē/a/); CE "yō labhatē svakarmaṇā"

> tasmāt ¹prasūtō yaḥ kartā ²kāraṇaṃ kāryam ēva ca  
> ³tasmāt sarvaṃ ⁴sambhavati jagat sthāvarajaṅgamam  
> sō’niruddhaḥ sa īṡānō ⁵vyaktaḥ sa sarvakarmasu /6.38/  

¹ B0,7,8,9 CE AB GP; B6 Da "prabhūtō"  
² all; CE "kāryaṃ kāraṇam" (swap)  
³ all; CE "yasmāt"  
⁴ all; CE "prabhavati"  
⁵ B0,6,7,8 AB GP; B9 Da "vyaktiḥ sa"; CE "vyaktiḥ sā"

> yō vāsudēvō bhagavān kṣētrajn̄ō nirguṇātmakaḥ  
> jn̄ēyaḥ ¹sa ēva ²rājēndra jīvaḥ saṅkarṣaṇaḥ prabhuḥ /6.39/  

¹ ?B0,8 Da4 CE AB GP; B6 "sa caiva"; B7 "sma sa ca"; Da3 "sa sa ca" (sic)  
² B6,7,8 Da3 AB GP MND "rājēndra"; B0 "viprēndra"; CE "bhagavān";
  B9 subst. L2 "jn̄ēyaḥ sa bhagavān̄ jīvaḥ";
  Da4 subst. L2 "vijn̄ēyaḥ sarva rājēndra" (sic)  

B6,7,8 Da all use "rājēndra" instead of "bhagavān"

> saṅkarṣaṇāc ca pradyumnō manōbhūtaḥ sa ucyatē  
> pradyumnād yō’niruddhas tu sō’haṅkārō ¹sa īṡvaraḥ /6.40/  

¹ all; CE "mahēṡvaraḥ"

> mattaḥ sarvaṃ sambhavati jagat sthāvarajaṅgamam  
> akṣaraṃ ca kṣaraṃ caiva sac cāsac caiva nārada /6.41/  

> māṃ praviṡya bhavantīha muktā bhaktās tu yē mama  
> ahaṃ hi puruṣō jn̄ēyō niṣkriyaḥ pan̄caviṃṡakaḥ /6.42/  

> nirguṇō niṣkalaṡ caiva nirdvandvō niṣparigrahaḥ  
> ētat tvayā na vijn̄ēyaṃ rūpavān iti dṛṡyatē  
> icchan muhūrtān naṡyēyam īṡō’haṃ jagatō guruḥ /6.43/  

> māyā hy ēṣā ¹purā sṛṣṭā yan māṃ paṡyasi nārada  
> sarvabhūtaguṇair yuktaṃ naivaṃ tvaṃ jn̄ātum arhasi /6.44/  

¹ B6,9 Da; B7 "parā" (s/a/u/); B0,8 CE AB GP "mayā"

> mayaitat kathitaṃ samyak tava mūrticatuṣṭayam  
> siddhā ¹hi tē mahābhāgā narā hy ēkāntinō’bhavan /6.45/  

¹ all; CE "hy ētē"

B0 AB GP MND read /6.45L2/ and /6.46L1/ after /6.48/ but not others.

> tamōrajōbhyāṃ nirmuktāḥ pravēkṣyanti ca māṃ munē  
> ahaṃ kartā ca kāryaṃ ca kāraṇaṃ cāpi nārada /6.46/  

B0 AB GP MND om. /6.46L2/.

> ahaṃ hi ¹jīvasan̄jnātō vai mayi jīvaḥ samāhitaḥ  
> ²naivaṃ tē buddhir atrābhūd dṛṣṭō jīvō mayēti ³vai /6.47/  

¹ all; CE "jīvasan̄jn̄ō"  
² B0,6,8,9 AB GP; B7 Da CE "maivaṃ"  
³ all; CE "ca"

> ¹ahaṃ sarvatragō brahman bhūtagrāmānta²rānugaḥ  
> bhūtagrāmaṡarīrēṣu naṡyatsu na naṡāmy aham /6.48/  

¹ all; B8 "ahaṃ hi sarvagō brahman"  
² ?B6,7 Da; B0,8,9 "-rātmagaḥ"; CE AB GP "-rātmakaḥ"

> hiraṇyagarbhō lōkādiṡ caturvaktrō niruktagaḥ  
> ¹brahmā sanātanō dēvō mama bahvarthacintakaḥ /6.49/  

¹ all; B9 "tatō brahmāsanō dēvō"

> lalāṭaṡ caiva mē rudrō dēvaḥ krōdhād viniḥsṛtaḥ  
> paṡyaikādaṡa mē rudrān dakṣiṇaṃ pārṡvam āsthitān /6.50/  

CE om. /6.50L1/ (= CE@833)

> dvādaṡaiva tathādityān ¹vāmaṃ pārṡvaṃ samāsthitān  
> agrataṡ caiva mē paṡya vasūn aṣṭau surōttamān /6.51/  

¹ ?B8 Da4 CE; B6,7,9 "vāmapārṡvasamā-"; B0 Da3 AB "vāmapārṡvaṃ samā-"; GP "vāmapārṡvē samā-"

> nāsatyaṃ caiva dasraṃ ca bhiṣajau paṡya pṛṣṭhataḥ  
> sarvān prajāpatīn paṡya paṡya sapta ṛṣīn ¹tathā /6.52/  

¹ all; CE "api"

> vēdān yajn̄āṃṡ ca ṡataṡaḥ paṡyāmṛtam ¹ahauṣadhīḥ  
> tapāṃsi niyamāṃṡ caiva yamān api pṛthagvidhān /6.53/  

¹ B6,7,8,9 Da; B0 CE AB GP "athauṣadhīḥ"

> tathāṣṭaguṇam aiṡvaryam ēkasthaṃ paṡya mūrtimat  
> ṡriyaṃ lakṣmīṃ ca kīrtiṃ ca pṛthivīṃ ca ¹kakudmatīm /6.54/  

¹ B Da; CE AB GP "kakudminīm"

> vēdānāṃ mātaraṃ paṡya matsthāṃ dēvīṃ sarasvatīm  
> dhruvaṃ ca jyōtiṣāṃ ṡrēṣṭhaṃ paṡya nārada khēcaram  
> ambhōdharān samudrāṃṡ ca sarāṃsi saritas tathā /6.55/  

> mūrtimantaḥ pitṛgaṇāṃṡ caturaḥ paṡya sattama  
> trīṃṡ caivēmān guṇān paṡya matsthān mūrtivivarjitān /6.56/  

> dēvakāryād api munē pitṛkāryaṃ viṡiṣyatē  
> dēvānāṃ ca pitṝṇāṃ ca pitā hy ēkō’ham āditaḥ /6.57/  

> ahaṃ ¹hayaṡirā bhūtvā samudrē paṡcimōttarē  
> pibāmi suhutaṃ havyaṃ kavyaṃ ca ṡraddhayānvitam /6.58/  

¹ all; CE "hayaṡirō"

> mayā sṛṣṭaḥ purā brahmā ¹māṃ ²yajn̄am ayajat svayam  
> tatas ³tasmai varān prītō ⁴dadyāmy aham anuttamān /6.59/  

¹ all; CE "madyaj-"  
² ?B0,8,9 B6 (orig.) CE AB GP; B6 (marg.), B7 Da "yajnair"  
³ B6,7,8,9 Da3 CE; B0 Da4 AB GP "tasmin"  
⁴ B Da; AB GP "dattavānasmy anu-"; CE "dadāv aham anu-"

> matputratvaṃ ca kalpādau lōkādhyakṣatvam ēva ca  
> ahaṅkārakṛtaṃ caiva nāma paryāyavācakam /6.60/  

> tvayā kṛtāṃ ca maryādāṃ nāti¹kramyati kaṡcana  
> tvaṃ caiva varadō brahman varēpsūnāṃ bhaviṣyasi /6.61/  

¹ B0,6,9 Da AB GP; B7,8 "-kramsyati"; CE "-krāmyati"

> surāsuragaṇānāṃ ca ṛṣīṇāṃ ca tapōdhana  
> pitṝṇāṃ ca mahābhāga satataṃ saṃṡitavrata  
> vividhānāṃ ca bhūtānāṃ tvam upāsyō bhaviṣyasi /6.62/  

> prādurbhāvagataṡ cāhaṃ surakāryēṣu nityadā  
> anuṡāsyas tvayā brahman niyōjyaṡ ca sutō yathā /6.63/  

> ētāṃṡ cānyāṃṡ ca rucirān brahmaṇē’mitatējasē  
> ahaṃ dattvā varān prītō nivṛttiparamō’bhavam /6.64/  

> nirvāṇaṃ sarvadharmāṇāṃ nivṛttiḥ paramā smṛtā  
> tasmān nivṛttim āpannaṡ carēt sarvāṅganirvṛtaḥ /6.65/  

> vidyāsahāya¹vantaṡ ²ca ādityasthaṃ ³samāhitam  
> kapilaṃ prāhur ācāryāḥ ⁴sāṅkhyaniṡcitaniṡcayāḥ /6.66/  

¹ B0,6,7,9; B8 Da CE AB GP "-vantaṃ"  
² all; CE "mām"  
³ all; CE "sanātanam"  
⁴ B0,7,8 Da4 CE AB GP; B6,9 Da3 "sāṅkhyā-"

> hiraṇyagarbhō bhagavān ēṣa chandasi suṣṭutaḥ  
> sō’haṃ yōga¹ratir brahman yōgaṡāstrēṣu ṡabditaḥ /6.67/  

¹ all; CE "-gatir"

> ēṣō’haṃ vyaktim ¹āgatya tiṣṭhāmi divi ṡāṡvataḥ  
> tatō yugasahasrāntē saṃhariṣyē jagat punaḥ /6.68/  

¹ B0,7,8 Da AB GP; B9 CE "āgamya"; B6 om.

> kṛtvātmasthāni bhūtāni sthāvarāṇi carāṇi ca  
> ēkākī vidyayā sārdhaṃ vihariṣyē ¹jagatpunaḥ /6.69/  

¹ all; CE "dvijōttama"

> tatō bhūyō jagat sarvaṃ kariṣyāmīha vidyayā  
> asmanmūrtiṡ caturthī yā sāsṛjac chēṣam avyayam /6.70/  

> sa hi saṅkarṣaṇaḥ prōktaḥ pradyumnaṃ sō’py ajījanat  
> pradyumnād ¹aniruddhō’haṃ sargō mama punaḥ punaḥ /6.71/  

¹ B6,7,8 Da4 CE AB GP; B0 "-ddhō’yaṃ"; B9 Da3 "-ddhō’bhūt"

> aniruddhāt tathā brahmā ¹tatrādikamalōdbhavaḥ  
> brahmaṇaḥ sarvabhūtāni ²carāṇi sthāvarāṇi ca /6.72/  

¹ B Da CE; AB GP "tannābhika-"  
² B0,7,8 Da3 CE AB GP; B6,9 Da4 "trasāni"

> ētāṃ sṛṣṭiṃ vijānīhi ¹kalpāntēṣu punaḥ punaḥ  
> yathā sūryasya gaganād udayāsta¹manē iha /6.73/  

¹ B6,9 Da; B0,7,8 CE AB GP "kalpādiṣu"  
² all; B8 CE "-mayāv"

> ¹naṣṭē punar balāt kāla ānayaty amitadyutiḥ  
> tathā balād ahaṃ pṛthvīṃ sarvabhūtahitāya vai /6.74/  

¹ all; CE "naṣṭau"

> sattvair ākrāntasarvāṅgāṃ naṣṭāṃ sāgaramēkhalām  
> ānayiṣyāmi ¹svaṃ sthānaṃ vārāhaṃ rūpam āsthitaḥ /6.75/  

¹ ?B6,8,9 CE; B0,7 Da3 AB GP "svasthā-"; Da4 "saṃsthā-"

> hiraṇyākṣaṃ ¹haniṣyāmi daitēyaṃ balagarvitam  
> nārasiṃhaṃ vapuḥ kṛtvā hiraṇyakaṡipuṃ punaḥ  
> surakāryē haniṣyāmi yajn̄aghnaṃ ditinandanam /6.76/  

¹ B6,7,9 Da3 CE; B0,8 Da4 AB GP "vadhiṡyāmi"

> virōcanasya balavān baliḥ putrō mahāsuraḥ  
> avadhyaḥ sarvalōkānāṃ sadēvāsurarakṣasām  
> bhaviṣyati sa ṡakraṃ ca svarājyāc cyāvayiṣyati /6.77/  

/6.77L2/ is CE@836.

> trailōkyē’pahṛtē tēna vimukhē ca ṡacīpatau  
> adityāṃ ¹dvādaṡadityāḥ sambhaviṣyāmi kaṡyapāt /6.78/  

¹ all; CE "dvādaṡaḥ putraḥ"

> tatō rājyaṃ pradāsyāmi ṡakrāyāmitatējasē  
> dēvatāḥ sthāpayiṣyāmi ¹svēṣu sthānēṣu nārada /6.79/  

¹ B6,9 CE AB; B0,7 Da "svē svē"; GP "sva sva"

> baliṃ caiva kariṣyāmi pātālatalavāsinam  
> dānavaṃ balināṃ ṡrēṣṭham avadhyaṃ sarvadaivataiḥ /6.80/  

/6.80L2/ is CE@838.

> trētāyugē bhaviṣyāmi rāmō bhṛgukulōdvahaḥ  
> kṣatraṃ cōtsādayiṣyāmi samṛddhabalavāhanam /6.81/  

> ¹sandhyāṃṡō samanuprāptē trētāyāṃ dvāparasya ca  
> ²ahaṃ dāṡarathī rāmō bhaviṣyāmi jagatpatiḥ /6.82/  

¹ all; CE "sandhau tu"  
² all; CE "rāmō dāṡarathir bhūtvā"

> tritōpaghātād vairūpyam ēkatō’tha dvitas tathā  
> ¹prāpsyētē vānaratvaṃ hi prajāpatisutāv ṛṣī /6.83/  

¹ B AB GP; Da3 "prāpsyatē"; Da4 "prāpsasē"; CE "prāpsyatō"

> tayōr yē tv anvayē jātā bhaviṣyanti ¹divaukasaḥ  
> mahābalā mahāvīryāḥ ṡakratulyaparākramāḥ  
> tē sahāyā bhaviṣyanti surakāryē mama dvija /6.84/  

¹ B6,9 Da; B0,7,8 CE AB GP "vanaukasaḥ"

/6.84L2/ is CE@840.

> tatō rakṣaḥpatiṃ ghōraṃ pulastyakulapāṃsanam  
> ¹hariṣyē rāvaṇaṃ ²raudraṃ sagaṇaṃ lōkakaṇṭakam /6.85/  

¹ B GP; Da CE AB "haniṣyē"  
² all; CE "saṅkhyē"

> dvāparasya kalēṡ caiva sandhau paryavasānikē  
> prādurbhāvaḥ kaṃsahētōr mathurāyāṃ bhaviṣyati /6.86/  

> tatrāhaṃ dānavān hatvā subahūn dēvakaṇṭakān  
> kuṡasthalīṃ kariṣyāmi ¹nivēṡaṃ dvārakāṃ purīm /6.87/  

¹ all; CE "nivāsaṃ"

> vasānas tatra vai puryām aditēr vipriyaṅkaram  
> haniṣyē narakaṃ bhaumaṃ ¹muruṃ pīṭhaṃ ca dānavam /6.88/  

¹ B Da MND; CE "muraṃ"; GP "guruṃ"; AB "maruṃ"

As per MW "muru" is alternative spelling of daitya Mura.

> ¹prāgjōtiṣaṃ puraṃ ramyaṃ nānādhanasamanvitam  
> kuṡasthalīṃ nayiṣyāmi hatvā vai dānavōtta²mam /6.89/  

¹ all; CE "prāgjyōtiṣapuraṃ"  
² all; CE "-mān"

> ¹mahēṡvaramahāsēnau bāṇapriya²hitauṣiṇau  
> parājēṣyāmy athōdyuktau ³dēvaulōkanamaskṛtau /6.90/  

¹ all; CE "ṡaṅkaraṃ ca mahāsēnaṃ"  
² B AB GP; Da "-hitē ratam"; CE "-hitaiṣiṇam"  
³ all; CE "dēvalō-"

> tataḥ sutaṃ balēr jitvā bāṇaṃ bāhusahasriṇam  
> vināṡayiṣyāmi tataḥ sarvān saubhanivāsinaḥ /6.91/  

> yaḥ kālayavanaḥ khyātō ¹gargyatējō’bhisambhṛtaḥ  
> bhaviṣyati vadhas tasya matta ēva dvijōttama /6.92/  

¹ B7,9 Da AB; B0,6,8 CE GP "gargatējōbhisaṃvṛtaḥ"

> jarāsandhaṡ ca balavān sarvarāja¹virōdhanaḥ  
> bhaviṣyaty asuraḥ sphītō bhūmipālō girivrajē /6.93/  

¹ B0,7,8 Da3 AB GP; B6,9 CE "virōdhakaḥ"; Da4 om.

> mama buddhiparispandād vadhas tasya bhaviṣyati  
> ṡiṡupālaṃ vadhiṣyāmi yajn̄ē dharmasutasya vai /6.94/  

> samāgatēṣu baliṣu pṛthivyāṃ sarvarājasu  
> vāsaviḥ susahāyō vai mama hy ēkō bhaviṣyati /6.95/  

> yudhiṣṭhiraṃ sthāpayiṣyē svarājyē bhrātṛbhiḥ saha  
> ēvaṃ lōkā vadiṣyanti naranārāyaṇāv ṛṣī /6.96/  

/6.96L1/ is CE@848.

> udyuktau dahataḥ kṣatraṃ lōkakāryārtham īṡvarau  
> kṛtvā bhārāvataraṇaṃ ¹vasudhāyā yathēpsitam /6.97/  

¹ B6,7,8,9 CE AB GP; B0 Da "vasudhāyāṃ"  

> sarvasātvatamukhyānāṃ dvārakāyāṡ ca sattama  
> kariṣyē pralayaṃ ghōram ātma¹jn̄ānābhisaṃhitaḥ /6.98/  

¹ ?B6,7 Da GP; B0,9 "-jn̄ānābhisaṃjn̄itam"; B6 (marg.) "-jn̄ānābhisaṃhitam";
  B8 AB "-jn̄ānābhisaṃvṛtam"; CE "-jn̄ātivināṡanam"  

> karmāṇy ¹aparimēyāni caturmūrtidharō hy aham  
> kṛtvā lōkān gamiṣyāmi svān ahaṃ brahmasatkṛtān /6.99/  

¹ B0,7 Da CE; B6,8,9 AB GP "aparimēyāṇi"

> ¹hamsaḥ kūrmaṡ ca matyaṡ ca prādurbhāvā dvijōttama  
> vārāhō nārasiṃhaṡ ca vāmanō rāma ēva ca  
> rāmō dāṡarathiṡ caiva sātvataḥ kalkir ēva ca /6.100/  

¹ all; CE "haṃsō hayaṡirāṡ caiva"

/6.100L2/ and /6.100L3/ is CE@851.

> yadā vēdaṡrutir naṣṭā mayā pratyāhṛtā ¹punaḥ  
> savēdāḥ saṡrutīkāṡ ca kṛtāḥ pūrvaṃ kṛtē yugē /6.101/  

¹ all; CE "tadā"

> atikrāntāḥ purāṇēṣu ṡrutās tē yadi vā kvacit  
> atikrāntāṡ ca bahavaḥ prādurbhāvā mamōttamāḥ  
> lōkakāryāṇi kṛtvā ca punaḥ svāṃ prakṛtiṃ gatāḥ /6.102/  

> na hy ētad brahmaṇā prāptam īdṛṡaṃ mama darṡanam  
> yat tvayā prāptam ¹adyēha ēkāntagatabuddhinā /6.103/  

¹ B0 B6 (marg.) B8,9 CE AB GP; B6 (orig.) B7 Da "adyēdam"

> ētat tē sarvam ākhyātaṃ brahman bhaktimatō mayā  
> purāṇaṃ ca bhaviṣyaṃ ca sarahasyaṃ ca sattama /6.104/  

>    ¹bhīṣma uvāca  
> ēvaṃ sa bhagavān dēvō viṡvamūrtidharō’vyayaḥ  
> ētāvad uktvā vacanaṃ tatraiv²āntardadhē prabhuḥ /6.105/  

¹ CE om.  
² B Da; AB GP "-āntardadhē punaḥ"; CE "-āntaradhīyata"

> nāradō’pi mahātējāḥ prāpyānugraham īpsitam  
> naranārāyaṇau draṣṭuṃ ¹badaryāṡramam ādravat /6.106/  

¹ all; CE "prādravad badarāṡramam"

> idaṃ mahōpaniṣadaṃ caturvēdasamanvitam  
> sāṅkhyayōgakṛtaṃ tēna pan̄carātrānuṡabditam /6.107/  

> nārāyaṇamukhōdgītaṃ nāradō’ṡrāvayat punaḥ  
> brahmaṇaḥ sadanē tāta yathā dṛṣṭaṃ yathā ṡrutam /6.108/  

B6,8 end a chapter here and begin a new one from next.

>    yudhiṣṭhira uvāca  
> ētad āṡcaryabhūtaṃ hi māhātmyaṃ tasya dhīmataḥ  
> ¹kiṃ vai brahmā na jānītē yataḥ ṡuṡrāva nāradāt /6.109/  

¹ all; CE "kiṃ brahmā na vijānītē"

> pitāmahō hi bhagavāṃs tasmād dēvād anantaraḥ  
> kathaṃ sa na vijānīyāt prabhāvam amitaujasaḥ /6.110/  

>    bhīṣma uvāca  
> mahākalpasahasrāṇi mahākalpaṡatāni ca  
> samatītāni rājēndra sargāṡ ca pralayāṡ ca ha /6.111/  

> sargasyādau smṛtō brahmā prajāsargakaraḥ prabhuḥ  
> jānāti dēvapravaraṃ bhūyaṡ cātō’dhikaṃ nṛpa /6.112/  

> paramātmānam īṡānam ātmanaḥ prabhavaṃ tathā  
> yē tv anyē brahmasadanē siddhasaṅghāḥ samāgatāḥ /6.113/  

> tēbhyas tac chrāvayām āsa purāṇaṃ vēdasaṃmitam  
> tēṣāṃ sakāṡāt ¹sūryas tu ṡrutvā vai bhāvitātmanām /6.114/  

¹ all; CE "sūryaṡ ca"  

> ātmānugāmināṃ ¹rājan ṡrāvayām āsa ²vai tataḥ  
> ṣaṭṣaṣṭir hi sahasrāṇi ṛṣīṇāṃ bhāvitātmanām /6.115/  

¹ all; CE "brahma"  
² all; CE "bhārata"

> sūryasya tapatō lōkān nirmitā yē puraḥsarāḥ  
> tēṣām akathayat sūryaḥ sarvēṣāṃ bhāvitātmanām /6.116/  

> sūryānugāmibhis tāta ṛṣibhis tair mahātmabhiḥ  
> mērau samāgatā dēvāḥ ṡrāvitāṡ cēdam uttamam /6.117/  

> dēvānāṃ tu sakāṡād vai tataḥ ṡrutvāsitō dvijaḥ  
> ṡrāvayām āsa ¹rājēndra pitṝṇāṃ munisattamaḥ /6.118/  

¹ B0,7,8,9 CE AB GP; B6 Da "vai tat tu"

> mama cāpi pitā tāta kathayām āsa ¹ṡāntanuḥ  
> tatō ²mayā’pi chrutvā ca kīrtitaṃ tava bhārata /6.119/  

¹ all; CE "ṡantanuḥ"  
² all; CE "mayaitac"

> surair vā munibhir vāpi purāṇaṃ yair idaṃ ṡrutam  
> sarvē tē paramātmānaṃ ¹pūjayanti punaḥ punaḥ /6.120/  

¹ B Da CE; AB GP "pūjayantē samantataḥ"

> idam ākhyānam ārṣēyaṃ pāramparyāgataṃ nṛpa  
> nāvāsudēvabhaktāya tvayā dēyaṃ kathan̄cana /6.121/  

> mattō’nyāni ca tē rājann upākhyānaṡatāni vai  
> yāni ṡrutāni ¹sarvāṇi tēṣāṃ sārō’yam uddhṛtaḥ /6.122/  

¹ B0,6,7,9 Da AB GP; B8 "dharmāṇi"; CE "dharmyāṇi"

> surāsurair yathā rājan nirmathyāmṛtam uddhṛtam  
> ēvam ētat purā vipraiḥ kathāmṛtam ¹ivāddhṛtam /6.123/  

¹ B7,8,9 Da; B6 CE AB GP "ihōdd-"

> yaṡ cēdaṃ paṭhatē nityaṃ yaṡ cēdaṃ ṡṛṇuyān naraḥ  
> ēkāntabhāvōpagata ¹ēkāntē susamāhitaḥ /6.124/  

¹ B0,8,9 Da CE; B6,7 AB GP "ēkāntēṣu samāhitaḥ"

> prāpya ṡvētaṃ mahādvīpaṃ bhūtvā candraprabhō naraḥ  
> sa sahasrārciṣaṃ dēvaṃ praviṡēn nātra saṃṡayaḥ /6.125/  

> mucyēd ārtas tathā rōgāc chrutvēmām āditaḥ kathām  
> jijn̄āsur labhatē kāmān bhaktō bhaktagatiṃ vrajēt /6.126/  

> tvayāpi satataṃ rājann abhyarcyaḥ puruṣōttamaḥ  
> sa hi mātā pitā caiva kṛtsnasya jagatō guruḥ /6.127/  

> brahmaṇyadēvō bhagavān prīyatāṃ tē sanātanaḥ  
> yudhiṣṭhira mahābāhō mahā¹buddhir janārdanaḥ /6.128/  

¹ all; CE "-bāhur"

> pūjanīyō sadāvyagrō munibhir vēdapāragaiḥ  
> ētat tē sarvam ākhyātaṃ nāradōktaṃ ¹mayēritam /6.129/  

¹ B0 B6 (marg.) B8 B9 AB GP; B6 (orig.) B7 Da4 "samīritam"; Da3 om.

B6,7 in. CE@858 (/6.129L1/) but others om.  
B8 in. CE@859ab (/6.129L2/) but om. CE@859cd. All others in. CE@859 after /6.136L2/.

>    vaiṡampāyana uvāca  
> ṡrutvaitad ākhyānavaraṃ dharmarāḍ janamējaya  
> bhrātaraṡ cāsya tē sarvē nārāyaṇaparābhavan /6.130/  

> jitaṃ bhagavatā tēna puruṣēṇēti bhārata  
> nityaṃ japyaparā bhūtvā sarasvatīm udīrayan /6.131/  

> yō hy asmākaṃ guruḥ ṡrēṣṭhaḥ kṛṣṇadvaipāyanō muniḥ  
> sa jagau paramaṃ japyaṃ nārāyaṇam udīrayan /6.132/  

> gatv¹antarikṣāt satataṃ kṣīrōdam amṛtāṡayam  
> pūjayitvā ca dēvēṡaṃ punar āyāt svam āṡramam /6.133/  

¹ B; Da CE AB GP "-āntarikṣāt"

After /6.133L2/ CE ends this chapter.  
B Da AB GP MND in. CE@860 (/6.134/ to /6.136/)

>    sūta uvāca  
> ētat tē sarvam ākhyātaṃ vaiṡaṃpāyanakīrtitam  
> janamējayēna yac chrutvā kṛtaṃ samyag yathāvidhi /6.134/  

> yūyaṃ hi taptatapasaḥ sarvē ca caritavratāḥ  
> sarvē vēdavidō mukhyā naimiṣāraṇyavāsinaḥ /6.135/  

> ṡaunakasya mahāsatraṃ prāptāḥ sarvē dvijōttamāḥ  
> yajadhvaṃ suhutair yajn̄air ¹ātmānaṃ paramēṡvaram  
> ²pāraṃparyāgataṃ hy ētat pitrā mē kathitaṃ purā /6.136/  

¹ B0,8,9 CE; B6,7 Da4 AB GP "ṡāvataṃ"; Da3 om.  
² B6,7,9 Da4 AB GP MND repeat L2 as the last line of chapter;
  B8 om. here but in. only as the last line of chapter.  

Thus ends chapter B7 163; B8 165;

It has 136 verses, 282 lines;
    CE 124 verses, 266 lines;
    GP 141 verses, 282 lines;
    Ku (76 + 91 = 167) verses, (157 + 193 = 350) lines.
