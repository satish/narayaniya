# Chapter 02

>    bhīṣma uvāca  
> sa ēvam uktō dvipadāṃ variṣṭhō  
>  nārāyaṇēnōttamapūruṣēṇa  
> jagāda vākyaṃ dvipadāṃ variṣṭhaṃ  
>  nārāyaṇaṃ lōkahitādhivāsam /2.1/  

>    nārada uvāca  
> yadartham ātmaprabha¹vēna janma  
>  ²tavōttamaṃ dharmagṛhē caturdhā  
> tat sādhyatāṃ lōkahitārtham adya  
>  gacchāmi draṣṭuṃ prakṛtiṃ tavādyām /2.2/  

¹ ?B6,7 Da; B0,8,9 GP "-vēṇa"; CE "-vēha"  
² B6,7,9 Da CE; B0 GP MND "kṛtaṃ tvayā"; B8 "tvayā kṛtaṃ"

> pūjāṃ gurūṇāṃ satataṃ karōmi  
>  parasya guhyaṃ na ca bhinnapūrvam  
> vēdāḥ svadhītā mama lōkanātha  
>  taptaṃ tapō nānṛtam uktapūrvam /2.3/  

/2.3L1/ and /2.3L2/ are swapped in CE.

> guptāni catvāri yathāgamaṃ mē  
>  ṡatrau ca mitrē ca samō’smi nityam  
> taṃ cādidēvaṃ satataṃ prapanna  
>  ēkāntabhāvēna vṛṇōmy ajasram /2.4/  

> ēbhir viṡēṣaiḥ pariṡuddhasattvaḥ  
>  kasmān na paṡyēyam anantam īṡam  
> tat pāramēṣṭhyasya vacō niṡamya  
>  nārāyaṇaḥ ¹sātvatadharmagōptā /2.5/  

¹ Da CE; B AB "ṡāṡvata-"

Note that it must be "sātvata-" to align with /2.19/

Before /2.5L2/ B6,8,9 in. "bhīṣma uvāca" but B0,7 Da CE om.

> gacchēti taṃ nāradam uktavān sa  
>  sampūjayitvātmavidhikriyābhiḥ  
> tatō visṛṣṭaḥ paramēṣṭhiputraḥ  
>  ¹sōpyarcayitvā tam ṛṣiṃ purāṇam /2.6/  

¹ B6,7 Da; B0,8,9 CE "sō’bhy-"

> kham utpapātōttama¹yōgayuktas  
>  tatō’dhimērau sahasā nililyē  
> tatrāvatasthē ca munir muhūrtam  
>  ēkāntam āsādya girēḥ sa ṡṛṅgē /2.7/  

¹ B Da; CE "-vēga-"

> ālōkayann uttarapaṡcimēna  
>  dadarṡa cātyadbhuta¹muktarūpam  
> kṣīrōdadhēr ²yōttaratō hi dvīpaḥ  
>  ṡvētaḥ sa nāmnā prathitō viṡālaḥ /2.8/  

¹ B0,6,7,9 Da4; B8 Da3 "-yuktarūpam"; CE "-rūpayuktam"  
² B0,6,7,8 Da GP; B9 "yāntaratō"; CE "uttaratō"

> mērōḥ sahasraiḥ sa hi yōjanānāṃ  
>  dvātriṃṡatōrdhvaṃ kavibhir niruktaḥ  
> ¹anindriyāṡ cānaṡanāṡ ca tatra  
>  ²niṣpandahīnāḥ susugandhinaṡ ca /2.9/  

¹ B Da3 MND; Da4 CE "atīndriyāṡ"  
² B0,6,8 Da CE GP; B7 "visyanda-"; B9 "vispanda-"; AB "nispanda-"

> ṡvētāḥ pumāṃsō gatasarvapāpāṡ  
>  cakṣurmuṣaḥ pāpakṛtāṃ narāṇām  
> vajrāsthikāyāḥ samamānōnmānā  
>  ¹divyānvayarūpāḥ ṡubhasārōpētāḥ /2.10/  

¹ ?Da CE; B0,6,9 "divyānva-"; B7,8 GP "divyāvayava-"

> chatrākṛtiṡīrṣā mēghaughaninādāḥ  
>  ¹samamuṣkacatuṣkā rājīvaṡatapādāḥ  
> ṣaṣṭyā dantair yuktāḥ ṡuklair  
>  aṣṭābhir ²yē daṃṣṭrābhiḥ   
> jihvābhir yē ³viṡvavaktraṃ  
>  lēlihyantē sūryaprakhyam /2.11/  

¹ B6,8,9 Da4 GP; B0 "saṃṡuṣka-"; B7 "samaṡuṣka-"; Da3 "saṃmukta-"; CE "satpuṣkara-"  
² B0,7,9 Da; B8 CE GP "daṃṣṭrābhir yē" (swap)  
³ B0,8 Da GP; B7,9 "viṡvaṃ vaktraṃ"; CE "viṣvagvaktraṃ"

> bhaktyā dēvaṃ viṡvōtpannaṃ  
>  yasmāt sarvē lōkāḥ ¹syūtāḥ  
> vēdā dharmā munayaḥ ṡāntā  
>  dēvāḥ sarvē tasya ²nisargāḥ /2.12/  

¹ ?B6 Da; B8 AB GP "samprasūtāḥ"; B9 CE "sūtāḥ"; B0 "ṡrutāḥ"; B7 "smṛtāḥ"  
² B0,9 Da; B6,7 AB GP "nisargaḥ"; B8 CE "visargāḥ"

MND mistakes it as "bhuṅktyā" (= as if by devouring) for "bhaktyā" (by devotion).

>    yudhiṣṭhira uvāca  
> ¹anindriyā nirāhārā ²aniṣpandāḥ sugandhinaḥ  
> kathaṃ tē puruṣā jātāḥ kā tēṣāṃ gatir uttamā /2.13/  

¹ B Da GP; CE "atīndriyā"  
² B0,8 CE GP; B6,7 "avispandāḥ"; B9 AB "anispandāḥ"; Da "avisyandāḥ"

Note that it must be "aniṣpandāḥ" to match with /2.9/ and /3.28/

> ¹yē vimuktā bhavantīha narā bharatasattama  
> tēṣāṃ lakṣaṇam ētad dhi ²tac chvētadvīpavāsinām /2.14/  

¹ ?B0 Da CE; B6,7,8 "yē hi muktā"; B9 "yadi muktā"; AB GP "yē ca muktā"  
² B0,7,8,9 AB GP; B6 Da CE "yac"

> tasmān mē saṃṡayaṃ chindhi paraṃ kautūhalaṃ hi mē  
> tvaṃ hi sarvakathārāmas tvāṃ ¹caivāpāṡritā vayam /2.15/  

¹ B0,6-8 Da3 AB; B9 Da4 CE GP "caivōpāṡritā"

>    bhīṣma uvāca  
> vistīrṇaiṣā kathā rājan̄ ṡrutā mē pitṛsannidhau  
> ¹yaiṣā tava hi vaktavyā kathāsārō hi ¹sā matā /2.16/  

¹ B0,6-9 Da AB GP; CE "saiṣā"  
² B0,6,8,9 AB GP; Da "mē matā"; CE "sa smṛtaḥ"

> rājōparicarō nāma babhūvādhipatir bhuvaḥ  
> ākhaṇḍalasakhaḥ khyātō bhaktō nārāyaṇaṃ harim /2.17/  

> dhārmikō nityabhaktaṡ ca ¹pitur nityam atandritaḥ  
> sāmrājyaṃ tēna samprāptaṃ nārāyaṇavarāt purā /2.18/  

¹ B Da AB GP; CE "pitṝn"

> ¹sātvataṃ vidhim āsthāya prāk sūryamukhaniḥsṛtam  
> pūjayām āsa dēvēṡaṃ tacchēṣēṇa pitāmahān /2.19/  

¹ B6,7,8 Da AB GP; B0,9 "ṡāṡvataṃ"

> pitṛṡēṣēṇa viprāṃṡ ca saṃvibhajyāṡritāṃṡ ca saḥ  
> ṡēṣānnabhuk satyaparaḥ sarvabhūtēṣv ahiṃsakaḥ /2.20/  

After /2.21L1/ B Da AB GP in. CE@803 (/2.21L1/) but CE om.

> sarvabhāvēna bhaktaḥ sa dēvadēvaṃ janārdanam  
> anādimadhyanidhanaṃ lōkakartāram avyayam /2.21/  

> tasya nārāyaṇē bhaktiṃ vahatō’mitra¹karṣiṇaḥ  
> ēkaṡayyāsanaṃ ¹ṡakrō dattavān dēvarāṭ svayam /2.22/  

¹ B0,7,9 Da3 AB GP; B8 "karṣaṇaḥ", "karṡiṇaḥ"; B6 "darṡanaḥ"; Da4 CE "-karṡana"  
² B6,7,9 Da CE; B0,8 AB GP "dēvō"

> ātmā rājyaṃ dhanaṃ caiva kalatraṃ ¹vāhanaṃ tathā  
> ²ētad bhāgavataṃ sarvam iti tat ³prōkṣitaṃ sadā /2.23/  

¹ B Da AB GP; CE "vāhanāni ca"  
² B7,8,9 Da4; B6 "ētad bhāgavatē"; B0 AB GP "yat tad bhāgavataṃ"; CE "ētad bhagavatē"  
³ B0,6,8 Da Ca AB GP; B6 (orig.) "prēkṣataṃ"; B7 "prēkṣyatē"; B9 "prōkṣitē"; CE "prēkṣitaṃ"

> kāmyanaimittikā ¹rājan yajn̄iyāḥ paramakriyāḥ  
> sarvāḥ sātvatam āsthāya vidhiṃ cakrē samāhitaḥ /2.24/  

¹ B Da AB GP; CE "-jasraṃ"

> pan̄carātravidō mukhyās tasya gēhē mahātmanaḥ  
> ¹prāyaṇaṃ bhagavatprōktaṃ bhun̄jatē ²cāgrabhōjanam /2.25/  

¹ B8 Da3 Ca CE GP; B6,7,9 "pāyasaṃ"; Da4 "prayāṇā"; B0 "prāpaṇaṃ"; AB "prāṇayaṃ"  
² B0,8 Da4 AB GP CE; B6,7,9 Da3 "cāgryabhōjanam"

> tasya praṡāsatō rājyaṃ dharmēṇāmitraghātinaḥ  
> nānṛtā vāk samabhavan manō duṣṭaṃ na cābhavat /2.26/  

> na ca kāyēna kṛtavān sa pāpaṃ param aṇv api  
> yē hi tē ¹ṛṣayaḥ khyātāḥ sapta citraṡikhaṇḍinaḥ /2.27/  

¹ B D AB GP; CE "munayaḥ"

After /2.28L1/ B Da AB GP in. CE@804 (/2.28L2/ to /2.29L1/)

> tair ēkamatibhir bhūtvā yat prōktaṃ ṡāstram uttamam  
> vēdaiṡ caturbhiḥ samitaṃ kṛtaṃ mērau mahāgirau /2.28/  

> āsyaiḥ saptabhir udgīrṇaṃ lōkadharmam anuttamam  
> marīcir atryaṅgirasau pulastyaḥ pulahaḥ kratuḥ  
> vasiṣṭhaṡ ca mahātējā ētē citraṡikhaṇḍinaḥ /2.29/  

> sapta prakṛtayō hy ētās tathā svāyambhuvō’ṣṭamaḥ  
> ētābhir dhāryatē lōkas tābhyaḥ ṡāstraṃ viniḥsṛtam /2.30/  

After /2.31L1/ B Da AB GP in. CE@805 (/2.31L2/)

> ēkāgramanasō dāntā munayaḥ saṃyamē ratāḥ  
> bhūtabhavyabhaviṣyajñāḥ satyadharmaparāyaṇāḥ /2.31/  

> idaṃ ṡrēya idaṃ brahma idaṃ hitam anuttamam  
> lōkān san̄cintya manasā tataḥ ṡāstraṃ pracakrirē /2.32/  

> tatra dharmārthakāmā hi mōkṣaḥ paṡcāc ca kīrtitaḥ  
> maryādā vividhāṡ caiva divi bhūmau ca saṃsthitāḥ /2.33/  

> ārādhya tapasā dēvaṃ hariṃ nārāyaṇaṃ prabhum  
> divyaṃ varṣasahasraṃ vai sarvē tē ṛṣibhiḥ saha /2.34/  

> nārāyaṇānu¹ṡāstā hi tadā dēvī sarasvatī  
> vivēṡa tān ṛṣīn sarvāl̐ lōkānāṃ hitakāmyayā /2.35/  

¹ B Da CE MND; AB GP "-ṡiṣṭā"

> tataḥ pravartitā samyak tapōvidbhir dvijātibhiḥ  
> ṡabdē cārthē ca hētau ca ēṣā prathamasargajā /2.36/  

> ādāv ēva hi tac chāstram ōṅkārasvara¹bhūṣitam  
> ṛṣibhir bhāvitaṃ tatra yatra kāruṇikō hy asau /2.37/  

¹ B6,7,9 Da CE; B0,8 GP AB "-pūjitam"

> tataḥ prasannō bhagavān anirdiṣṭaṡarīragaḥ  
> ṛṣīn uvāca tān sarvān adṛṡyaḥ puruṣōttamaḥ /2.38/  

> kṛtaṃ ṡatasahasraṃ hi ṡlōkānām idam uttamam  
> lōkatantrasya kṛtsnasya yasmād dharmaḥ pravartatē /2.39/  

> pravṛttau ca nivṛttau ca ¹yasmād ētad bhaviṣyati  
> ²yajur ṛk sāmabhir juṣṭam atharvāṅgirasais tathā /2.40/  

¹ B Da AB GP; CE "yōnir"  
² B0,6,7 Da AB GP; CE "ṛgyajuḥ"

> ¹tathā pramāṇaṃ hi mayā kṛtō ²brahma³prasādajam  
> ⁴rudrasya ⁵krōdhajō viprā yūyaṃ prakṛtayas tathā /2.41/  

¹ B Da CE; AB GP "yathā"  
² B Da AB; CE GP "brahmā"  
³ B6,7,9 Da; AB GP "prasādataḥ"; B0,8 CE "prasādajaḥ"  
⁴ B0,6,8,9; Da3 "ruddhaṡ ca" (sic); B7 "rudrasya krōdhajām rudrā"; Da4 AB GP CE "rudraṡ ca"  
⁵ B6-9 CE AB GP; B0 Da "krōdhajā"

> sūryācandramasau vāyur bhūmir āpō’gnir ēva ca  
> sarvē ca nakṣatragaṇā yac ca bhūtābhiṡabditam /2.42/  

> adhikārēṣu vartantē yathāsvaṃ brahmavādinaḥ  
> sarvē pramāṇaṃ hi yathā ¹tathaitac chāstram uttamam /2.43/  

¹ B Da CE; AB GP "tathā tat"

> bhaviṣyati pramāṇaṃ vai ētan madanuṡāsanam  
> ¹tasmāt pravakṣyatē dharmān manuḥ svāyambhuvaḥ svayam /2.44/  

¹ B0,6,7,8 Da AB GP; B9 CE "asmāt"

> uṡanā bṛhaspatiṡ caiva yadōtpannau bhaviṣyataḥ  
> tadā pravakṣyataḥ ṡāstraṃ yuṣmanmatibhir uddhṛtam /2.45/  

> svāyambhuvēṣu dharmēṣu ṡāstrē ¹cauṡanasē kṛtē  
> bṛhaspatimatē caiva lōkēṣu ²praticāritē /2.46/  

¹ B0,7,8,9 Da AB GP; CE "cōṡanasā"  
² B0,6,9 Da3 AB GP; B7 "pravitāritē"; B8 "paricāritē"; Da4 CE "pravicāritē"

> yuṣmatkṛtam idaṃ ṡāstraṃ prajāpālō vasus tataḥ  
> ¹bṛhaspatēḥ sakāṡād vai prāpsyatē dvijasattamāḥ /2.47/  

¹ B6,7,8,9 Da4; Da3 AB GP CE "bṛhaspasti-"

> sa hi ¹madbhāvitō rājā madbhaktaṡ ca bhaviṣyati  
> tēna ṡāstrēṇa lōkēṣu kriyāḥ sarvāḥ kariṣyati /2.48/  

¹ B0,6,7,9 Da CE; B8 AB GP "sadbhā-"

> ētad dhi ¹sarvaṡāstrāṇāṃ ṡāstram uttamasan̄jn̄itam  
> ētad arthyaṃ ca dharmyaṃ ca ²rahasyaṃ caitad uttamam /2.49/  

¹ B Da CE; AB GP MND "yuṣmac chāstrā-"  
² B6,7,8 Da AB GP; CE "yaṡasyaṃ"

> asya pravartanāc caiva prajāvantō bhaviṣyatha  
> sa ca ¹rājā ṡriyā yuktō bhaviṣyati mahān vasuḥ /2.50/  

¹ ?B9 Da CE AB MND; B0,6,7,8 GP "rājaṡrīyā"

> saṃsthitē ¹nṛpau tasmin̄ ṡāstram ētat sanātanam  
> antardhāsyati tat ²satyam ētad vaḥ kathitaṃ mayā /2.51/  

¹ B6,7 Da; B8,9 "bhūpatau"; CE AB GP "tu nṛpē"  
² B6,8,9 Da CE; B0,7 AB GP "sarvam"

> ētāvad uktvā vacanam adṛṡyaḥ puruṣōttamaḥ  
> visṛjya tān ṛṣīn sarvān kām api ¹prasthitō diṡam /2.52/  

¹ B7,9 Da CE; B0,6,8 AB GP "prasṛtō"

Before /2.52/ B8 in. "bhīṣma uvāca"

> tatas tē lōkapitaraḥ sarvalōkārthacintakāḥ  
> prāvartayanta tac chāstraṃ dharmayōniṃ sanātanam /2.53/  

> utpannē’’ṅgirasē caiva ¹yōgē prathamakalpitē  
> sāṅgōpaniṣadaṃ ṡāstraṃ sthāpayitvā bṛhaspatau /2.54/  

¹ B0,9 Da; B6,7,8 CE AB GP "yugē"

> jagmur yathēpsitaṃ dēṡaṃ tapasē kṛtaniṡcayāḥ  
> ¹dhāraṇāt sarvalōkānāṃ sarvadharmapravartakāḥ /2.55/  

¹ B0,6,8,9 Da CE; B7 AB GP "dhāraṇāḥ"

Thus ends chapter B7 159; B8 261; AB 337;

It has 55 verses, 112 lines;
    GP 55 verses, 112 lines;
    CE 52 verses, 108 lines;
    Ku 56 verses, ?? lines


NOTE:

It is observed that the Calcutta edition (Asiatic Society of Bengal) often
agrees with the Nīlakaṇṭha recension, even though the Bengali manuscripts
(B0,6-9) themselves disagree. Seems AB is influenced/corrupted by it.
