**"Narayaniya of the Mahabharata -- an Eastern Recension and a Southern Recension"**

Copyright © 2021 by Satish is licensed under Creative Commons
[Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1).
